package org.nuiton.topia.flyway;

/*
 * #%L
 * ToPIA :: Flyway integration service
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaApplicationContext;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.jdbc.JdbcH2Helper;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaServiceFlywayInitOnCreateSchemaTest {

    private static final Log log = LogFactory.getLog(TopiaServiceFlywayInitOnCreateSchemaTest.class);

    protected static final String FLYWAY_SCHEMA_HISTORY_TABLE = "flyway_schema_history";

    /**
     * SQL query to read the highest version in flyway schema_version table.
     *
     * Be careful, H2 is case sensitive and Flyway uses double quotes (") in table/column names
     */
    protected static final String READ_LAST_VERSION_IN_SCHEMA_VERSION_SQL = String.format(" SELECT MAX(\"version\") FROM \"PUBLIC\".\"%s\" ", FLYWAY_SCHEMA_HISTORY_TABLE);

    @Test
    public void testInitWithUseModelVersion() {

        BeanTopiaConfiguration config = new TopiaConfigurationBuilder().forTest(getClass(), "testInitWithUseModelVersion");
        config.addDeclaredService("migration", TopiaFlywayServiceImpl.class.getName(), ImmutableMap.of(TopiaFlywayService.USE_MODEL_VERSION, "true"));

        JdbcH2Helper jdbcH2Helper = new JdbcH2Helper(config);

        Assert.assertFalse(jdbcH2Helper.isTableExist(FLYWAY_SCHEMA_HISTORY_TABLE));
        Assert.assertFalse(jdbcH2Helper.isTableExist("A6"));

        // new application context will init database schema because of topia.persistence.initSchema=true
        TopiaItMappingTopiaApplicationContext applicationContext = new TopiaItMappingTopiaApplicationContext(config);

        Assert.assertTrue(jdbcH2Helper.isTableExist(FLYWAY_SCHEMA_HISTORY_TABLE));
        Assert.assertTrue(jdbcH2Helper.isTableExist("A6"));

        applicationContext.close();

        // Schema has been created, schema_version should contains the model's version
        String actual = jdbcH2Helper.runSelectOnString(READ_LAST_VERSION_IN_SCHEMA_VERSION_SQL);
        Assert.assertEquals("1.0.5", actual);

    }

    @Test
    public void testInitWithoutUseModelVersion() {

        BeanTopiaConfiguration config = new TopiaConfigurationBuilder().forTest(getClass(), "testInitWithoutUseModelVersion");
        config.addDeclaredService("migration", TopiaFlywayServiceImpl.class.getName(), ImmutableMap.of(TopiaFlywayService.USE_MODEL_VERSION, "false"));

        JdbcH2Helper jdbcH2Helper = new JdbcH2Helper(config);

        Assert.assertFalse(jdbcH2Helper.isTableExist(FLYWAY_SCHEMA_HISTORY_TABLE));
        Assert.assertFalse(jdbcH2Helper.isTableExist("A6"));

        // new application context will init database schema because of topia.persistence.initSchema=true
        TopiaItMappingTopiaApplicationContext applicationContext = new TopiaItMappingTopiaApplicationContext(config);

        Assert.assertTrue(jdbcH2Helper.isTableExist(FLYWAY_SCHEMA_HISTORY_TABLE));
        Assert.assertTrue(jdbcH2Helper.isTableExist("A6"));

        // Schema has been created, schema_version should contains the latest migration script version
        String actual = jdbcH2Helper.runSelectOnString(READ_LAST_VERSION_IN_SCHEMA_VERSION_SQL);
        Assert.assertEquals("1.2.3.4", actual);

        applicationContext.close();

    }

    @Test
    @Ignore // This test won't work since it is not possible to give flyway another test dir than 'db/migration"
    public void testInitWithFlywayInitVersion() {

        BeanTopiaConfiguration config = new TopiaConfigurationBuilder().forTest(getClass(), "testInitWithFlywayInitVersion");
        config.addDeclaredService(
                "migration",
                TopiaFlywayServiceImpl.class.getName(),
                ImmutableMap.of(
                        TopiaFlywayService.USE_MODEL_VERSION, "false",
                        TopiaFlywayService.FLYWAY_BASELINE_VERSION, "1.2.42"
                ));

        JdbcH2Helper jdbcH2Helper = new JdbcH2Helper(config);

        Assert.assertFalse(jdbcH2Helper.isTableExist(FLYWAY_SCHEMA_HISTORY_TABLE));
        Assert.assertFalse(jdbcH2Helper.isTableExist("A6"));

        // new application context will init database schema because of topia.persistence.initSchema=true
        TopiaItMappingTopiaApplicationContext applicationContext = new TopiaItMappingTopiaApplicationContext(config);

        Assert.assertTrue(jdbcH2Helper.isTableExist(FLYWAY_SCHEMA_HISTORY_TABLE));
        Assert.assertTrue(jdbcH2Helper.isTableExist("A6"));

        applicationContext.close();

        // Schema has been created, schema_version should contains the model's version
        String actual = jdbcH2Helper.runSelectOnString(READ_LAST_VERSION_IN_SCHEMA_VERSION_SQL);
        Assert.assertEquals("1.2.42", actual);
    }

    @Test(expected = IllegalStateException.class)
    public void testInitWithInvalidConf() {

        BeanTopiaConfiguration config = new TopiaConfigurationBuilder().forTest(getClass(), "testInitWithInvalidConf");

        config.addDeclaredService(
                "migration",
                TopiaFlywayServiceImpl.class.getName(),
                ImmutableMap.of(
                        TopiaFlywayService.USE_MODEL_VERSION, "true",
                        TopiaFlywayService.FLYWAY_BASELINE_VERSION, "1.2.42"
                ));

        // new application context will init database schema because of topia.persistence.initSchema=true
        new TopiaItMappingTopiaApplicationContext(config);

    }

}
