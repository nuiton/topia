package org.nuiton.topia.flyway;

/*
 * #%L
 * ToPIA :: Flyway integration service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationVersion;
import org.flywaydb.core.internal.util.Location;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaMigrationServiceException;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Implementation for {@link org.nuiton.topia.flyway.TopiaFlywayService}.
 *
 * @since 3.0
 */
public class TopiaFlywayServiceImpl implements TopiaFlywayService {

    private static final Log log = LogFactory.getLog(TopiaFlywayServiceImpl.class);

    /**
     * Value for {@link #FLYWAY_BASELINE_VERSION} configuration parameter.
     */
    protected String flywayBaselineVersion = null;

    /**
     * Value for {@link #USE_MODEL_VERSION} configuration parameter.
     */
    protected boolean useModelVersion = true;

    protected Flyway flyway;

    protected String modelVersion;

    @Override
    public void initTopiaService(TopiaApplicationContext topiaApplicationContext, Map<String, String> serviceConfiguration) {

        modelVersion = topiaApplicationContext.getModelVersion();

        // set flywayBaselineVersion if provided
        if (serviceConfiguration.containsKey(FLYWAY_BASELINE_VERSION)) {
            flywayBaselineVersion = serviceConfiguration.get(FLYWAY_BASELINE_VERSION);
            if (StringUtils.isBlank(flywayBaselineVersion)) {
                throw new IllegalArgumentException("'" + FLYWAY_BASELINE_VERSION + "' must not be empty");
            }
        } else if (serviceConfiguration.containsKey(FLYWAY_INIT_VERSION)) {
            if (log.isWarnEnabled()) {
                log.warn(FLYWAY_INIT_VERSION + " is deprecated, use " + FLYWAY_BASELINE_VERSION + " instead");
            }
            flywayBaselineVersion = serviceConfiguration.get(FLYWAY_INIT_VERSION);
            if (StringUtils.isBlank(flywayBaselineVersion)) {
                throw new IllegalArgumentException("'" + FLYWAY_INIT_VERSION + "' must not be empty");
            }
        }

        // set useModelVersion if provided
        if (serviceConfiguration.containsKey(USE_MODEL_VERSION)) {
            String useModelVersionString = serviceConfiguration.get(USE_MODEL_VERSION);
            if (StringUtils.isBlank(useModelVersionString)) {
                throw new IllegalArgumentException("'" + USE_MODEL_VERSION + "' must not be empty");
            }
            useModelVersion = Boolean.valueOf(useModelVersionString);
        }

        if (log.isInfoEnabled()) {
            log.info("init flyway service");
        }

        flyway = new Flyway();

        setDataSource(flyway, topiaApplicationContext);

        setLocations(flyway, topiaApplicationContext);

        doExtraConfiguration(flyway, topiaApplicationContext);

    }

    /**
     * Define flyway database credentials.
     *
     * This implementation search for parameters given in
     * {@link org.nuiton.topia.persistence.TopiaApplicationContext#getConfiguration()}. We use
     * the same credentials to migrate the database as the one used when we use it.
     */
    protected void setDataSource(Flyway flyway, TopiaApplicationContext topiaApplicationContext) {

        JdbcConfiguration configuration = topiaApplicationContext.getConfiguration();

        String url = configuration.getJdbcConnectionUrl();
        String user = configuration.getJdbcConnectionUser();
        String password = configuration.getJdbcConnectionPassword();

        flyway.setDataSource(url, user, password);

    }

    /**
     * Define where Flyway should look for migrations.
     *
     * This implementation search for *.sql migration files in "db/migration" resources directory
     * and for JDBC migrations in package.to.ApplicationContext<strong>.migration</strong> package.
     */
    protected void setLocations(Flyway flyway, TopiaApplicationContext topiaApplicationContext) {

        String classpathMigrationPackage = topiaApplicationContext.getClass().getPackage().getName() + ".migration";
        ImmutableSet<String> defaultLocations = ImmutableSet.of("db/migration", classpathMigrationPackage);

        // detects migrations and configure flyway locations
        Set<String> locations = new LinkedHashSet<String>();
        for (String defaultLocation : defaultLocations) {
            if (log.isInfoEnabled()) {
                log.info("will search for migration in location " + defaultLocation);
            }
            try {
                Location location = new Location(defaultLocation);
                Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources(location.getPath());
                if (resources.hasMoreElements()) {
                    locations.add(defaultLocation);
                    if (log.isInfoEnabled()) {
                        log.info("migrations found in " + defaultLocation);
                    }
                } else {
                    if (log.isInfoEnabled()) {
                        log.info("no migration found in " + defaultLocation);
                    }
                }
            } catch (IOException e) {
                throw new TopiaException(e);
            }
        }

        String[] locationsArray = locations.toArray(new String[locations.size()]);
        flyway.setLocations(locationsArray);
    }

    /**
     * Opened hook to override in a sub-class.
     */
    protected void doExtraConfiguration(Flyway flyway, TopiaApplicationContext topiaApplicationContext) {

    }

    @Override
    public String getSchemaVersion() throws TopiaMigrationServiceException {

        MigrationInfo currentOrNull = flyway.info().current();

        if (currentOrNull == null) {
            throw new TopiaMigrationServiceException("schema version is unknown");
        }

        String schemaVersion = currentOrNull.getVersion().getVersion();

        return schemaVersion;

    }

    @Override
    public void initOnCreateSchema() {

        String baselineVersion;

        if (useModelVersion) {

            // Use model version, flywayInitVersion should not be specified
            Preconditions.checkState(Strings.isNullOrEmpty(flywayBaselineVersion),
                    FLYWAY_INIT_VERSION + " is not not compatible with " + USE_MODEL_VERSION + "=true");

            if (log.isInfoEnabled()) {
                log.info("Using model version: " + modelVersion);
            }
            baselineVersion = modelVersion;
        } else {

            // Do not use model version. That means we have to "guess" which is minimal the model version from the available migration scripts
            MigrationInfo[] allMigrations = flyway.info().all();

            if (ArrayUtils.isEmpty(allMigrations)) {

                // Check that flywayInitVersion is declared
                Preconditions.checkState(!Strings.isNullOrEmpty(flywayBaselineVersion),
                        "No migration found and " + USE_MODEL_VERSION + "=false. You need to declare a " + FLYWAY_BASELINE_VERSION);

                if (log.isInfoEnabled()) {
                    log.info("Using " + FLYWAY_BASELINE_VERSION + " version: " + flywayBaselineVersion);
                }
                baselineVersion = flywayBaselineVersion;

            } else {

                // useModelVersion=false and some migrations found, flywayInitVersion should not be specified
                Preconditions.checkState(Strings.isNullOrEmpty(flywayBaselineVersion),
                        "Migrations found with " + USE_MODEL_VERSION + "=false. " + FLYWAY_BASELINE_VERSION + " shouldn't be set");

                // TreeSet will sort the versions because MigrationInfo implements Comparable
                TreeSet<MigrationInfo> treeSet = Sets.newTreeSet(Arrays.asList(allMigrations));

                // The latest will be the higher version number
                MigrationInfo last = Iterables.getLast(treeSet);
                String version = last.getVersion().getVersion();

                if (log.isInfoEnabled()) {
                    log.info("Using highest migration version found: " + version);
                }
                baselineVersion = version;
            }

        }

        if (log.isInfoEnabled()) {
            log.info("baseline flyway to version " + baselineVersion);
        }

        flyway.setBaselineVersion(MigrationVersion.fromVersion(baselineVersion));
        flyway.setBaselineDescription("schema creation called on application context by topia flyway service");
        flyway.baseline();

    }

    @Override
    public void runSchemaMigration() {

        if (flywayBaselineVersion == null) {
            if (log.isDebugEnabled()) {
                log.debug("schema exists, no flywayInitVersion found, let suppose flyway is already initialized");
            }
            // if flyway is not initialized, we will get
//            Grave: Exception sending context initialized event to listener instance of class fr.ifremer.wao.web.WaoApplicationListener
//            com.googlecode.flyway.core.api.FlywayException: Found non-empty schema "public" without metadata table! Use init() or set initOnMigrate to true to initialize the metadata table.
//                    at com.googlecode.flyway.core.Flyway$1.execute(Flyway.java:848)
//            at com.googlecode.flyway.core.Flyway$1.execute(Flyway.java:819)
//            at com.googlecode.flyway.core.Flyway.execute(Flyway.java:1200)
//            at com.googlecode.flyway.core.Flyway.migrate(Flyway.java:819)

        } else {
            if (log.isDebugEnabled()) {
                log.debug("schema exists, will ask flyway to init if necessary to version " + flywayBaselineVersion);
            }
            flyway.setBaselineOnMigrate(true);
            flyway.setBaselineVersion(MigrationVersion.fromVersion(flywayBaselineVersion));
        }

        if (useModelVersion) {
            String targetVersion = modelVersion;

            if (log.isInfoEnabled()) {
                log.info("schema exists, will run flyway migration up to target version " + targetVersion);
            }

            flyway.setTarget(MigrationVersion.fromVersion(targetVersion));
        } else {

            if (log.isInfoEnabled()) {
                log.info("schema exists, no target version specified, will run flyway migration for all existing versions");
            }

        }

        if (log.isInfoEnabled()) {
            log.info("run flyway migration");
        }

        flyway.migrate();

    }

    @Override
    public void close() {
        // nothing to do
    }

}
