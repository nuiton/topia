package org.nuiton.topia.flyway;

/*
 * #%L
 * ToPIA :: Flyway integration service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.flywaydb.core.internal.configuration.ConfigUtils;
import org.nuiton.topia.persistence.TopiaMigrationService;

/**
 * Integrates Topia with <a href="http://flywaydb.org/">Flyway database migration framework</a>.
 *
 * @since 3.0
 */
public interface TopiaFlywayService extends TopiaMigrationService {

    /**
     * @deprecated use {@link #FLYWAY_BASELINE_VERSION}
     */
    @Deprecated
    String FLYWAY_INIT_VERSION = "flyway.initVersion";

    /**
     * If you want flyway to init with an already existing DB, you must use
     * this configuration and give, as value the version of the already
     * existing schema.
     */
    String FLYWAY_BASELINE_VERSION = ConfigUtils.BASELINE_VERSION;

    /**
     * Tells ToPIA to set (or not) the Flyway's target version from the model version. Expected values are "true" or
     * "false". The default one is "true".
     */
    String USE_MODEL_VERSION = "useModelVersion";

}
