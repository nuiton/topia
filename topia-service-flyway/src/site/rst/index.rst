.. -
.. * #%L
.. * ToPIA :: Flyway integration service
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=============================
ToPIA :: topia-service-flyway
=============================

Ce module fournit une intégration de `Flyway`_ avec ToPIA pour gérer les évolutions du modèle en base.

Ce module est une alternative aux modules `topia-service-liquibase`_ et `topia-service-migration`_.

.. _Flyway: http://flywaydb.org/
.. _topia-service-liquibase: ../topia-service-liquibase/
.. _topia-service-migration: ../topia-service-migration/

