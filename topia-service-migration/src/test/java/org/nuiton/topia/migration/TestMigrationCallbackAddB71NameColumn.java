package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;

import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TestMigrationCallbackAddB71NameColumn extends AbstractTopiaMigrationCallback {

    @Override
    public Version[] getAvailableVersions() {
        Version[] result = new Version[1];
        result[0] = VersionBuilder.create("1.0.5").build();
        return result;
    }

    @Override
    public boolean askUser(Version dbVersion, List list) {
        return true;
    }

    @Override
    protected void migrateForVersion(Version version, TopiaSqlSupport sqlSupport, boolean showSql, boolean showProgression) throws Exception {
        if (VersionBuilder.create("1.0.5").build().equals(version)) {
            sqlSupport.executeSql(" ALTER TABLE B71 ADD COLUMN name VARCHAR(255) ");
        }
    }


}
