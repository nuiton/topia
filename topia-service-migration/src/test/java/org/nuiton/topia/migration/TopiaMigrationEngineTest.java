package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaApplicationContext;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.SchemaValidationTopiaException;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcH2Helper;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaMigrationEngineTest {

    /**
     * Tests if a given table exists. WARNING : this may not work is the tableName is not exactly the same.
     *
     * This method is designed for H2 only.
     */
    protected boolean isTableExist(JdbcConfiguration jdbcConfiguration, String tableName) throws Exception {
        return new JdbcH2Helper(jdbcConfiguration).isTableExist(tableName);
    }

    protected String runSelectOnString(JdbcConfiguration jdbcConfiguration, String sql) throws Exception {
        return new JdbcHelper(jdbcConfiguration).runSelectOnString(sql);
    }

    protected int runUpdate(JdbcConfiguration jdbcConfiguration, String sql) throws Exception {
        return new JdbcHelper(jdbcConfiguration).runUpdate(sql);
    }

    protected String readVersion(JdbcConfiguration jdbcConfiguration) throws Exception {
        return runSelectOnString(jdbcConfiguration, "SELECT VERSION FROM TMS_VERSION");
    }

    @Test
    public void testMigrationHasRan() throws Exception {

        BeanTopiaConfiguration config = new TopiaConfigurationBuilder().forTest(getClass(), "testMigrationHasRan");
        config.addDeclaredService("migration", TopiaMigrationEngine.class,
                ImmutableMap.of(TopiaMigrationService.MIGRATION_CALLBACK, TestMigrationCallbackCreateWhateverTable.class.getName()));

        { // Create database

            Assert.assertFalse(isTableExist(config, "TMS_VERSION"));
            Assert.assertFalse(isTableExist(config, "A6"));

            // new application context will init database schema because of topia.persistence.initSchema=true
            TopiaItMappingTopiaApplicationContext applicationContext = new TopiaItMappingTopiaApplicationContext(config);

            Assert.assertTrue(isTableExist(config, "TMS_VERSION"));
            Assert.assertTrue(isTableExist(config, "A6"));
            Assert.assertFalse(isTableExist(config, "PUET")); // Created by the migration process

            applicationContext.close();

            // Schema has been created, TMSVersion should contains the model's version
            String actual = readVersion(config);
            Assert.assertEquals(applicationContext.getModelVersion(), actual);
            Assert.assertEquals("1.0.5", actual);

        }

        // Downgrade schema version
        runUpdate(config, "UPDATE TMS_VERSION SET VERSION='1.0.4'");
        Assert.assertEquals("1.0.4", readVersion(config));

        { // start again context database

            Assert.assertTrue(isTableExist(config, "TMS_VERSION"));
            Assert.assertTrue(isTableExist(config, "A6"));
            Assert.assertFalse(isTableExist(config, "WHATEVER"));

            // new application context will init database schema because of topia.persistence.initSchema=true
            TopiaItMappingTopiaApplicationContext applicationContext = new TopiaItMappingTopiaApplicationContext(config);

            Assert.assertTrue(isTableExist(config, "TMS_VERSION"));
            Assert.assertTrue(isTableExist(config, "A6"));
            Assert.assertTrue(isTableExist(config, "WHATEVER")); // Created by the migration process

            applicationContext.close();

            String actual = readVersion(config);
            Assert.assertEquals(applicationContext.getModelVersion(), actual);
            Assert.assertEquals("1.0.5", actual);


        }

    }

    @Test
    public void testValidateAfterMigration() throws Exception {

        // new application context will init database schema because of topia.persistence.initSchema=true
        BeanTopiaConfiguration config = new TopiaConfigurationBuilder().forTest(getClass(), "testValidateAfterMigration");

        TopiaItMappingTopiaApplicationContext applicationContext = new TopiaItMappingTopiaApplicationContext(config);

        Assert.assertFalse(isTableExist(config, "TMS_VERSION"));
        Assert.assertTrue(isTableExist(config, "A6"));

        applicationContext.close();

        // Drop a column
        runUpdate(config, "ALTER TABLE B71 DROP COLUMN name");

        // Declare some dummy migration service (does nothing)
        config.addDeclaredService("migration", TopiaMigrationEngine.class,
                ImmutableMap.of(TopiaMigrationService.MIGRATION_CALLBACK, TestMigrationCallbackDoNothing.class.getName()));

        { // Start again context, will fail because of validation

            // new application context will init database schema because of topia.persistence.initSchema=true
            applicationContext = new TopiaItMappingTopiaApplicationContext(config);

            // Schema has been created, TMSVersion should contains the model's version
            String actual = readVersion(config);
            Assert.assertEquals(applicationContext.getModelVersion(), actual);
            Assert.assertEquals("1.0.5", actual);

            try {
                TopiaItMappingTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();
                persistenceContext.close();
                Assert.fail("Validation should have failed");
            } catch (SchemaValidationTopiaException e) {
                Assert.assertEquals("Schema-validation: missing column [name] in table [b71]", e.getMessage());
            }

            applicationContext.close();

        }

        // Declare the migration service
        config.addDeclaredService("migration", TopiaMigrationEngine.class.getName(),
                ImmutableMap.of(TopiaMigrationService.MIGRATION_CALLBACK, TestMigrationCallbackAddB71NameColumn.class.getName()));

        runUpdate(config, "UPDATE TMS_VERSION SET VERSION='1.0.4'");
        Assert.assertEquals("1.0.4", readVersion(config));

        { // Start again with migration service, will create the tms_version table

            // new application context will init database schema because of topia.persistence.initSchema=true
            applicationContext = new TopiaItMappingTopiaApplicationContext(config);

            Assert.assertTrue(isTableExist(config, "TMS_VERSION"));
            Assert.assertTrue(isTableExist(config, "A6"));

            applicationContext.close();

            // Schema has been created, TMSVersion should contains the model's version
            String actual = readVersion(config);
            Assert.assertEquals(applicationContext.getModelVersion(), actual);
            Assert.assertEquals("1.0.5", actual);

        }

    }

}
