package org.nuiton.topia.migration.mappings;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.version.Version;

/**
 * TMSVersion DAO helper.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.4
 */
@Deprecated
public class TMSVersionDAO extends TMSVersionHibernateDao {

    /** logger */
    private final static Log log = LogFactory.getLog(TMSVersionDAO.class);

    public static final String LEGACY_TABLE_NAME = "tmsVersion";

    public static final String TABLE_NAME = "tms_version";

    public static TMSVersion get(TopiaHibernateSupport tx) throws TopiaException {
        return readVersion(tx.getHibernateSession());
    }

    public static TMSVersion create(TopiaHibernateSupport tx, String version) throws TopiaException {

        return saveVersion(tx.getHibernateSession(), version);
    }

    public static void update(TopiaHibernateSupport tx, TMSVersion version) throws TopiaException {
        update(tx.getHibernateSession(), version);
    }

    public static void deleteAll(TopiaHibernateSupport tx) throws TopiaException {
        deleteAll(tx.getHibernateSession());
    }

    public static final String LEGACY_MAPPING =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<!DOCTYPE hibernate-mapping PUBLIC \"-//Hibernate/Hibernate Mapping DTD 3.0//EN\" \"classpath://org/hibernate/hibernate-mapping-3.0.dtd\">\n" +
            "<hibernate-mapping>\n" +
            "    <class name=\"" + TMSVersion.class.getName() + "\" table=\"" + LEGACY_TABLE_NAME + "\">\n" +
            "    <id column=\"" + TMSVersion.PROPERTY_VERSION + "\" name=\"" + TMSVersion.PROPERTY_VERSION + "\"/>\n" +
            "  </class>\n" +
            "</hibernate-mapping>";

    public static Version getVersion(TopiaHibernateSupport tx, String tableName) {
        return getVersion(tx.getHibernateConfiguration(), tx.getHibernateSession(), tableName);
    }

}
