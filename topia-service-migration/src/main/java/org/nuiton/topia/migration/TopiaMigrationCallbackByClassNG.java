package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.VersionComparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeMap;

/**
 * Migration callback which use a different class for each version to migrate.
 *
 * You must fill in the constructor the mapping for each version of
 * {@link #getAvailableVersions()} a matching migrator for version which
 * extends {@link MigrationCallBackForVersion}.
 *
 * Use the callback when you have a lot of version to migrate and the
 * {@link TopiaMigrationCallbackByMethod} begins to be messy.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9.11
 */
public abstract class TopiaMigrationCallbackByClassNG extends AbstractTopiaMigrationCallback {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(TopiaMigrationCallbackByClassNG.class);

    protected MigrationCallBackForVersionResolver callBackResolver;

    protected TopiaMigrationCallbackByClassNG(MigrationCallBackForVersionResolver callBackResolver) {

        this.callBackResolver = callBackResolver;
    }

    @Override
    public Version[] getAvailableVersions() {
        Set<Version> allVersions = callBackResolver.getAllVersions();
        return allVersions.toArray(new Version[allVersions.size()]);
    }

    @Override
    protected void migrateForVersion(Version version,
                                     TopiaSqlSupport sqlSupport,
                                     boolean showSql,
                                     boolean showProgression) throws Exception {

        MigrationCallBackForVersion migrator = callBackResolver.getCallBack(version);

        migrator.setCallBack(this);

        String[] queries = migrator.prepareMigration(sqlSupport, showSql, showProgression);

        executeSQL(sqlSupport, showSql, showProgression, queries);

    }

    /**
     * Call back for a given version.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.5
     */
    public abstract static class MigrationCallBackForVersion {

        protected TopiaMigrationCallbackByClassNG callBack;

        public abstract Version getVersion();

        public void setCallBack(TopiaMigrationCallbackByClassNG callBack) {
            this.callBack = callBack;
        }

        protected String[] prepareMigration(TopiaSqlSupport sqlSupport,
                                            boolean showSql,
                                            boolean showProgression) throws TopiaException {

            List<String> queries = new ArrayList<String>();

            prepareMigrationScript(sqlSupport, queries, showSql, showProgression);

            return queries.toArray(new String[queries.size()]);
        }

        protected abstract void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                                       List<String> queries,
                                                       boolean showSql,
                                                       boolean showProgression) throws TopiaException;

        public void executeSQL(TopiaSqlSupport sqlSupport,
                               String... sqls) throws TopiaException {
            callBack.executeSQL(sqlSupport, sqls);
        }

        public void executeSQL(TopiaSqlSupport sqlSupport,
                               boolean showSql,
                               boolean showProgression,
                               String... sqls) throws TopiaException {
            callBack.executeSQL(sqlSupport, showSql, showProgression, sqls);
        }

    }

    /**
     * Resolver to obtain the correct migration class for a given version.
     *
     * @since 2.6.11
     */
    public interface MigrationCallBackForVersionResolver {

        /**
         * Returns all detected versions.
         *
         * @return all detected versions.
         */
        Set<Version> getAllVersions();

        /**
         * for a given version, returns his migration callback.
         *
         * @param version the version to migrate
         * @return the migration call for the given version, or {@code null}
         * if no such migration callback exists for the version
         */
        MigrationCallBackForVersion getCallBack(Version version);
    }

    /**
     * A simple call back resolver via a service loader.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.9.11
     */
    public static class MigrationCallBackForVersionResolverByServiceLoader implements MigrationCallBackForVersionResolver {

        protected final Map<Version, MigrationCallBackForVersion> versionMigrationMapping;

        public MigrationCallBackForVersionResolverByServiceLoader() {
            this.versionMigrationMapping = new TreeMap<Version, MigrationCallBackForVersion>(
                    new VersionComparator());
            ServiceLoader<MigrationCallBackForVersion> load = ServiceLoader.load(MigrationCallBackForVersion.class);
            for (MigrationCallBackForVersion callBackForVersion : load) {
                Version version = callBackForVersion.getVersion();
                if (log.isInfoEnabled()) {
                    log.info("Detects migration version " + version + " [" +
                             callBackForVersion + "]");
                }
                versionMigrationMapping.put(version, callBackForVersion);
            }
        }

        @Override
        public MigrationCallBackForVersion getCallBack(Version version) {
            return versionMigrationMapping.get(version);
        }

        @Override
        public Set<Version> getAllVersions() {
            return versionMigrationMapping.keySet();
        }
    }
}
