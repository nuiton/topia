package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.nuiton.topia.migration.mappings.TMSVersion;
import org.nuiton.topia.migration.mappings.TMSVersionHibernateDao;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaMigrationServiceException;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import org.nuiton.topia.persistence.internal.HibernateProvider;
import org.nuiton.topia.persistence.util.TopiaUtil;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;
import org.nuiton.version.VersionComparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Le moteur de migration proposé par topia. Il est basé sur un {@link AbstractTopiaMigrationCallback}
 * qui donne la version de l'application, les version de mises à jour disponibles.
 *
 * Le call back offre aussi les commandes sql à passer pour chaque version de mise à jour.
 *
 * FIXME Finir cette documentation
 *
 * @author tchemit
 * @since 2.3.4
 * @deprecated no longer maintained since using Hibernate 6 (we could not fix issues using new configuration directives)
 * and is no longer user. Migrate to Flyway or Liquibase modules.
 */
@Deprecated
public class TopiaMigrationEngine implements TopiaMigrationService {

    private final static Log log = LogFactory.getLog(TopiaMigrationEngine.class);

    /**
     * Hibernate Configuration only with TMSVersion entity (set during init)
     */
    protected Configuration versionConfiguration;

    /**
     * Flag to know if the TMSVersion table is existing (set during init)
     */
    protected boolean versionTableExist;

    /**
     * Current database version (set during init)
     */
    protected Version dbVersion;

    /**
     * Drapeau pour savoir si la base est versionnée ou non
     */
    protected boolean dbNotVersioned;

    /**
     * A flag to know if none of the dealed entities tables exists in db.
     *
     * @since 2.5.3
     */
    protected boolean dbEmpty;

    /**
     * CallbackHandler list (set during init)
     */
    protected AbstractTopiaMigrationCallback callback;

    /**
     * Topia application context (set during init)
     */
    protected TopiaApplicationContext applicationContext;

    /**
     * Flag to know if the service is correctly initialized
     */
    protected boolean init;

    /**
     * A flag to check if version was detected in database.
     *
     * This flag is set to {@code true} at the end of method {@link #detectDbVersion()}.
     */
    protected boolean versionDetected;

    /**
     * Flag to display SQL requests or not
     */
    protected boolean showSql;

    /**
     * Flag to display progress or not
     */
    protected boolean showProgression;

    protected SessionFactory sessionFactory;
    protected Metadata metaData;

    //--------------------------------------------------------------------------
    //-- TopiaService implementation
    //--------------------------------------------------------------------------

    @Override
    public void initTopiaService(TopiaApplicationContext topiaApplicationContext, Map<String, String> serviceConfiguration) {

        if (log.isWarnEnabled()) {
            log.warn(this.getClass().getName() + " is deprecated. You should migrate to flyway or liquibase.");
        }

        Properties config = new Properties();
        config.putAll(serviceConfiguration);

        Preconditions.checkState(StringUtils.isBlank(config.getProperty(MIGRATION_MIGRATE_ON_INIT)),
                MIGRATION_MIGRATE_ON_INIT + " directive is deprecated, you must replace it by setting TopiaConfiguration#isInitSchema to true");

        showSql = Boolean.valueOf(config.getProperty(MIGRATION_SHOW_SQL, String.valueOf(Boolean.FALSE)));
        if (log.isDebugEnabled()) {
            log.debug("Show sql                - " + showSql);
        }

        showProgression = Boolean.valueOf(config.getProperty(MIGRATION_SHOW_PROGRESSION, String.valueOf(Boolean.FALSE)));
        if (log.isDebugEnabled()) {
            log.debug("Show progression        - " + showProgression);
        }

        String callbackStr = getSafeParameter(config, MIGRATION_CALLBACK);
        if (log.isDebugEnabled()) {
            log.debug("Use callback            - " + callbackStr);
        }

        // Create the callback instance
        try {
            Class<?> clazz = Class.forName(callbackStr);
            callback = (AbstractTopiaMigrationCallback) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            throw new TopiaException("Unable to find callback class " + callbackStr, e);
        } catch (InstantiationException e) {
            throw new TopiaException("Unable to instantiate callback " + callbackStr, e);
        } catch (IllegalAccessException e) {
            throw new TopiaException("Unable to instantiate callback " + callbackStr, e);
        }

        applicationContext = topiaApplicationContext;

        // Create the dedicated Hibernate Configuration which is just about the TMSVersion entity in order to create
        // the table using schemaExport if necessary
        versionConfiguration = createTMSHibernateConfiguration(topiaApplicationContext);
        versionConfiguration.getProperties().remove(AvailableSettings.HBM2DDL_AUTO); // Make sure schema is not created by Hibernate

        HibernateProvider hibernateProvider = ((AbstractTopiaApplicationContext) applicationContext).getHibernateProvider();


        sessionFactory = hibernateProvider.newSessionFactory(versionConfiguration);

        metaData = hibernateProvider.newMetaData(versionConfiguration,sessionFactory, Collections.<Class<?>>singleton(TMSVersion.class));

        if (applicationContext.getConfiguration().isInitSchema() && TopiaUtil.isSchemaEmpty(versionConfiguration, metaData)) {
            TMSVersionHibernateDao.createTMSSchema(metaData);
        }

        init = true;

        if (log.isDebugEnabled()) {
            log.debug("Service [" + this.getClass().getName() + "] initialized.");
        }

    }

    //--------------------------------------------------------------------------
    //-- TopiaMigrationService implementation
    //--------------------------------------------------------------------------

    @Deprecated
    @Override
    public boolean migrateSchema() throws MigrationServiceException {
        try {
            runSchemaMigration();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Unable to complete schema migration", eee);
            }
            throw new MigrationServiceException("Unable to complete schema migration", eee);
        }
        return true;
    }

    @Override
    public String getSchemaVersion() throws TopiaMigrationServiceException {

        checkInit();

        detectDbVersion();

        return dbVersion.getVersion();

    }

    @Override
    public void initOnCreateSchema() {

        // Schema has just been created, save the application version
        saveApplicationVersion();

    }

    /**
     * Filter versions.
     *
     * @param versions   versions to filter
     * @param min        min version to accept
     * @param max        max version to accept
     * @param includeMin flag to include min version
     * @param includeMax flag to include max version
     * @return versions between min and max
     */
    public static List<Version> filterVersions(Set<Version> versions,
                                                               Version min,
                                                               Version max,
                                                               boolean includeMin,
                                                               boolean includeMax) {
        List<Version> toApply = new ArrayList<Version>();
        for (Version v : versions) {
            int t;
            if (min != null) {
                t = v.compareTo(min);
                if (t < 0 || t == 0 && !includeMin) {
                    // version trop ancienne
                    continue;
                }
            }
            if (max != null) {
                t = v.compareTo(max);
                if (t > 0 || t == 0 && !includeMax) {
                    // version trop recente
                    continue;
                }
            }
            toApply.add(v);
        }
        return toApply;
    }

    @Override
    public void runSchemaMigration() {

        checkInit();

        detectDbVersion();

        Version applicationVersion = getApplicationVersion();

        log.info(String.format("Starting Topia Migration Service  - Application version : %1$s, Database version : %2$s",
                        applicationVersion.getVersion(),
                        dbVersion.getVersion())
        );

        if (log.isDebugEnabled()) {
            log.debug("Migrate schema to version = " + dbVersion);
            log.debug("is db not versionned ?    = " + dbNotVersioned);
            log.debug("is db empty ?             = " + dbEmpty);
            log.debug("TMSVersion exists         = " + versionTableExist);
        }

        if (dbEmpty) {
            // XXX brendan 16/06/14 strange case, should never occur
            if (log.isWarnEnabled()) {
                log.warn("strange case, should never occur");
            }
            // db is empty (no table, no migration to apply)
            return;
        }

        if (versionTableExist && dbVersion.equals(applicationVersion)) {
            // db is up to date
            if (log.isInfoEnabled()) {
                log.info("Database is up to date, no migration needed.");
            }
            return;
        }

        // Aucune version existante, la base de données est vierge
        if (versionTableExist && dbNotVersioned) {
            log.info("Database is empty, no migration needed.");
            // la base est vierge, aucune migration nécessaire
            // mise à jour de la table tmsversion
            saveApplicationVersion();
            return;
        }

        SortedSet<Version> allVersions =
                new TreeSet<Version>(new VersionComparator());
        allVersions.addAll(Arrays.asList(callback.getAvailableVersions()));
        if (log.isInfoEnabled()) {
            log.info(String.format("Available versions: %1$s", allVersions));
        }

        // tell if migration is needed
        boolean migrationDone = false;

        if (dbVersion.before(applicationVersion)) {

            // on filtre les versions a appliquer
            List<Version> versionsToApply =
                    filterVersions(allVersions,
                            dbVersion,
                            applicationVersion,
                            false,
                            true
                    );

            if (versionsToApply.isEmpty()) {
                if (log.isInfoEnabled()) {
                    log.info("No version to apply, no migration needed.");
                }
            } else {
                if (log.isInfoEnabled()) {
                    log.info(String.format("Versions to apply: %1$s", versionsToApply));
                }

                // perform the migration
                migrationDone = callback.doMigration(versionConfiguration,
                        dbVersion,
                        showSql,
                        showProgression,
                        versionsToApply);

                if (log.isDebugEnabled()) {
                    log.debug("Handler choose : " + migrationDone);
                }
                if (!migrationDone) {
                    // l'utilisateur a annule la migration
                    return;
                }
            }
        }

        // on sauvegarde la version si necessaire (base non versionnee ou migration realisée)
        if (!versionTableExist || migrationDone) {

            if (log.isDebugEnabled()) {
                log.debug("Set application version in database to " + applicationVersion);
            }

            // put version in database and create table if required
            saveApplicationVersion();
        }

        // - no migration needed
        // - or migration needed and accepted
    }

    /**
     * Save the application's model version in the database.
     */
    protected void saveApplicationVersion() {

        checkInit();

        final Version version = getApplicationVersion();

        detectDbVersion();

        if (log.isDebugEnabled()) {
            log.debug("Save version     = " + version);
            log.debug("Table exists     = " + versionTableExist);
            log.debug("Detected version = " + dbVersion);
        }

        TopiaUtil.runInSession(versionConfiguration, new Function<Session, TMSVersion>() {

            @Override
            public TMSVersion apply(Session input) {

                // delete all previous data in table
                TMSVersionHibernateDao.deleteAll(input);

                if (log.isInfoEnabled()) {
                    log.info(String.format("Saving new database version: %1$s", version));
                }

                // create new version and store it in table
                TMSVersion tmsVersion =
                        TMSVersionHibernateDao.saveVersion(input, version.getVersion());
                if (log.isDebugEnabled()) {
                    log.debug("Created version: " + tmsVersion.getVersion());
                }

                return tmsVersion;
            }

        });

        // on change les etats internes du service
        // ainsi cela empechera le redeclanchement de la migration
        // suite a une creation de schema
        versionTableExist = true;
        dbVersion = version;
    }

    protected Version getApplicationVersion() {
        String modelVersion = applicationContext.getModelVersion();
        Version result = VersionBuilder.create(modelVersion).build();
        return result;
    }

    /**
     * Recupere depuis la base les états internes du service :
     *
     * <ul>
     * <li>{@link #versionTableExist}</li>
     * <li>{@link #dbVersion}</li>
     * <li>{@link #dbEmpty}</li>
     * </ul>
     */
    protected void detectDbVersion() {

        if (versionDetected) {

            // this method was already invoked
            if (log.isDebugEnabled()) {
                log.debug("Version already detected : " + dbVersion);
            }
            return;
        }

        // compute dbEmpty field value
        dbEmpty = detectDbEmpty();

        if (log.isDebugEnabled()) {
            log.debug("Is DB empty ? " + dbEmpty);
        }


        Version v = null;
        try {

            // on vérifie que la table de versionning existe déjà
            versionTableExist = TopiaUtil.isSchemaExist(versionConfiguration,metaData, TMSVersion.class.getName());


            Preconditions.checkState(versionTableExist, "TMSVersion table should have be created during init");

            TMSVersion tmsVersion = TopiaUtil.runInSession(versionConfiguration, new Function<Session, TMSVersion>() {
                @Override
                public TMSVersion apply(Session object) {
                    TMSVersion tmsVersion = TMSVersionHibernateDao.readVersion(object);
                    return tmsVersion;
                }
            });

            if (tmsVersion != null) {
                v = tmsVersion.toVersion();
            }

            if (log.isWarnEnabled()) {
                if (v == null) {
                    log.warn("Version not found on table " + TMSVersionHibernateDao.TABLE_NAME);
                }
            }

        } finally {

            if (v == null) {
                // la base dans ce cas n'est pas versionee.
                // On dit que la version de la base est 0
                // et les schema de cette version 0 doivent
                // etre detenu en local
                v = Version.VZERO;
                dbNotVersioned = true;
                log.info("Database version not found, so database schema is considered as V0");
            } else {
                log.info(String.format("detected database version: %1$s", v));
            }
            dbVersion = v;
            versionDetected = true;
        }
    }

    /**
     * Detects if there is some schema existing for at least one of the dealed
     * entity of the underlying db context.
     *
     * @return {@code true} if there is no schema for any of the dealed entities,
     * {@code false} otherwise.
     * @since 2.5.3
     */
    protected boolean detectDbEmpty() {

        // XXX brendan 30/04/14 cast show a design smell
        HibernateProvider hibernateProvider =
                ((AbstractTopiaApplicationContext) applicationContext).getHibernateProvider();

        // Get a new Configuration instance (not initialized)
        Configuration applicationHibernateConfiguration = hibernateProvider.newHibernateConfiguration();
        applicationHibernateConfiguration.getProperties().remove(AvailableSettings.HBM2DDL_AUTO); // Make sure schema is not created by Hibernate

        boolean result = TopiaUtil.isSchemaEmpty(applicationHibernateConfiguration, metaData);
        return result;

    }

    protected String getSafeParameter(Properties config, String key) {
        String value = config.getProperty(key, null);
        Preconditions.checkState(StringUtils.isNotEmpty(value), "'" + key + "' not set.");
        return value;
    }

    protected void checkInit() {
        Preconditions.checkState(init, "Service is not yet initialized!");
    }

    /**
     * Creates the hibernate configuration to be used by the service.
     *
     * @param appContext the topia application context used to copy database credentials
     * @since 2.5.3
     */
    protected static Configuration createTMSHibernateConfiguration(TopiaApplicationContext appContext) {

        TopiaConfiguration topiaConfiguration = appContext.getConfiguration();

        Properties prop = new Properties();

        prop.put(AvailableSettings.URL, topiaConfiguration.getJdbcConnectionUrl());
        prop.put(AvailableSettings.USER, topiaConfiguration.getJdbcConnectionUser());
        prop.put(AvailableSettings.PASS, topiaConfiguration.getJdbcConnectionPassword());
        prop.put(AvailableSettings.DRIVER, topiaConfiguration.getJdbcDriverClass().getName());
        prop.put(AvailableSettings.DIALECT, HibernateProvider.getHibernateDialect(topiaConfiguration));
        prop.putAll(topiaConfiguration.getHibernateExtraConfiguration());

        Configuration result = new Configuration();
        result.setProperties(prop);
        result.addClass(TMSVersion.class);

        return result;
    }

    @Override
    public void close() {
        if (sessionFactory!=null) {
            sessionFactory.close();
        }
    }

}
