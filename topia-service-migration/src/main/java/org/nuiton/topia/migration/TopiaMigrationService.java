package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * TopiaMigrationService.java
 *
 * @author Chatellier Eric
 * @author Chevallereau Benjamin
 * @author Eon Sébastien
 * @author Trève Vincent
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface TopiaMigrationService extends org.nuiton.topia.persistence.TopiaMigrationService {

    /**
     * The callback class name to use. Expected format is a FQN.
     */
    String MIGRATION_CALLBACK = "callback";

    /**
     * Migrate during init or not ? Default value is true.
     * @deprecated use {@link org.nuiton.topia.persistence.TopiaConfiguration#isInitSchema()}
     */
    @Deprecated
    String MIGRATION_MIGRATE_ON_INIT = "migrateOnInit";

    /**
     * Display or not SQL requests ? Default value is false.
     */
    String MIGRATION_SHOW_SQL = "showSql";

    /**
     * Display or not migration progress ? Default value is false.
     */
    String MIGRATION_SHOW_PROGRESSION = "showProgression";

    /**
     * @deprecated use {@link org.nuiton.topia.persistence.TopiaMigrationService#runSchemaMigration()}
     */
    @Deprecated
    boolean migrateSchema() throws MigrationServiceException;

}
