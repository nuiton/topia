package org.nuiton.topia.migration.mappings;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.Configuration;
import org.hibernate.jdbc.Work;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.util.TopiaUtil;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.List;

/**
 * TMSVersion DAO helper.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.4
 */
public class TMSVersionHibernateDao {

    /**
     * logger
     */
    private final static Log log = LogFactory.getLog(TMSVersionHibernateDao.class);

    public static final String LEGACY_TABLE_NAME = "tmsVersion";

    public static final String TABLE_NAME = "tms_version";

    public static TMSVersion readVersion(Session session) throws TopiaException {

        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<TMSVersion> criteria = builder.createQuery(TMSVersion.class);
            criteria.from(TMSVersion.class);
            List<TMSVersion> list = session.createQuery(criteria).getResultList();
            TMSVersion result = list.isEmpty() ? null : list.get(0);
            return result;
        } catch (HibernateException he) {
            throw new TopiaException("Could not obtain version", he);
        }
    }

    public static void createTMSSchema(Metadata metadata) {
        // creer le schema en base
        // dans la configuration il n'y a que la table version
        SchemaExport schemaExport = new SchemaExport();
        EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
        if (log.isDebugEnabled()) {
            targetTypes.add(TargetType.STDOUT);
        }
        schemaExport.create(targetTypes, metadata);
    }

    public static void dropTMSSchema(Metadata metadata) {
        // supprimer le schema en base
        // dans la configuration il n'y a que la table version
        SchemaExport schemaExport = new SchemaExport();
        EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
        if (log.isDebugEnabled()) {
            targetTypes.add(TargetType.STDOUT);
        }
        schemaExport.drop(targetTypes,metadata);
    }

    public static TMSVersion saveVersion(Session session, String version) throws TopiaException {

        try {
            TMSVersion result = TMSVersion.valueOf(version);
            session.persist(result);
            return result;
        } catch (HibernateException he) {
            throw new TopiaException("Could not create version " + version, he);
        }
    }

    public static void update(Session session, TMSVersion version) throws TopiaException {
        try {
            session.saveOrUpdate(version);
        } catch (HibernateException he) {
            throw new TopiaException("Could not update version " + version, he);
        }
    }

    public static void deleteAll(Session session) throws TopiaException {
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<TMSVersion> criteria = builder.createQuery(TMSVersion.class);
            criteria.from(TMSVersion.class);
            List<TMSVersion> list = session.createQuery(criteria).getResultList();
            for (TMSVersion o : list) {
                session.remove(o);
            }
        } catch (HibernateException he) {
            throw new TopiaException("Could not delete all versions", he);
        }
    }

    public static Version getVersion(Configuration hibernateConfiguration, Session session, String tableName) {
        try {
            // Get schema name
            String schemaName = TopiaUtil.getSchemaName(hibernateConfiguration);

            GetVersionWork work = new GetVersionWork(schemaName, tableName);
            session.doWork(work);
            Version v = work.getVersion();
            return v;
        } catch (HibernateException he) {
            throw new TopiaException("Can't obtain dbVersion for reason " + he.getMessage(), he);
        }
    }

    public static class GetVersionWork implements Work {

        protected Version version;

        private final String tableName;

        private final String schemaName;

        public GetVersionWork(String schemaName, String tableName) {
            this.tableName = tableName;
            this.schemaName = schemaName;
        }

        @Override
        public void execute(Connection connection) throws SQLException {

            String fullTableName = schemaName == null ?
                    tableName : schemaName + "." + tableName;


            try (PreparedStatement st = connection.prepareStatement("select " + TMSVersion.PROPERTY_VERSION + " from " + fullTableName + ";")) {
                ResultSet set = st.executeQuery();

                if (set.next()) {
                    version = VersionBuilder.create(set.getString(1)).build();
                }
            } catch (SQLException eee) {
                if (log.isErrorEnabled()) {
                    log.error("Could not find version", eee);
                }
                version = null;
            }
        }

        public Version getVersion() {
            return version;
        }
    }
}
