package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.util.ObjectUtil;
import org.nuiton.version.Version;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Migration callback which use a different class for each version to migrate.
 *
 * You must fill in the constructor the mapping for each version of
 * {@link #getAvailableVersions()} a matching migrator for version which
 * extends {@link MigrationCallBackForVersion}.
 *
 * Use the callback when you have a lot of version to migrate and the
 * {@link TopiaMigrationCallbackByMethod} begins to be messy.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public abstract class TopiaMigrationCallbackByClass extends AbstractTopiaMigrationCallback {

    protected MigrationCallBackForVersionResolver callBackResolver;

    protected TopiaMigrationCallbackByClass(MigrationCallBackForVersionResolver callBackResolver) {

        this.callBackResolver = callBackResolver;

        // check for each version of migration we have a migrator
        for (Version version : getAvailableVersions()) {
            Class<? extends MigrationCallBackForVersion> callBack = this.callBackResolver.getCallBack(version);
            if (callBack == null) {
                throw new IllegalStateException("It misses a migration class for version " + version);
            }
        }
    }

    @Override
    protected void migrateForVersion(Version version,
                                     TopiaSqlSupport sqlSupport,
                                     boolean showSql,
                                     boolean showProgression) throws Exception {

        Class<? extends MigrationCallBackForVersion> migratorClass = callBackResolver.getCallBack(version);

        MigrationCallBackForVersion migrator = ObjectUtil.newInstance(migratorClass, Arrays.asList(this), true);

        String[] queries = migrator.prepareMigration(sqlSupport, showSql, showProgression);

        executeSQL(sqlSupport, showSql, showProgression, queries);

    }

    /**
     * Call back for a given version.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.5
     */
    public abstract static class MigrationCallBackForVersion {

        protected final Version version;

        protected final TopiaMigrationCallbackByClass callBack;

        public MigrationCallBackForVersion(Version version, TopiaMigrationCallbackByClass callBack) {
            this.version = version;
            this.callBack = callBack;

        }

        protected String[] prepareMigration(TopiaSqlSupport sqlSupport,
                                            boolean showSql,
                                            boolean showProgression) throws TopiaException {

            List<String> queries = new ArrayList<String>();

            prepareMigrationScript(sqlSupport, queries, showSql, showProgression);

            return queries.toArray(new String[queries.size()]);
        }

        protected abstract void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                                       List<String> queries,
                                                       boolean showSql,
                                                       boolean showProgression) throws TopiaException;

        public void executeSQL(TopiaSqlSupport sqlSupport,
                               String... sqls) throws TopiaException {
            callBack.executeSQL(sqlSupport, sqls);
        }

        public void executeSQL(TopiaSqlSupport sqlSupport,
                               boolean showSql,
                               boolean showProgression,
                               String... sqls) throws TopiaException {
            callBack.executeSQL(sqlSupport, showSql, showProgression, sqls);
        }

    }

    /**
     * Resolver to obtain the correct migration class for a given version.
     *
     * @since 2.5
     */
    public interface MigrationCallBackForVersionResolver {

        Class<? extends MigrationCallBackForVersion> getCallBack(Version version);

    }

    /**
     * A simple call back resolver via a constant map.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.5
     */
    public static class MigrationCallBackForVersionResolverByMap implements MigrationCallBackForVersionResolver {

        protected final Map<Version, Class<? extends MigrationCallBackForVersion>> versionMigrationMapping;

        public MigrationCallBackForVersionResolverByMap(Map<Version, Class<? extends MigrationCallBackForVersion>> versionMigrationMapping) {
            this.versionMigrationMapping = versionMigrationMapping;
        }

        @Override
        public Class<? extends MigrationCallBackForVersion> getCallBack(Version version) {
            return versionMigrationMapping.get(version);
        }
    }
}
