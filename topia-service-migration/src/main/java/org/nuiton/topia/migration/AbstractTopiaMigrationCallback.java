package org.nuiton.topia.migration;

/*
 * #%L
 * ToPIA :: Service Migration
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.nuiton.topia.migration.mappings.TMSVersionHibernateDao;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.util.TopiaUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.version.Version;

import java.util.List;

/**
 * Abstract migration callback.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public abstract class AbstractTopiaMigrationCallback {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(AbstractTopiaMigrationCallback.class);

    /**
     * @return the available versions from the call back
     */
    public abstract Version[] getAvailableVersions();

    /**
     * Hook to ask user if migration can be performed.
     *
     * @param dbVersion the actual db version
     * @param versions  the versions to update
     * @return {@code false} if migration is canceled, {@code true} otherwise.
     */
    public abstract boolean askUser(Version dbVersion, List<Version> versions);

    protected abstract void migrateForVersion(Version version,
                                              TopiaSqlSupport sqlSupport,
                                              boolean showSql,
                                              boolean showProgression) throws Exception;

    /**
     * Tentative de migration depuis la version de la base version la version
     * souhaitee.
     *
     * On applique toutes les migrations de version indiquee dans le parametre
     * {@code version}.
     *
     * Pour chaque version, on cherche la methode migrateTo_XXX ou XXX est la
     * version transforme en identifiant java via la methode
     * {@link Version#getValidName()} et on l'execute.
     *
     * Note: pour chaque version a appliquer, on ouvre une nouvelle transaction.
     *
     * @param versionConfiguration configuration Hibernate permettant d'accéder à la base
     * @param dbVersion            database version
     * @param showSql              drapeau pour afficher les requete sql
     * @param showProgression      drapeau pour afficher la progression
     * @param versions             all versions knwon by service  @return migration a
     *                             ggrement
     * @return {@code true} si la migration est accepté, {@code false} autrement.
     */
    public boolean doMigration(Configuration versionConfiguration,
                               Version dbVersion,
                               boolean showSql,
                               boolean showProgression,
                               List<Version> versions) {

        boolean doMigrate = askUser(dbVersion, versions);
        if (doMigrate) {

            for (Version v : versions) {
                // ouverture d'une connexion direct JDBC sur la base
                try {

                    SessionFactory sessionFactory = null;
                    Session session = null;
                    try {
                        sessionFactory = TopiaUtil.newSessionFactory(versionConfiguration);
                        session = sessionFactory.openSession();

                        session.getTransaction().begin();

                        if (log.isInfoEnabled()) {
                            log.info(String.format("Start migration to version %s", v));
                        }

                        TopiaSqlSupport sqlSupport = new HibernateTopiaSqlSupport(session);

                        migrateForVersion(v, sqlSupport, showSql, showProgression);

                        TMSVersionHibernateDao.saveVersion(session, v.toString());
                        session.getTransaction().commit();

                    } catch (Exception eee) {
                        // Exception, rollback transaction
                        if (log.isDebugEnabled()) {
                            log.error("Exception during schema migration, rollbacking transaction", eee);
                        }
                        if (session != null && session.isOpen() && TransactionStatus.ACTIVE == session.getTransaction().getStatus()) {
                            session.getTransaction().rollback();
                        }
                        throw new TopiaException("Exception during schema migration", eee);
                    } finally {
                        if (session != null && session.isOpen()) {
                            session.close();
                        }
                        if (sessionFactory != null && !sessionFactory.isClosed()) {
                            sessionFactory.close();
                        }
                    }

                } catch (Exception eee) {
                    // toute erreur arrête la migration
                    throw new TopiaException("Exception during schema migration", eee);
                }
            }
        }
        return doMigrate;
    }

    public void executeSQL(TopiaSqlSupport sqlSupport, String... sqls)
            throws TopiaException {
        executeSQL(sqlSupport, false, false, sqls);
    }

    /**
     * Executes the given {@code sqls} requests.
     *
     * @param showSql         flag to see sql requests
     * @param showProgression flag to see progession on console
     * @param sqls            requests to execute
     * @throws TopiaException if any pb
     * @since 2.3.0
     */
    public void executeSQL(TopiaSqlSupport sqlSupport,
                           final boolean showSql,
                           final boolean showProgression,
                           final String... sqls) throws TopiaException {

        if (log.isInfoEnabled()) {

            log.info(String.format("Will execute %1$s requests...", sqls.length));
        }
        if (showSql) {
            StringBuilder buffer = new StringBuilder();
            for (String s : sqls) {
                buffer.append(s).append("\n");
            }
            log.info("SQL TO EXECUTE :\n" +
                            "--------------------------------------------------------------------------------\n" +
                            "--------------------------------------------------------------------------------\n" +
                            buffer.toString() +
                            "--------------------------------------------------------------------------------\n" +
                            "--------------------------------------------------------------------------------\n"
            );
        }

        int index = 0;
        int max = sqls.length;
        for (String sql : sqls) {
            index++;
            long t0 = System.nanoTime();
            if (log.isInfoEnabled()) {
                String message = "";

                if (showProgression) {
                    message = String.format("Executing request [%1$-4s/%2$-4s]", index, max);
                }
                if (showSql) {
                    message += "\n" + sql;
                }
                if (showProgression || showSql) {

                    log.info(message);
                }
            }

            sqlSupport.executeSql(sql);

            if (log.isDebugEnabled()) {
                String message;
                message = String.format("Request [%1$-4s/%2$-4s] executed in %3$s.", index, max, StringUtil.convertTime(System.nanoTime() - t0));
                log.debug(message);
            }
        }
    }

}
