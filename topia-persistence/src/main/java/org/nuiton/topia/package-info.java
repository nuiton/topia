/**
 *  TODO-FD20100507 : Need update this javadoc for ToPIA 2.4
 *
 *  <p>ToPIA est un framework de persistence basé sur Hibernate. Il
 *  contient un point d'entré le TopiaContext qui permet d'ouvrir des
 *  transactions qui retourne des TopiaContext fils sur lequel on peut
 *  récupéré des DAO pour accèder aux entités.</p>
 *
 *  <p>ToPIA offre en plus la possibilité de générer à partir d'une
 *  fichier XMI toutes les classes utiles pour la persistence. Ce qui
 *  permet d'évité un travail fastidieux d'ecriture de classe, d'être sur
 *  que le jour on l'on veut ajouter une méthode technique (getXML, ...) à
 *  toutes ces entités cela sera fait de façon simple et automatique
 *  (modification du générateur puis regénération). Et surtout d'avoir des
 *  classes générés qui permettent un typage fort de l'application (pas de
 *  cast, pas de générique)</p>
 *
 *  <p>Bien sur il est possible d'utilisé ToPIA sans générateur, il est
 *  d'ailleurs utilisé ainsi pour les tests</p>
 *
 *  <p>ToPIA contient aussi des classes techniques réutilisables dans ses
 *  applications pour tout ce qui touche à l'authentification et
 *  l'autorisation (TopiaUser, TopiaGroup, TopiaPermission) (partie non
 *  encore développé)</p>
 *
 *  <h2>La persistance</h2>
 *
 *  <p>La persistence se base complètement sur hibernate, mais il est
 *  aussi possible d'indiquer que certaine classe doivent être sauvé dans
 *  des fichiers textes plutot qu'une base de données, cette persistence
 *  est appelée FlatFile.</p>
 *
 *  <p>Le TopiaContext doit être configuré avec un objet Properties.
 *  Celui-ci peut contenir différentes entrées. Vu que la persistence est
 *  complètement basé sur hibernate, même si vous n'avez que des entités
 *  sauvé en FlatFile il vous faudra fournir à hibernate un accès à une
 *  base de données. Celle-ci peu très bien être une base embarqué comme
 *  hsql, mckoi ou derby.</p>
 *
 *  <dl>
 *    <dt>topia.persistence.properties.file</dt>
 *
 *    <dd>le fichier de propriété a utiliser pour configurer
 *    hibernate</dd>
 *
 *    <dt>topia.persistence.directories</dt>
 *
 *    <dd>la liste des repertoires contenant les mappings hibernates
 *    (.hbm.xml) la liste de repertoire est separer par des virgules
 *    ','</dd>
 *
 *    <dt>topia.persistence.classes</dt>
 *
 *    <dd>la liste des classes que doit géré ToPIA. On peut tres bien
 *    utiliser topia.persistence.directories pour un ensemble d'entié du
 *    meme repertoire et topia.persistence.classes pour d'autres
 *    classes</dd>
 *  </dl>
 *
 *  <dl>
 *    <dt>topia.dao.flatfile.properties.file</dt>
 *
 *    <dd>indique le fichier de configuration a utiliser en plus de la
 *    configuration du context</dd>
 *
 *    <dt>topia.dao.flatfile.directory</dt>
 *
 *    <dd>indique le répertoire au sauver les entités</dd>
 *
 *    <dt>topia.dao.flatfile.directory.[fqn-entity]</dt>
 *
 *    <dd>permet de spécifier un répertoire différent pour une entity
 *    spécifique</dd>
 *
 *    <dt>topia.dao.flatfile.mapping</dt>
 *
 *    <dd>permet d'indique le mapping a utiliser pour les entités</dd>
 *
 *    <dt>topia.dao.flatfile.mapping.[fqn-entity]</dt>
 *
 *    <dd>permet d'indique un mapping différent pour entité</dd>
 *  </dl>
 *
 *  <p>à la place flatfile il est possible de mettre le FQN du DAO utilisé
 *  par exemple
 *  org.nuiton.topia.persistence.flatfile.TopiaDAOFlatFile</p>
 *
 *  <p>Si directory est absent alors "." est utilisé</p>
 *
 *  <p>les mappings s'écrivent de la façon suivant:</p>
 *
 *  <ul>
 *    <li>ext=extension a ajouter au fichier contenant l'entity</li>
 *
 *    <li>key=attribute</li>
 *
 *    <li>body=attribute</li>
 *  </ul>
 *
 *  <p>Si key est absent alors on utilise le topiaId, key est utilisé
 *  comme nom de fichier de sauvegarde</p>
 *
 *  <p>Si body est absent alors on utilise un fichier de propriété pour
 *  sauver l'entity. Si body est présent seul cet attribut sera sauvé.</p>
 *
 *  <pre>
 *    topia.dao.flatfile.mapping.key=topiaId
 *
 *    topia.dao.flatfile.mapping.fr.ifremer.isisfish.entities.Script.key=name
 *    topia.dao.flatfile.mapping.fr.ifremer.isisfish.entities.Script.body=script
 *  </pre>
 */
package org.nuiton.topia;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as  
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
