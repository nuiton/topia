package org.nuiton.topia.persistence.legacy;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Map;

/**
 * A simple contract to load an object from another one.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @param <E> type of bean to load
 */
public interface Loador<E> extends Serializable {

    /**
     * Obtains the type of the entity.
     *
     * @return the type of entity
     */
    Class<E> getSourceType();

    /**
     * Obtain from an entity all data to bind to another one according the
     * definition of the loador.
     *
     * This method is usefull when you can not directly used the
     * {@link #load(Object, Object, boolean, String...)} method.
     *
     * For example, when an entity has a immutable business key (says with an
     * hibernate naturalId for example),
     * and that you want to create the data in a db, you must give all the
     * properties at the create time so this method allow you to do it).
     *
     * @param from          the entity to bind
     * @param propertyNames subset of properties to load
     * @return the map of properties to bind from the given entity.
     */
    Map<String, Object> obtainProperties(E from, String... propertyNames);

    /**
     * Bind an entity to another.
     *
     * @param from          the source entity
     * @param dst           the destination entity
     * @param tech          a flag to bind or not the technical values of the entity
     *                      (says TopiaId, TopiaVersion and TopiaCreateDate).
     * @param propertyNames subset of properties to load
     */
    void load(E from, E dst, boolean tech, String... propertyNames);
}
