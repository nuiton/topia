package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaReplicationDestination;

import java.util.List;

/**
 * This API provides methods about entities replication to a destination
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaReplicationSupport {

    /**
     * Makes a replication of some entities from this context to the given
     * context without any entity modification.
     *
     * <b>Note:</b> If the {@code entityAndCondition} parameter is empty,
     * all the database will be replicated
     *
     * <b>Note 2:</b> The simple replication may not be sufficent. You may want
     * to replicate only a part of some entities : use the method {@link
     * #replicateEntities(TopiaReplicationDestination, java.util.List)}.
     *
     * @param topiaReplicationDestination the destination context
     * @param entityAndCondition          [key;value;...] parameter which key is the
     *                                    entity class to replicate, and value the
     *                                    "where" condition to use when querying entities
     * @throws IllegalArgumentException if one of the context is closed or if
     *                                  trying to replicate within the same
     *                                  database
     */
    void replicate(TopiaReplicationDestination topiaReplicationDestination,
                   Object... entityAndCondition) throws IllegalArgumentException;

    /**
     * Replicate a given entity from this context to the given context.
     *
     * @param topiaReplicationDestination the destination context
     * @param entity                      the entity instance to replicate
     * @param <T>                         type of entity
     * @throws IllegalArgumentException if one of the context is closed or if
     *                                  trying to replicate within the same
     *                                  database
     */
    <T extends TopiaEntity> void replicateEntity(TopiaReplicationDestination topiaReplicationDestination,
                                                 T entity) throws IllegalArgumentException;

    /**
     * Makes a replication of some entities from this context to the given
     * context without any entity modification.
     *
     * @param topiaReplicationDestination the destination context
     * @param entities                    the list of entities instance to replicate
     * @param <T>                         type of entity
     * @throws IllegalArgumentException if one of the context is closed or if
     *                                  trying to replicate within the same
     *                                  database
     */
    <T extends TopiaEntity> void replicateEntities(TopiaReplicationDestination topiaReplicationDestination,
                                                   List<T> entities) throws IllegalArgumentException;

}
