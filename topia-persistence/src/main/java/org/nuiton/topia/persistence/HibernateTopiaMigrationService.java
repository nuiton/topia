package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.Map;

public class HibernateTopiaMigrationService implements TopiaMigrationService {

    protected TopiaApplicationContext topiaApplicationContext;

    @Override
    public void initTopiaService(TopiaApplicationContext topiaApplicationContext, Map<String, String> serviceConfiguration) {
        this.topiaApplicationContext = topiaApplicationContext;
    }

    @Override
    public String getSchemaVersion() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void initOnCreateSchema() {
        // do nothing
    }

    @Override
    public void runSchemaMigration() {
        topiaApplicationContext.updateSchema();
    }

    @Override
    public void close() {

    }
}
