package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A dictionnary of {@link TopiaEntity} associated to a {@link TopiaEntityEnum}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.3
 */
public abstract class TopiaEntityMap<K extends TopiaEntityEnum, V extends TopiaEntity> extends HashMap<K, List<? extends V>> {

    private static final long serialVersionUID = 1L;

    public TopiaEntityMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public TopiaEntityMap(int initialCapacity) {
        super(initialCapacity);
    }

    public TopiaEntityMap() {
    }

    public TopiaEntityMap(Map<? extends K, ? extends List<? extends V>> m) {
        super(m);
    }

    /**
     * Obtains from a entity his key.
     *
     * @param e the entity on which to find the key.
     * @return the key of the given entity.
     */
    protected abstract K getType(Class<?> e);

    public <T extends V> List<T> get(Class<T> type) {
        K key = getType(type);
        List<T> vs = (List<T>) get(key);
        return vs;
    }

    /**
     * Adds the given entity to the dictonary only if it does not exists.
     *
     * Will return {@code true} if entity was added, {@code false} otherwise.
     *
     * @param entity the entity to add
     * @param <T>    the type of entity to add
     * @return {@code true} if entity was added, {@code false} otherwise.
     */
    public <T extends V> boolean addUniqueEntity(T entity) {
        List<T> list = getList(entity);
        if (!list.contains(entity)) {
            list.add(entity);
            return true;
        }
        return false;
    }

    /**
     * Adds the given entity to the dictonary (even if it does already exists).
     *
     * @param entity the entity to add
     * @param <T>    the type of entity to add
     */
    public <T extends V> void addEntity(T entity) {
        List<T> list = getList(entity);
        list.add(entity);
    }

    @SuppressWarnings({"unchecked"})
    protected <T extends V> List<T> getList(T entity) {
        K type = getType(entity.getClass());
        List<T> list = (List<T>) get(type);
        if (list == null) {
            list = new ArrayList<T>();
            put(type, list);
        }
        return list;
    }
}
