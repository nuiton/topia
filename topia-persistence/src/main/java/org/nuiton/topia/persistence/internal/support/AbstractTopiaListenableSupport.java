package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.event.EntityState;
import org.nuiton.topia.persistence.event.TopiaEntitiesVetoable;
import org.nuiton.topia.persistence.event.TopiaEntityListener;
import org.nuiton.topia.persistence.event.TopiaEntityVetoable;
import org.nuiton.topia.persistence.event.TopiaSchemaListener;
import org.nuiton.topia.persistence.event.TopiaTransactionListener;
import org.nuiton.topia.persistence.event.TopiaTransactionVetoable;
import org.nuiton.topia.persistence.support.TopiaListenableSupport;
import org.nuiton.util.CategorisedListenerSet;
import org.nuiton.util.ListenerSet;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class AbstractTopiaListenableSupport implements TopiaListenableSupport {

    /**
     * Map to collect entities modifications during the current transaction
     */
    protected Map<TopiaEntity, EntityState> transactionEntities = new IdentityHashMap<TopiaEntity, EntityState>();

    /**
     * Listeners on entity's properties events
     */
    protected Set<PropertyChangeListener> propertyChangeListeners = new HashSet<PropertyChangeListener>();

    /**
     * Listeners on schema events
     */
    protected ListenerSet<TopiaSchemaListener> topiaSchemaListeners = new ListenerSet<TopiaSchemaListener>();

    /**
     * Listeners on transaction events
     */
    protected ListenerSet<TopiaTransactionListener> transactionListeners = new ListenerSet<TopiaTransactionListener>();

    /**
     * Listeners on transaction events that can prevent things
     */
    protected ListenerSet<TopiaTransactionVetoable> transactionVetoables = new ListenerSet<TopiaTransactionVetoable>();

    /**
     * Listeners on entity events
     */
    protected CategorisedListenerSet<TopiaEntityListener> entityListeners = new CategorisedListenerSet<TopiaEntityListener>();

    /**
     * Listeners on entity events that can prevent things
     */
    protected CategorisedListenerSet<TopiaEntityVetoable> entityVetoables = new CategorisedListenerSet<TopiaEntityVetoable>();

    /**
     * Listeners on list of entities events
     */
    protected ListenerSet<TopiaEntitiesVetoable> entitiesVetoables = new ListenerSet<TopiaEntitiesVetoable>();

//    /* Getters */
//
//    protected CategorisedListenerSet<TopiaEntityListener> getEntityListeners() {
//        return entityListeners;
//    }
//
//    protected CategorisedListenerSet<TopiaEntityVetoable> getEntityVetoables() {
//        return entityVetoables;
//    }
//
//    protected ListenerSet<TopiaTransactionListener> getTransactionListeners() {
//        return transactionListeners;
//    }
//
//    protected ListenerSet<TopiaTransactionVetoable> getTransactionVetoable() {
//        return transactionVetoables;
//    }
//
//    protected ListenerSet<TopiaSchemaListener> getTopiaSchemaListeners() {
//        return topiaSchemaListeners;
//    }
//
//    protected ListenerSet<TopiaEntitiesVetoable> getTopiaEntitiesVetoable() {
//        return entitiesVetoables;
//    }

    /* Adders */

    public void addTopiaEntityListener(TopiaEntityListener listener) {
        addTopiaEntityListener(TopiaEntity.class, listener);
    }

    public void addTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityListeners.add(entityClass, listener);
    }

    public void addTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        addTopiaEntityVetoable(TopiaEntity.class, vetoable);
    }

    public void addTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityVetoables.add(entityClass, vetoable);
    }

    public void addTopiaTransactionListener(TopiaTransactionListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionListeners.add(listener);
    }

    public void addTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionVetoables.add(vetoable);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        propertyChangeListeners.add(listener);
    }

    public void addTopiaSchemaListener(TopiaSchemaListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        topiaSchemaListeners.add(listener);
    }

    public void addTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entitiesVetoables.add(vetoable);
    }

    /* Removers */

    public void removeTopiaEntityListener(TopiaEntityListener listener) {
        removeTopiaEntityListener(TopiaEntity.class, listener);
    }

    public void removeTopiaEntityListener(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityListeners.remove(entityClass, listener);
    }

    public void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        removeTopiaEntityVetoable(TopiaEntity.class, vetoable);
    }

    public void removeTopiaEntityVetoable(
            Class<? extends TopiaEntity> entityClass,
            TopiaEntityVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entityVetoables.remove(entityClass, vetoable);
    }

    public void removeTopiaTransactionListener(TopiaTransactionListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionListeners.remove(listener);
    }

    public void removeTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        transactionVetoables.remove(vetoable);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        propertyChangeListeners.remove(listener);
    }

    public void removeTopiaSchemaListener(TopiaSchemaListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener can not be null.");
        }
        topiaSchemaListeners.remove(listener);
    }

    public void removeTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        if (vetoable == null) {
            throw new NullPointerException("listener can not be null.");
        }
        entitiesVetoables.remove(vetoable);
    }

}
