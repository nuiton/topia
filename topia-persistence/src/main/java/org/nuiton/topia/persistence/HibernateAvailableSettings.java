package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.hibernate.cfg.AvailableSettings;

/**
 * It's just a shortcut that allows you to use {@link org.hibernate.cfg.AvailableSettings}
 * constants without adding Hibernate to your dependencies so one can fill the
 * {@link TopiaConfiguration#getHibernateExtraConfiguration()}.
 *
 * @since 3.0
 */
public interface HibernateAvailableSettings extends AvailableSettings {

    /**
     * {@link org.hibernate.cfg.AvailableSettings} seems to lack a constant for this configuration
     * directive.
     *
     * @deprecated configuration directive is deprecated, you should use
     *             {@link AvailableSettings#IMPLICIT_NAMING_STRATEGY} and/or
     *             {@link AvailableSettings#PHYSICAL_NAMING_STRATEGY}
     */
    @Deprecated
    String NAMING_STRATEGY = "hibernate.ejb.naming_strategy";

}
