package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.QueryMissingOrderException;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.TopiaNonUniqueResultException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.TopiaQueryBuilderRunQueryStep;
import org.nuiton.topia.persistence.event.TopiaEntityListener;
import org.nuiton.topia.persistence.event.TopiaEntityVetoable;
import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;
import org.nuiton.topia.persistence.internal.support.TopiaHibernateEventListener;
import org.nuiton.topia.persistence.pager.FilterRuleGroupOperator;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.util.TopiaUtil;
import org.nuiton.util.pagination.PaginationOrder;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

/**
 * <p>
 * This class has the common methods usable for each Dao managed by Topia. It is not JPA implementation dependent, it
 * only relies on {@link TopiaJpaSupport}.
 * </p>
 * <p>
 * This class is directly extended by the GeneratedXyzTopiaDao which groups all the Xyz specific methods.
 * </p>
 * Instances are created bt the model's specific {@link TopiaPersistenceContext}, which implements the
 * {@link TopiaDaoSupplier} contract.
 *
 * @param <E> the managed entity type
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 * @author Tony Chemit - tchemit@codelutin.com
 */
public abstract class AbstractTopiaDao<E extends TopiaEntity> implements TopiaDao<E> {

    private static Log log = LogFactory.getLog(AbstractTopiaDao.class);


    protected static final Function<PaginationOrder, String> PAGINATION_ORDER_TO_HQL = new Function<PaginationOrder, String>() {

        @Override
        public String apply(PaginationOrder input) {
            String result = String.format("%s %s", input.getClause(), input.isDesc() ? "DESC" : "ASC");
            return result;
        }
    };

    /**
     * Default batch size used to iterate on data.
     *
     * @since 2.6.14
     */
    protected int batchSize = 1000;

    protected TopiaJpaSupport topiaJpaSupport;

    protected TopiaHibernateSupport topiaHibernateSupport;

    protected TopiaSqlSupport topiaSqlSupport;

    protected TopiaIdFactory topiaIdFactory;

    protected TopiaFiresSupport topiaFiresSupport;

    protected TopiaDaoSupplier topiaDaoSupplier;

    public abstract TopiaEntityEnum getTopiaEntityEnum();

    public abstract Class<E> getEntityClass();

    /**
     * When AbstractTopiaContext create the TopiaDAOHibernate, it must call this
     * method just after.
     *
     * @param topiaJpaSupport       FIXME
     * @param topiaHibernateSupport FIXME
     * @param topiaSqlSupport       FIXME
     * @param topiaIdFactory        FIXME
     * @param topiaFiresSupport     FIXME
     * @param topiaDaoSupplier      FIXME
     */
    public void init(
            TopiaJpaSupport topiaJpaSupport,
            TopiaHibernateSupport topiaHibernateSupport,
            TopiaSqlSupport topiaSqlSupport,
            TopiaIdFactory topiaIdFactory,
            TopiaFiresSupport topiaFiresSupport,
            TopiaDaoSupplier topiaDaoSupplier) {
        if (log.isDebugEnabled()) {
            log.debug("init dao for " + getEntityClass());
        }
        this.topiaJpaSupport = topiaJpaSupport;
        this.topiaHibernateSupport = topiaHibernateSupport;
        this.topiaSqlSupport = topiaSqlSupport;
        this.topiaIdFactory = topiaIdFactory;
        this.topiaFiresSupport = topiaFiresSupport;
        this.topiaDaoSupplier = topiaDaoSupplier;
    }

    public TopiaFiresSupport getTopiaFiresSupport() {
        return topiaFiresSupport;
    }

    @Override
    public int getBatchSize() {
        return batchSize;
    }

    @Override
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    protected String newFromClause() {
        return newFromClause(null);
    }

    protected String newFromClause(String alias) {
        String hql = "from " + getTopiaEntityEnum().getImplementationFQN();
        if (StringUtils.isNotBlank(alias)) {
            hql += " " + alias;
        }
        return hql;
    }

    @Override
    public E newInstance() {
        if (log.isTraceEnabled()) {
            log.trace("entityClass = " + getEntityClass());
        }
        Class<E> implementation = (Class<E>)
                getTopiaEntityEnum().getImplementation();
        try {
            E newInstance = implementation.newInstance();

            TopiaHibernateEventListener.attachContext(newInstance, topiaDaoSupplier, topiaFiresSupport);

            return newInstance;
        } catch (InstantiationException e) {
            throw new TopiaException(
                    "Impossible de trouver ou d'instancier la classe "
                    + implementation);
        } catch (IllegalAccessException e) {
            throw new TopiaException(
                    "Impossible de trouver ou d'instancier la classe "
                    + implementation);
        }
    }

    @Override
    public E newInstance(Map<String, Object> properties) {
        E result = newInstance();

        try {
            for (Map.Entry<String, Object> e : properties.entrySet()) {
                String propertyName = e.getKey();
                Object value = e.getValue();
                PropertyUtils.setProperty(result, propertyName, value);
            }
        } catch (IllegalAccessException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        } catch (InvocationTargetException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        } catch (NoSuchMethodException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        }
        return result;
    }

    @Override
    public E newInstance(String propertyName, Object propertyValue, Object... otherPropertyNamesAndValues) {
        Map<String, Object> properties =
                TopiaUtil.convertPropertiesArrayToMap(propertyName, propertyValue, otherPropertyNamesAndValues);
        return newInstance(properties);
    }

    @Override
    public PaginationResult<E> initPagination(int pageSize) {
        PaginationResult<E> result = initPagination(newFromClause(), new HashMap<String, Object>(), pageSize);
        return result;
    }

    @Override
    public PaginationResult<E> initPagination(String hql, Map<String, Object> params, int pageSize) {
        PaginationParameter firstPage = PaginationParameter.of(0, pageSize);

        if (hqlContainsOrderBy(hql)) {
            // must remove the order by clause, otherwise some sql queries won't work.
            hql = hql.substring(0, hql.toLowerCase().indexOf("order by"));
        }

        String countCondition = "*";
        if (hqlStartsWithSelect(hql)) {
            // must remove the select, otherwise some sql queries won't work.
            int selectIndex = hql.toLowerCase().indexOf("select");
            int fromIndex = hql.toLowerCase().indexOf("from");

            // A: select * from ...           →  select count(*) from ...
            // B: select a from ...           →  select count(*) from ...
            // C: select a,b from ...         →  select count(*) from ...
            // D: select distinct a from ...  →  select count(distinct a) from ...

            // Here is a fix for case D
            String selectCondition = hql.toLowerCase().substring(selectIndex + "select".length(), fromIndex);
            if (selectCondition.contains("distinct")) {
                // AThimel 18/07/14 Hibernate does not support "select count(distinct(name))", need to use "select count(distinct name)"
                Preconditions.checkState(!selectCondition.replaceAll(" ", "").toLowerCase().contains("distinct("),
                                         "This method needs to run count(...), but Hibernate does not support " +
                                         "\"select count(distinct(name))\", please use \"select distinct name\" (without brackets)");
                countCondition = selectCondition;
            }

            hql = hql.substring(fromIndex);
        }

        String countHql = String.format("SELECT COUNT(%s) %s", countCondition, hql);
        long count = count(countHql, params);
        List<E> emptyList = Lists.newArrayList(); // AThimel 22/05/14 To keep the old behavior, we do not load the elements
        PaginationResult<E> result = PaginationResult.of(emptyList, count, firstPage);
        return result;
    }

    @Override
    public void addTopiaEntityListener(TopiaEntityListener listener) {
        topiaFiresSupport.addTopiaEntityListener(getEntityClass(), listener);
    }

    @Override
    public void addTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        topiaFiresSupport.addTopiaEntityVetoable(getEntityClass(), vetoable);
    }

    @Override
    public void removeTopiaEntityListener(TopiaEntityListener listener) {
        topiaFiresSupport.removeTopiaEntityListener(getEntityClass(), listener);
    }

    @Override
    public void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        topiaFiresSupport.removeTopiaEntityVetoable(getEntityClass(), vetoable);
    }

    @Override
    public E create(E entity) {
        // first set topiaId
        if (StringUtils.isBlank(entity.getTopiaId())) {

            // only set id if not already on
            String topiaId = topiaIdFactory.newTopiaId(getEntityClass(), entity);
            entity.setTopiaId(topiaId);
        }

        if (entity instanceof TopiaEntityContextable) {
            TopiaEntityContextable contextable = (TopiaEntityContextable) entity;
            contextable.setTopiaDaoSupplier(this.topiaDaoSupplier);
        }

        // save entity
        topiaJpaSupport.save(entity);
        topiaFiresSupport.notifyEntityCreated(entity);
        return entity;
    }

    @Override
    public E create(String propertyName, Object propertyValue, Object... otherPropertyNamesAndValues) {
        Map<String, Object> properties =
                TopiaUtil.convertPropertiesArrayToMap(propertyName, propertyValue, otherPropertyNamesAndValues);
        E result = create(properties);
        return result;
    }

    @Override
    public E create(Map<String, Object> properties) {
        E result = newInstance(properties);
        create(result);
        return result;
    }

    @Override
    public E create() {
        E result = newInstance();
        create(result);
        return result;
    }

    @Override
    public Iterable<E> createAll(Iterable<E> entities) {
        for (E entity : entities) {
            create(entity);
        }
        return entities;
    }

    @Override
    public E update(E entity) {
        topiaJpaSupport.saveOrUpdate(entity);
        topiaFiresSupport.notifyEntityUpdated(entity);
        return entity;
    }

    @Override
    public Iterable<E> updateAll(Iterable<E> entities) {
        for (E entity : entities) {
            update(entity);
        }
        return entities;
    }

    @Override
    public void delete(E entity) {
        topiaJpaSupport.delete(entity);
        entity.notifyDeleted();
        topiaFiresSupport.notifyEntityDeleted(entity);
    }

    @Override
    public void deleteAll(Iterable<E> entities) {
        for (E entity : entities) {
            delete(entity);
        }
    }

    protected HqlAndParametersBuilder<E> newHqlAndParametersBuilder(FilterRuleGroupOperator filterRuleGroupOperator) {
        HqlAndParametersBuilder<E> result = new HqlAndParametersBuilder<E>(getEntityClass(), filterRuleGroupOperator);
        return result;
    }

    protected HqlAndParametersBuilder<E> newHqlAndParametersBuilder() {
        return newHqlAndParametersBuilder(FilterRuleGroupOperator.AND);
    }

    protected HqlAndParametersBuilder<E> getHqlForProperties(String propertyName,
                                                             Object propertyValue,
                                                             Object... otherPropertyNamesAndValues) {
        Map<String, Object> properties =
                TopiaUtil.convertPropertiesArrayToMap(propertyName, propertyValue, otherPropertyNamesAndValues);
        HqlAndParametersBuilder<E> result = getHqlForProperties(properties);
        return result;
    }

    protected HqlAndParametersBuilder<E> getHqlForNoConstraint() {
        Map<String, Object> properties = Collections.emptyMap();
        HqlAndParametersBuilder<E> result = getHqlForProperties(properties);
        return result;
    }

    protected HqlAndParametersBuilder<E> getHqlForProperties(Map<String, Object> properties) {
        HqlAndParametersBuilder<E> result = newHqlAndParametersBuilder();
        for (Map.Entry<String, Object> property : properties.entrySet()) {
            result.addEquals(property.getKey(), property.getValue());
        }
        return result;
    }

    protected InnerTopiaQueryBuilderRunQueryStep<E> forHql(String hql) {
        Map<String, Object> properties = Collections.emptyMap();
        InnerTopiaQueryBuilderRunQueryStep<E> result = forHql(hql, properties);
        return result;
    }

    protected InnerTopiaQueryBuilderRunQueryStep<E> forHql(String hql, Map<String, Object> hqlParameters) {
        //FIXME tchemit-2016-05-01 Should we scan in hql code if there is an orderBy ?
        boolean withOrderByClause = false;
        InnerTopiaQueryBuilderRunQueryStep<E> result = new InnerTopiaQueryBuilderRunQueryStep<E>(this, true, withOrderByClause, hql, hqlParameters);
        return result;
    }

    protected InnerTopiaQueryBuilderRunQueryStep<E> forHql(String hql, String parameterName,
                                                           Object parameterValue,
                                                           Object... otherParameterNamesAndValues) {
        Map<String, Object> hqlParameters =
                TopiaUtil.convertPropertiesArrayToMap(parameterName, parameterValue, otherParameterNamesAndValues);
        InnerTopiaQueryBuilderRunQueryStep<E> result = forHql(hql, hqlParameters);
        return result;
    }

    @Override
    public InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forAll() {
        InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = newQueryBuilder();
        return result;
    }

    @Override
    public InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forProperties(Map<String, Object> properties) {
        HqlAndParametersBuilder<E> hqlAndParametersBuilder = getHqlForProperties(properties);
        InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = new InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E>(this, hqlAndParametersBuilder);
        return result;
    }

    @Override
    public InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forProperties(String propertyName,
                                                                            Object propertyValue,
                                                                            Object... otherPropertyNamesAndValues) {
        HqlAndParametersBuilder<E> hqlAndParametersBuilder = getHqlForProperties(propertyName, propertyValue, otherPropertyNamesAndValues);
        InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = new InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E>(this, hqlAndParametersBuilder);
        return result;
    }

    @Override
    public InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> newQueryBuilder() {
        return newQueryBuilder(FilterRuleGroupOperator.AND);
    }

    @Override
    public InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> newQueryBuilder(FilterRuleGroupOperator filterRuleGroupOperator) {
        HqlAndParametersBuilder<E> hqlAndParametersBuilder = newHqlAndParametersBuilder(filterRuleGroupOperator);
        InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = new InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E>(this, hqlAndParametersBuilder);
        return result;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forContains(String propertyName, Object propertyValue) {
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = newQueryBuilder().addContains(propertyName, propertyValue);
        return result;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forEquals(String propertyName, Object propertyValue) {
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = newQueryBuilder().addEquals(propertyName, propertyValue);
        return result;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forIn(String propertyName, Collection<?> propertyValues) {
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = newQueryBuilder().addIn(propertyName, propertyValues);
        return result;
    }

    protected boolean exists(String hql, Map<String, Object> hqlParameters) {
        // TODO AThimel 12/09/14 Avoid loading entity, just count the results
        E entity = topiaJpaSupport.findAny(hql, hqlParameters);
        boolean result = entity != null;
        return result;
    }

    protected long count(String hql, Map<String, Object> hqlParameters) {
        Preconditions.checkArgument(hql.toLowerCase().trim().startsWith("select count("), "Your HQL query must start with \"select count(\"");
        Long result = findUnique(hql, hqlParameters);
        return result;
    }

    protected <O> O findUnique(String hql, Map<String, Object> hqlParameters) throws TopiaNoResultException, TopiaNonUniqueResultException {
        O result = findUniqueOrNull(hql, hqlParameters);
        if (result == null) {
            throw new TopiaNoResultException(hql, hqlParameters);
        }
        return result;
    }

    protected <O> Optional<O> tryFindUnique(String hql, Map<String, Object> hqlParameters) throws TopiaNonUniqueResultException {
        O uniqueOrNull = findUniqueOrNull(hql, hqlParameters);
        Optional<O> result = Optional.fromNullable(uniqueOrNull);
        return result;
    }

    protected <O> O findUniqueOrNull(String hql, Map<String, Object> hqlParameters) throws TopiaNonUniqueResultException {
        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);
        O result = topiaJpaSupport.findUnique(hql, hqlParameters);
        return result;
    }

    protected <O> O findFirst(String hql, Map<String, Object> hqlParameters) throws QueryMissingOrderException {
        O result = findFirstOrNull(hql, hqlParameters);
        if (result == null) {
            throw new TopiaNoResultException(hql, hqlParameters);
        }
        return result;
    }

    protected <O> Optional<O> tryFindFirst(String hql, Map<String, Object> hqlParameters) throws QueryMissingOrderException {
        O firstOrNull = findFirstOrNull(hql, hqlParameters);
        Optional<O> result = Optional.fromNullable(firstOrNull);
        return result;
    }

    protected <O> O findFirstOrNull(String hql, Map<String, Object> hqlParameters) throws QueryMissingOrderException {
        if (!hqlContainsOrderBy(hql)) {
            throw new QueryMissingOrderException(hql, hqlParameters);
        }
        O result = findAnyOrNull(hql, hqlParameters);
        return result;
    }

    protected <O> O findAny(String hql, Map<String, Object> hqlParameters) throws TopiaNoResultException {
        O result = findAnyOrNull(hql, hqlParameters);
        if (result == null) {
            throw new TopiaNoResultException(hql, hqlParameters);
        }
        return result;
    }

    protected <O> Optional<O> tryFindAny(String hql, Map<String, Object> hqlParameters) {
        O anyOrNull = findAnyOrNull(hql, hqlParameters);
        Optional<O> result = Optional.fromNullable(anyOrNull);
        return result;
    }

    protected <O> O findAnyOrNull(String hql) {
        Preconditions.checkNotNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        O result = findAnyOrNull(hql, hqlParameters);
        return result;
    }

    protected <O> O findAnyOrNull(String hql, Map<String, Object> hqlParameters) {
        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);
        O result = topiaJpaSupport.findAny(hql, hqlParameters);
        return result;
    }

    protected <O> List<O> findAll(String hql) {
        Preconditions.checkNotNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        List<O> result = findAll(hql, hqlParameters);
        return result;
    }

    protected <O> List<O> findAll(String hql, Map<String, Object> hqlParameters) {
        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);
        List<O> result = topiaJpaSupport.findAll(hql, hqlParameters);
        return result;
    }

    protected <O> Stream<O> stream(String hql) {
        Preconditions.checkNotNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        Stream<O> result = stream(hql, hqlParameters);
        return result;
    }

    protected <O> Stream<O> stream(String hql, Map<String, Object> hqlParameters) {
        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);
        Stream<O> result = topiaJpaSupport.stream(hql, hqlParameters);
        return result;
    }

    protected <O> List<O> find(String hql, int startIndex, int endIndex) {
        Preconditions.checkNotNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        List<O> result = find(hql, hqlParameters, startIndex, endIndex);
        return result;
    }

    protected <O> List<O> find(String hql, Map<String, Object> hqlParameters, int startIndex, int endIndex) {
        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);
        List<O> result = topiaJpaSupport.find(hql, startIndex, endIndex, hqlParameters);
        return result;
    }

    protected <O> List<O> find(String hql, Map<String, Object> hqlParameters, PaginationParameter page) {
        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);
        Preconditions.checkNotNull(page);

        boolean hqlContainsOrderClause = hqlContainsOrderBy(hql);
        boolean pageContainsOrderClause = !page.getOrderClauses().isEmpty();

        if (!hqlContainsOrderClause && !pageContainsOrderClause) {
            throw new QueryMissingOrderException(hql, hqlParameters, page);
        }

        // Must have one (and only one) order by clause in query
        Preconditions.checkArgument(
                hqlContainsOrderClause ^ pageContainsOrderClause,
                String.format(
                        "One 'order by' clause (and only one) must be specified. [orderByInHql=%b] [orderByInPage=%b]",
                        hqlContainsOrderClause,
                        pageContainsOrderClause)
        );

        if (pageContainsOrderClause) {

            hql += " ORDER BY ";
            Iterable<String> orderClauses = Iterables.transform(page.getOrderClauses(), PAGINATION_ORDER_TO_HQL);
            hql += Joiner.on(", ").join(orderClauses);
        }

        List<O> result = topiaJpaSupport.find(
                hql,
                page.getStartIndex(),
                page.getEndIndex(),
                hqlParameters);

        return result;
    }

    protected <O> PaginationResult<O> findPage(String hql, Map<String, Object> hqlParameters, PaginationParameter page) {
        List<O> elements = find(hql, hqlParameters, page);

        String countHql = "select count(topiaId) ";
        if (hqlStartsWithSelect(hql)) {
            // must remove the from clause, otherwise some sql queries won't work.
            countHql += hql.substring(hql.toLowerCase().indexOf(" from "));
        } else {
            countHql += hql;
        }

        if (hqlContainsOrderBy(countHql)) {
            // must remove the order by clause, otherwise some sql queries won't work.
            countHql = countHql.substring(0, countHql.toLowerCase().indexOf("order by"));
        }

        long count = count(countHql, hqlParameters);
        PaginationResult<O> result = PaginationResult.of(elements, count, page);
        return result;
    }

    protected <O> Iterable<O> findAllLazy(String hql) {
        Map<String, Object> hqlParameters = Collections.emptyMap();
        Iterable<O> result = findAllLazy(hql, hqlParameters);
        return result;
    }

    protected <O> Iterable<O> findAllLazy(String hql, Map<String, Object> hqlParameters) {

        Iterable<O> result = findAllLazy(hql, hqlParameters, batchSize);
        return result;
    }

    protected <O> Iterable<O> findAllLazy(String hql, int batchSize) {
        Map<String, Object> hqlParameters = Collections.emptyMap();
        Iterable<O> result = findAllLazy(hql, hqlParameters, batchSize);
        return result;
    }

    protected <O> Iterable<O> findAllLazy(String hql, Map<String, Object> hqlParameters, int batchSize) {

        Preconditions.checkNotNull(hql);
        Preconditions.checkNotNull(hqlParameters);

        final Iterator<O> iterator = new FindAllIterator<E, O>(this,
                                                               batchSize,
                                                               hql,
                                                               hqlParameters);

        Iterable<O> result = new Iterable<O>() {

            @Override
            public Iterator<O> iterator() {
                return iterator;
            }
        };

        return result;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forTopiaIdEquals(String topiaId) {
        Preconditions.checkArgument(StringUtils.isNotBlank(topiaId), "given topiaId is blank");
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = forEquals(TopiaEntity.PROPERTY_TOPIA_ID, topiaId);
        return result;
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forTopiaIdIn(Collection<String> topiaIds) {
        Preconditions.checkNotNull(topiaIds, "given topiaIds is null");
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> result = forIn(TopiaEntity.PROPERTY_TOPIA_ID, (Collection) topiaIds);
        return result;
    }

    @Override
    @Deprecated
    public E findByTopiaId(String topiaId) throws TopiaNoResultException {
        // AThimel 30/10/13 Not using findUnique to avoid querying several elements (cf. findUnique implementation)
        try {
            E result = forTopiaIdEquals(topiaId).findAny();
            return result;
        } catch (TopiaNoResultException tnre) {
            if (log.isWarnEnabled()) {
                String message = String.format("Unexpected behavior : entity '%s' not found with topiaId='%s'",
                                               getEntityClass().getName(), topiaId);
                log.warn(message);
            }
            throw tnre;
        }
    }

    @Override
    @Deprecated
    public Optional<E> tryFindByTopiaId(String topiaId) {
        Optional<E> result = forTopiaIdEquals(topiaId).tryFindAny();
        return result;
    }

    @Override
    public List<String> findAllIds() {
        List<String> result = newQueryBuilder().findAllIds();
        return result;
    }

    @Override
    public List<E> findAll() {
        List<E> result = newQueryBuilder().findAll();
        return result;
    }

    @Override
    public Stream<E> streamAll() {
        Stream<E> result = newQueryBuilder().stream();
        return result;
    }

    @Override
    public Iterable<E> findAllLazy() {
        String hql = " FROM " + getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id ";
        Map<String, Object> hqlParameters = Collections.emptyMap();
        Iterable<E> result = findAllLazy(hql, hqlParameters);
        return result;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E> result = findAllLazy().iterator();
        return result;
    }

    @Override
    public long count() {
        long result = newQueryBuilder().count();
        return result;
    }

    protected boolean hqlContainsOrderBy(String hql) {
        return hql.toLowerCase().contains("order by");
    }

    protected boolean hqlStartsWithSelect(String hql) {
        return hql.toLowerCase().trim().startsWith("select ");
    }

    protected boolean hqlContainsCount(String hql) {
        return hql.toLowerCase().contains("count(");
    }

//    /**
//     * package locale method because this is hibernate specific method and
//     * we don't want expose it.
//     *
//     * @return the meta-data of the entity
//     * @throws org.nuiton.topia.persistence.TopiaException if any pb
//     */
//    protected ClassMetadata getClassMetadata() {
//        ClassMetadata meta = topiaHibernateSupport.getHibernateFactory().getClassMetadata(getEntityClass());
//        if (meta == null) {
//            String implementationFQN = getTopiaEntityEnum().getImplementationFQN();
//            meta = topiaHibernateSupport.getHibernateFactory().getClassMetadata(implementationFQN);
//        }
//        return meta;
//    }

    public static class FindAllIterator<E extends TopiaEntity, O> implements Iterator<O> {

        protected Iterator<O> data;

        protected final AbstractTopiaDao<E> dao;

        protected final String hql;

        protected final Map<String, Object> params;

        protected PaginationResult<E> pager;

        protected boolean firstPageLoaded;

        public FindAllIterator(AbstractTopiaDao<E> dao,
                               int batchSize,
                               String hql,
                               Map<String, Object> params) {

            if (!dao.hqlContainsOrderBy(hql)) {
                throw new QueryMissingOrderException(hql, params);
            }

            this.dao = dao;
            this.hql = hql;
            this.params = params;
            pager = dao.initPagination(hql, params, batchSize);

            // empty iterator (will be changed at first next call)
            data = Collections.emptyIterator();
            firstPageLoaded = false;
        }

        public boolean hasNext() {
            boolean result = data.hasNext() || // no more data
                             (!firstPageLoaded && pager.getCount() > 0) || // first page not yet loaded and there is data to load
                             (firstPageLoaded && pager.hasNextPage()); // the first page has been loaded and there is another page
            return result;
        }

        public O next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            if (!data.hasNext()) {

                // must load iterator

                PaginationParameter pageToLoad;
                if (firstPageLoaded) {
                    pageToLoad = pager.getNextPage();
                } else {
                    pageToLoad = pager.getCurrentPage();
                    firstPageLoaded = true;
                }

                // load new window of data
                List<O> values = dao.find(hql, params, pageToLoad);
                data = values.iterator();

                // Create a new pager on the next page
                pager = PaginationResult.of(pager.getElements(), pager.getCount(), pageToLoad);

            }

            O next = data.next();
            return next;
        }


        public void remove() {
            throw new UnsupportedOperationException(
                    "This iterator does not support remove operation.");
        }
    }

    public static class InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<E extends TopiaEntity> implements TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> {

        protected AbstractTopiaDao<E> topiaDao;

        protected HqlAndParametersBuilder<E> hqlAndParametersBuilder;

        protected InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep(AbstractTopiaDao<E> topiaDao, HqlAndParametersBuilder<E> hqlAndParametersBuilder) {
            this.topiaDao = topiaDao;
            this.hqlAndParametersBuilder = hqlAndParametersBuilder;
        }

        @Override
        public InnerTopiaQueryBuilderRunQueryStep<E> setOrderByArguments(LinkedHashSet<String> orderByArguments) {
            hqlAndParametersBuilder.setOrderByArguments(orderByArguments);
            return getNextStep();
        }

        @Override
        public InnerTopiaQueryBuilderRunQueryStep<E> setOrderByArguments(String... orderByArguments) {
            hqlAndParametersBuilder.setOrderByArguments(orderByArguments);
            return getNextStep();
        }

        @Override
        public TopiaQueryBuilderRunQueryStep<E> setOrderByArguments(Collection<PaginationOrder> paginationOrders) {
            hqlAndParametersBuilder.setOrderByArguments(paginationOrders);
            return getNextStep();
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addEquals(String property, Object value) {
            hqlAndParametersBuilder.addEquals(property, value);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotEquals(String property, Object value) {
            hqlAndParametersBuilder.addNotEquals(property, value);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addIn(String property, Collection<?> values) {
            hqlAndParametersBuilder.addIn(property, values);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotIn(String property, Collection<?> values) {
            hqlAndParametersBuilder.addNotIn(property, values);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addContains(String property, Object value) {
            hqlAndParametersBuilder.addContains(property, value);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotContains(String property, Object value) {
            hqlAndParametersBuilder.addNotContains(property, value);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNull(String property) {
            hqlAndParametersBuilder.addNull(property);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addNotNull(String property) {
            hqlAndParametersBuilder.addNotNull(property);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdEquals(String property, String topiaId) {
            hqlAndParametersBuilder.addTopiaIdEquals(property, topiaId);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdIn(String property, Collection<String> topiaIds) {
            hqlAndParametersBuilder.addTopiaIdIn(property, topiaIds);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdNotEquals(String property, String topiaId) {
            hqlAndParametersBuilder.addTopiaIdNotEquals(property, topiaId);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addTopiaIdNotIn(String property, Collection<String> topiaIds) {
            hqlAndParametersBuilder.addTopiaIdNotIn(property, topiaIds);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addFetch(String property) {
            hqlAndParametersBuilder.addFetch(property);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addAllFetches(String property, String... otherProperties) {
            hqlAndParametersBuilder.addAllFetches(property, otherProperties);
            return this;
        }

        @Override
        public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> addAllFetches(Collection<String> properties) {
            hqlAndParametersBuilder.addAllFetches(properties);
            return this;
        }

        // shortcuts to next step

        @Override
        public boolean exists() {
            return getNextStep().exists();
        }

        @Override
        public E findAnyOrNull() {
            return getNextStep().findAnyOrNull();
        }

        @Override
        public E findUniqueOrNull() {
            return getNextStep().findUniqueOrNull();
        }

        @Override
        public E findAny() {
            return getNextStep().findAny();
        }

        @Override
        public E findUnique() {
            return getNextStep().findUnique();
        }

        @Override
        public E findFirst() {
            return getNextStep().findFirst();
        }

        @Override
        public E findFirstOrNull() {
            return getNextStep().findFirstOrNull();
        }

        @Override
        public Optional<E> tryFindAny() {
            return getNextStep().tryFindAny();
        }

        @Override
        public Optional<E> tryFindFirst() {
            return getNextStep().tryFindFirst();
        }

        @Override
        public Optional<E> tryFindUnique() {
            return getNextStep().tryFindUnique();
        }

        @Override
        public List<E> findAll() {
            return getNextStep().findAll();
        }

        @Override
        public Stream<E> stream() {
            return getNextStep().stream();
        }

        @Override
        public List<E> find(int startIndex, int endIndex) {
            return getNextStep().find(startIndex, endIndex);
        }

        @Override
        public List<E> find(PaginationParameter page) {
            return getNextStep().find(page);
        }

        @Override
        public PaginationResult<E> findPage(PaginationParameter page) {
            return getNextStep().findPage(page);
        }

        @Override
        public Iterable<E> findAllLazy() {
            return getNextStep().findAllLazy();
        }

        @Override
        public Iterable<E> findAllLazy(int batchSize) {
            return getNextStep().findAllLazy(batchSize);
        }

        @Override
        public long count() {
            return getNextStep().count();
        }

        @Override
        public List<String> findIds(int startIndex, int endIndex) {
            return getNextStep().findIds(startIndex, endIndex);
        }

        @Override
        public List<String> findIds(PaginationParameter page) {
            return getNextStep().findIds(page);
        }

        @Override
        public PaginationResult<String> findIdsPage(PaginationParameter page) {
            return getNextStep().findIdsPage(page);
        }

        @Override
        public List<String> findAllIds() {
            return getNextStep().findAllIds();
        }

        protected InnerTopiaQueryBuilderRunQueryStep<E> getNextStep() {
            String hql = hqlAndParametersBuilder.getHql();
            Map<String, Object> hqlParameters = hqlAndParametersBuilder.getHqlParameters();
            boolean withOrderByClause  = hqlAndParametersBuilder.isOrderByClausePresent();
            InnerTopiaQueryBuilderRunQueryStep<E> nextStep;
            if (hqlAndParametersBuilder.hasFetchProperties()) {
                String hqlForFetchStep1 = hqlAndParametersBuilder.getHqlForFetchStep1();
                String hqlForFetchStep2 = hqlAndParametersBuilder.getHqlForFetchStep2();
                nextStep = new InnerTopiaQueryBuilderRunQueryStep<E>(topiaDao, false, withOrderByClause, hql, hqlParameters, hqlForFetchStep1, hqlForFetchStep2);
            } else {

                nextStep = new InnerTopiaQueryBuilderRunQueryStep<E>(topiaDao, false, withOrderByClause, hql, hqlParameters);
            }
            return nextStep;
        }

    }

    public static class InnerTopiaQueryBuilderRunQueryStep<E extends TopiaEntity> implements TopiaQueryBuilderRunQueryStep<E> {

        protected final String hql;

        protected final Map<String, Object> hqlParameters;

        protected final AbstractTopiaDao<E> topiaDao;

        protected final boolean fromHql;

        protected final boolean withOrderByClause;

        protected final String hqlForFetchStep1;

        protected final String hqlForFetchStep2;

        protected InnerTopiaQueryBuilderRunQueryStep(AbstractTopiaDao<E> topiaDao,
                                                     boolean fromHql,
                                                     boolean withOrderByClause,
                                                     String hql,
                                                     Map<String, Object> hqlParameters) {
            this(topiaDao, fromHql, withOrderByClause, hql, hqlParameters, null, null);
        }

        protected InnerTopiaQueryBuilderRunQueryStep(AbstractTopiaDao<E> topiaDao,
                                                     boolean fromHql,
                                                     boolean withOrderByClause,
                                                     String hql,
                                                     Map<String, Object> hqlParameters,
                                                     String hqlForFetchStep1,
                                                     String hqlForFetchStep2) {
            this.fromHql = fromHql;
            this.withOrderByClause = withOrderByClause;
            this.hql = hql;
            this.hqlParameters = hqlParameters;
            this.topiaDao = topiaDao;
            this.hqlForFetchStep1 = hqlForFetchStep1;
            this.hqlForFetchStep2 = hqlForFetchStep2;
        }

        @Override
        public boolean exists() {
            return topiaDao.exists(hql, hqlParameters);
        }

        @Override
        public long count() {
            String hqlWithSelectClause = "select count(topiaId) " + hql;
            return topiaDao.count(hqlWithSelectClause, hqlParameters);
        }

        @Override
        public E findUnique() throws TopiaNoResultException, TopiaNonUniqueResultException {
            return topiaDao.findUnique(hql, hqlParameters);
        }

        @Override
        public E findUniqueOrNull() throws TopiaNonUniqueResultException {
            return topiaDao.findUniqueOrNull(hql, hqlParameters);
        }

        @Override
        public Optional<E> tryFindUnique() throws TopiaNonUniqueResultException {
            return topiaDao.tryFindUnique(hql, hqlParameters);
        }

        @Override
        public E findFirst() throws QueryMissingOrderException, TopiaNoResultException {
            return topiaDao.findFirst(hql, hqlParameters);
        }

        @Override
        public E findFirstOrNull() throws QueryMissingOrderException {
            return topiaDao.findFirstOrNull(hql, hqlParameters);
        }

        @Override
        public Optional<E> tryFindFirst() throws QueryMissingOrderException {
            return topiaDao.tryFindFirst(hql, hqlParameters);
        }

        @Override
        public E findAny() throws TopiaNoResultException {
            return topiaDao.findAny(hql, hqlParameters);
        }

        @Override
        public E findAnyOrNull() {
            return topiaDao.findAnyOrNull(hql, hqlParameters);
        }

        @Override
        public Optional<E> tryFindAny() {
            return topiaDao.tryFindAny(hql, hqlParameters);
        }

        @Override
        public List<E> findAll() {
            return topiaDao.findAll(hql, hqlParameters);
        }

        @Override
        public Stream<E> stream() {
            return topiaDao.stream(hql, hqlParameters);
        }

        @Override
        public Iterable<E> findAllLazy() {
            return topiaDao.findAllLazy(hql + (fromHql || withOrderByClause ? "" : " ORDER BY id "), hqlParameters);
        }

        @Override
        public Iterable<E> findAllLazy(int batchSize) {
            return topiaDao.findAllLazy(hql + (fromHql || withOrderByClause ? "" : " ORDER BY id "), hqlParameters, batchSize);
        }

        @Override
        public List<E> find(int startIndex, int endIndex) {
            List<E> result;
            if (!Strings.isNullOrEmpty(hqlForFetchStep1) && !Strings.isNullOrEmpty(hqlForFetchStep2)) {
                List<String> step1ResultTopiaIds = topiaDao.find(hqlForFetchStep1, hqlParameters, startIndex, endIndex);

                if (CollectionUtils.isEmpty(step1ResultTopiaIds)) {
                    result = Lists.newArrayList();
                } else {
                    Map<String, Object> step2Args = Maps.newHashMap();
                    step2Args.put("topiaIdsForFetch_", step1ResultTopiaIds);
                    List<E> entities = topiaDao.forHql(hqlForFetchStep2, step2Args).findAll();

                    result = sortAccordingToIds(entities, step1ResultTopiaIds);
                }
            } else {
                result = topiaDao.find(hql, hqlParameters, startIndex, endIndex);
            }
            return result;
        }

        @Override
        public List<E> find(PaginationParameter page) {
            return topiaDao.find(hql, hqlParameters, page);
        }

        @Override
        public PaginationResult<E> findPage(PaginationParameter page) {
            PaginationResult<E> result;
            if (!Strings.isNullOrEmpty(hqlForFetchStep1) && !Strings.isNullOrEmpty(hqlForFetchStep2)) {
                PaginationResult<String> pageResult = topiaDao.findPage(hqlForFetchStep1, hqlParameters, page);
                List<String> step1ResultTopiaIds = pageResult.getElements();

                List<E> sortedEntities;
                if (CollectionUtils.isEmpty(step1ResultTopiaIds)) {
                    sortedEntities = Lists.newArrayList();
                } else {
                    Map<String, Object> step2Args = Maps.newHashMap();
                    step2Args.put("topiaIdsForFetch_", step1ResultTopiaIds);
                    List<E> entities = topiaDao.forHql(hqlForFetchStep2, step2Args).findAll();

                    sortedEntities = sortAccordingToIds(entities, step1ResultTopiaIds);
                }

                result = PaginationResult.of(sortedEntities, pageResult.getCount(), pageResult.getCurrentPage());
            } else {
                result = topiaDao.findPage(hql, hqlParameters, page);
            }
            return result;
        }

        /**
         * This method can be used when it is not possible to sort entities in SQL/HQL. The list of entities
         * will be sorted according to the given list of topiaId.
         *
         * @param entities the list en entities (unsorted)
         * @param idsList  the list of ids (sorted)
         * @param <O>      must be a TopiaEntity
         * @return FIXME
         */
        protected <O extends TopiaEntity> List<O> sortAccordingToIds(List<O> entities, final List<String> idsList) {

            // Cannot sort on second query, will sort according to the first result list
            final Map<String, O> entitiesIndex = Maps.uniqueIndex(entities, TopiaEntities.getTopiaIdFunction());
            Iterable<O> transformed = Iterables.transform(idsList, new Function<String, O>() {

                @Override
                public O apply(String input) {
                    return entitiesIndex.get(input);
                }
            });

            List<O> result = Lists.newArrayList(transformed);
            return result;
        }

        @Override
        public List<String> findAllIds() {
            String hqlWithSelectClause = "select topiaId " + hql;
            return topiaDao.findAll(hqlWithSelectClause, hqlParameters);
        }

        @Override
        public List<String> findIds(int startIndex, int endIndex) {
            String hqlWithSelectClause = "select topiaId " + hql;
            return topiaDao.find(hqlWithSelectClause, hqlParameters, startIndex, endIndex);
        }

        @Override
        public List<String> findIds(PaginationParameter page) {
            String hqlWithSelectClause = "select topiaId " + hql;
            return topiaDao.find(hqlWithSelectClause, hqlParameters, page);
        }

        @Override
        public PaginationResult<String> findIdsPage(PaginationParameter page) {
            List<String> elements = findIds(page);
            long count = count();
            PaginationResult<String> result = PaginationResult.of(elements, count, page);
            return result;
        }

    }

}
