package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * @author Arnaud Thimel (Code Lutin)
 * @deprecated use {@link org.nuiton.topia.persistence.jdbc.JdbcH2Helper} out of the hibernate session
 */
@Deprecated
public class TopiaH2Util {

    protected TopiaFiresSupport firesSupport;
    protected TopiaSqlSupport sqlSupport;

    public TopiaH2Util(TopiaSqlSupport context, TopiaFiresSupport firesSupport) {
        this.sqlSupport = context;
        this.firesSupport = firesSupport;
    }

    /**
     * Backup database in gzip compressed file.
     *
     * <b>Note: </b> Only works for h2 database.
     *
     * @param file     file to write backup
     * @param compress if true then use gzip to compress file
     */
    public void backup(File file, boolean compress) throws TopiaException {
//        context.checkClosed("backup");

        String options = "";
        if (compress) {
            options += " COMPRESSION GZIP";
        }

        sqlSupport.executeSql("SCRIPT TO '" + file.getAbsolutePath() + "'" + options);
    }

    /**
     * Read database from gzip compressed file
     *
     * Only work for h2 database
     *
     * @param file     the source file to use for restore
     */
    public void restore(File file) throws TopiaException {

        String sql = null;
        String options = "";
        try {
            // decompresse file in temporary file
            InputStream in = new BufferedInputStream(new FileInputStream(file));
            try {
                in.mark(2);

                // read header to see if is compressed file
                int b = in.read();
                // redundant cast : int magic = ((int) in.read() << 8) | b;
                int magic = in.read() << 8 | b;
                in.reset();

                if (magic == GZIPInputStream.GZIP_MAGIC) {
                    options += " COMPRESSION GZIP";
                }
            } finally {

                in.close();
            }

            sqlSupport.executeSql(
                    "RUNSCRIPT FROM '" + file.getAbsolutePath() + "'" + options);

        } catch (Exception eee) {
            throw new TopiaH2UtilException(
                    "Unable to restore from file : " + eee.getMessage() + ". " +
                            "SQL is " + sql, eee);
        }
    }

//    /**
//     * Only h2 supported for now
//     *
//     * @see org.nuiton.topia.TopiaContext#clear(boolean)
//     */
//    public void clear(boolean dropDatabase) throws TopiaException {
//        try {
//            TopiaContextImpl root = (TopiaContextImpl) context.getRootContext();
//            TopiaContextImpl tx = (TopiaContextImpl) root.beginTransaction();
//
//            String sql = "DROP ALL OBJECTS";
//            if (dropDatabase) {
//                sql += " DELETE FILES";
//            }
//            Query query = tx.getEntityManager().createNativeQuery(sql);
//            query.executeUpdate();
//            tx.close();
//            root.finalize();
//        } catch (Throwable eee) {
//            throw new TopiaH2UtilException(
//                    "Unable to perform clear operation : " + eee.getMessage(), eee);
//        }
//    }

    /**
     * Exception only used within TopiaH2Util
     */
    private static class TopiaH2UtilException extends TopiaException {

        private static final long serialVersionUID = -7874205136201318340L;

        public TopiaH2UtilException(String message, Throwable cause) {
            super(message, cause);
        }

    }
}
