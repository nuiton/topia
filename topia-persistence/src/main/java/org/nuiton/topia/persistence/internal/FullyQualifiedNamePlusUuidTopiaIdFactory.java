package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.util.UUID;

/**
 * Default implementation of {@link TopiaIdFactory}. Generates a FQN followed by a random UUID.
 *
 * @author Brendan Le Ny - bleny@codelutin.com
 * @since 3.0
 */
public class FullyQualifiedNamePlusUuidTopiaIdFactory implements TopiaIdFactory {

    private static final long serialVersionUID = 1;

    private static final Log log = LogFactory.getLog(FullyQualifiedNamePlusUuidTopiaIdFactory.class);

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity) {
        String randomPart = UUID.randomUUID().toString();
        String result = newTopiaId(entityClass, randomPart);
        return result;
    }

    @Override
    public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, String randomPart) {
        if (!entityClass.isInterface()) {
            throw new IllegalArgumentException(
                    "Only interface is permit to create id: " + entityClass);
        }
        String className = entityClass.getName();
        Preconditions.checkArgument(!className.contains(getSeparator()), "Your entity class name must not contains the separator");
        String result = className + getSeparator() + randomPart;
        return result;
    }

    @Override
    public <E extends TopiaEntity> Class<E> getClassName(String topiaId) {
        String className = StringUtils.substringBefore(topiaId, getSeparator());
        try {
            Class<E> entityClass = (Class<E>) Class.forName(className);
            return entityClass;
        } catch (ClassNotFoundException e) {
            throw new TopiaException("can't find class for topiaId = '" + topiaId + "'", e);
        }
    }

    @Override
    public String getSeparator() {
        return "_";
    }

    @Override
    public String getRandomPart(String topiaId) {
        return StringUtils.substringAfter(topiaId, getSeparator());
    }

    @Override
    public boolean isTopiaId(String str) {
        boolean isTopiaId = false;
        if (str != null) {
            String[] split = str.split(getSeparator());
            if (split.length == 2) {
                String className = split[0];
                try {
                    Class.forName(className);
                    isTopiaId = StringUtils.isNotBlank(split[1]);
                } catch (ClassNotFoundException eee) {
                    // nothing to do, just return false
                    if (log.isDebugEnabled()) {
                        log.debug(eee);
                    }
                }
            }
        }
        return isTopiaId;
    }

}
