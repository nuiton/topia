package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.EventObject;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Event fires for {@link org.nuiton.topia.persistence.event.TopiaTransactionListener}.
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @see org.nuiton.topia.persistence.event.TopiaTransactionListener
 */
public class TopiaTransactionEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    private Map<TopiaEntity, EntityState> entities = new IdentityHashMap<TopiaEntity, EntityState>();

    public TopiaTransactionEvent(TopiaPersistenceContext source) {
        super(source);
    }

    public TopiaTransactionEvent(TopiaPersistenceContext source,
                                 Map<TopiaEntity, EntityState> entities) {
        this(source);
        this.entities.putAll(entities);
    }

    public Set<TopiaEntity> getEntities() {
        return entities.keySet();
    }

    public boolean isLoaded(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isLoaded();
    }

    public boolean isRead(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isRead();
    }

    public boolean isCreated(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isCreated();
    }

    public boolean isWritten(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isWritten();
    }

    public boolean isUpdated(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isUpdated();
    }

    public boolean isDeleted(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null && state.isDeleted();
    }

    public boolean isModification(TopiaEntity entity) {
        EntityState state = entities.get(entity);
        return state != null
               && (state.isCreated() || state.isUpdated() || state.isDeleted());
    }

    @Override
    public TopiaPersistenceContext getSource() {
        return (TopiaPersistenceContext) super.getSource();
    }

    @Override
    protected void finalize() throws Throwable {
        if (entities != null) {
            entities.clear();
            entities = null;
        }
        super.finalize();
    }
}
