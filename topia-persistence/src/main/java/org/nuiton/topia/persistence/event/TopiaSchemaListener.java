package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.EventListener;

/**
 * Listener for TopiaContext actions.
 *
 * Listener are notified for action such as :
 * <ul>
 * <li>createSchema</li>
 * <li>updateSchema</li>
 * <li>...</li>
 * </ul>
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaSchemaListener extends EventListener {

    /**
     * Called before createSchema call
     *
     * @param event event
     */
    void preCreateSchema(TopiaContextEvent event);

    /**
     * Called after createSchema call
     *
     * @param event event
     */
    void postCreateSchema(TopiaContextEvent event);

    /**
     * Called before updateSchema call
     *
     * @param event event
     */
    void preUpdateSchema(TopiaContextEvent event);

    /**
     * Called after updateSchema call
     *
     * @param event event
     */
    void postUpdateSchema(TopiaContextEvent event);

    /**
     * Called before dropSchema call
     *
     * @param event event
     * @since 3.0
     */
    void preDropSchema(TopiaContextEvent event);

    /**
     * Called after dropSchema call
     *
     * @param event event
     * @since 3.0
     */
    void postDropSchema(TopiaContextEvent event);

}
