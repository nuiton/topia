package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Use this contract on a object which use a {@code TopiaContext} as a
 * transaction.
 *
 * The method {@link #getTransaction()} returns the internal transaction used.
 *
 * the method {@link #setTransaction(TopiaTransaction)} put the internal
 * transaction.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.1
 * @deprecated This is probably not useful anymore.
 */
public interface TopiaTransactionAware {

    /**
     * Obtains the internal transaction.
     *
     * If no transaction was opened, can return the {@code null} object.
     *
     * @return the current transaction (can be null or closed...).
     */
    TopiaTransaction getTransaction();

    /**
     * Put in the instance, the given transaction.
     *
     * @param transaction the transaction to push
     */
    void setTransaction(TopiaTransaction transaction);
}
