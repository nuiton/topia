package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaService;
import org.nuiton.topia.persistence.support.TopiaServiceSupport;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This is the default {@link org.nuiton.topia.persistence.support.TopiaServiceSupport} implementation.
 *
 * It will look in {@link org.nuiton.topia.persistence.TopiaApplicationContext#getConfiguration()}
 * to find services declarations and associated configuration. All of those properties match
 * the {@code topia.service.*} prefix.
 *
 * A service is declared by giving it a name and the implementation class FQN by following the
 * pattern.
 *
 * <pre>
 * topia.service.myServiceName=com.my.company.my.app.MyServiceImpl
 * </pre>
 *
 * Here, {@code myServiceName} is the name of the service, you can choose any String. Value
 * must be the FQN of class that provide an empty constructor and implements
 * {@link org.nuiton.topia.persistence.TopiaService}.
 *
 * To passes configuration to a service, you can do as follow:
 *
 * <pre>
 * topia.service.myServiceName=com.my.company.my.app.MyServiceImpl
 * topia.service.myServiceName.myConfigurationDirective=myConfigurationValue
 * </pre>
 *
 * When {@link org.nuiton.topia.persistence.TopiaService#initTopiaService(org.nuiton.topia.persistence.TopiaApplicationContext, java.util.Map)} will be
 * called, the given map will contain a single entry which key is "myConfigurationDirective" and value
 * is "myConfigurationValue".
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaServiceSupportImpl implements TopiaServiceSupport {

    private static final Log log = LogFactory.getLog(TopiaServiceSupportImpl.class);

    protected ImmutableMap<String, TopiaService> services;

    public void initServices(TopiaApplicationContext topiaApplicationContext) {

        TopiaConfiguration topiaConfiguration =
                topiaApplicationContext.getConfiguration();

        Map<String, Class<? extends TopiaService>> declaredServices =
                topiaConfiguration.getDeclaredServices();

        // the services: name → instance
        Map<String, TopiaService> services = new HashMap<String, TopiaService>();

        // instantiate all services
        for (Map.Entry<String, Class<? extends TopiaService>> entry : declaredServices.entrySet()) {
            String serviceName = entry.getKey();
            Class<? extends TopiaService> serviceClass = entry.getValue();
            try {
                Object newInstance = serviceClass.getConstructor().newInstance();
                TopiaService service1 = (TopiaService) newInstance;
                services.put(serviceName, service1);
                if (log.isInfoEnabled()) {
                    log.info("instantiated service " + serviceName + ": " + service1);
                }
            } catch (InstantiationException e) {
                throw new TopiaException("unable to instantiate service class " + serviceClass, e);
            } catch (IllegalAccessException e) {
                throw new TopiaException("unable to instantiate service class " + serviceClass, e);
            } catch (InvocationTargetException e) {
                throw new TopiaException("unable to instantiate service class " + serviceClass, e);
            } catch (NoSuchMethodException e) {
                throw new TopiaException("unable to instantiate service class " + serviceClass, e);
            }
        }

        // the services configurations (key is the name of the service)
        Map<String, Map<String, String>> declaredServicesConfiguration =
                topiaConfiguration.getDeclaredServicesConfiguration();

        // now all the services are instantiated and and the configuration are known, call init on all services
        for (Map.Entry<String, TopiaService> entry : services.entrySet()) {
            String serviceName = entry.getKey();
            TopiaService topiaService = entry.getValue();
            Map<String, String> serviceConfiguration = declaredServicesConfiguration.get(serviceName);
            if (serviceConfiguration == null) {
                serviceConfiguration = Collections.emptyMap();
            }
            if (log.isInfoEnabled()) {
                log.info("will init service " + serviceName + " with configuration " + serviceConfiguration);
            }
            topiaService.initTopiaService(topiaApplicationContext, serviceConfiguration);
        }

        // keep references of all the service created for further use
        this.services = ImmutableMap.copyOf(services);

    }


    @Override
    public Map<String, TopiaService> getServices() {
        return services;
    }

    @Override
    public <T extends TopiaService> Map<String, T> getServices(Class<T> interfaceService) {
        Map<String, T> result = new LinkedHashMap<String, T>();
        for (Map.Entry<String, TopiaService> entry : services.entrySet()) {
            String serviceName = entry.getKey();
            TopiaService service = entry.getValue();
            if (interfaceService.isAssignableFrom(service.getClass())) {
                result.put(serviceName, (T) service);
            }
        }
        return result;
    }
}
