package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import jakarta.persistence.FlushModeType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.loader.MultipleBagFetchException;
import org.hibernate.query.Query;
import org.nuiton.topia.persistence.QueryMissingOrderException;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaNonUniqueResultException;
import org.nuiton.topia.persistence.TopiaQueryException;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * This class is the Hibernate implementation of TopiaJpaSupport. It realizes the bridge between the JPA specification
 * and the technical choice made for its implementation : Hibernate.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class HibernateTopiaJpaSupport implements TopiaJpaSupport {

    private static final Log log = LogFactory.getLog(HibernateTopiaJpaSupport.class);

    /**
     * Object to get Hibernate's Session, Configuration, ...
     */
    protected TopiaHibernateSupport hibernateSupport;

    /**
     * Object that handles each event propagation. Listeners are registered inside this instance. This instance is the
     * persistenceContext's one.
     */
    protected TopiaFiresSupport firesSupport;

    /**
     * This flag permits to use (or not) the flush mode when doing queries.
     *
     * <p>The normal usage is to says yes (that's why the default value is {@code true}), in that case when doing
     * queries (says in method {@link #findAll(String, java.util.Map)} or
     * {@link #find(String, int, int, java.util.Map)}) it will use the flush mode
     * {@link org.hibernate.FlushMode#AUTO}).</p>
     *
     * <p>But sometimes, when doing a lot of queries (for some imports for example),
     * we do NOT want the session to be flushed each time we do a find, then you
     * can set this flag to {@code false} using the method {@link #setUseFlushMode(boolean)}</p>
     *
     * @since 2.5
     */
    protected boolean useFlushMode = true;

    /**
     * Delay after which Topia should consider a query as slow then log a warn. If no value is present, the slow queries
     * won't be tracked.
     *
     * @since 3.8
     */
    protected Optional<Duration> slowQueriesThreshold;

    public HibernateTopiaJpaSupport(TopiaHibernateSupport hibernateSupport, TopiaFiresSupport firesSupport, Optional<Duration> slowQueriesThreshold) {
        this.hibernateSupport = hibernateSupport;
        this.firesSupport = firesSupport;
        this.slowQueriesThreshold = slowQueriesThreshold;
    }

    public TopiaHibernateSupport getHibernateSupport() {
        return hibernateSupport;
    }

    @Override
    public void setUseFlushMode(boolean useFlushMode) {
        this.useFlushMode = useFlushMode;
    }

    @Override
    public void setSlowQueriesThreshold(Duration slowQueriesThreshold) {
        this.slowQueriesThreshold = Optional.ofNullable(slowQueriesThreshold);
    }

    protected Query prepareQuery(String jpaql, Map<String, Object> parameters) {
        checkHqlParameters(parameters);
        Query query = hibernateSupport.getHibernateSession().createQuery(jpaql);
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            String name = entry.getKey();
            Object value = entry.getValue();

            // since Hibernate 6, we must unproxify parameters. Otherwise,
            // we get a validation error if the parameter is a ToPIA entity
            // due to proxy being on the contract/interface while the
            // mapping expect an *Impl (which is not declared in the proxy)
            Object unproxyfied = Hibernate.unproxy(value);

            if (value.getClass().isArray()) {
                query.setParameterList(name, (Object[]) unproxyfied);
            } else if (value instanceof Collection<?>) {
                query.setParameterList(name, (Collection<?>) unproxyfied);
            } else {
                query.setParameter(name, unproxyfied);
            }
        }
        // tchemit 2010-11-30 reproduce the same behaviour than before with the dao legacy
        if (useFlushMode) { // FIXME AThimel 06/08/14 I think this is the reason of the unexpected flush we have
            query.setFlushMode(FlushModeType.AUTO);
        }
        return query;
    }

    protected void checkHqlParameters(Map<String, Object> parameters) {
        Preconditions.checkArgument(!parameters.containsKey("object"), "'object' is not a valid parameter name in HQL");
    }

    @Override
    public <T> List<T> findAll(String jpaql, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);

            try (SlowQueryWatcher ignored = monitorQuery(query, parameters)) {
                List result = query.list();
                result = firesSupport.fireEntitiesLoad(this, result); // FIXME brendan 08/01/18 unchecked assignment warn is legit: we can't be sure it topia entities, it may be any object
                return result;
            }
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags during findAll: " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (HibernateException he) {
            throw new TopiaQueryException(
                    "unable to findAll",
                    he,
                    jpaql,
                    parameters);
        } catch (RuntimeException re) {
            throw new TopiaQueryException(
                    "unable to findAll",
                    re,
                    jpaql,
                    parameters);
        }
    }

    /**
     * Like {@link #findAll(String, Map)} but getting a stream that may lazily fetch data.
     *
     * Actual behavior rely on JPA implementation.
     *
     * According to {@link Query#stream()}, caller should {@link Stream#close()} the stream.
     *
     * @since 3.4
     */
    @Override
    public <T> Stream<T> stream(String jpaql, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);

            Stream result = query.stream();
            Consumer<T> consumer = row -> {
                List singleton = Collections.singletonList(row);
                firesSupport.fireEntitiesLoad(this, singleton); // FIXME brendan 08/01/18 unchecked assignment warn is legit: we can't be sure it topia entities, it may be any object
            };
            return result.peek(consumer);
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags during " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (HibernateException he) {
            throw new TopiaQueryException(
                    "unable to stream",
                    he,
                    jpaql,
                    parameters);
        } catch (RuntimeException re) {
            throw new TopiaQueryException(
                    "unable to stream",
                    re,
                    jpaql,
                    parameters);
        }
    }

    @Override
    public <T> T findAny(String jpaql, Map<String, Object> parameters) {

        // Execute query, and ask for one result only
        List<T> results = find0(jpaql, 0, 0, parameters);

        // Return it or null
        T result = Iterables.getOnlyElement(results, null);
        return result;
    }

    @Override
    public <T> T findUnique(String jpaql, Map<String, Object> parameters) {

        // Execute query, and ask for up to 2 results only
        List<T> results = find0(jpaql, 0, 1, parameters);

        // If there is more than 1 result, throw an exception
        if (results.size() > 1) {
            String message = String.format(
                    "Query '%s' returns more than 1 unique result", jpaql);
            throw new TopiaNonUniqueResultException(message, parameters);
        }

        // otherwise return the first one, or null
        T result = Iterables.getOnlyElement(results, null);
        return result;
    }

    // FIXME AThimel 11/09/14 This method duplicates org.nuiton.topia.persistence.internal.AbstractTopiaDao.hqlContainsOrderBy()
    protected boolean hqlContainsOrderBy(String hql) {
        return hql.toLowerCase().contains("order by");
    }

    @Override
    public <T> List<T> find(String jpaql, int startIndex, int endIndex, Map<String, Object> parameters)
            throws QueryMissingOrderException {

        if (!hqlContainsOrderBy(jpaql)) {
            throw new QueryMissingOrderException(jpaql, parameters);
        }

        List<T> result = find0(jpaql, startIndex, endIndex, parameters);
        return result;
    }

    protected <T> List<T> find0(String jpaql, int startIndex, int endIndex, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);

            // Set bounds
            query.setFirstResult(startIndex);
            if (endIndex >= 0 && endIndex != Integer.MAX_VALUE) {
                Preconditions.checkArgument(startIndex <= endIndex, "startIndex " + startIndex + " > " + "endIndex" + endIndex);
                query.setMaxResults(endIndex - startIndex + 1);
            }

            try (SlowQueryWatcher ignored = monitorQuery(query, parameters)) {
                List result = query.list();
                result = firesSupport.fireEntitiesLoad(this, result);
                return result;
            }
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags " +
                            "during find page startIndex=" + startIndex + ", endIndex=" + endIndex
                            + ": " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (HibernateException e) {
            throw new TopiaQueryException(
                    "unable to find page startIndex=" + startIndex + ", endIndex=" + endIndex,
                    e,
                    jpaql,
                    parameters);
        } catch (RuntimeException e) {
            throw new TopiaQueryException(
                    "unable to find page startIndex=" + startIndex + ", endIndex=" + endIndex,
                    e,
                    jpaql,
                    parameters);
        }
    }

    @Override
    public int execute(String jpaql, Map<String, Object> parameters) {
        try {
            Query query = prepareQuery(jpaql, parameters);

            try (SlowQueryWatcher ignored = monitorQuery(query, parameters)) {
                int result = query.executeUpdate();
                return result;
            }
        } catch (MultipleBagFetchException mbfe) {
            throw new TopiaQueryException(
                    "unable to fetch multiple bags during execute: " + mbfe.getBagRoles(),
                    mbfe,
                    jpaql,
                    parameters);
        } catch (HibernateException e) {
            throw new TopiaQueryException(
                    "unable to execute query",
                    e,
                    jpaql,
                    parameters);
        } catch (RuntimeException e) {
            throw new TopiaQueryException(
                    "unable to execute query",
                    e,
                    jpaql,
                    parameters);
        }
    }

    @Override
    public void save(Object object) {
        try {
            hibernateSupport.getHibernateSession().save(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'save' instance", eee);
        }
    }

    @Override
    public void update(Object object) {
        try {
            hibernateSupport.getHibernateSession().update(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'update' instance", eee);
        }
    }

    @Override
    public void saveOrUpdate(Object object) {
        try {
            hibernateSupport.getHibernateSession().saveOrUpdate(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'saveOrUpdate' instance", eee);
        }
    }

    @Override
    public void delete(Object object) {
        try {
            hibernateSupport.getHibernateSession().delete(object);
        } catch (HibernateException eee) {
            throw new TopiaException("Unable to 'delete' instance", eee);
        }
    }

    protected SlowQueryWatcher monitorQuery(final Query query, final Map<String, Object> args) {
        // Si aucun seuil n'est défini, on ne créé pas de watcher
        if (!slowQueriesThreshold.isPresent()) {
            return null;
        }

        Supplier<String> descriptionSupplier = () -> {
            String description = "HQL query: " + query.getQueryString();
            if (MapUtils.isNotEmpty(args)) {
                Map<String, Object> notTooLongArgs = truncateLongArgs(args, 100);
                description += " -- with args: " + notTooLongArgs;
            }
            return description;
        };

        long thresholdMillis = slowQueriesThreshold.get().toMillis();
        SlowQueryWatcher result = SlowQueryWatcher.start(descriptionSupplier, thresholdMillis);
        return result;
    }

    /**
     * Some of the values may be very long. This methods truncates any Iterable value to a maximum of
     * {@code truncateSize} elements and display how many more.
     *
     * @param args         the raw map of args
     * @param truncateSize who many elements to include before truncating
     * @return a view over the given map which each value is potentially truncated
     */
    protected Map<String, Object> truncateLongArgs(final Map<String, Object> args, final int truncateSize) {
        Preconditions.checkNotNull(args);
        Map<String, Object> result = Maps.transformValues(args, value -> {
            if (value instanceof Collection) {
                Collection<?> collection = (Collection<?>) value;
                int size = collection.size();
                if (size > truncateSize) {
                    Iterable<?> limited = Iterables.limit(collection, truncateSize);
                    String limitedString = limited.toString();
                    String valueAsString = String.format("%s ... and %d more]",
                            StringUtils.removeEnd(limitedString, "]"),
                            size - truncateSize);
                    return valueAsString;
                }
            }
            return value;
        });
        return result;
    }

} // HibernateTopiaJpaSupport
