package org.nuiton.topia.persistence.jdbc;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.StringUtils;

import java.sql.Driver;

public class BeanJdbcConfiguration implements JdbcConfiguration {

    protected Class<? extends Driver> jdbcDriverClass;

    protected String jdbcConnectionUrl;

    protected String jdbcConnectionUser;

    protected String jdbcConnectionPassword;

    @Override
    public Class<? extends Driver> getJdbcDriverClass() {
        return jdbcDriverClass;
    }

    @Override
    public String getJdbcConnectionUrl() {
        return jdbcConnectionUrl;
    }

    @Override
    public String getJdbcConnectionUser() {
        return jdbcConnectionUser;
    }

    @Override
    public String getJdbcConnectionPassword() {
        return jdbcConnectionPassword;
    }

    public void setJdbcDriverClass(Class<? extends Driver> jdbcDriverClass) {
        this.jdbcDriverClass = jdbcDriverClass;
    }

    public void setJdbcConnectionUrl(String jdbcConnectionUrl) {
        this.jdbcConnectionUrl = jdbcConnectionUrl;
    }

    public void setJdbcConnectionUser(String jdbcConnectionUser) {
        this.jdbcConnectionUser = jdbcConnectionUser;
    }

    public void setJdbcConnectionPassword(String jdbcConnectionPassword) {
        this.jdbcConnectionPassword = jdbcConnectionPassword;
    }

    public String dangerousToString() {
        return MoreObjects.toStringHelper(this)
                .add("jdbcConnectionUrl", jdbcConnectionUrl)
                .add("jdbcConnectionUser", jdbcConnectionUser)
                .add("jdbcConnectionPassword", jdbcConnectionPassword)
                .add("jdbcDriverClass", jdbcDriverClass)
                .toString();
    }

    @Override
    public String toString() {
        String safeToString = StringUtils.replace(
                dangerousToString(),
                getJdbcConnectionPassword(),
                "***** hidden by BeanTopiaConfiguration#toString *****");
        return safeToString;
    }
}
