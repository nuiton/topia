package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.legacy.Loador;
import org.nuiton.util.beans.Binder;

/**
 * A {@link Binder} dedicated to {@link TopiaEntity} dealing with technical
 * values.
 *
 * There is an internal sate {@link #empty} to deal with default values of
 * properties of an entity.
 *
 * <b>Note:</b> Should remove {@link Loador} contract soon.
 *
 * @param <E> the type of entity
 * @author Tony Chemit - tchemit@codelutin.com
 * @since 2.3.0
 */
public class TopiaEntityBinder<E extends TopiaEntity> extends Binder<E, E>
        implements Loador<E> {

    private static final long serialVersionUID = 1L;

    protected E empty;

    public void setEmpty(E empty) {
        this.empty = empty;
    }

    @Override
    public void load(E source, E target, boolean tech,
                     String... propertyNames) {
        if (source == null) {
            source = empty;
        }
        if (tech) {
            TopiaEntityHelper.bindTechnical(source, target);
        }
        copy(source, target, propertyNames);
    }


    @Override
    protected Object bind(Binder binder, Object read) throws IllegalAccessException, InstantiationException {

        Object result = read.getClass().newInstance();

        if (binder instanceof Loador) {

            // load entity 
            ((Loador) binder).load(read, result, true);
        } else {

            // simple copy
            binder.copy(read, result);
        }

        return result;
    }
}
