package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.QueryMissingOrderException;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This API provides methods to use persistence using JPA queries
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaJpaSupport {

    /**
     * Allow to do some JPA-QL query
     *
     * WARNING : Depending on the registered service, this method may not
     * support something else than queries on TopiaEntity
     *
     * @param <T>        type of result
     * @param jpaql      the JPA-QL query
     * @param parameters a map which keys are the attribute names and values are the attributes expected values
     * @return The result list
     */
    <T> List<T> findAll(String jpaql, Map<String, Object> parameters);

    /**
     * Streaming on the result of a {@link #findAll}.
     *
     * Streaming is not part of JPA but may be part a JPA implementation so we provide
     * a default implementation that will work on any JPA vendor.
     *
     * @since 3.4
     */
    default <T> Stream<T> stream(String jpaql, Map<String, Object> parameters) {
        return this.<T>findAll(jpaql, parameters).stream();
    }

    /**
     * Allow to do some JPA-QL query using the given bounds.
     * <ul>
     * <li>No lower bound : {@code startIndex = 0}.</li>
     * <li>No upper bound : {@code endIndex = -1}.</li>
     * </ul>
     *
     * WARNING : Depending on the registered service, this method may not
     * support something else than queries on TopiaEntity
     *
     * @param <T>        type of result
     * @param jpaql      the JPA-QL query
     * @param startIndex first index of entity to return
     * @param endIndex   last index of entity to return
     * @param parameters a map which keys are the attribute names and values are the attributes expected values
     * @return The result list
     * @throws QueryMissingOrderException if no order by clause is specified
     */
    <T> List<T> find(String jpaql,
                     int startIndex,
                     int endIndex,
                     Map<String, Object> parameters)
            throws QueryMissingOrderException;

    /**
     * Allow to do some JPA-QL query and return a single result. If nothing is
     * found by the query, will return null.
     *
     * WARNING : Depending on the registered service, this method may not
     * support something else than queries on TopiaEntity
     *
     * @param <T>        type of result
     * @param jpaql      the JPA-QL query
     * @param parameters a map which keys are the attribute names and values are the attributes expected values
     * @return The result instance or null
     */
    <T> T findAny(String jpaql, Map<String, Object> parameters);

    /**
     * Allow to do some JPA-QL query and return an unique result. If nothing is
     * found by the query, will return null. If more than one result is found,
     * will throw an exception.
     *
     * WARNING : Depending on the registered service, this method may not
     * support something else than queries on TopiaEntity
     *
     * @param <T>        type of result
     * @param jpaql      the JPA-QL query
     * @param parameters a map which keys are the attribute names and values are the attributes expected values
     * @return The result instance or null
     */
    <T> T findUnique(String jpaql, Map<String, Object> parameters);

    /**
     * Execute JPA-QL operation on data (Update, Delete).
     *
     * @param jpaql      the JPA-QL query
     * @param parameters a map which keys are the attribute names and values are the attributes expected values
     * @return The number of entities updated or deleted.
     */
    int execute(String jpaql, Map<String, Object> parameters);

    /**
     * Tells to the context if it has to use a flush mode before each query.
     *
     * By default, we use a flush mode, but in some case it costs to much doing
     * this, that's why you can disable it setting the value to {@code false}.
     *
     * @param useFlushMode the new value to set
     * @since 2.5
     */
    void setUseFlushMode(boolean useFlushMode);

    /**
     * Tells to the context after which delay a query should be considered as slow.
     *
     * Default value is to skip slow queries watching unless specified in application configuration.
     *
     * @param threshold the new value to set
     * @since 3.8
     */
    void setSlowQueriesThreshold(Duration threshold);

    /**
     * Persist the given transient instance, first assigning a generated identifier. This method is JPA implementation
     * independent.
     *
     * This method is "inspired" of the Hibernate's Session#save method.
     *
     * @param object a transient instance of a persistent class
     */
    void save(Object object);

    /**
     * Update the persistent instance with the identifier of the given detached instance.
     *
     * This method is "inspired" of the Hibernate's Session#update method.
     *
     * @param object a detached instance containing updated state
     */
    void update(Object object);

    /**
     * Either {@link #save(Object)} or {@link #update(Object)} the given instance.
     *
     * This method is "inspired" of the Hibernate's Session#saveOrUpdate method.
     *
     * @param object a transient or detached instance containing new or updated state
     * @see #save(java.lang.Object)
     * @see #update(Object object)
     */
    void saveOrUpdate(Object object);

    /**
     * Remove a persistent instance.
     *
     * This method is "inspired" of the Hibernate's Session#delete method.
     *
     * @param object the instance to be removed
     */
    void delete(Object object);

}
