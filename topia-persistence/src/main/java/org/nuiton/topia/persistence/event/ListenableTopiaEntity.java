package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;
import java.beans.VetoableChangeListener;

/**
 * Contract to centralize usable methods to register/unregister pre/post read/write listeners.
 *
 * If you need to match the java beans expectations, prefer using
 * {@link org.nuiton.topia.persistence.event.ListenableBean}.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @see org.nuiton.topia.persistence.event.ListenableBean
 */
public interface ListenableTopiaEntity extends ListenableBean {

    //------------------------------------------------------------------------//
    //-- Pre Read (VetoableChangeListener) methods ---------------------------//
    //------------------------------------------------------------------------//

    /**
     * Adds a {@link java.beans.VetoableChangeListener} on any property reading.
     *
     * @param listener the listener to register
     * @see java.beans.VetoableChangeSupport
     */
    void addPreReadListener(VetoableChangeListener listener);

    /**
     * Adds a {@link java.beans.VetoableChangeListener} on the given property reading.
     *
     * @param propertyName the property name to listen
     * @param listener     the listener to register
     * @see java.beans.VetoableChangeSupport
     */
    void addPreReadListener(String propertyName, VetoableChangeListener listener);

    /**
     * Remove the given {@link java.beans.VetoableChangeListener} registered for any property reading.
     *
     * @param listener the listener to unregister
     * @see java.beans.VetoableChangeSupport
     */
    void removePreReadListener(VetoableChangeListener listener);

    /**
     * Remove the given {@link java.beans.VetoableChangeListener} registered the given property reading.
     *
     * @param propertyName the property name to unregister reading
     * @param listener     the listener to unregister
     * @see java.beans.VetoableChangeSupport
     */
    void removePreReadListener(String propertyName, VetoableChangeListener listener);

    //------------------------------------------------------------------------//
    //-- Post Read (PropertyChangeListener) methods --------------------------//
    //------------------------------------------------------------------------//

    /**
     * Adds a {@link java.beans.PropertyChangeListener} on any property reading.
     *
     * @param listener the listener to register
     * @see java.beans.PropertyChangeSupport
     */
    void addPostReadListener(PropertyChangeListener listener);

    /**
     * Adds a {@link java.beans.PropertyChangeListener} on the given property reading.
     *
     * @param propertyName the property name to listen
     * @param listener     the listener to register
     * @see java.beans.PropertyChangeSupport
     */
    void addPostReadListener(String propertyName, PropertyChangeListener listener);

    /**
     * Remove the given {@link java.beans.PropertyChangeListener} registered for any property reading.
     *
     * @param listener the listener to unregister
     * @see java.beans.PropertyChangeSupport
     */
    void removePostReadListener(PropertyChangeListener listener);

    /**
     * Remove the given {@link java.beans.PropertyChangeListener} registered the given property reading.
     *
     * @param propertyName the property name to unregister reading
     * @param listener     the listener to unregister
     * @see java.beans.PropertyChangeSupport
     */
    void removePostReadListener(String propertyName, PropertyChangeListener listener);

    //------------------------------------------------------------------------//
    //-- Pre Write (VetoableChangeListener) methods --------------------------//
    //------------------------------------------------------------------------//

    /**
     * Adds a {@link java.beans.VetoableChangeListener} on any property writing.
     *
     * @param listener the listener to register
     * @see java.beans.VetoableChangeSupport
     */
    void addPreWriteListener(VetoableChangeListener listener);

    /**
     * Adds a {@link java.beans.VetoableChangeListener} on the given property writing.
     *
     * @param propertyName the property name to listen
     * @param listener     the listener to register
     * @see java.beans.VetoableChangeSupport
     */
    void addPreWriteListener(String propertyName, VetoableChangeListener listener);

    /**
     * Remove the given {@link java.beans.VetoableChangeListener} registered for any property writing.
     *
     * @param listener the listener to unregister
     * @see java.beans.VetoableChangeSupport
     */
    void removePreWriteListener(VetoableChangeListener listener);

    /**
     * Remove the given {@link java.beans.VetoableChangeListener} registered the given property writing.
     *
     * @param propertyName the property name to unregister writing
     * @param listener     the listener to unregister
     * @see java.beans.VetoableChangeSupport
     */
    void removePreWriteListener(String propertyName, VetoableChangeListener listener);

    //------------------------------------------------------------------------//
    //-- Post Write (PropertyChangeListener) methods -------------------------//
    //------------------------------------------------------------------------//

    /**
     * Adds a {@link java.beans.PropertyChangeListener} on any property writing.
     *
     * @param listener the listener to register
     * @see java.beans.PropertyChangeSupport
     */
    void addPostWriteListener(PropertyChangeListener listener);

    /**
     * Adds a {@link java.beans.PropertyChangeListener} on the given property writing.
     *
     * @param propertyName the property name to listen
     * @param listener     the listener to register
     * @see java.beans.PropertyChangeSupport
     */
    void addPostWriteListener(String propertyName, PropertyChangeListener listener);

    /**
     * Remove the given {@link java.beans.PropertyChangeListener} registered for any property writing.
     *
     * @param listener the listener to unregister
     * @see java.beans.PropertyChangeSupport
     */
    void removePostWriteListener(PropertyChangeListener listener);

    /**
     * Remove the given {@link java.beans.PropertyChangeListener} registered the given property writing.
     *
     * @param propertyName the property name to unregister writing
     * @param listener     the listener to unregister
     * @see java.beans.PropertyChangeSupport
     */
    void removePostWriteListener(String propertyName, PropertyChangeListener listener);

}
