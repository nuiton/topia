package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;

import java.util.List;

/**
 * This API provides methods to run SQL queries
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaSqlSupport {

    /**
     * Execute a given SQL code inside this transaction.
     *
     * @param sqlScript the sql script to execute
     */
    void executeSql(String sqlScript);

    /**
     * Runs the given SQL work on the current context
     *
     * @param sqlWork the SQL work instance to execute
     */
    void doSqlWork(TopiaSqlWork sqlWork);

    /**
     * Runs the given SQL query and return its first result if there is some.
     *
     * @param <O>   type of result
     * @param query query to play
     * @return the single result or {@code null} if none found.
     * @throws TopiaException for any pb
     */
    <O> O findSingleResult(TopiaSqlQuery<O> query) throws TopiaException;

    /**
     * Runs the given SQL query and return all his result if there is some.
     *
     * @param <O>   type of result
     * @param query query to play
     * @return the list of results (the list is empty if query returns no result).
     * @throws TopiaException for any pb
     */
    <O> List<O> findMultipleResult(TopiaSqlQuery<O> query) throws TopiaException;

}
