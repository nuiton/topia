package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.legacy.Collector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Une classe avec des méthodes utiles sur les entités.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TopiaEntityHelper {

    private static final Log log = LogFactory.getLog(TopiaEntityHelper.class);

    /** Le pattern d'une reference sur une association */
    public static final String ASSOCIATION_PATTERN =
            "%1$s[@" + TopiaEntity.PROPERTY_TOPIA_ID + "=\"%2$s\"]";

    /**
     * Bind les valeurs techniques depuis une entitée vers une autre.
     *
     * @param from l'entité source
     * @param dst  l'entité destination
     */
    public static void bindTechnical(TopiaEntity from, TopiaEntity dst) {
        if (from == null) {
            dst.setTopiaId(null);
            dst.setTopiaVersion(0);
            dst.setTopiaCreateDate(null);
        } else {
            dst.setTopiaId(from.getTopiaId());
            dst.setTopiaVersion(from.getTopiaVersion());
            dst.setTopiaCreateDate(from.getTopiaCreateDate());
        }
    }

    /**
     * Récupère une entité qui doit exister à partir de son id.
     *
     * Si l'entité n'existe pas, on déclanche une exception {@link
     * IllegalArgumentException}.
     *
     * @param dao     la dao pour récupérer la valeur
     * @param topiaId l'id de l'entité recherchée
     * @param <E>    le type de l'entité
     * @return l'entité recherché
     * @throws TopiaException           pour tout pb lors de la récupération de
     *                                  l'entité
     * @throws IllegalArgumentException si l'entité n'existe pas.
     */
    public static <E extends TopiaEntity> E getExistingEntity(
            TopiaDao<E> dao, String topiaId) throws TopiaException,
            IllegalArgumentException {
        E entity = dao.forTopiaIdEquals(topiaId).findUniqueOrNull();
        Preconditions.checkArgument(entity != null,
                                    "could not find entity with topiaId " + topiaId);
        return entity;
    }

    /**
     * Récupère une entité dans une liste d'entités à partir de son {@link
     * TopiaEntity#getTopiaId()}.
     *
     * @param entities la liste des entités à scanner
     * @param topiaId  l'id de l'entité recherchée
     * @param <E>     le type de l'entité
     * @return l'entité trouvée, ou {@code null} si elle n'est pas
     *         trouvée.
     */
    public static <E extends TopiaEntity> E getEntityByTopiaId(
            Collection<E> entities, String topiaId) {
        if (entities != null) {
            for (E e : entities) {
                if (topiaId.equals(e.getTopiaId())) {
                    return e;
                }
            }
        }
        return null;
    }

    /**
     * Get entity at {@code index} position in the collection is not null.
     *
     * @param entities collection of entities (can be null)
     * @param index    position to get
     * @param <O>      type of object
     * @return the entity at the given index, or {@code null} if entities are null
     * @throws IndexOutOfBoundsException if index is out of bounds
     * @since 3.0
     */
    public static <O> O getEntityByIndex(Collection<O> entities, int index) throws IndexOutOfBoundsException {
        if (entities != null) {
            return Iterables.get(entities, index);
        }
        return null;
    }

    /**
     * Teste si une entité possède un topiaId.
     *
     * @param paramName le nom du paramètre à afficher en casd'erreur
     * @param bean      l'entité  à tester
     * @param <E>      le type de l'entité
     * @throws IllegalStateException si l'entité n'a pas de topiaId
     * @throws NullPointerException  si l'entité  est null
     */
    public static <E extends TopiaEntity> void checkNotNullAndExistingEntity(
            String paramName, E bean) throws IllegalStateException,
            NullPointerException {
        Preconditions.checkNotNull(bean, paramName + " can not be null");
        Preconditions.checkState(bean.getTopiaId() != null,
                                 "can not create " + bean.getClass() + " here...");
    }

    /**
     * Teste si une entité ne possède pas un topiaId.
     *
     * @param paramName le nom de paramètre à afficher en cas d'erreur
     * @param bean      l'entité  à tester
     * @param <E>      le type del'entité
     * @throws NullPointerException  si l'entité est nulle
     * @throws IllegalStateException si l'entité possède un topiaId.
     */
    public static <E extends TopiaEntity> void checkNotNullAndNoneExistingEntity(
            String paramName, E bean) throws NullPointerException,
            IllegalStateException {
        Preconditions.checkNotNull(bean, paramName + " can not be null");
        Preconditions.checkState(bean.getTopiaId() == null,
                                 "can not update " + bean.getClass() + " here...");
    }

//    /**
//     * Create a new database from a sql dump locating in a gzip file.
//     *
//     * @param dbDirectory  the directory where to create the db
//     * @param topiaContext the topiaContext to use to create the databse
//     * @param resource     the url of the sql dump gzip file to use
//     * @throws TopiaException       if any pb while creating db
//     * @throws IOException          if any io exception
//     * @throws NullPointerException if parameters are null
//     */
//    public static void createDBFromSQL(
//            File dbDirectory,
//            TopiaContext topiaContext,
//            URI resource) throws IOException,
//            TopiaException, NullPointerException {
//        Preconditions.checkNotNull(dbDirectory, dbDirectory + " can not be null");
//        Preconditions.checkNotNull(topiaContext, topiaContext + " can not be null");
//        Preconditions.checkNotNull(resource, resource + " can not be null");
//
//        File databaseDump;
//        if (resource.isOpaque()) {
//
//            ByteArrayOutputStream output =
//                    FileUtil.readBytesFrom(resource.toURL().openStream(), 8048);
//            File tempFile = File.createTempFile("topiaDumpDatabase", ".sql.gz");
//
//            FileOutputStream stream = new FileOutputStream(tempFile);
//            try {
//                output.writeTo(stream);
//            } finally {
//                stream.close();
//            }
//
//            tempFile.deleteOnExit();
//            databaseDump = tempFile;
//
//        } else {
//
//            databaseDump = new File(resource);
//        }
//
//        if (!dbDirectory.exists()) {
//            if (!dbDirectory.mkdirs()) {
//                throw new IOException(
//                        "could not create directory " + dbDirectory);
//            }
//        }
//
//        log.info("create database from [" + databaseDump + "] at [" +
//                 dbDirectory + "]");
//
//        TopiaContext ctxt = topiaContext.beginTransaction();
//
//        try {
//            ctxt.restore(databaseDump);
//            ctxt.commit();
//        } catch (TopiaException e) {
//            ctxt.rollback();
//            throw e;
//        } finally {
//            ctxt.close();
//        }
//    }

//    /**
//     * Save the given database to a gzip file.
//     *
//     * @param gzipFile     the file where to store db
//     * @param topiaContext the topiaContext of the db to store
//     * @throws TopiaException       if any pb while saving db
//     * @throws NullPointerException if parameters are null
//     * @throws IOException          if could not create gzipFile container
//     *                              directory
//     */
//    public static void saveDB(
//            File gzipFile, TopiaContext topiaContext)
//            throws TopiaException, IOException, NullPointerException {
//        Preconditions.checkNotNull(gzipFile, gzipFile + " can not be null");
//        Preconditions.checkNotNull(topiaContext, topiaContext + " can not be null");
//
//        if (!gzipFile.getParentFile().exists()) {
//            if (!gzipFile.getParentFile().mkdirs()) {
//                throw new IOException("could not create directory " +
//                                      gzipFile.getParentFile());
//            }
//        }
//        log.info("store database to [" + gzipFile + "]");
//
//        TopiaContext ctxt = null;
//
//        try {
//            ctxt = topiaContext.beginTransaction();
//            ctxt.backup(gzipFile, true);
//        } finally {
//            if (ctxt != null) {
//                ctxt.close();
//            }
//        }
//    }

    /**
     * Obtain a new {@link Comparator} pour {@link TopiaEntity} based on the
     * {@link TopiaEntity#getTopiaId()} method.
     *
     * @return the new instanciated comparator
     */
    public static Comparator<TopiaEntity> getTopiaIdComparator() {
        return new Comparator<TopiaEntity>() {

            @Override
            public int compare(TopiaEntity o1, TopiaEntity o2) {
                if (o1.getTopiaId() == null) {
                    return -1;
                }
                if (o2.getTopiaId() == null) {
                    return 1;
                }
                return o1.getTopiaId().compareTo(o2.getTopiaId());
            }
        };
    }

    /**
     * Filter a list of entities, and keep only the ones from a given type.
     *
     * @param entities    the list of entities to filter
     * @param filterClass the type of entities to keep
     * @return the list of filtered entities for the given entity type in the
     *         list
     * @since 2.1.4
     */
    public static List<TopiaEntity> filter(
            Collection<TopiaEntity> entities,
            Class<? extends TopiaEntity> filterClass) {
        List<TopiaEntity> result = new ArrayList<TopiaEntity>();
        for (TopiaEntity e : entities) {
            if (filterClass.isAssignableFrom(e.getClass())) {
                result.add(e);
            }
        }
        return result;
    }

    public static Class<? extends TopiaEntity> getContractClass(
            TopiaEntityEnum[] contracts,
            Class<? extends TopiaEntity> klass) {
        if (contracts == null || contracts.length == 0) {
            // pas de contract connus...
            return null;
        }

        // on recupere l'ensemble des interfaces connues pour la klass donnee
        Set<Class<? extends TopiaEntity>> klassInterfaces = getInterfaces(
                klass,
                new HashSet<Class<? extends TopiaEntity>>()
        );

        for (TopiaEntityEnum contract : contracts) {
            Class<? extends TopiaEntity> contractClass = contract.getContract();
            if (klassInterfaces.contains(contractClass)) {
                return contractClass;
            }
        }
        return null;
    }

    public static Set<Class<? extends TopiaEntity>> getContractClasses(
            TopiaEntityEnum[] contracts,
            Iterable<Class<? extends TopiaEntity>> klasses) {
        Set<Class<? extends TopiaEntity>> result = new HashSet<Class<? extends TopiaEntity>>();
        if (contracts == null || contracts.length == 0) {
            // pas de contract connus...
            return result;
        }

        for (Class<? extends TopiaEntity> klass : klasses) {
            Class<? extends TopiaEntity> c = getContractClass(contracts, klass);
            if (c != null) {
                result.add(c);
            }
        }
        return result;
    }

    public static TopiaEntityEnum getEntityEnum(
            Class<? extends TopiaEntity> klass, TopiaEntityEnum... contracts) {
        if (contracts == null || contracts.length == 0) {
            // pas de contract connus...
            return null;
        }
        Class<? extends TopiaEntity> contractClass =
                getContractClass(contracts, klass);
        if (contractClass != null) {
            for (TopiaEntityEnum t : contracts) {
                if (t.accept(contractClass)) {
                    return t;
                }
            }
        }
        return null;
    }

    /**
     * Filtre un ensemble de classes d'entites en ne conservant que les contrats
     * des entites.
     *
     * @param contracts les contracts connus
     * @param classes   l'ensemble des classes a filter
     * @return l'ensemble des contrats filtres
     * @since 2.2.0
     */
    public static Set<Class<? extends TopiaEntity>> retainContracts(
            TopiaEntityEnum[] contracts,
            Set<Class<? extends TopiaEntity>> classes) {
        Map<Class<? extends TopiaEntity>, Class<? extends TopiaEntity>> dico;
        dico = new HashMap<Class<? extends TopiaEntity>,
                Class<? extends TopiaEntity>>();
        try {
            Set<Class<? extends TopiaEntity>> result =
                    new HashSet<Class<? extends TopiaEntity>>();

            for (Class<? extends TopiaEntity> c : classes) {
                Class<? extends TopiaEntity> contractClass = dico.get(c);
                if (contractClass == null) {
                    // not already detected class
                    for (TopiaEntityEnum contract : contracts) {
                        if (contract.accept(c)) {
                            contractClass = contract.getContract();
                            dico.put(c, contractClass);
                        }
                    }
                }
                if (contractClass != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("detected type : " + contractClass);
                    }
                    result.add(contractClass);
                }
            }
            return result;
        } finally {
            dico.clear();
        }
    }

    /**
     * Ontenir l'ensemble des contrats d'entites a partir des descriptions
     * d'entites.
     *
     * @param contracts les contracts connus
     * @return l'ensemble des contrats d'entites
     * @since 2.2.0
     */
    @SuppressWarnings({"unchecked"})
    public static Class<? extends TopiaEntity>[] getContracts(
            TopiaEntityEnum[] contracts) {
        Class<?>[] result = new Class<?>[contracts.length];
        int index = 0;
        for (TopiaEntityEnum e : contracts) {
            result[index++] = e.getContract();
        }
        return (Class<? extends TopiaEntity>[]) result;
    }

    /**
     * Collecte l'ensemble des types d'entites (via un parcours en profondeur).
     *
     * On retourne toujours les contrats des entites et jamais les
     * implantations.
     *
     * @param contracts les definitions d'entites connues
     * @param entities  les entites a parcourir
     * @return l'ensemble des types d'entites decouverts (uniquement les
     *         contrats des entites).
     * @throws TopiaException if a problem while visiting entities
     */
    public static Set<Class<? extends TopiaEntity>> detectTypes(
            final TopiaEntityEnum[] contracts,
            TopiaEntity... entities) throws TopiaException {

        Collector<Set<Class<? extends TopiaEntity>>> collector;

        collector =
                new Collector<Set<Class<? extends TopiaEntity>>>(contracts) {

                    /**
                     * La liste des types d'entités détectées
                     */
                    protected Set<Class<? extends TopiaEntity>> detectedTypes =
                            new HashSet<Class<? extends TopiaEntity>>();

                    @Override
                    protected boolean onStarting(TopiaEntity e) {
                        super.onStarting(e);
                        Class<? extends TopiaEntity> entityClass = e.getClass();
                        if (log.isDebugEnabled()) {
                            log.debug(entityClass + " : " + e.getTopiaId());
                        }
                        detectedTypes.add(entityClass);
                        return true;
                    }

                    @Override
                    protected Set<Class<? extends TopiaEntity>> afterAll(
                            CollectorVisitor visitor, TopiaEntity... entities) {
                        Set<Class<? extends TopiaEntity>> result =
                                retainContracts(contracts, detectedTypes);
                        return result;
                    }
                };

        Set<Class<? extends TopiaEntity>> result = collector.detect(entities);
        return result;
    }

    /**
     * Collecte l'ensemble des entites (via un parcours en profondeur) avec un
     * filtrage sur les types d'entites a retourner.
     *
     * On retourne toujours les contrats des entites et jamais les
     * implantations.
     *
     * @param contracts les definitions d'entites connues
     * @param types     l'ensemble des types acceptables
     * @param entities  les entites a parcourir
     * @return l'ensemble des entites decouverts (on converse uniquement les
     *         entites dont le contract est dans l'ensemble voulu).
     * @throws TopiaException if a pb while visiting entities
     */
    public static Map<Class<? extends TopiaEntity>, List<TopiaEntity>>
    detectEntities(final TopiaEntityEnum[] contracts,
                   final Set<Class<? extends TopiaEntity>> types,
                   TopiaEntity... entities) throws TopiaException {

        Collector<Map<Class<? extends TopiaEntity>, List<TopiaEntity>>> collector;
        collector = new Collector<Map<Class<? extends TopiaEntity>, List<TopiaEntity>>>(contracts) {

            /**
             * le dictionnaire des références détectées indexées par leur type
             */
            protected Map<Class<? extends TopiaEntity>, List<TopiaEntity>> detectedRefs =
                    new HashMap<Class<? extends TopiaEntity>, List<TopiaEntity>>();

            @Override
            protected void onStarted(TopiaEntity e, boolean enter) {
                super.onStarted(e, enter);
                Class<? extends TopiaEntity> entityClass = getContractClass(e);

                if (entityClass == null || !types.contains(entityClass)) {
                    // entite non connue ou rejetee (pas dans un type accepte)
                    return;
                }

                if (log.isDebugEnabled()) {
                    log.debug(entityClass + " : " + e.getTopiaId());
                }

                List<TopiaEntity> refs = detectedRefs.get(entityClass);
                if (refs == null) {
                    refs = new ArrayList<TopiaEntity>();
                    detectedRefs.put(entityClass, refs);
                }
                refs.add(e);
            }

            @Override
            protected Map<Class<? extends TopiaEntity>, List<TopiaEntity>>
            afterAll(CollectorVisitor visitor, TopiaEntity... entities) {
                return detectedRefs;
            }
        };

        Map<Class<? extends TopiaEntity>, List<TopiaEntity>> result =
                collector.detect(entities);
        return result;
    }

    /**
     * Collecte l'ensemble des ids d'entites (via un parcours en profondeur)
     * avec un filtrage sur les types d'entites a retourner.
     *
     * On retourne toujours les contrats des entites et jamais les
     * implantations.
     *
     * @param contracts les definitions d'entites connues
     * @param types     l'ensemble des types acceptables
     * @param entities  les entites a parcourir
     * @return l'ensemble des ids d'entites decouverts (on converse uniquement
     *         les entites dont le contract est dans l'ensemble voulu).
     * @throws TopiaException if a pb while visiting entities
     */
    public static TopiaEntityIdsMap
    detectEntityIds(final TopiaEntityEnum[] contracts,
                    final Set<Class<? extends TopiaEntity>> types,
                    TopiaEntity... entities) throws TopiaException {

        Collector<TopiaEntityIdsMap> collector;

        collector = new Collector<TopiaEntityIdsMap>(contracts) {

            /**
             * le dictionnaire des références détectées indexées par leur type
             */
            protected TopiaEntityIdsMap detectedRefs =
                    new TopiaEntityIdsMap();

            @Override
            protected void onStarted(TopiaEntity e, boolean enter) {
                super.onStarted(e, enter);
                Class<? extends TopiaEntity> entityClass = getContractClass(e);

                if (entityClass == null || !types.contains(entityClass)) {
                    // entite non connue ou rejetee (pas dans un type accepte)
                    return;
                }

                if (log.isDebugEnabled()) {
                    log.debug(entityClass + " : " + e.getTopiaId());
                }

                List<String> refs = detectedRefs.get(entityClass);
                if (refs == null) {
                    refs = new ArrayList<String>();
                    detectedRefs.put(entityClass, refs);
                }
                refs.add(e.getTopiaId());
            }

            @Override
            protected TopiaEntityIdsMap
            afterAll(CollectorVisitor visitor, TopiaEntity... entities) {
                return detectedRefs;
            }
        };

        TopiaEntityIdsMap result = collector.detect(entities);
        return result;
    }

    /**
     * Collecte toutes les references d'un ensemble d'entites donnees par leur
     * topiaId sur un ensemble d'entites donne.
     *
     * @param contracts   les definitions d'entites connues
     * @param expressions l'ensemble des ids a detecter
     * @param entities    les entites a parcourir
     * @return l'ensemble des references decouvertes.
     * @throws TopiaException if a pb while visiting entities
     * @see TopiaEntityRef
     */
    public static SortedMap<TopiaEntity, List<TopiaEntityRef>> detectReferences(
            TopiaEntityEnum[] contracts,
            String[] expressions,
            TopiaEntity entities) throws TopiaException {
        return detectReferences(contracts, expressions,
                                Collections.singleton(entities));
    }

    /**
     * Collecte toutes les references d'un ensemble d'entites donnees par leur
     * topiaId sur un ensemble d'entites donne.
     *
     * @param contracts   les definitions d'entites connues
     * @param expressions l'ensemble des ids a detecter
     * @param entities    les entites a parcourir
     * @return l'ensemble des references decouvertes.
     * @throws TopiaException if a pb while visiting entities
     * @see TopiaEntityRef
     */
    public static SortedMap<TopiaEntity, List<TopiaEntityRef>>
    detectReferences(
            final TopiaEntityEnum[] contracts,
            final String[] expressions,
            Collection<? extends TopiaEntity> entities) throws TopiaException {

        Collector<SortedMap<TopiaEntity, List<TopiaEntityRef>>> collector;
        collector = new Collector<SortedMap<TopiaEntity, List<TopiaEntityRef>>>(
                contracts) {

            /** la liste des ids a accepter ou rejecter selon acceptMode */
            List<String> ids = Arrays.asList(expressions);

            /** le dictionnaire des références détectées indexées par leur type */
            SortedMap<TopiaEntity, List<TopiaEntityRef>> refs;

            {
                refs = new TreeMap<TopiaEntity, List<TopiaEntityRef>>(
                        getTopiaIdComparator());
            }

            /** le path courant depuis le depart */
            Deque<TopiaEntity> path = new LinkedList<TopiaEntity>();

            StringBuilder accessorExpression = new StringBuilder();

            TopiaEntity root;

            @Override
            protected void before(CollectorVisitor visitor, TopiaEntity entity) {
                super.before(visitor, entity);
                // on vide le visiteur car on a le droit de reparcourir
                // des donnees deja visitees, on peut donc nettoyer dans
                // le visiteur la liste des noeud explores (ainsi cela engendrea
                // moins de tests)
                visitor.clear();
            }

            @Override
            protected boolean onStarting(TopiaEntity e) {
                if (log.isDebugEnabled()) {
                    log.debug(e.getTopiaId());
                }
                if (root == null) {
                    // start come in start method since
                    // last clear method invocation
                    root = e;
                    addPath(e, "$root", -1);
                    if (accept(e)) {
                        registerEntity(e);
                    }
                }
                return true;
            }

            @Override
            protected void onEnded(TopiaEntity e, boolean enter) {
                super.onEnded(e, enter);
                if (root == e) {
                    // global visit is done
                    root = null;
                    removePath();
                }
            }

            @Override
            protected boolean onVisiting(TopiaEntity e,
                                         String name,
                                         Class<?> type, Object value) {
                TopiaEntity e1 = getTopiaValue(value);
                if (e1 != null) {
                    // on est sur une entite
                    addPath(e1, name, -1);
                    if (accept(e1)) {
                        registerEntity(e1);
                    }
                    // le DepthEntityVisitor gere les cycles de facon trop
                    // strict : on peut accepter de visiter une entite
                    // meme si elle a deja rencontree, du moment qu'elle
                    // n'est pas dans le chemin de parcours
                    if (visitor.getAlreadyExplored().contains(e1)) {
                        boolean contains = visitor.getStack().contains(e1);
                        if (log.isDebugEnabled()) {
                            log.debug("already enter " + e1.getTopiaId() +
                                      ", can reenter ? " + !contains);
                        }
                        if (!contains) {
                            try {
                                // on se substitue au depth visitor
                                // et on accepte le reparcours
                                // ce qui est necessaire pour obtenir toutes
                                // les references (but de ce collecteur)
                                e1.accept(visitor);
                            } catch (TopiaException ex) {
                                if (log.isErrorEnabled()) {
                                    log.error("Error on depth exploration", ex);
                                }
                            }
                        }
                    }
                    return true;
                }
                return false;
            }

            @Override
            protected void onVisited(TopiaEntity e,
                                     String name,
                                     Class<?> type,
                                     Object value, boolean enter) {
                super.onVisited(e, name, type, value, enter);
                if (enter) {
                    removePath();
                }
            }

            @Override
            protected boolean onVisiting(TopiaEntity e,
                                         String name,
                                         Class<?> collectionType,
                                         Class<?> type,
                                         int index, Object value) {
                TopiaEntity e1 = getTopiaValue(value);
                if (e1 != null) {
                    addPath(e1, name, index);
                    if (accept(e1)) {
                        registerEntity(e1);
                    }
                    // le DepthEntityVisitor gere les cycles de facon trop
                    // strict : on peut accepter de visiter une entite
                    // meme si elle a deja rencontree, du moment qu'elle
                    // n'est pas dans le chemin de parcours
                    if (visitor.getAlreadyExplored().contains(e1)) {
                        boolean contains = visitor.getStack().contains(e1);
                        if (log.isDebugEnabled()) {
                            log.debug("already enter " + e1.getTopiaId() +
                                      ", can reenter ? " + !contains);
                        }
                        if (!contains) {
                            try {
                                // on se substitue au depth visitor
                                // et on accepte le reparcours
                                // ce qui est necessaire pour obtenir toutes
                                // les references (but de ce collecteur)
                                e1.accept(visitor);
                            } catch (TopiaException ex) {
                                if (log.isErrorEnabled()) {
                                    log.error("Error on depth exploration", ex);
                                }
                            }
                        }
                    }
                    return true;
                }
                return false;
            }

            @Override
            protected void onVisited(TopiaEntity e,
                                     String name,
                                     Class<?> collectionType,
                                     Class<?> type,
                                     int index, Object value, boolean enter) {
                super.onVisited(e, name, collectionType, type, index, value,
                                enter);
                if (enter) {
                    removePath();
                }
            }

            @Override
            protected SortedMap<TopiaEntity, List<TopiaEntityRef>> afterAll(
                    CollectorVisitor visitor, TopiaEntity... entities) {
                return refs;
            }

            @Override
            protected void clear() {
                super.clear();
                ids = null;
                refs = null;
                path.clear();
                path = null;
                accessorExpression = null;
                root = null;
            }

            boolean accept(TopiaEntity e) {
                return ids.contains(e.getTopiaId());
            }

            void addPath(TopiaEntity e, String name, int index) {
                path.add(e);
                if (accessorExpression.length() > 0) {
                    accessorExpression.append(TopiaEntityRef.SEPARATOR);
                }
                accessorExpression.append(name);
                if (index > -1) {
                    String association = String.format(
                            ASSOCIATION_PATTERN, "", e.getTopiaId());
                    accessorExpression.append(association);
                }
//                if (index > -1) {
//                    accessorExpression.append("[@topiaId=\"");
//                    accessorExpression.append(e.getTopiaId());
//                    accessorExpression.append("\"]");
//                }
                if (log.isTraceEnabled()) {
                    log.trace("add to stack : " + e.getTopiaId() +
                              ", new size : " + path.size() + ", path : " +
                              accessorExpression.toString());
                }
            }

            void removePath() {
                TopiaEntity e = path.removeLast();
                if (path.isEmpty()) {
                    accessorExpression.setLength(0);
                } else {
                    int index = accessorExpression.lastIndexOf(
                            TopiaEntityRef.SEPARATOR);
                    if (index > -1) {
                        accessorExpression.delete(index,
                                                  accessorExpression.length());
                    }
                }
                if (log.isTraceEnabled()) {
                    log.trace("remove from stack : " + e.getTopiaId() +
                              ", new size : " + path.size() + ", path : " +
                              accessorExpression.toString());
                }
            }

            void registerEntity(TopiaEntity e) {

                List<TopiaEntityRef> list = refs.get(e);
                if (list == null) {
                    // first time for this entity
                    list = new ArrayList<TopiaEntityRef>();
                    refs.put(e, list);
                }

                String expression = accessorExpression.toString();

                // add the path for the detected
                list.add(new TopiaEntityRef(
                        root,
                        e,
                        expression,
                        path.toArray(new TopiaEntity[path.size()]))
                );

                if (log.isDebugEnabled()) {
                    log.debug(expression + " (" + e.getTopiaId() + ") - " +
                              list.size() + " , root:" + root.getTopiaId());
                }
                if (log.isTraceEnabled()) {
                    log.trace(e.getTopiaId() + " : new size " + list.size() +
                              ", path : " + expression);
                }
            }
        };

        SortedMap<TopiaEntity, List<TopiaEntityRef>> result =
                collector.detect(
                        entities.toArray(new TopiaEntity[entities.size()])
                );
        return result;
    }

    /**
     * Construit la liste des topiaId d'une liste donnée d'entités.
     *
     * @param entities la liste des entités
     * @return la liste des topiaId
     */
    public static List<String> getTopiaIdList(
            Collection<? extends TopiaEntity> entities) {
        List<String> ids = new ArrayList<String>(entities.size());
        for (TopiaEntity entity : entities) {
            ids.add(entity.getTopiaId());
        }
        return ids;
    }

    /**
     * Construit un tableau des topiaId d'une liste donnée d'entités.
     *
     * @param entities la liste des entités
     * @return le tableau des topiaId
     * @since 2.5.2
     */
    public static String[] getTopiaIdArray(
            Collection<? extends TopiaEntity> entities) {
        String[] ids = new String[entities.size()];
        int i = 0;
        for (TopiaEntity entity : entities) {
            ids[i++] = entity.getTopiaId();
        }
        return ids;
    }

    /**
     * Construit une list d'entite dont les ids sont tous dans la liste d'ids
     * donnee.
     *
     * @param <E>     le type des entites de la liste
     * @param list     la liste a filter
     * @param topiaIds la liste des ids a retenir
     * @return la nouvelle liste filtree
     */
    public static <E extends TopiaEntity> List<E> retainEntities(
            Collection<E> list, List<String> topiaIds) {

        List<E> r = new ArrayList<E>(list == null ? 0 : list.size());
        if (list != null) {
            for (E e : list) {
                if (topiaIds.contains(e.getTopiaId())) {
                    r.add(e);
                }
            }
        }
        return r;
    }

    /**
     * Construit le dictionnaire des differences entre deux listes d'entites.
     *
     * @param <E>        le type des entites
     * @param referentiel la liste considere comme reference
     * @param locale      la liste a mettre a jour
     * @return le dictionnaire des etats des entites ajoutees, modifiees ou
     *         obsoletes.
     * @see DiffState
     * @since 2.2.0
     */
    public static <E extends TopiaEntity> DiffState.DiffStateMap
    buildDifferentiel(List<E> referentiel, List<E> locale) {
        DiffState.DiffStateMap result = DiffState.newMap();

        // construction des deux listes d'id
        List<String> referentielIdList = getTopiaIdList(referentiel);
        List<String> localeIdList = getTopiaIdList(locale);

        for (E aReferentiel : referentiel) {
            TopiaEntity referentielEntity = aReferentiel;
            String refId = referentielEntity.getTopiaId();
            if (localeIdList.contains(refId)) {
                // id existant sur le storage locale

                // il se peut que les listes ne soient pas triées sur le même
                // ordre, on parcourt donc la liste cible pour retrouver
                // l'entité cible
                TopiaEntity localeEntity = Iterables.find(locale, TopiaEntities.entityHasId(refId));
                boolean wasModified = referentielEntity.getTopiaVersion() > localeEntity.getTopiaVersion();

                if (wasModified) {
                    result.get(DiffState.MODIFIED).add(refId);
                }
            } else {
                // id non existant sur le second storage: donc en état 'a creer'
                result.get(DiffState.NEW).add(refId);
            }
        }
        // recuperation des ids existant uniquement sur le second storage
        // pour detecter les entites non presente sur le premier storage
        localeIdList.removeAll(referentielIdList);
        if (!localeIdList.isEmpty()) {
            // il existe sur le storage locale, ils sont donc en etat 'a supprimer'
            List<String> toRemove = result.get(DiffState.REMOVED);
            for (String id : localeIdList) {
                toRemove.add(id);
            }
        }
        referentielIdList.clear();
        localeIdList.clear();
        return result;
    }

    @SuppressWarnings({"unchecked"})
    public static Set<Class<? extends TopiaEntity>> getInterfaces(
            Class<? extends TopiaEntity> klass,
            Set<Class<? extends TopiaEntity>> klassInterfaces) {
        if (klassInterfaces.contains(klass)) {
            // deja traite
            return klassInterfaces;
        }
        if (klass.isInterface()) {
            // on ajoute l'interface dans la liste
            // avec remplacement de toutes interfaces parent (promotion)
            // afin de n'avoir que les interfaces de plus plus haut niveau
            addInterface(klassInterfaces, klass);
            return klassInterfaces;
        }

        Class<?>[] interfaces = klass.getInterfaces();
        if (interfaces.length > 0) {
            // des interfaces detectees, on les ajoutes
            for (Class<?> interfac : interfaces) {
                getInterfaces(
                        (Class<? extends TopiaEntity>) interfac,
                        klassInterfaces
                );
            }
        }

        if (klass.getSuperclass() != null &&
            TopiaEntity.class.isAssignableFrom(klass.getSuperclass())) {
            getInterfaces(
                    (Class<? extends TopiaEntity>) klass.getSuperclass(),
                    klassInterfaces
            );
        }
        return klassInterfaces;
    }

    protected static void addInterface(
            Set<Class<? extends TopiaEntity>> interfaces,
            Class<? extends TopiaEntity> klass) {
        Iterator<Class<? extends TopiaEntity>> iterator = interfaces.iterator();
        for (; iterator.hasNext(); ) {
            Class<? extends TopiaEntity> next = iterator.next();
            if (next.isAssignableFrom(klass)) {
                // cette interface herite de klass
                // donc on la supprime
                iterator.remove();
                continue;
            }
            if (klass.isAssignableFrom(next)) {
                // cette interface herite de klass
                // donc on la supprime
                return;
            }

        }
        interfaces.add(klass);
    }

    @SuppressWarnings({"unchecked"})
    public static <E extends TopiaEntity> List<E> getEntities(
            TopiaPersistenceContext srcCtxt,
            List<E> entityList,
            boolean canBeNull) throws TopiaException {
        List<E> srcList = new ArrayList<E>(entityList.size());
        for (E e : entityList) {
            E e2 = (E) srcCtxt.findByTopiaId(e.getTopiaId());
            if (e2 == null && !canBeNull) {
                continue;
            }
            srcList.add(e2);
        }
        return srcList;
    }

    public static TopiaEntity[] getEntities(TopiaPersistenceContext srcCtxt,
                                            String... entityList)
            throws TopiaException {
        TopiaEntity[] srcList = new TopiaEntity[entityList.length];
        int index = 0;
        for (String id : entityList) {
            TopiaEntity e2 = srcCtxt.findByTopiaId(id);
            srcList[index++] = e2;
        }
        return srcList;
    }

    public static List<? extends TopiaEntity> getEntitiesList(
            TopiaPersistenceContext srcCtxt,
            String... entityList) throws TopiaException {
        List<TopiaEntity> srcList = new ArrayList<TopiaEntity>(entityList.length);
        for (String id : entityList) {
            TopiaEntity e2 = srcCtxt.findByTopiaId(id);
            srcList.add(e2);
        }
        return srcList;
    }

    public static void checkNotNull(String methodName,
                                    String parameterName,
                                    Object value) {
        Preconditions.checkNotNull(value,
                                   String.format("The method '%1$s' requires a non null parameter '%2$s'.", methodName, parameterName));
    }

    public static void checkParameters(Class<?>[] paramsType,
                                       Object... params) {
        checkSize(paramsType.length, params);
        for (int i = 0, j = paramsType.length; i < j; i++) {
            checkType(paramsType, i, params);
        }
    }

    public static void checkSize(int size, Object[] params) {
        Preconditions.checkArgument(params.length == size, String.format("Invalid parameters size. Expected:%d but was:%d", size, params.length));
    }

    public static void checkType(Class<?>[] paramsType,
                                 int index,
                                 Object[] params) {
        Class<?> requiredType = paramsType[index];
        Object value = params[index];
        Preconditions.checkArgument(value != null, String.format("The parameter at index %d must not be null", index));

        Class<?> foundType = value.getClass();
        Preconditions.checkArgument(requiredType.isAssignableFrom(foundType), String.format("The parameter at index %d is not the expected type. Expected:%s but was:%s",
                                                                                            index, requiredType, foundType));
    }

    /**
     * Given two names (representing two types of entity), obtains the
     * association table name in the format {@code X_Y} where X est the table
     * name smaller (in natural order on {@link String}).
     *
     * Example: from {@code A} and {@code B}, we get {@code A_B}.
     *
     * @param table1 the first table
     * @param table2 the second table
     * @return the normalized association table name
     * @since 2.6.12
     */
    public static String getNormalizedAssociationTableName(String table1,
                                                           String table2) {
        String result;
        if (table1.compareTo(table2) > 0) {
            // table1 > table 2
            result = table2 + "_" + table1;
        } else {
            // table2 > table1
            result = table1 + "_" + table2;
        }
        return result;
    }
}
