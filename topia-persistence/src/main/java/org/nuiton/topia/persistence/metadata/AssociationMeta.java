package org.nuiton.topia.persistence.metadata;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.persistence.util.EntityOperatorStore;
import org.nuiton.util.ObjectUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.Collection;

/**
 * Define the meta data of a entity association field.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class AssociationMeta<T extends TopiaEntityEnum> implements Serializable, MetaFilenameAware<T> {

    private static final long serialVersionUID = 1L;

    /** Association source entity type. */
    protected final T source;

    /** Association target entity type. */
    protected final T target;

    /** Name fo the association. */
    protected final String name;

    /** Operator of the source entity used to get / set associations. */
    protected transient EntityOperator<TopiaEntity> operator;

    protected static <T extends TopiaEntityEnum> AssociationMeta<T> newMeta(T source,
                                                                            T target,
                                                                            String name) {
        return new AssociationMeta<T>(source, target, name);
    }

    public AssociationMeta(T source,
                           T target,
                           String name) {
        this.source = source;
        this.target = target;
        this.name = name;
    }

    @Override
    public T getSource() {
        return source;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFilename() {
        return source.name() + "_" + name + CSV_EXTENSION;
    }

    @Override
    public File newFile(File container) {
        return new File(container, getFilename());
    }

    @Override
    public Writer newWriter(File container) {
        File file = newFile(container);
        try {
            return new OutputStreamWriter(new FileOutputStream(file),
                                          Charsets.UTF_8);
        } catch (FileNotFoundException e) {
            throw new TopiaException("Could not find file " + file);
        }
    }

    public T getTarget() {
        return target;
    }

    public TopiaEntity newEntity() {
        return ObjectUtil.newInstance(source.getImplementation());
    }

    public Object newAssociation() {
        return ObjectUtil.newInstance(target.getImplementation());
    }

    public Collection<TopiaEntity> getChilds(TopiaEntity entity) {
        Object o = getOperator().get(name, entity);
        return (Collection<TopiaEntity>) o;
    }

    public void setChilds(TopiaEntity entity, Collection<TopiaEntity> childs) {
        getOperator().addAllChild(name, entity, childs);
    }

    public boolean isChildEmpty(TopiaEntity entity) {
        boolean result = getOperator().isChildEmpty(name, entity);
        return result;
    }

    public EntityOperator<TopiaEntity> getOperator() {
        if (operator == null) {
            operator = EntityOperatorStore.getOperator(source);
        }
        return operator;
    }

    @Override
    public String toString() {
        return "<" + source + ":" + name + " " + target + ">";
    }
}
