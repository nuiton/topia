package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Object which helps to wrap some static generated code (entityEnum).
 *
 * This contract used to be named TopiaPersistenceHelper.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public interface TopiaEntityEnumProvider<T extends TopiaEntityEnum> {

    /**
     * Method that returns the TopiaEntityEnum corresponding to the given entity's class.
     *
     * @param type the entity's class
     * @param <E>  works only for TopiaEntity, thus E must extend TopiaEntity
     * @return the found TopiaEntityEnum. Should not be null.
     */
    <E extends TopiaEntity> T getEntityEnum(Class<E> type);

    /**
     * Method that returns the TopiaEntityEnum corresponding to the given entity's enum name.
     *
     * @param enumName the entity's enum name
     * @return the found TopiaEntityEnum. Should not be null.
     */
    T getEntityEnum(String enumName);

}
