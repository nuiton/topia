package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

/**
 * Utility class that provides useful methods for {@link org.nuiton.topia.persistence.TopiaEntity} manipulation.
 *
 * @author Brendan Le Ny : leny@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class TopiaEntities {

    /**
     * Creates a Guava's {@link com.google.common.base.Predicate} which tests if a
     * {@link org.nuiton.topia.persistence.TopiaEntity} has the given {@code topiaId}
     *
     * @param <E> type of entity
     * @param id  the expected topiaId
     * @return the created Predicate
     */
    public static <E extends TopiaEntity> Predicate<E> entityHasId(String id) {
        Predicate<String> equalsPredicate = Predicates.equalTo(id);
        Predicate<E> result = Predicates.compose(equalsPredicate, TopiaEntities.<E>getTopiaIdFunction());
        return result;
    }

    /**
     * Iterate over the given {@link java.lang.Iterable} looking for a {@link org.nuiton.topia.persistence.TopiaEntity}
     * having the given {@code topiaId}.
     *
     * @param entities the {@link java.lang.Iterable} instance to iterate through
     * @param id       the expected {@code topiaId}
     * @param <E>      the type of {@link org.nuiton.topia.persistence.TopiaEntity} of the given Iterable
     * @return the found {@link org.nuiton.topia.persistence.TopiaEntity} or null
     */
    public static <E extends TopiaEntity> E findByTopiaId(Iterable<E> entities, String id) {
        E result;
        if (entities == null) {
            result = null;
        } else {
            Optional<E> eOptional = Iterables.tryFind(entities, entityHasId(id));
            result = eOptional.orNull();
        }
        return result;
    }

    /**
     * Function to obtain {@link org.nuiton.topia.persistence.TopiaEntity#getTopiaId()} from any entity.
     *
     * @param <E> type of entity
     * @return FIXME
     * @since 2.6.12
     */
    public static <E extends TopiaEntity> Function<E, String> getTopiaIdFunction() {
        return new Function<E, String>() {

            @Override
            public String apply(TopiaEntity input) {
                return input == null ? null : input.getTopiaId();
            }
        };
    }

}
