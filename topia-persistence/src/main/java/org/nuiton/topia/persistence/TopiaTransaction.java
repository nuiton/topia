package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * This contract represents a transaction and provides methods to manipulate it.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaTransaction {

    /**
     * Applies all the modifications made to this context on the persistence device.
     *
     * Once commit is done, a new internal transaction is started, but you do not have to get a new instance of
     * {@link TopiaTransaction}.
     */
    void commit();

    /**
     * Cancels all the modifications made to this context, coming back to the state when this transaction has been
     * created (using {@link TopiaApplicationContext#newPersistenceContext()}) or previously rollbacked (using the
     * current method).
     *
     * Once rollback is done, a new internal transaction is started, but you do not have to get a new instance of
     * {@link TopiaTransaction}.
     */
    void rollback();

}
