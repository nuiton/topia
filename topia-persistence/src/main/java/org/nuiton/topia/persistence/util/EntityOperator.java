package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.google.common.base.Defaults;

/**
 * Un objet qui permet d'effecuter des operations de manipulation des donnees
 * dans les entites du type donne.
 *
 * L'objet connait la liste des proprietes et des associations du type donne et
 * permet de modifier ces valeurs :
 *
 * {@link #get(String, TopiaEntity)}
 * {@link #set(String, TopiaEntity, Object)}
 * {@link #copy(String, TopiaEntity, TopiaEntity)}
 * {@link #getChild(String, TopiaEntity, String)}
 * {@link #addChild(String, TopiaEntity, Object)}
 * {@link #removeChild(String, TopiaEntity, Object)}
 * ...
 *
 * D'autres methodes permettent d'effectuer des operations en lot (sur plusieurs
 * proprietes en meme temps) sur les proprietes :
 *
 * {@link #copyProperties(TopiaEntity, TopiaEntity, boolean, String...)}
 * {@link #obtainProperties(TopiaEntity, String...)}
 * {@link #clearProperties(TopiaEntity, String...)}
 *
 * Note : cet objet ne permet pas d'operation vers les bases.
 *
 * @param <B> type de l'entite
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class EntityOperator<B extends TopiaEntity> {

    /** Logger */
    private static Log log = LogFactory.getLog(EntityOperator.class);

    /** the constant of the entity */
    protected final TopiaEntityEnum contract;

    /** list of property names available on the entity. */
    protected List<String> properties;

    /**
     * list of property names available on the entity used in a natural ids or
     * marked as not-null.
     *
     * @since 2.6.9
     */
    protected Set<String> naturalIdsOnNotNullsProperties;

    /** list of association names available on the entity. */
    protected List<String> associationProperties;

    /** cache of getter methods. */
    protected Method[] getMethods;

    /** cache of setter methods. */
    protected Method[] setMethods;

    /** cache of assocation {@code get} methods. */
    protected Method[] childGetMethods;

    /** cache of assocation {@code add} methods. */
    protected Method[] childAddMethods;

    /** cache of assocation {@code addAll} methods. */
    protected Method[] childAddAllMethods;

    /** cache of assocation {@code remove} methods. */
    protected Method[] childRemoveMethods;

    /** cache of assocation {@code size} methods. */
    protected Method[] childSizeMethods;

    /** cache of assocation {@code isEmpty} methods. */
    protected Method[] childIsEmptyMethods;

    /** cache of assocation {@code clear}methods. */
    protected Method[] childClearMethods;

    protected EntityOperator(TopiaEntityEnum contract) {
        if (contract == null) {
            throw new NullPointerException(
                    "'contract' parameter can not be null!");
        }
        this.contract = contract;
        init();
    }

    /**
     * Recupere la valeur de la propriete donnee.
     *
     * Note : cela apellera la methode {@code getXXX()}.
     *
     * @param name le nom de la propriete
     * @param bean l'instance a interroger
     * @return la valeur de la propriete
     */
    public Object get(String name, B bean) {
        int index = checkPropertyIndex(name);
        Object result = invokeWithResult(getMethods[index], bean);
        return result;
    }

    /**
     * Positionner la valeur donne de la propriete donnee.
     *
     * Note : cela apellera la methode {@code setXXX(value)}.
     *
     * @param name  le nom de la propriete
     * @param bean  l'instance a mettre a jour
     * @param value la valeur a positionner
     */
    public void set(String name, B bean, Object value) {
        int index = checkPropertyIndex(name);
        invoke(setMethods[index], bean, value);
    }

    /**
     * Pour obtenir un dictionnaire de la clef naturelle (clef métier) du {@code bean}
     * donne.
     *
     * @param bean le bean a inspecter
     * @return le dictionnaire de la clef naturel du bean
     * @see TopiaEntityEnum#getNaturalIds()
     * @since 2.4.1
     */
    public Map<String, Object> getNaturalId(B bean) {
        Map<String, Object> result = new TreeMap<String, Object>();
        String[] ids = contract.getNaturalIds();
        if (ids != null) {
            for (String id : ids) {
                result.put(id, get(id, bean));
            }
        }
        return result;
    }

    /**
     * Pour obtenir un dictionnaire des propriétés marqués not-null du
     * {@code bean} donne.
     *
     * @param bean le bean a inspecter
     * @return le dictionnaire des propriétés marquées not-null du bean
     * @see TopiaEntityEnum#getNotNulls()
     * @since 2.6.9
     */
    public Map<String, Object> getNotNull(B bean) {
        Map<String, Object> result = new TreeMap<String, Object>();
        String[] ids = contract.getNotNulls();
        if (ids != null) {
            for (String id : ids) {
                result.put(id, get(id, bean));
            }
        }
        return result;
    }

    /**
     * Get all properties from a natural id or marked as not-null.
     *
     * @return all property names froma natural id or  marked as not-null
     * @since 2.6.9
     */
    public String[] getNaturalIdsOnNotNullsProperties() {
        return naturalIdsOnNotNullsProperties.toArray(new String[naturalIdsOnNotNullsProperties.size()]);
    }

    /**
     * Pour obtenir un dictionnaire des propriétés marqués not-null et la clef naturelle
     * du {@code bean} donne.
     *
     * Cette methode est utilisée pour faire un dao.create, pour s'assurer que
     * tout ce qui ne doit pas pas être à null est bien fourni à la création de
     * l'objet, sinon on obtient des erreurs.
     *
     * @param bean le bean a inspecter
     * @return le dictionnaire des propriétés marquées not-null + la clef
     *         naturelle du bean
     * @see TopiaEntityEnum#getNotNulls()
     * @see TopiaEntityEnum#getNaturalIds()
     * @since 2.6.9
     */
    public Map<String, Object> getNaturalIsdAndNotNulls(B bean) {
        Map<String, Object> result = new TreeMap<String, Object>();
        for (String id : naturalIdsOnNotNullsProperties) {
            result.put(id, get(id, bean));
        }
        return result;
    }

    /**
     * Copie une propriete de src vers dst.
     *
     * Note : cela apellera la methode {@code setXXX(value)}.
     *
     * @param name le nom de la propriete
     * @param from l'instance a interroger
     * @param dst  l'instance a mettre a jour
     */
    public void copy(String name, B from, B dst) {
        int index = checkPropertyIndex(name);
        Object value = invokeWithResult(getMethods[index], from);
        invoke(setMethods[index], dst, value);
    }

    /**
     * Positionner la valeur nulle de la propriete donnee.
     *
     * Note : cela apellera la methode {@code setXXX(nullValue)}.
     *
     * @param name le nom de la propriete
     * @param bean l'instance a mettre a jour
     */
    public void setNull(String name, B bean) {
        int index = checkPropertyIndex(name);
        Class<?> propertyType = getPropertyType(index);
        Method method = setMethods[index];
        Object nullValue = Defaults.defaultValue(propertyType);
        invoke(method, bean, nullValue);
    }

    /**
     * Recupere une entite d'association a partir de son id.
     *
     * Note : cela apellera la methode {@code getXXXByTopiaId(topiaId)}.
     *
     * @param name    le nom de la propriete d'association
     * @param bean    l'instance a interroger
     * @param topiaId l'id de l'entite recherchee
     * @return l'entite
     */
    public Object getChild(String name, B bean, String topiaId) {
        int index = checkAssociationPropertyIndex(name);
        Object result = invokeWithResult(childGetMethods[index], bean, topiaId);
        return result;
    }

    /**
     * Test s'il existe des entites d'association pour la propriete donnee.
     *
     * Note : cela apellera la methode {@code isXXXEmpty()}.
     *
     * @param name le nom de la propriete d'association
     * @param bean l'instance a interroger
     * @return {@code true} si pas d'entite d'association
     */
    public boolean isChildEmpty(String name, B bean) {
        int index = checkAssociationPropertyIndex(name);
        Boolean result = invokeWithResult(childIsEmptyMethods[index], bean);
        return result;
    }

    /**
     * Ajoute une entite d'association.
     *
     * Note : cela apellera la methode {@code addXXX(child)}.
     *
     * @param name  le nom de la propriete d'association
     * @param bean  l'instance a mettre a jour
     * @param child l'entite a ajouter
     */
    public void addChild(String name, B bean, Object child) {
        int index = checkAssociationPropertyIndex(name);
        invoke(childAddMethods[index], bean, child);
    }

    /**
     * Ajoute toutes les entites d'association.
     *
     * Note : cela apellera la methode {@code addXXX(child)}.
     *
     * @param name   le nom de la propriete d'association
     * @param bean   l'instance a mettre a jour
     * @param childs les entites a ajouter
     */
    public void addAllChild(String name, B bean, Collection<?> childs) {
        int index = checkAssociationPropertyIndex(name);
        invoke(childAddAllMethods[index], bean, childs);
    }

    /**
     * Retire une entite d'association.
     *
     * Note : cela apellera la methode {@code removeXXX(child)}.
     *
     * @param name  le nom de la propriete d'association
     * @param bean  l'instance a mettre a jour
     * @param child l'entite a retirer
     */
    public void removeChild(String name, B bean, Object child) {
        int index = checkAssociationPropertyIndex(name);
        invoke(childRemoveMethods[index], bean, child);
    }

    /**
     * Retourne le nombre d'entite d'association.
     *
     * Note : cela apellera la methode {@code sizeXXX(childs)}.
     *
     * @param name le nom de la propriete d'association
     * @param bean l'instance a mettre a jour
     * @return le nombre d'entite d'associaotion
     */
    public int sizeChild(String name, B bean) {
        int index = checkAssociationPropertyIndex(name);
        Integer result = invokeWithResult(childSizeMethods[index], bean);
        return result;
    }

    /**
     * Retire toutes les entites d'association.
     *
     * Note : cela apellera la methode {@code clearXXX(childs)}.
     *
     * @param name le nom de la propriete d'association
     * @param bean l'instance a mettre a jour
     */
    public void clearChild(String name, B bean) {
        int index = checkAssociationPropertyIndex(name);
        invoke(childClearMethods[index], bean);
    }

    /**
     * Recopie toutes les proprietes donnes depuis src vers dst.
     *
     * Note : si aucune propriete n'est donnee, on utilise toutes les proprietes
     * connues par l'operateur.
     *
     * @param from       l'entite a interroger
     * @param dst        l'entite a mettre a jour
     * @param tech       un drapeau pour recopier aussi les infos techniques
     * @param properties les proprietes a recopier
     */
    public void copyProperties(B from, B dst, boolean tech,
                               String... properties) {
//        if (from == null) {
//
//            from = newInstance();
//        }
        if (tech) {
            TopiaEntityHelper.bindTechnical(from, dst);
        }
        if (from == null) {
            // reset all fields
            clearProperties(dst, properties);
        } else {
            // copyProperties all fields
            Collection<String> names = getProperties(properties);

            for (String name : names) {
                copy(name, from, dst);
            }
        }
    }

    /**
     * Obtenir dans un dictionnaire, les valeurs des proprietes donnees.
     *
     * Si aucune proropiete n'est donne, alors on utilise toutes les proprietes
     * connu par l'operateur.
     *
     * @param from       l'object a scanne
     * @param properties les proprietes a retenir (vide si on les veut toutes)
     * @return le dictionnaire des valeurs des proprietes
     */
    public Map<String, Object> obtainProperties(B from, String... properties) {
        if (from == null) {
            return Collections.emptyMap();
        }
        Collection<String> names = getProperties(properties);

        Map<String, Object> result = new HashMap<String, Object>();

        for (String name : names) {
            Object read = get(name, from);
            if (read != null) {
                result.put(name, read);
            }
        }
        return result;
    }

    /**
     * Met a null toutes les proprietes donnees.
     *
     * Si aucune proropiete n'est donnee, alors on utilise toutes les proprietes
     * connu par l'operateur.
     *
     * @param from       l'object a scanne
     * @param properties les proprietes a retenir (vide si on les veut toutes)
     */
    public void clearProperties(B from, String... properties) {
        if (from == null) {
            return;
        }
        Collection<String> names = getProperties(properties);

        for (String name : names) {
            setNull(name, from);
        }
    }

    public List<String> getProperties() {
        return properties;
    }

    public List<String> getAssociationProperties() {
        return associationProperties;
    }

    public Class<?> getPropertyType(String name) {
        int index = checkPropertyIndex(name);
        return getPropertyType(index);
    }

    public Class<?> getAssociationPropertyType(String name) {
        int index = checkAssociationPropertyIndex(name);
        return getAssociationPropertyType(index);
    }

    @SuppressWarnings("unchecked")
    public Class<B> getClazz() {
        return (Class<B>) contract.getContract();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null &&
               obj instanceof EntityOperator<?> &&
               contract == ((EntityOperator<?>) obj).contract;
    }

    @Override
    public int hashCode() {
        return contract.hashCode();
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("could not clone " + this +
                                       " for reason : " + e.getMessage(), e);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        properties = null;
        naturalIdsOnNotNullsProperties = null;
        associationProperties = null;
        getMethods = null;
        setMethods = null;
        childGetMethods = null;
        childAddMethods = null;
        childAddAllMethods = null;
        childRemoveMethods = null;
        childSizeMethods = null;
        childClearMethods = null;
        childIsEmptyMethods = null;
        super.finalize();
    }

    @Override
    public String toString() {
        return super.toString() + '<' + getClazz() + '>';
    }

    protected Collection<String> getProperties(String[] properties) {
        Collection<String> names;
        if (properties.length == 0) {
            names = this.properties;
        } else {
            names = Arrays.asList(properties);
        }
        return names;
    }

    protected Class<?> getPropertyType(int index) {
        Method method = getMethods[index];
        return method.getReturnType();
    }

    protected Class<?> getAssociationPropertyType(int index) {
        Method method = childGetMethods[index];
        return method.getReturnType();
    }

    @SuppressWarnings("unchecked")
    protected B newInstance() {
        try {
            return (B) contract.getImplementation().newInstance();
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void init() {
        List<Method> getters = new ArrayList<Method>();
        List<Method> setters = new ArrayList<Method>();
        List<Method> childGetters = new ArrayList<Method>();
        List<Method> childAdders = new ArrayList<Method>();
        List<Method> childAddersAll = new ArrayList<Method>();
        List<Method> childRemovers = new ArrayList<Method>();
        List<Method> childSize = new ArrayList<Method>();
        List<Method> childIsEmpty = new ArrayList<Method>();
        List<Method> childClearers = new ArrayList<Method>();
        Set<Class<?>> explored = new HashSet<Class<?>>();
        properties = new ArrayList<String>();
        associationProperties = new ArrayList<String>();
        naturalIdsOnNotNullsProperties = new HashSet<String>();
        try {


            if (log.isDebugEnabled()) {
                log.debug("===== start for " + getClazz());
            }

            init(getClazz(), explored, properties, associationProperties,
                 getters, setters, childGetters, childAdders,
                 childAddersAll, childRemovers, childSize,
                 childClearers, childIsEmpty);

            if (!properties.isEmpty()) {
                int size = properties.size();

                getMethods = getters.toArray(new Method[size]);
                setMethods = setters.toArray(new Method[size]);
                properties = Collections.unmodifiableList(properties);
            }

            if (!associationProperties.isEmpty()) {
                int size = associationProperties.size();

                childGetMethods = childGetters.toArray(new Method[size]);
                childAddMethods = childAdders.toArray(new Method[size]);
                childAddAllMethods = childAddersAll.toArray(new Method[size]);
                childRemoveMethods = childRemovers.toArray(new Method[size]);
                childSizeMethods = childSize.toArray(new Method[size]);
                childIsEmptyMethods = childIsEmpty.toArray(new Method[size]);
                childClearMethods = childClearers.toArray(new Method[size]);
                associationProperties = Collections.unmodifiableList(
                        associationProperties);
            }

            if (contract.isUseNaturalIds()) {
                Collections.addAll(naturalIdsOnNotNullsProperties,
                                   contract.getNaturalIds());
            }
            if (contract.isUseNotNulls()) {
                Collections.addAll(naturalIdsOnNotNullsProperties,
                                   contract.getNotNulls());
            }
            if (log.isDebugEnabled()) {
                log.debug("===== end for " + getClazz() + " (" +
                          properties.size() + " properties, " +
                          associationProperties.size() + " associations)");
            }
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        } finally {
            getters.clear();
            setters.clear();
            childGetters.clear();
            childAdders.clear();
            childAddersAll.clear();
            childRemovers.clear();
            childSize.clear();
            childClearers.clear();
            childIsEmpty.clear();
            explored.clear();
        }
    }

    private Method getMethod(String name, int nbParams,
                             Method[] methodDescriptors) {
        for (Method m : methodDescriptors) {
            if (name.equals(m.getName()) &&
                m.getParameterTypes().length == nbParams) {
                return m;
            }
        }
        return null;
    }

    private int checkPropertyIndex(String name) {
        int index = properties.indexOf(name);
        if (index == -1) {
            throw new IllegalArgumentException("property " + name +
                                               " is unknown for " + this);
        }
        return index;
    }

    private int checkAssociationPropertyIndex(String name) {
        int index = associationProperties.indexOf(name);
        if (index == -1) {
            throw new IllegalArgumentException("association property " +
                                               name + " is unknown for " + this);
        }
        return index;
    }

    protected static void invoke(Method m, Object bean, Object... args) {
        try {
            if (log.isTraceEnabled()) {
                log.trace(bean.getClass() + "#" + m.getName());
            }
            m.invoke(bean, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings({"unchecked"})
    protected static <V> V invokeWithResult(Method m, Object bean,
                                            Object... args) {
        try {
            if (log.isTraceEnabled()) {
                log.trace(bean.getClass() + "#" + m.getName());
            }
            return (V) m.invoke(bean, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    protected void init(Class<?> entityClass,
                        Set<Class<?>> explored,
                        List<String> properties,
                        List<String> associationProperties,
                        List<Method> getters,
                        List<Method> setters,
                        List<Method> childGetters,
                        List<Method> childAdders,
                        List<Method> childAddersAll,
                        List<Method> childRemovers,
                        List<Method> childSize,
                        List<Method> childClearers,
                        List<Method> childIsEmpty) throws IntrospectionException {

        if (entityClass == null) {
            throw new NullPointerException("entityClass can not be null!");
        }
        if (!entityClass.isInterface()) {
            throw new IllegalArgumentException("entityClass parameter " +
                                               entityClass + " is not interface!");
        }
        if (explored.contains(entityClass)) {
            return;
        }

        explored.add(entityClass);

        if (log.isDebugEnabled()) {
            log.debug("enter " + entityClass);
        }

        BeanInfo beanInfo = Introspector.getBeanInfo(entityClass);
        PropertyDescriptor[] propertyDescriptors =
                beanInfo.getPropertyDescriptors();

        Method[] methods = entityClass.getMethods();

        for (PropertyDescriptor propertydescriptor : propertyDescriptors) {
            String propertyName = propertydescriptor.getName();
            if (propertydescriptor.getReadMethod() != null &&
                propertydescriptor.getWriteMethod() != null) {
                // on a detecte une propriete (lecture/ecriture)
                if (log.isDebugEnabled()) {
                    log.debug("detected property : " + propertyName);
                }
                properties.add(propertyName);
                getters.add(propertydescriptor.getReadMethod());
                setters.add(propertydescriptor.getWriteMethod());
            }

            Class<?> propertyType = propertydescriptor.getPropertyType();
            String cap = StringUtils.capitalize(propertyName);
            if (Collection.class.isAssignableFrom(propertyType)) {
                // on ne connait pas le type de l'association, on fait donc
                // une approximation (nom + nombre de params)
                // on pourrait tester que les parametre (ou le type de retour)
                // est un TopiaEntity TODO
                Method getMethod = getMethod("get" + cap + "ByTopiaId", 1,
                                             methods);
                Method addMethod = getMethod("add" + cap, 1, methods);
                Method addAllMethod = getMethod("addAll" + cap, 1, methods);
                Method removeMethod = getMethod("remove" + cap, 1, methods);
                Method sizeMethod = getMethod("size" + cap, 0, methods);
                Method clearMethod = getMethod("clear" + cap, 0, methods);
                Method isEmptyMethod = getMethod("is" + cap + "Empty", 0,
                                                 methods);
                if (addMethod != null &&
                    addAllMethod != null &&
                    removeMethod != null &&
                    sizeMethod != null &&
                    getMethod != null &&
                    clearMethod != null &&
                    isEmptyMethod != null) {
                    // on a detecte une association
                    if (log.isDebugEnabled()) {
                        log.debug("detected association : " + propertyName);
                    }
                    childGetters.add(getMethod);
                    childAdders.add(addMethod);
                    childAddersAll.add(addAllMethod);
                    childRemovers.add(removeMethod);
                    childSize.add(sizeMethod);
                    childClearers.add(clearMethod);
                    childIsEmpty.add(isEmptyMethod);
                    associationProperties.add(propertyName);
                }
            }
        }
        Class<?>[] interfaces = entityClass.getInterfaces();
        if (interfaces.length > 0) {
            for (Class<?> c : interfaces) {
                if (TopiaEntity.class.equals(c) ||
                    !TopiaEntity.class.isAssignableFrom(c) ||
                    explored.contains(c)) {
                    // on ne traite pas
                    continue;
                }
                init(c,
                     explored,
                     properties,
                     associationProperties,
                     getters,
                     setters,
                     childGetters,
                     childAdders,
                     childAddersAll,
                     childRemovers,
                     childSize,
                     childClearers,
                     childIsEmpty);
            }
        }

    }
}
