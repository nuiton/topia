package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;

import java.util.EventListener;

/**
 * To listen transaction operations such as commit and rollback.
 *
 * <b>Warning:</b> Must be attached to the current transaction.
 *
 * {@link org.nuiton.topia.persistence.support.TopiaListenableSupport} listens such listeners via javaBeans methods :
 * <ul>
 * <li>{@link org.nuiton.topia.persistence.support.TopiaListenableSupport#addTopiaTransactionListener(TopiaTransactionListener)}</li>
 * <li>{@link org.nuiton.topia.persistence.support.TopiaListenableSupport#removeTopiaTransactionListener(TopiaTransactionListener)}</li>
 * </ul>
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @see org.nuiton.topia.persistence.support.TopiaListenableSupport
 * @see org.nuiton.topia.persistence.event.TopiaTransactionEvent
 * @see TopiaFiresSupport#fireOnPostCommit(org.nuiton.topia.persistence.TopiaPersistenceContext)
 * @see TopiaFiresSupport#fireOnPostRollback(org.nuiton.topia.persistence.TopiaPersistenceContext)
 */
public interface TopiaTransactionListener extends EventListener {

    /**
     * Fired by {@link TopiaFiresSupport#fireOnPostCommit(org.nuiton.topia.persistence.TopiaPersistenceContext)}.
     *
     * Says after a commit was performed on listened transaction.
     *
     * @param event the transaction event
     */
    void commit(TopiaTransactionEvent event);

    /**
     * Fired by {@link TopiaFiresSupport#fireOnPostRollback(org.nuiton.topia.persistence.TopiaPersistenceContext)}.
     *
     * Says after a rollback was performed on listened transaction.
     *
     * @param event the transaction event
     */
    void rollback(TopiaTransactionEvent event);

}
