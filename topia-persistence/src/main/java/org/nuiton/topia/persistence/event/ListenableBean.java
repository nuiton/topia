package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;

/**
 * Contract to centralize usable methods to register/unregister PropertyChangeListener. This contract respects the
 * java beans expectations.
 *
 * If you do not need to match the java beans expectations, prefer using
 * {@link org.nuiton.topia.persistence.event.ListenableTopiaEntity}.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @see org.nuiton.topia.persistence.event.ListenableTopiaEntity
 */
public interface ListenableBean {

    //------------------------------------------------------------------------//
    //-- Post Write that respects the java beans expectations ----------------//
    //------------------------------------------------------------------------//

    /**
     * Adds a {@link java.beans.PropertyChangeListener} on any property writing.
     *
     * This method does the same than {@link ListenableTopiaEntity#addPostWriteListener(java.beans.PropertyChangeListener)}
     * and respects the java beans expectations.
     *
     * @param listener the listener to register
     * @see java.beans.PropertyChangeSupport
     * @see ListenableTopiaEntity#addPostWriteListener(java.beans.PropertyChangeListener)
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Adds a {@link java.beans.PropertyChangeListener} on the given property writing.
     *
     * This method does the same than {@link ListenableTopiaEntity#addPostWriteListener(String, java.beans.PropertyChangeListener)}
     * and respects the java beans expectations.
     *
     * @param propertyName the property name to listen
     * @param listener     the listener to register
     * @see java.beans.PropertyChangeSupport
     * @see ListenableTopiaEntity#addPostWriteListener(String, java.beans.PropertyChangeListener)
     */
    void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Remove the given {@link java.beans.PropertyChangeListener} registered for any property writing.
     *
     * This method does the same than {@link ListenableTopiaEntity#removePostWriteListener(java.beans.PropertyChangeListener)}
     * and respects the java beans expectations.
     *
     * @param listener the listener to unregister
     * @see java.beans.PropertyChangeSupport
     * @see ListenableTopiaEntity#removePostWriteListener(java.beans.PropertyChangeListener)
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Remove the given {@link java.beans.PropertyChangeListener} registered the given property writing.
     *
     * This method does the same than {@link ListenableTopiaEntity#removePostWriteListener(String, java.beans.PropertyChangeListener)}
     * and respects the java beans expectations.
     *
     * @param propertyName the property name to unregister writing
     * @param listener     the listener to unregister
     * @see java.beans.PropertyChangeSupport
     * @see ListenableTopiaEntity#removePostWriteListener(String, java.beans.PropertyChangeListener)
     */
    void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);

}
