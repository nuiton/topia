/**
 * <p>This package contains most of the needed contracts when using ToPIA.</p>
 *
 * <p>To manipulate ToPIA you need to understand the role of both interfaces
 * {@link org.nuiton.topia.persistence.TopiaApplicationContext} and
 * {@link org.nuiton.topia.persistence.TopiaPersistenceContext}. The implementation of these contracts has been
 * generated in your own project under the name "XxxTopiaApplicationContext" and "XxxTopiaPersistenceContext" where
 * "Xxx" is the name of your project.</p>
 *
 * <p>When configuring an application, you may need to use ToPIA defined constants that are listed in the
 * {@link org.nuiton.topia.persistence.TopiaConfigurationConstants} contract.</p>
 *
 * <p>You might want to use a cache in order to keep a list of the running application context, for this particular case
 * you can use {@link org.nuiton.topia.persistence.TopiaApplicationContextCache}.</p>
 *
 * <p>Each entity managed by ToPIA implements the {@link org.nuiton.topia.persistence.TopiaEntity} contract through the
 * abstract class {@link org.nuiton.topia.persistence.internal.AbstractTopiaEntity}. You can find some useful methods on
 * entities in the {@link org.nuiton.topia.persistence.TopiaEntities} class.</p>
 *
 * <p>Each Dao implements the {@link org.nuiton.topia.persistence.TopiaDao} contract through the abstract class
 * {@link org.nuiton.topia.persistence.internal.AbstractTopiaDao}.</p>
 *
 * <p>Each service implements the {@link org.nuiton.topia.persistence.TopiaService}</p>
 *
 */
package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as  
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
