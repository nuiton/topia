package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * This class lists the configuration variables that ToPIA expects
 *
 * Created on 12/20/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 * @deprecated if you want to dynamically create configuration through Java, use {@link org.nuiton.topia.persistence.TopiaConfigurationBuilder},
 *             if you need Hibernate constants, use {@link org.nuiton.topia.persistence.HibernateAvailableSettings}
 */
@Deprecated
public interface TopiaConfigurationConstants {

    /**
     * @deprecated use {@link TopiaConfiguration#getSchemaName()}
     */
    @Deprecated
    String CONFIG_DEFAULT_SCHEMA = TopiaConfigurationBuilder.CONFIG_DEFAULT_SCHEMA;

    /**
     * @deprecated use {@link TopiaConfiguration#getJdbcConnectionUser()}
     */
    @Deprecated
    String CONFIG_USER = TopiaConfigurationBuilder.CONFIG_USER;

    /**
     * @deprecated use {@link TopiaConfiguration#getJdbcConnectionPassword()}
     */
    @Deprecated
    String CONFIG_PASS = TopiaConfigurationBuilder.CONFIG_PASS;

    /**
     * @deprecated use {@link TopiaConfiguration#getJdbcDriverClass()}
     */
    @Deprecated
    String CONFIG_DRIVER = TopiaConfigurationBuilder.CONFIG_DRIVER;

    /**
     * @deprecated dialect is now guessed by Topia, you may use {@link TopiaConfiguration#getHibernateExtraConfiguration()}
     * and add a key using constant {@link HibernateAvailableSettings#DIALECT}.
     */
    @Deprecated
    String CONFIG_DIALECT = HibernateAvailableSettings.DIALECT;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_CONNECTION_PROVIDER = HibernateAvailableSettings.CONNECTION_PROVIDER;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_BYTECODE_PROVIDER = HibernateAvailableSettings.BYTECODE_PROVIDER;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_CURRENT_SESSION_CONTEXT_CLASS = HibernateAvailableSettings.CURRENT_SESSION_CONTEXT_CLASS;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_GENERATE_STATISTICS = HibernateAvailableSettings.GENERATE_STATISTICS;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_FORMAT_SQL = HibernateAvailableSettings.FORMAT_SQL;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_HBM2DDL_AUTO = HibernateAvailableSettings.HBM2DDL_AUTO;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_POOL_SIZE = HibernateAvailableSettings.POOL_SIZE;

    /**
     * @deprecated use same constant in {@link HibernateAvailableSettings}
     */
    @Deprecated
    String CONFIG_SHOW_SQL = HibernateAvailableSettings.SHOW_SQL;

    /**
     * @deprecated use {@link TopiaConfiguration#getJdbcConnectionUrl()}
     */
    @Deprecated
    String CONFIG_URL = TopiaConfigurationBuilder.CONFIG_URL;

    /**
     * @deprecated use {@link TopiaConfiguration#getTopiaIdFactory()}
     */
    @Deprecated
    String CONFIG_PERSISTENCE_TOPIA_ID_FACTORY_CLASS_NAME =
            TopiaConfigurationBuilder.CONFIG_PERSISTENCE_TOPIA_ID_FACTORY_CLASS_NAME;

    /**
     * @deprecated use {@link TopiaConfiguration#isInitSchema()}
     */
    @Deprecated
    String CONFIG_PERSISTENCE_INIT_SCHEMA = TopiaConfigurationBuilder.CONFIG_PERSISTENCE_INIT_SCHEMA;

}
