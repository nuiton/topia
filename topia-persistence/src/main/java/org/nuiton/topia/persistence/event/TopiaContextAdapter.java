package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Adapter pattern of {@link TopiaSchemaListener}.
 *
 * This implementation does nothing but permits developpers to use this adapater
 * without to have to implements all methods.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.4
 */
public class TopiaContextAdapter implements TopiaSchemaListener {

    @Override
    public void preDropSchema(TopiaContextEvent event) {
    }

    @Override
    public void postDropSchema(TopiaContextEvent event) {
    }

    @Override
    public void preCreateSchema(TopiaContextEvent event) {
    }

    @Override
    public void postCreateSchema(TopiaContextEvent event) {
    }

    @Override
    public void preUpdateSchema(TopiaContextEvent event) {
    }

    @Override
    public void postUpdateSchema(TopiaContextEvent event) {
    }

}
