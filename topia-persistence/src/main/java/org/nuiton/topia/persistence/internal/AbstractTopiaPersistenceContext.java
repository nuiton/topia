package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import jakarta.persistence.FlushModeType;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.Configuration;
import org.hibernate.metamodel.MappingMetamodel;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaReplicationDestination;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaReplicationSupport;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaSqlSupport;
import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * Abstract implementation of the TopiaPersistenceContext. This class will be extended by a generated one in order to
 * generate getXxxDao methods.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public abstract class AbstractTopiaPersistenceContext implements TopiaPersistenceContext {

    private static final Log log = LogFactory.getLog(AbstractTopiaPersistenceContext.class);

    /**
     * Already loaded DAO cache within this persistence context
     */
    protected Map<Class<? extends TopiaEntity>, TopiaDao<? extends TopiaEntity>> daoCache = Maps.newConcurrentMap();

    protected HibernateTopiaReplicationSupport hibernateTopiaReplicationSupport;

    /**
     * Used to affect a new topiaId when create is called.
     */
    protected TopiaIdFactory topiaIdFactory;

    /**
     * The shared instance of TopiaHibernateSessionRegistry
     */
    protected TopiaHibernateSessionRegistry sessionRegistry;

    /**
     * Object that handles each event propagation. Listeners are registered inside this instance.
     */
    protected TopiaFiresSupport firesSupport;

    /**
     * This subclass of TopiaHibernateSupport is made to be used only internally within this persistence context. This
     * instance is created by the persistence context itself.
     */
    protected InternalTopiaHibernateSupport hibernateSupport;

    /**
     * This instance of TopiaJpaSupport is created by the persistence context itself. It is actually using the
     * TopiaHibernateSupport instance.
     */
    protected TopiaJpaSupport jpaSupport;

    /**
     * This instance of TopiaSqlSupport is created by the persistence context itself. It is actually using the
     * TopiaHibernateSupport instance.
     */
    protected TopiaSqlSupport sqlSupport;

    /**
     * Flog used to check if this persistence context is closed
     */
    protected boolean closed = false;

    /**
     * Creating a new TopiaPersistenceContext is equivalent to creating a new transaction
     *
     * @param parameter everything needed (specific parameter-object)
     */
    public AbstractTopiaPersistenceContext(AbstractTopiaPersistenceContextConstructorParameter parameter) {

        this.topiaIdFactory = parameter.getTopiaIdFactory();
        this.sessionRegistry = parameter.getSessionRegistry();
        this.firesSupport = new TopiaFiresSupport(parameter.getApplicationFiresSupport());

        // Hibernate support can be created using the given hibernateProvider
        this.hibernateSupport = new InternalTopiaHibernateSupport(parameter.getHibernateProvider());

        // Now starts the transaction, as this persistenceContext IS the TopiaTransaction
        startTransaction();

        // Create the different supports that may be needed by the DAOs
        this.jpaSupport = new HibernateTopiaJpaSupport(hibernateSupport, firesSupport, parameter.getSlowQueriesThreshold());
        this.sqlSupport = new HibernateTopiaSqlSupport(hibernateSupport, parameter.getSlowQueriesThreshold());
    }

    public TopiaHibernateSupport getHibernateSupport() {
        return hibernateSupport;
    }

    public TopiaSqlSupport getSqlSupport() {
        return sqlSupport;
    }

    @Override
    public TopiaIdFactory getTopiaIdFactory() {
        return topiaIdFactory;
    }

    @Override
    public TopiaFiresSupport getTopiaFiresSupport() {
        return firesSupport;
    }

    /**
     * This subclass of TopiaHibernateSupport is made to be used only internally within this persistence context. This
     * class only acts as an information container (as a structure does).
     */
    protected class InternalTopiaHibernateSupport implements TopiaHibernateSupport {

        protected HibernateProvider hibernateProvider;
        protected Session hibernateSession;

        protected InternalTopiaHibernateSupport(HibernateProvider hibernateProvider) {
            this.hibernateProvider = hibernateProvider;
        }

        public void setHibernateSession(Session hibernateSession) {
            this.hibernateSession = hibernateSession;
        }

        @Override
        public Session getHibernateSession() {
            Preconditions.checkState(hibernateSession != null, "Session is not yet initialized");
            return hibernateSession;
        }

        @Override
        public SessionFactory getHibernateFactory() {
            return hibernateProvider.getSessionFactory();
        }

        @Override
        public Metadata getHibernateMetadata() {
            return hibernateProvider.getMetaData();
        }

        @Override
        public Configuration getHibernateConfiguration() {
            return hibernateProvider.getHibernateConfiguration();
        }
    }

    protected void startTransaction() throws TopiaException {

        SessionFactory factory = hibernateSupport.getHibernateFactory();
        Session result = factory.openSession();
        hibernateSupport.setHibernateSession(result);

        // Never synchronise the entity state (flush) until the user use ".commit()"
        result.setFlushMode(FlushModeType.COMMIT);

        // tchemit 2010-12-06 propagates the value of the flag
//        result.useFlushMode = useFlushMode;

        sessionRegistry.register(result, this);

        // 20060926 poussin ajouter pour voir si cas regle les problemes de
        // deadlock h2. Conclusion, il faut bien ouvrir une transaction
        // maintenant, sinon lorsque l'on fait des acces a la base, une
        // transaction par defaut est utilisé mais elle n'est jamais vraiment
        // fermé ce qui pose des problemes de lock sur les tables.
        try {
            result.beginTransaction();
        } catch (Exception eee) {

            // Transaction may be opened: make sure it is closed then throw an exception
            try {
                result.close();
                sessionRegistry.unregister(result);
            } catch (HibernateException e1) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close hibernate session", e1);
                }
            }

            String message = String.format("An error occurs while asking a new transaction: %1$s", eee.getMessage());
            throw new TopiaException(message, eee);
        }

        // fire event
        getTopiaFiresSupport().fireOnBeginTransaction(this);
    }

    protected void checkNotClosed() {
        Preconditions.checkState( ! closed, "persistence context " + this + " is closed");
    }

    @Override
    public <E extends TopiaEntity> E findByTopiaId(String topiaId) {
        checkNotClosed();

        Class<E> entityClass = getTopiaIdFactory().getClassName(topiaId);
        TopiaDao<E> dao = getDao(entityClass);
        E result = dao.forTopiaIdEquals(topiaId).findUnique();
        return result;
    }

    @Override
    public void update(TopiaEntity entity) {
        checkNotClosed();

        String topiaId = entity.getTopiaId();
        Class<TopiaEntity> entityClass = getTopiaIdFactory().getClassName(topiaId);
        TopiaDao<TopiaEntity> dao = getDao(entityClass);
        dao.update(entity);

    }

    @Override
    public void delete(TopiaEntity entity) {
        checkNotClosed();

        String topiaId = entity.getTopiaId();
        Class<TopiaEntity> entityClass = getTopiaIdFactory().getClassName(topiaId);
        TopiaDao<TopiaEntity> dao = getDao(entityClass);
        dao.delete(entity);

    }

    @Override
    public <E extends TopiaEntity> void deleteAll(Iterable<E> entities) {
        for (E entity : entities) {
            delete(entity);
        }
    }

    @Override
    public <E extends TopiaEntity> TopiaDao<E> getDao(Class<E> entityClass) {
        Preconditions.checkArgument(entityClass != null, "The method 'getDao' requires a non null 'entityClass' parameter");

        SessionFactory hibernateFactory = hibernateSupport.getHibernateFactory();
        Preconditions.checkState(hibernateFactory != null, "The Hibernate SessionFactory is null, please initialize");

        //TODO Use this code for hibernate >= 5.2
//        boolean found = false;
//        try {
//            EntityType<E> entity = hibernateFactory.getClassMetadata(entityClass).;
//            found = entity != null;
//        } catch (Exception e) {
//        }
//        if (!found) {
//            try {
//                EntityType<Object> entity = hibernateFactory.getMetamodel().entity(entityClass.getName() + "Impl");
//                found = entity != null;
//            } catch (Exception e) {
//            }
//        }
//        if (!found) {
//            try {
//                EntityType<Object> entity = hibernateFactory.getMetamodel().entity(entityClass.getName() + "Abstract");
//                found = entity != null;
//            } catch (Exception e) {
//            }
//        }

        boolean found = ((MappingMetamodel) hibernateFactory.getMetamodel()).getEntityDescriptor(entityClass.getName() + "Impl") != null
                || ((MappingMetamodel) hibernateFactory.getMetamodel()).getEntityDescriptor(entityClass.getName() + "Abstract") != null;

        if (! found) {

            if (log.isInfoEnabled()) {
                String format = "List of supported persistence classes: %s";
                String message = String.format(format, (Object) hibernateFactory.getStatistics().getEntityNames());
                log.info(message);
            }
            String format = "The following entity type %s is not managed by this context, you probably forgot to declare it.";
            String message = String.format(format, entityClass.getName());
            throw new TopiaException(message);
        }

        TopiaDao<E> dao = (TopiaDao<E>) daoCache.get(entityClass);
        if (dao == null) {

            // Looking for specialized DAO
            // This DAO is supposed to exist, as created by generation
            String daoClassName = entityClass.getName() + "TopiaDao";
            try {
                Class<TopiaDao<E>> daoClass = (Class<TopiaDao<E>>) Class.forName(daoClassName);
                dao = daoClass.getConstructor().newInstance();
            } catch (InstantiationException e) {
                log.fatal("Unable to instantiate DAO class " + daoClassName, e);
                throw new TopiaException("unable to instantiate DAO class " + daoClassName, e);
            } catch (IllegalAccessException e) {
                log.fatal("Unable to instantiate DAO class " + daoClassName, e);
                throw new TopiaException("unable to instantiate DAO class " + daoClassName, e);
            } catch (InvocationTargetException e) {
                log.fatal("Unable to instantiate DAO class " + daoClassName, e);
                throw new TopiaException("unable to instantiate DAO class " + daoClassName, e);
            } catch (NoSuchMethodException e) {
                log.fatal("Unable to instantiate DAO class " + daoClassName, e);
                throw new TopiaException("unable to instantiate DAO class " + daoClassName, e);
            } catch (ClassNotFoundException e) {
                log.fatal("Unable to find DAO class " + daoClassName, e);
                throw new TopiaException("unable to find DAO class " + daoClassName, e);
            }

            if (dao instanceof AbstractTopiaDao) {
                AbstractTopiaDao abstractTopiaDao = (AbstractTopiaDao) dao;
                abstractTopiaDao.init(jpaSupport, hibernateSupport, sqlSupport, topiaIdFactory, firesSupport, this);
            }

            daoCache.put(entityClass, dao);

        }

        return dao;
    }

    @Override
    public <E extends TopiaEntity, D extends TopiaDao<E>> D getDao(Class<E> entityClass, Class<D> daoClass) {
        TopiaDao<E> dao = getDao(entityClass);
        D result = (D) dao;
        return result;
    }

    @Override
    public void commit() {
        checkNotClosed();

        try {

            Session hibernateSession = hibernateSupport.getHibernateSession();

            Transaction transaction = hibernateSession.getTransaction();
            hibernateSession.flush();
            transaction.commit();

            getTopiaFiresSupport().fireOnPostCommit(this);

            hibernateSession.beginTransaction();

        } catch (Exception eee) {
            String message = String.format("An error occurred during commit operation: %1$s", eee.getMessage());
            throw new TopiaException(message, eee);
        }
    }

    @Override
    public void rollback() {
        checkNotClosed();
        // Rollback and create a new transaction
        rollback0(true);
    }

    protected void rollback0(boolean beginAfterRollback) {
        try {
            Session hibernateSession = hibernateSupport.getHibernateSession();

            Transaction transaction = hibernateSession.getTransaction();
            hibernateSession.clear();
            transaction.rollback();
            hibernateSession.close();

            sessionRegistry.unregister(hibernateSession);

            if (beginAfterRollback) {
                // it's very important to change the session after rollback
                // otherwise there are many error during next Entity's modification
                hibernateSession = hibernateSupport.getHibernateFactory().openSession();
                hibernateSupport.setHibernateSession(hibernateSession);
                hibernateSession.setFlushMode(FlushModeType.COMMIT);

                sessionRegistry.register(hibernateSession, this);

                hibernateSession.beginTransaction();
            }

            getTopiaFiresSupport().fireOnPostRollback(this);

        } catch (HibernateException eee) {
            String message = String.format("An error occurred during rollback operation: %1$s", eee.getMessage());
            throw new TopiaException(message, eee);
        }
    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public void close() {

        checkNotClosed();

        if (log.isDebugEnabled()) {
            log.debug("will close " + this);
        }

        closed = true;

        // Rollback and do not create a new transaction
        rollback0(false);

        // Now close the current Hibernate session
        Session hibernateSession = hibernateSupport.getHibernateSession();
        Preconditions.checkState(!hibernateSession.isOpen(), "Session should be closed after rollback0(false)");

        if (log.isDebugEnabled()) {
            log.debug(this + " closed");
        }
    }

    protected HibernateTopiaReplicationSupport getHibernateTopiaReplicationSupport() {
        if (hibernateTopiaReplicationSupport == null) {
            hibernateTopiaReplicationSupport = new HibernateTopiaReplicationSupport(hibernateSupport);
        }
        return hibernateTopiaReplicationSupport;
    }

    @Override
    public void replicate(TopiaEntity entity) {
        getHibernateTopiaReplicationSupport().replicate(entity);
    }

    @Override
    public <T extends TopiaEntity> void replicateEntities(TopiaReplicationDestination topiaReplicationDestination, List<T> entities) throws IllegalArgumentException {
        getHibernateTopiaReplicationSupport().replicateEntities(topiaReplicationDestination, entities);
    }

    @Override
    public <T extends TopiaEntity> void replicateEntity(TopiaReplicationDestination topiaReplicationDestination, T entity) throws IllegalArgumentException {
        getHibernateTopiaReplicationSupport().replicateEntity(topiaReplicationDestination, entity);
    }

    @Override
    public void replicate(TopiaReplicationDestination topiaReplicationDestination, Object... entityAndCondition) throws IllegalArgumentException {
        getHibernateTopiaReplicationSupport().replicate(topiaReplicationDestination, entityAndCondition);
    }
}
