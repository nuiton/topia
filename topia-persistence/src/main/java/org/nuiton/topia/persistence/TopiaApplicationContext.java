package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.nuiton.topia.persistence.support.TopiaListenableSupport;
import org.nuiton.topia.persistence.support.TopiaServiceSupport;

import java.io.Closeable;
import java.util.List;

/**
 * <p>This is the main entry point for ToPIA. This interface represents the root context of an application.</p>
 *
 * <p>Most of the time, there is only one instance of this context during the entire lifecycle of a application.</p>
 *
 * <p>This contract provides facility to :</p>
 * <ul>
 * <li>create a new {@link org.nuiton.topia.persistence.TopiaPersistenceContext};</li>
 * <li>get meta information about the application (model name, version);</li>
 * <li>get meta information about the entities;</li>
 * <li>do schema related operations;</li>
 * <li>shutdown the application</li>
 * </ul>
 *
 * <p>This contract has an abstract implementation
 * ({@link org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext}) which is itself extended by a
 * generated {@code AbstractXxxTopiaApplicationContext} (where {@code Xxx} is the project name), itself
 * extended by {@code XxxTopiaApplicationContext}.</p>
 *
 * <p>The full hierarchy is :</p> <pre>
 * TopiaApplicationContext (this contract)
 *  \--AbstractTopiaApplicationContext
 *      \--AbstractXxxTopiaApplicationContext (generated)
 *          \--XxxTopiaApplicationContext (generated)
 * </pre>
 * <p>(where {@code Xxx} is the project name)</p>
 *
 * <p>Created on 12/20/13.</p>
 *
 * @author Tony Chemit - tchemit@codelutin.com
 * @since 3.0
 */
public interface TopiaApplicationContext<K extends TopiaPersistenceContext>
        extends TopiaListenableSupport, TopiaServiceSupport, Closeable {

    /**
     * Creates a new instance of XxxTopiaPersistenceContext (where {@code Xxx} is the project name). This is the
     * method to use in order to start a new transaction.
     *
     * @return a newly created instance of your project's specific
     * {@link org.nuiton.topia.persistence.TopiaPersistenceContext}
     * @see org.nuiton.topia.persistence.TopiaPersistenceContext
     */
    K newPersistenceContext();

    String getModelVersion();

    String getModelName();

    <T extends TopiaEntity> Class<T> getContractClass(Class<T> klass);

    <T extends TopiaEntity> Class<T> getImplementationClass(Class<T> klass);

    Class<? extends TopiaEntity>[] getContractClasses();

    TopiaConfiguration getConfiguration();

    /**
     * Get the list of entities (ToPIA or not) that Hibernate has to handle. By default it only contains ToPIA entities
     * but you may override it to add any other Hibernate entity.
     */
    List<Class<?>> getPersistenceClasses();

    void initSchema();

    boolean isSchemaEmpty();

    boolean isTableExists(Class<?> clazz);

    /**
     *
     * @return the default schema name.
     * @deprecated since 3.0.1, prefer use {@link #getSchemaNames()}.
     */
    @Deprecated
    String getSchemaName();

    /**
     * @return all schema names used by persistent entities.
     * @since 3.0.1
     */
    ImmutableSet<String> getSchemaNames();

    void createSchema();

    void showCreateSchema();

    void updateSchema();

    void dropSchema();

    /**
     * @return true is the application context is opened (ie not yet {@link #isClosed()})
     */
    boolean isOpened();

    /**
     * @return true is the application context is already closed, false otherwise
     */
    boolean isClosed();

    /**
     * Method to call when the application is going for shutdown. It will trigger the shutdown of all the opened
     * children {@link org.nuiton.topia.persistence.TopiaPersistenceContext}
     */
    @Override
    void close();
}
