package org.nuiton.topia.persistence.metadata;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaEntityEnumProvider;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.persistence.util.EntityOperatorStore;
import org.nuiton.util.ObjectUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderModelBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Define metas of a given db table.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class TableMeta<T extends TopiaEntityEnum> implements Serializable, Iterable<ColumnMeta>, MetaFilenameAware<T> {

    private static final long serialVersionUID = 1L;

    public static <T extends TopiaEntityEnum> TableMeta<T> newMeta(T entityEnum,
                                                                   TopiaEntityEnumProvider<T> typeProvider) {
        return new TableMeta<T>(entityEnum, typeProvider);
    }

    /** Type of the entity. */
    protected final T source;

    /** List of columns of the entity. */
    protected List<ColumnMeta> columns;

    /** List of dependencies (says all property with a topiaentity type) */
    protected final Set<T> dependencies;

    /** List of associations of the entity. */
    protected List<AssociationMeta<T>> associations;

    /** Binder used to copy entities (lazy loaded). */
    protected Binder<TopiaEntity, TopiaEntity> binder;

    /** Entity operator used in generic algorithms. */
    protected transient EntityOperator<TopiaEntity> operator;

    protected boolean useNaturalIdsOrNotNulls;

    @Override
    public T getSource() {
        return source;
    }

    @Override
    public String getName() {
        return source.name();
    }

    @Override
    public String getFilename() {
        return source.name() + ".csv";
    }

    @Override
    public File newFile(File container) {
        return new File(container, getFilename());
    }

    @Override
    public Writer newWriter(File container) {
        File file = newFile(container);
        try {
            return new OutputStreamWriter(new FileOutputStream(file),
                                          Charsets.UTF_8);
        } catch (FileNotFoundException e) {
            throw new TopiaException("Could not find file " + file);
        }
    }

    @Override
    public String toString() {
        return "<" + source + ">";
    }

    public Class<? extends TopiaEntity> getEntityType() {
        return source.getContract();
    }

    public ColumnMeta getColumns(String columnName) {
        Preconditions.checkNotNull(columnName);
        ColumnMeta result = null;
        for (ColumnMeta columnMeta : getColumns()) {
            if (columnName.equals(columnMeta.getName())) {
                result = columnMeta;
                break;
            }
        }
        return result;
    }

    public String[] getColumnNamesAsArray() {
        List<String> columnNames = getColumnNames();
        return columnNames.toArray(new String[columnNames.size()]);
    }

    public List<String> getColumnNames() {
        List<String> result = Lists.newLinkedList();
        for (ColumnMeta columnMeta : getColumns()) {
            result.add(columnMeta.getName());
        }
        return result;
    }

    public List<ColumnMeta> getColumns() {
        return columns;
    }

    public List<AssociationMeta<T>> getAssociations() {
        return associations;
    }

    public Set<T> getDependencies() {
        return dependencies;
    }

    public AssociationMeta<T> getAssociations(String name) {
        AssociationMeta<T> result = null;
        for (AssociationMeta<T> meta : getAssociations()) {
            if (name.equals(meta.getName())) {
                result = meta;
                break;
            }
        }
        return result;
    }

    public void copy(TopiaEntity source, TopiaEntity target) {
        getBinder().copy(source, target);
    }

    public Map<String, Object> prepareCreate(TopiaEntity bean,
                                             String topiaId) {
        Map<String, Object> result = getOperator().getNaturalIsdAndNotNulls(bean);
        if (topiaId != null) {
            result.put(TopiaEntity.PROPERTY_TOPIA_ID, topiaId);
        }
        return result;
    }

    @Override
    public Iterator<ColumnMeta> iterator() {
        return getColumns().iterator();
    }

    public TopiaEntity newEntity() {
        return ObjectUtil.newInstance(source.getImplementation());
    }

    protected Binder<TopiaEntity, TopiaEntity> getBinder() {
        if (binder == null) {
            BinderModelBuilder<TopiaEntity, TopiaEntity> binderModelBuilder =
                    (BinderModelBuilder<TopiaEntity, TopiaEntity>) BinderModelBuilder.newEmptyBuilder(getEntityType());
            for (ColumnMeta columnMeta : this) {
                binderModelBuilder.addSimpleProperties(columnMeta.getName());
            }
            binder = binderModelBuilder.toBinder();
        }
        return binder;
    }

    protected TableMeta(T source, TopiaEntityEnumProvider<T> typeProvider) {
        Preconditions.checkNotNull(source);
        this.source = source;

        associations = Lists.newArrayList();
        columns = Lists.newLinkedList();
        Set<T> deps = Sets.newHashSet();

        // fill associations
        List<String> associationProperties = getOperator().getAssociationProperties();
        for (String property : associationProperties) {
            Class<?> propertyType = getOperator().getAssociationPropertyType(property);
            if (TopiaEntity.class.isAssignableFrom(propertyType)) {

                // only use it for entity
                T targetEnum = typeProvider.getEntityEnum((Class<TopiaEntity>) propertyType);

                AssociationMeta<T> meta = AssociationMeta.newMeta(source,
                                                                  targetEnum,
                                                                  property
                );
                associations.add(meta);
            }
        }

        // fill properties (remove all asociations)
        List<String> properties = Lists.newArrayList(getOperator().getProperties());
        properties.removeAll(associationProperties);
        for (String property : properties) {
            Class<?> propertyType = getOperator().getPropertyType(property);
            ColumnMeta meta = ColumnMeta.newMeta(property, propertyType);
            columns.add(meta);
            if (meta.isFK()) {
                T dependency = typeProvider.getEntityEnum((Class<TopiaEntity>) propertyType);
                deps.add(dependency);
            }
        }
        dependencies = deps;

        useNaturalIdsOrNotNulls = source.isUseNotNulls() ||
                                  source.isUseNaturalIds();
    }

    public EntityOperator<TopiaEntity> getOperator() {
        if (operator == null) {
            operator = EntityOperatorStore.getOperator(source);
        }
        return operator;
    }
}
