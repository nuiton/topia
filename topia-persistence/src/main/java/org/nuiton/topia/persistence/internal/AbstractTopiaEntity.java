package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.lang.ref.WeakReference;
import java.util.Date;

import com.google.common.base.MoreObjects;

/**
 * Base class of each entity. It contains the common attributes and a part of the entities event support.
 *
 * This class does not realize {@link org.nuiton.topia.persistence.event.ListenableTopiaEntity} but it implements its
 * methods to lighten base implementation.
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class AbstractTopiaEntity implements TopiaEntity {

    private static final long serialVersionUID = -7458577454878852241L;

    /**
     * Default instance used as fallback by the entities if they are out of a ToPIA runtime scope.
     */
    protected static final TopiaFiresSupport DEFAULT_INSTANCE = new TopiaFiresSupport();

    protected String topiaId;

    protected long topiaVersion;

    protected Date topiaCreateDate = new Date();

    transient protected boolean deleted;

    /**
     * A potential instance that may be injected by the Dao or retrieved in a contextable entity. The TopiaFiresSupport
     * instance can be linked to the {@link org.nuiton.topia.persistence.TopiaPersistenceContext} instance, thus its
     * life cycle may be shorter than the entity. This is why it is kept as a {@link java.lang.ref.WeakReference}.
     *
     * If not present, the entity will use the {@code DEFAULT_INSTANCE} as a fallback TopiaFiresSupport.
     */
    transient protected WeakReference<TopiaFiresSupport> firesSupport;

    transient protected VetoableChangeSupport preReadListeners;

    transient protected PropertyChangeSupport postReadListeners;

    transient protected VetoableChangeSupport preWriteListeners;

    transient protected PropertyChangeSupport postWriteListeners;

    public void setFiresSupport(TopiaFiresSupport firesSupport) {
        this.firesSupport = new WeakReference<TopiaFiresSupport>(firesSupport);
    }

    protected TopiaFiresSupport getFiresSupportOrNull() {
        TopiaFiresSupport result = firesSupport == null ? null : firesSupport.get();
        if (result == null) { // First call or the weak reference has expired
            if (this instanceof TopiaEntityContextable) {
                TopiaEntityContextable contextable = (TopiaEntityContextable) this;
                AbstractTopiaDao entityDao = (AbstractTopiaDao) contextable.getGenericEntityDao();
                // Dao may be null if the entity isn't managed by ToPIA (ie. created via "new XxxImpl()")
                if (entityDao != null) {
                    result = entityDao.getTopiaFiresSupport();
                    setFiresSupport(result);
                }
            }
        }
        return result;
    }

    protected TopiaFiresSupport getFiresSupport() {
        TopiaFiresSupport result = MoreObjects.firstNonNull(getFiresSupportOrNull(), DEFAULT_INSTANCE);
        return result;
    }

    /**
     * Initialize {@link #postReadListeners} at first use or after deserialisation.
     *
     * @param create indicates if the PropertyChangeSupport can be created if it does not exist
     * @return postReadListeners
     */
    protected PropertyChangeSupport getPostReadListeners(boolean create) {
        if (postReadListeners == null && create) {
            postReadListeners = new PropertyChangeSupport(this);
        }
        return postReadListeners;
    }

    /**
     * Initialize {@link #postWriteListeners} at first use or after deserialisation.
     *
     * @param create indicates if the PropertyChangeSupport can be created if it does not exist
     * @return postWriteListeners
     */
    protected PropertyChangeSupport getPostWriteListeners(boolean create) {
        if (postWriteListeners == null && create) {
            postWriteListeners = new PropertyChangeSupport(this);
        }
        return postWriteListeners;
    }

    /**
     * Initialize {@link #preReadListeners} at first use or after deserialisation.
     *
     * @param create indicates if the VetoableChangeSupport can be created if it does not exist
     * @return readVetoables
     */
    protected VetoableChangeSupport getPreReadListeners(boolean create) {
        if (preReadListeners == null && create) {
            preReadListeners = new VetoableChangeSupport(this);
        }
        return preReadListeners;
    }

    /**
     * Initialize {@link #preWriteListeners} at first use or after deserialisation.
     *
     * @param create indicates if the VetoableChangeSupport can be created if it does not exist
     * @return preWriteListeners
     */
    protected VetoableChangeSupport getPreWriteListeners(boolean create) {
        if (preWriteListeners == null && create) {
            preWriteListeners = new VetoableChangeSupport(this);
        }
        return preWriteListeners;
    }

    @Override
    public String getTopiaId() {
        return topiaId;
    }

    @Override
    public void setTopiaId(String v) {
        topiaId = v;
    }

    @Override
    public long getTopiaVersion() {
        return topiaVersion;
    }

    @Override
    public void setTopiaVersion(long v) {
        topiaVersion = v;
    }

    @Override
    public Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    @Override
    public void setTopiaCreateDate(Date topiaCreateDate) {
        this.topiaCreateDate = topiaCreateDate;
    }

    @Override
    public boolean isPersisted() {
        // Is or was the entity persisted ?
        boolean result = topiaId != null;
        // Is the entity deleted ?
        result &= !deleted;
        return result;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void notifyDeleted() {
        deleted = true;
    }

    /**
     * We are using the {@code topiaCreateDate} for the hashCode because it does not change through time.
     */
    @Override
    public int hashCode() {
        Date date = getTopiaCreateDate();
        //TC-20100220 : il se peut que la date de creation soit nulle
        // lorsque l'entite est utilise comme objet d'edition d'un formulaire
        // par exemple...
        int result = date == null ? 0 : date.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TopiaEntity)) {
            return false;
        }
        TopiaEntity other = (TopiaEntity) obj;
        if (getTopiaId() == null || other.getTopiaId() == null) {
            return false;
        }
        boolean result = getTopiaId().equals(other.getTopiaId());
        return result;
    }

    protected void fireOnPreRead(String propertyName, Object value) {
        VetoableChangeSupport listeners = getPreReadListeners(false);
        if (listeners != null) {
            TopiaFiresSupport topiaFiresSupport = getFiresSupport();
            topiaFiresSupport.fireOnPreRead(listeners,
                    this, propertyName, value);
        }
    }

    protected void fireOnPostRead(String propertyName, Object value) {
        PropertyChangeSupport listeners = getPostReadListeners(false);
        if (listeners != null) {
            TopiaFiresSupport topiaFiresSupport = getFiresSupport();
            topiaFiresSupport.fireOnPostRead(listeners,
                    this, propertyName, value);
        }
    }

    protected void fireOnPostRead(String propertyName, int index,
                                  Object value) {
        PropertyChangeSupport listeners = getPostReadListeners(false);
        if (listeners != null) {
            TopiaFiresSupport topiaFiresSupport = getFiresSupport();
            topiaFiresSupport.fireOnPostRead(listeners,
                    this, propertyName, index, value);
        }
    }

    protected void fireOnPreWrite(String propertyName, Object oldValue,
                                  Object newValue) {
        VetoableChangeSupport listeners = getPreWriteListeners(false);
        if (listeners != null) {
            TopiaFiresSupport topiaFiresSupport = getFiresSupport();
            topiaFiresSupport.fireOnPreWrite(listeners,
                    this, propertyName, oldValue, newValue);
        }
    }

    protected void fireOnPostWrite(String propertyName, Object oldValue,
                                   Object newValue) {
        PropertyChangeSupport listeners = getPostWriteListeners(false);
        if (listeners != null) {
            TopiaFiresSupport topiaFiresSupport = getFiresSupport();
            topiaFiresSupport.fireOnPostWrite(
                    listeners, this, propertyName, oldValue, newValue);
        }
    }

    protected void fireOnPostWrite(String propertyName, int index,
                                   Object oldValue, Object newValue) {
        PropertyChangeSupport listeners = getPostWriteListeners(false);
        if (listeners != null) {
            TopiaFiresSupport topiaFiresSupport = getFiresSupport();
            topiaFiresSupport.fireOnPostWrite(
                    listeners, this, propertyName, index, oldValue,
                    newValue);
        }
    }

    public void addPreReadListener(String propertyName,
                                   VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreReadListeners(true);
        listeners.addVetoableChangeListener(propertyName, listener);
    }

    public void addPreReadListener(VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreReadListeners(true);
        listeners.addVetoableChangeListener(listener);
    }

    public void removePreReadListener(String propertyName,
                                      VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreReadListeners(false);
        if (listeners != null) {
            listeners.removeVetoableChangeListener(propertyName, listener);
        }
    }

    public void removePreReadListener(VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreReadListeners(false);
        if (listeners != null) {
            listeners.removeVetoableChangeListener(listener);
        }
    }

    public void addPostReadListener(String propertyName,
                                    PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostReadListeners(true);
        listeners.addPropertyChangeListener(propertyName, listener);
    }

    public void addPostReadListener(PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostReadListeners(true);
        listeners.addPropertyChangeListener(listener);
    }

    public void removePostReadListener(String propertyName,
                                       PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostReadListeners(false);
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
    }

    public void removePostReadListener(PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostReadListeners(false);
        if (listeners != null) {
            listeners.removePropertyChangeListener(listener);
        }
    }

    public void addPreWriteListener(String propertyName,
                                    VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreWriteListeners(true);
        listeners.addVetoableChangeListener(propertyName, listener);
    }

    public void addPreWriteListener(VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreWriteListeners(true);
        listeners.addVetoableChangeListener(listener);
    }

    public void removePreWriteListener(String propertyName,
                                       VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreWriteListeners(false);
        if (listeners != null) {
            listeners.removeVetoableChangeListener(propertyName, listener);
        }
    }

    public void removePreWriteListener(VetoableChangeListener listener) {
        VetoableChangeSupport listeners = getPreWriteListeners(false);
        if (listeners != null) {
            listeners.removeVetoableChangeListener(listener);
        }
    }

    public void addPostWriteListener(String propertyName,
                                     PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostWriteListeners(true);
        listeners.addPropertyChangeListener(propertyName, listener);
    }

    public void addPostWriteListener(PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostWriteListeners(true);
        listeners.addPropertyChangeListener(listener);
    }

    public void removePostWriteListener(String propertyName,
                                        PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostWriteListeners(false);
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
    }

    public void removePostWriteListener(PropertyChangeListener listener) {
        PropertyChangeSupport listeners = getPostWriteListeners(false);
        if (listeners != null) {
            listeners.removePropertyChangeListener(listener);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        addPostWriteListener(listener);
    }

    public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
        addPostWriteListener(property, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        removePostWriteListener(listener);
    }

    public void removePropertyChangeListener(String property, PropertyChangeListener listener) {
        removePostWriteListener(property, listener);
    }

}
