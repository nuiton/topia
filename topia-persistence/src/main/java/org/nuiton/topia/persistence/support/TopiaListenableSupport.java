package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;

import org.nuiton.topia.persistence.event.TopiaEntitiesVetoable;
import org.nuiton.topia.persistence.event.TopiaEntityListener;
import org.nuiton.topia.persistence.event.TopiaEntityVetoable;
import org.nuiton.topia.persistence.event.TopiaSchemaListener;
import org.nuiton.topia.persistence.event.TopiaTransactionListener;
import org.nuiton.topia.persistence.event.TopiaTransactionVetoable;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * This API provides methods to add/remove any kind of listener
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaListenableSupport {

    /* TopiaEntityListener */

    /**
     * Register to the context a TopiaEntityListener about any TopiaEntity.
     * {@code listener} instance will be notified AFTER any operation on
     * the entity.
     *
     * @param listener the listener instance to register
     */
    void addTopiaEntityListener(TopiaEntityListener listener);

    /**
     * Register to the context a TopiaEntityListener about the given entity
     * class. {@code listener} instance will be notified AFTER any
     * operation on the entity.
     *
     * @param entityClass the TopiaEntity's class to listen
     * @param listener    the listener instance to register
     */
    void addTopiaEntityListener(Class<? extends TopiaEntity> entityClass,
                                TopiaEntityListener listener);

    /**
     * Unregister the given TopiaEntityListener about any TopiaEntity from the
     * context
     *
     * @param listener the listener instance to unregister
     */
    void removeTopiaEntityListener(TopiaEntityListener listener);

    /**
     * Unregister the given TopiaEntityListener about the given entity class
     * from the context
     *
     * @param entityClass the listened TopiaEntity's class
     * @param listener    the listener instance to unregister
     */
    void removeTopiaEntityListener(Class<? extends TopiaEntity> entityClass,
                                   TopiaEntityListener listener);


    /* TopiaEntityVetoable */

    /**
     * Register to the context a TopiaEntityVetoable about any TopiaEntity.
     * {@code vetoable} instance will be notified BEFORE any operation on
     * the entity.
     *
     * @param vetoable the vetoable instance to register
     */
    void addTopiaEntityVetoable(TopiaEntityVetoable vetoable);

    /**
     * Register to the context a TopiaEntityVetoable about the given entity
     * class. {@code vetoable} instance will be notified BEFORE any
     * operation on the entity.
     *
     * @param entityClass the TopiaEntity's class to listen
     * @param vetoable    the vetoable instance to register
     */
    void addTopiaEntityVetoable(Class<? extends TopiaEntity> entityClass,
                                TopiaEntityVetoable vetoable);

    /**
     * Unregister the given TopiaEntityVetoable about any TopiaEntity from the
     * context
     *
     * @param vetoable the vetoable instance to unregister
     */
    void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable);

    /**
     * Unregister the given TopiaEntityVetoable about the given entity class
     * from the context
     *
     * @param entityClass the listened TopiaEntity's class
     * @param vetoable    the vetoable instance to unregister
     */
    void removeTopiaEntityVetoable(Class<? extends TopiaEntity> entityClass,
                                   TopiaEntityVetoable vetoable);


    /* TopiaEntitiesVetoable */

    /**
     * Register to the context a TopiaEntitiesVetoable about any TopiaEntity.
     * {@code vetoable} instance will be notified BEFORE any entity load
     *
     * @param vetoable the vetoable instance to register
     */
    void addTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable);

    /**
     * Unregister the given TopiaEntitiesVetoable about any TopiaEntity from the
     * context
     *
     * @param vetoable the vetoable instance to unregister
     */
    void removeTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable);


    /* TopiaTransactionListener */

    /**
     * Register to the context a TopiaTransactionListener about the transaction.
     * {@code listener} instance will be notified AFTER any operation on
     * the transaction.
     *
     * @param listener the listener instance to register
     */
    void addTopiaTransactionListener(TopiaTransactionListener listener);

    /**
     * Unregister the given TopiaTransactionListener about the transaction from
     * the context
     *
     * @param listener the listener instance to unregister
     */
    void removeTopiaTransactionListener(TopiaTransactionListener listener);


    /* TopiaTransactionVetoable */

    /**
     * Register to the context a TopiaTransactionVetoable about the transaction.
     * {@code vetoable} instance will be notified BEFORE any operation on
     * the transaction.
     *
     * @param vetoable the vetoable instance to register
     */
    void addTopiaTransactionVetoable(TopiaTransactionVetoable vetoable);

    /**
     * Unregister the given TopiaTransactionVetoable about the transaction from
     * the context
     *
     * @param vetoable the vetoable instance to unregister
     */
    void removeTopiaTransactionVetoable(TopiaTransactionVetoable vetoable);


    /* PropertyChangeListener */

    /**
     * Register to the context a PropertyChangeListener about some entity's
     * property change. {@code listener} instance will be notified AFTER
     * any change on the entity's property
     *
     * @param listener the listener instance to register
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Unregister the given PropertyChangeListener about some entity's
     * property change from the context
     *
     * @param listener the listener instance to unregister
     */
    void removePropertyChangeListener(PropertyChangeListener listener);


    /* TopiaSchemaListener */

    /**
     * Register to the context a TopiaSchemaListener about any schema
     * modification. {@code listener} instance will be notified BEFORE and
     * AFTER any change on the schema
     *
     * @param listener the listener instance to register
     * @since 3.0
     */
    void addTopiaSchemaListener(TopiaSchemaListener listener);

    /**
     * Unregister the given TopiaSchemaListener about any schema modification
     * from the context
     *
     * @param listener the listener instance to unregister
     * @since 3.0
     */
    void removeTopiaSchemaListener(TopiaSchemaListener listener);

}
