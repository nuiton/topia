package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.EventObject;
import java.util.List;

/**
 * TODO-fdesbois-20100507 : Need javadoc.
 *
 * @author Julien Ruchaud - jruchaud@codelutin.com
 */
public class TopiaEntitiesEvent<E extends TopiaEntity> extends EventObject {

    private static final long serialVersionUID = 1L;

    private List<E> entities;

    public TopiaEntitiesEvent(TopiaJpaSupport source, List<E> entities) {
        super(source);
        this.entities = entities;
    }

    public List<E> getEntities() {
        return entities;
    }

    @Override
    public TopiaJpaSupport getSource() {
        return (TopiaJpaSupport) super.getSource();
    }
}
