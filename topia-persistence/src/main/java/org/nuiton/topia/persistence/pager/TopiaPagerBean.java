package org.nuiton.topia.persistence.pager;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.util.PagerBean;

import java.util.List;

/**
 * Extension of a {@link PagerBean} to add filter and ordering capacities.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.14
 */
public class TopiaPagerBean extends PagerBean {

    private static final long serialVersionUID = 1L;

    // sorting order - asc or desc
    protected boolean sortAscendant;

    // get index row - i.e. user click to sort.
    protected String sortColumn;

    protected FilterRuleGroupOperator groupOp;

    private List<FilterRule> rules;

    public boolean canFilter() {
        return groupOp != null && CollectionUtils.isNotEmpty(rules);
    }

    public FilterRuleGroupOperator getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(FilterRuleGroupOperator groupOp) {
        this.groupOp = groupOp;
    }

    public List<FilterRule> getRules() {
        return rules;
    }

    public void setRules(List<FilterRule> rules) {
        this.rules = rules;
    }

    public boolean isSortAscendant() {
        return sortAscendant;
    }

    public void setSortAscendant(boolean sortAscendant) {
        this.sortAscendant = sortAscendant;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

}
