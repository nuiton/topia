package org.nuiton.topia.persistence.legacy;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;

/**
 * A simple contract to allow you to update some collections of an entity.
 *
 * The purpose of the contract is to make possible (via a StorageService for example) some
 * automatic and generic behaviour when you want to set a collection of childs into a entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @param <P> the type of the entity which contains the collection to update.
 * @param <E> the type of entities in the collection.
 */
public interface ListUpdator<P, E> {

    /**
     * Obtain the name of the property containing childs.
     *
     * @return the name of the property containing childs.
     */
    String getPropertyName();

    /**
     * Obtain a child from the entity given his id.
     *
     * @param parent  the entity to query
     * @param topiaId the id of the researched child entity.
     * @return the child entity or {@code null}, if not found.
     */

    E getChild(P parent, String topiaId);

    /**
     * Obtain the collection of childs from the entity.
     *
     * @param parent the entity to query.
     * @return the collection of childs
     */
    Collection<E> getChilds(P parent);

    /**
     * Obtain the number of childs for the given parent.
     *
     * @param parent the entity to query
     * @return the number of childs for the given entity
     */
    int size(P parent);

    /**
     * Tests if the given entity has some childs.
     *
     * @param parent the entity to query
     * @return {@code true} is the given parent has no child.
     */
    boolean isEmpty(P parent);

    /**
     * Set the childs of an entity
     *
     * @param parent the entity to be setted
     * @param childs the collection of childs to set.
     */
    void setChilds(P parent, Collection<E> childs);

    /**
     * Add a erntity to his parent
     *
     * @param parent the entity to modifiy
     * @param e      the entity to add in parent.
     * @throws TopiaException if any db problem while operation
     */
    void addToList(P parent, E e) throws TopiaException;

    /**
     * Remove from a prent entity a given child.
     *
     * @param parent the entity to modifiy
     * @param e      the child to remove.
     * @throws TopiaException if any pb while operation.
     */
    void removeFromList(P parent, E e) throws TopiaException;

    /**
     * Remove all childs of the given parent.
     *
     * @param parent the entity to modify
     */
    void removeAll(P parent);
}
