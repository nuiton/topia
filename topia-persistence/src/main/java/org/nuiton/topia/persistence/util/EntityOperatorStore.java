package org.nuiton.topia.persistence.util;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

/**
 * Un cache d'operateurs.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see EntityOperator
 */
public class EntityOperatorStore {

    /**
     * l'instance partagee
     */
    private static final EntityOperatorStore shared = new EntityOperatorStore();
    /**
     * le cache d'operateurs
     */
    protected final Map<TopiaEntityEnum, EntityOperator<?>> store;

    /**
     *
     * @param <E> le type de l'entite
     * @param contract le contrat de l'entite
     * @return l'operator associe au contrat
     * @see EntityOperator
     */
    @SuppressWarnings("unchecked")
    public static <E extends TopiaEntity> EntityOperator<E> getOperator(
            TopiaEntityEnum contract) {

        EntityOperator<E> operator =
                (EntityOperator<E>) shared.store.get(contract);
        if (operator == null) {
//            synchronized (shared.store) {
                operator = new EntityOperator<E>(contract);
                shared.store.put(contract, operator);
//            }
        }
        return operator;
        //TODO on devrait peut-etre toujours retourner un clone ?
        //EntityOperator<E> clone = (EntityOperator<E>) operator.clone();
        //return clone;
    }

    public static void clear() {
//        synchronized (shared.store) {
            shared.store.clear();
//        }
    }

    protected EntityOperatorStore() {
        store = new HashMap<TopiaEntityEnum, EntityOperator<?>>();
    }

    @Override
    protected void finalize() throws Throwable {
        store.clear();
        super.finalize();
    }
    
}
