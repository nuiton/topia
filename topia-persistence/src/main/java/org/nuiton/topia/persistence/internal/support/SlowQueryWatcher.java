package org.nuiton.topia.persistence.internal.support;

/*-
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2020 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.hash.Hashing;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Classe ayant pour utilité de surveiller le temps d'exécution d'une requête. Si cette requête dépasse le temps
 * imparti, on affiche un avertissement dans les logs.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.8
 */
public class SlowQueryWatcher implements AutoCloseable, Runnable {

    private final static Log log = LogFactory.getLog(SlowQueryWatcher.class);

    private final static ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * Démarre un nouveau watcher qui affichera un avertissement si le traitement n'est pas fini avec
     * {@code thresholdDelay} millisecondes. L'instance créée commence immédiatement le décompte du temps.
     *
     * Cet appel est fait pour être encapsulé dans un try-with-ressource afin que {@link #done()} soit appelé à la fin
     * du traitement (potentiellement via {@link #close()}.
     *
     * @param descriptionSupplier Permet de récupérer une description de la requête. On utilise {@link Supplier} de
     *                            façon à ne calculer sa valeur qu'en cas de besoin.
     * @param callerNameSupplier Permet de récupérer une information sur l'appelant pour savoir de quel cas d'usage il
     *                           s'agit. On utilise {@link Supplier} de façon à ne calculer sa valeur qu'en cas de
     *                           besoin.
     * @param thresholdDelay le délai (en millisecondes) au delà duquel on affiche l'avertissement
     * @return une nouvelle instance de SlowQueryWatcher pour ce traitement ou {@code null} si le niveau de log ne
     * permet pas de logguer les avertissements.
     */
    public static SlowQueryWatcher start(Supplier<String> descriptionSupplier, Supplier<String> callerNameSupplier, long thresholdDelay) {

        // Petite optimisation : si le niveau de log ne permet pas d'afficher les avertissements, on évite d'instancier
        // quoi que ce soit qui serait de toute façon inutile
        if (!log.isWarnEnabled()) {
            return null;
        }

        String threadName = Thread.currentThread().getName();

        SlowQueryWatcher instance = new SlowQueryWatcher(thresholdDelay, descriptionSupplier, threadName, callerNameSupplier);

        executorService.submit(instance);

        return instance;
    }

    /**
     * Démarre un nouveau watcher qui affichera un avertissement si le traitement n'est pas fini avec
     * {@code thresholdDelay} millisecondes. L'instance créée commence immédiatement le décompte du temps.
     *
     * Cet appel est fait pour être encapsulé dans un try-with-ressource afin que {@link #done()} soit appelé à la fin
     * du traitement (potentiellement via {@link #close()}.
     *
     * @param descriptionSupplier Permet de récupérer une description de la requête. On utilise {@link Supplier} de
     *                            façon à ne calculer sa valeur qu'en cas de besoin.
     * @param thresholdDelay le délai (en millisecondes) au delà duquel on affiche l'avertissement
     * @return une nouvelle instance de SlowQueryWatcher pour ce traitement.
     */
    public static SlowQueryWatcher start(Supplier<String> descriptionSupplier, long thresholdDelay) {

        // Petite optimisation : si le niveau de log ne permet pas d'afficher les avertissements, on évite d'instancier
        // quoi que ce soit qui serait de toute façon inutile
        if (!log.isWarnEnabled()) {
            return null;
        }

        String threadName = Thread.currentThread().getName();

        Supplier<String> callerNameSupplier = newCallerNameSupplier();

        SlowQueryWatcher instance = new SlowQueryWatcher(thresholdDelay, descriptionSupplier, threadName, callerNameSupplier);

        executorService.submit(instance);

        return instance;
    }

    /**
     * Fournit un Supplier dont le but est de retrouver la trace de l'appelant à la couche ToPIA. Ainsi on peut décrire
     * la méthode à l'origine de la requête en cours.
     */
    protected static Supplier<String> newCallerNameSupplier() {
        // On ne fait que mettre de côté le thread de l'appelant. Le reste sera fait uniquement à la demande
        Thread callerThread = Thread.currentThread();

        Function<StackTraceElement, String> formatter = element -> {
            String className = element.getClassName();
            String elementAsString = element.toString().replaceAll(className + ".", "#");
            return elementAsString;
        };

        // La même instance de prédicat va être appelée tout on long de la recherche dans la stacktrace. On cherche le
        // premier élément non TOPIA ayant fait appel à la couche ToPIA. Donc on va ignorer les appels
        // jusqu'à ToPIA puis on prend la première classe qui n'est pas dans ToPIA ni Hibernate
        Predicate<StackTraceElement> callerFinderPredicate = new Predicate<StackTraceElement>() {
            boolean topiaDepthFound = false;
            @Override
            public boolean test(StackTraceElement element) {
                String className = element.getClassName();
                if (!topiaDepthFound) {
                    topiaDepthFound = className.startsWith("org.nuiton.topia.persistence.internal");
                    return false;
                }
                boolean nonTopiaClass =
                        !className.startsWith("org.nuiton.topia.persistence.internal")
                        && !className.startsWith("org.hibernate");
                return nonTopiaClass;
            }
        };

        Supplier<String> supplier = () -> {
            StackTraceElement[] stackTrace = callerThread.getStackTrace();
            String caller = Arrays.stream(stackTrace)
                    .filter(callerFinderPredicate)
                    .map(formatter)
                    .findFirst()
                    .orElse("N/A");
            return caller;
        };

        return supplier;
    }

    /**
     * Délai au delà duquel on affiche l'avertissement
     */
    protected final long thresholdDelay;
    /**
     * Permet de récupérer la description de la requête. Utilisé uniquement si besoin
     */
    protected final Supplier<String> descriptionSupplier;
    /**
     * Permet de récupérer le nom de l'appelant. Utilisé uniquement si besoin
     */
    protected final Supplier<String> callerNameSupplier;
    /**
     * Nom du thread dans lequel s'exécute la requête
     */
    protected final String threadName;

    /**
     * Identifiant du présent watcher. Sert à faire le lien entre les 2 messages d'avertissement
     */
    protected final String id;
    /**
     * Instant de début du watcher
     */
    protected final long begin;

    /**
     * Instant de fin du watcher. Si la valeur est null cela signifie que la requête est toujours en cours
     */
    protected Long end;
    /**
     * Permet de savoir si l'avertissement a déjà été affiché pour éviter de le réafficher plusieurs fois
     */
    protected boolean warned;

    private SlowQueryWatcher(long thresholdDelay, Supplier<String> description, String threadName, Supplier<String> callerNameSupplier) {
        this.thresholdDelay = thresholdDelay;
        this.descriptionSupplier = description;
        this.threadName = threadName;
        this.callerNameSupplier = callerNameSupplier;

        this.id = Hashing.crc32().hashLong(System.currentTimeMillis()).toString();
        this.begin = System.currentTimeMillis();

        this.end = null;
        this.warned = false;
    }

    public boolean isStillRunning() {
        return end == null;
    }

    protected void displayWarn() {
        warned = true;
        String description = descriptionSupplier.get();
        String callerName = callerNameSupplier.get();
        String message = String.format(
                "[w=%s] Unexpected long query (> %dms) from '%s' in thread [%s] : %s",
                id,
                thresholdDelay,
                callerName,
                threadName,
                description);
        log.warn(message);
    }

    /**
     * Permet d'informer le watcher que la requête est terminée. Ça peut être fait directement en appelant cette méthode
     * explicitement ou bien au moment tu {@link #close()} dans le cas d'un usage via try-with-ressource.
     */
    public void done() {
        end = System.currentTimeMillis();

        long duration = end - begin;
        if (duration > thresholdDelay) {
            // Il se peut que la requête n'ait pas encore été affichée. Dans ce cas on prend les devant et on l'affiche
            if (!warned) {
                displayWarn();
            }
            final String message = String.format("[w=%s] Query took %dms", id, duration);
            log.warn(message);
        }
    }

    @Override
    public void run() {
        while (isStillRunning() && !warned) {
            long current = System.currentTimeMillis();
            if ((current - begin) > thresholdDelay) {
                displayWarn();
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // Rien à faire
            }
        }
    }

    @Override
    public void close() {
        if (isStillRunning()) {
            done();
        }
    }

}
