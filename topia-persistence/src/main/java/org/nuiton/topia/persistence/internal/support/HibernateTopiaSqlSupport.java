package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hibernate-based implementation of TopiaSqlSupport. It is used through Topia or directly with an Hibernate Session.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class HibernateTopiaSqlSupport implements TopiaSqlSupport {

    protected TopiaHibernateSupport hibernateSupport;
    protected Session session;

    public HibernateTopiaSqlSupport(TopiaHibernateSupport hibernateSupport) {
        this.hibernateSupport = hibernateSupport;
    }

    public HibernateTopiaSqlSupport(Session session) {
        this.session = session;
    }

    public static class HibernateSqlWork implements Work {

        protected final String script;

        public HibernateSqlWork(String script) {
            this.script = script;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            PreparedStatement sta = connection.prepareStatement(script);
            try {
                sta.execute();
            } finally {
                sta.close();
            }
        }
    }

    public static class HibernateTopiaSqlWork implements Work {

        protected final TopiaSqlWork work;

        public HibernateTopiaSqlWork(TopiaSqlWork work) {
            this.work = work;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            work.execute(connection);
        }
    }

    public static class HibernateTopiaSqlQueryWork<O> implements Work {

        protected final TopiaSqlQuery<O> query;

        protected final boolean multipleResult;

        protected final List<O> result = new ArrayList<O>();

        public HibernateTopiaSqlQueryWork(TopiaSqlQuery<O> query, boolean multipleResult) {
            this.query = query;
            this.multipleResult = multipleResult;
        }

        @Override
        public void execute(Connection connection) throws SQLException {

            PreparedStatement ps = query.prepareQuery(connection);
            try {
                ResultSet set = ps.executeQuery();

                query.afterExecuteQuery(set);

                if (set.next()) {
                    O singleResult = query.prepareResult(set);
                    if (singleResult != null) {
                        result.add(singleResult);
                    }
                    if (multipleResult) {
                        while (set.next()) {
                            singleResult = query.prepareResult(set);
                            if (singleResult != null) {
                                result.add(singleResult);
                            }
                        }
                    }
                }

            } catch (Exception e) {
                throw new TopiaException("Could not execute query", e);
            } finally {
                ps.close();
            }
        }

        public List<O> getResult() {
            return result;
        }
    }

    @Override
    public void executeSql(String sqlScript) {
        HibernateSqlWork work = new HibernateSqlWork(sqlScript);
        try {
            getHibernateSession().doWork(work);
        } catch (HibernateException e) {
            throw new TopiaException("Could not execute sql code", e);
        }
    }

    protected Session getHibernateSession() {
        Session result = session;
        if (result == null) {
            result = hibernateSupport.getHibernateSession();
        }
        return result;
    }

    @Override
    public void doSqlWork(TopiaSqlWork sqlWork) {
        HibernateTopiaSqlWork work = new HibernateTopiaSqlWork(sqlWork);
        try {
            getHibernateSession().doWork(work);
        } catch (HibernateException e) {
            throw new TopiaException("Could not execute sql code", e);
        }
    }

    @Override
    public <O> O findSingleResult(final TopiaSqlQuery<O> query) throws TopiaException {

        HibernateTopiaSqlQueryWork<O> work = new HibernateTopiaSqlQueryWork<O>(query, false);
        getHibernateSession().doWork(work);
        final List<O> result = work.getResult();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public <O> List<O> findMultipleResult(final TopiaSqlQuery<O> query) throws TopiaException {

        HibernateTopiaSqlQueryWork<O> work = new HibernateTopiaSqlQueryWork<O>(query, true);
        getHibernateSession().doWork(work);
        final List<O> result = work.getResult();
        return result;
    }

}
