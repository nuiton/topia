package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeSupport;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaVetoException;
import org.nuiton.topia.persistence.event.EntityState;
import org.nuiton.topia.persistence.event.TopiaContextEvent;
import org.nuiton.topia.persistence.event.TopiaEntitiesEvent;
import org.nuiton.topia.persistence.event.TopiaEntitiesVetoable;
import org.nuiton.topia.persistence.event.TopiaEntityEvent;
import org.nuiton.topia.persistence.event.TopiaEntityListener;
import org.nuiton.topia.persistence.event.TopiaEntityVetoable;
import org.nuiton.topia.persistence.event.TopiaSchemaListener;
import org.nuiton.topia.persistence.event.TopiaTransactionEvent;
import org.nuiton.topia.persistence.event.TopiaTransactionListener;
import org.nuiton.topia.persistence.event.TopiaTransactionVetoable;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaListenableSupport;
import org.nuiton.util.CategorisedListenerSet;
import org.nuiton.util.ListenerSet;

import com.google.common.base.Preconditions;

/**
 * Contains all about event listening and propagation.
 *
 *
 * @author jruchaud : jruchaud@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaFiresSupport extends AbstractTopiaListenableSupport implements TopiaListenableSupport {

    private static final Log log = LogFactory.getLog(TopiaFiresSupport.class);

    /**
     * Used to fire read event
     */
    protected static final Object NO_CHANGE = new Object();

    /**
     * Each TopiaFiresSupport may have a parent on which events will be propagated. Basically, a TopiaFiresSupport
     * without parent is used by the {@link org.nuiton.topia.persistence.TopiaApplicationContext}, and a
     * TopiaFiresSupport with a parent is used by the {@link org.nuiton.topia.persistence.TopiaPersistenceContext}.
     */
    protected TopiaFiresSupport parent;

    /**
     * Constructor to be used by the {@link org.nuiton.topia.persistence.TopiaApplicationContext}
     */
    public TopiaFiresSupport() {
    }

    /**
     * Constructor to be used by the {@link org.nuiton.topia.persistence.TopiaPersistenceContext}.
     *
     * @param parent The  {@link org.nuiton.topia.persistence.TopiaApplicationContext}'s instance
     */
    public TopiaFiresSupport(TopiaFiresSupport parent) {
        this.parent = parent;
    }

    /**
     * Get or create the entity state from the {@link #transactionEntities} map. This method always returns an instance.
     *
     * @param entity the entity instance
     * @return the {@link org.nuiton.topia.persistence.event.EntityState} for the given entity.
     */
    protected EntityState getEntityState(TopiaEntity entity) {
        EntityState state = transactionEntities.get(entity);
        if (state == null) {
            state = new EntityState();
            transactionEntities.put(entity, state);
        }
        return state;
    }

    /**
     * Register an entity loaded during the current transaction.
     *
     * @param entity the loaded entity
     */
    public void notifyEntityLoaded(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("notifyEntityLoaded");
        }
        EntityState state = getEntityState(entity);
        state.addLoaded();
    }

    /**
     * Register an entity created during the current transaction.
     *
     * @param entity the created entity
     */
    public void notifyEntityCreated(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("notifyEntityCreated");
        }
        EntityState state = getEntityState(entity);
        state.addCreated();
    }

    /**
     * used to register objects loaded during transaction.
     *
     * @param entity the read entity
     */
    public void notifyEntityRead(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("notifyEntityRead");
        }
        EntityState state = getEntityState(entity);
        state.addRead();
    }

    /**
     * used to register objects modified (update) during transaction.
     *
     * @param entity the updated entity
     */
    public void notifyEntityUpdated(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("notifyEntityUpdated");
        }

        EntityState state = getEntityState(entity);
        state.addUpdated();
    }

    /**
     * used to register objects modified (setter) during transaction.
     *
     * @param entity the updated entity
     */
    public void notifyEntityWritten(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("notifyEntityWritten");
        }

        EntityState state = getEntityState(entity);
        state.addWritten();
    }

    /**
     * used to register objects deleted during transaction.
     *
     * @param entity the deleted entity
     */
    public void notifyEntityDeleted(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("notifyEntityDeleted");
        }
        EntityState state = getEntityState(entity);
        state.addDeleted();
    }

    /* Fires sur les transactions */

    protected boolean isNotEmpty(ListenerSet<?> set) {
        return set.size() > 0;
    }

    protected boolean isNotEmpty(Set<?> set) {
        return set.size() > 0;
    }

    protected boolean isNotEmpty(CategorisedListenerSet<?> set, Class<?> category) {
        return set.iterator(category).hasNext();
    }

    public void fireOnBeginTransaction(TopiaPersistenceContext context) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnBeginTransaction");
        }
        if (isNotEmpty(transactionVetoables)) {
            TopiaTransactionEvent e = new TopiaTransactionEvent(context);
            for (TopiaTransactionVetoable listener : transactionVetoables) {
                try {
                    listener.beginTransaction(e);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }

        if (parent != null) {
            parent.fireOnBeginTransaction(context);
        }
    }

    public void fireOnPostCommit(TopiaPersistenceContext context) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostCommit");
        }
        if (isNotEmpty(transactionListeners)) {
            TopiaTransactionEvent e = new TopiaTransactionEvent(context, transactionEntities);
            for (TopiaTransactionListener listener : transactionListeners) {
                try {
                    listener.commit(e);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostCommit", eee);
                    }
                }
            }
        }
        transactionEntities.clear();

        if (parent != null) {
            parent.fireOnPostCommit(context);
        }
    }

    public void fireOnPostRollback(TopiaPersistenceContext context) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostRollback");
        }
        if (isNotEmpty(transactionListeners)) {
            TopiaTransactionEvent e = new TopiaTransactionEvent(context, transactionEntities);
            for (TopiaTransactionListener listener : transactionListeners) {
                try {
                    listener.rollback(e);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostRollback", eee);
                    }
                }
            }
        }
        transactionEntities.clear();

        if (parent != null) {
            parent.fireOnPostRollback(context);
        }
    }

    /* Fires sur les entités */

    public void fireOnPreLoad(TopiaPersistenceContext context,
                              TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreLoad");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().load(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }

        if (parent != null) {
            parent.fireOnPreLoad(context, entity, state);
        }
    }

    public void fireOnPostLoad(TopiaPersistenceContext context,
                               TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostLoad");
        }
        notifyEntityLoaded(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().load(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostLoad for entity: " + entity, eee);
                    }
                }
            }
        }

        if (parent != null) {
            parent.fireOnPostLoad(context, entity, state);
        }
    }

    public void fireOnPreCreate(TopiaPersistenceContext context,
                                TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreCreate");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().create(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }

        if (parent != null) {
            parent.fireOnPreCreate(context, entity, state);
        }
    }

    public void fireOnPostCreate(TopiaPersistenceContext context,
                                 TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostCreate");
        }
        notifyEntityCreated(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().create(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostCreate for entity: " + entity, eee);
                    }
                }
            }
        }

        if (parent != null) {
            parent.fireOnPostCreate(context, entity, state);
        }
    }

    public void fireOnPreUpdate(TopiaPersistenceContext context,
                                TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreUpdate");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().update(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }

        if (parent != null) {
            parent.fireOnPreUpdate(context, entity, state);
        }
    }

    public void fireOnPostUpdate(TopiaPersistenceContext context,
                                 TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostUpdate");
        }
        notifyEntityUpdated(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().update(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostUpdate for entity: " + entity, eee);
                    }
                }
            }
        }
        if (parent != null) {
            parent.fireOnPostUpdate(context, entity, state);
        }
    }

    public void fireOnPreDelete(TopiaPersistenceContext context,
                                TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPreDelete");
        }
        if (isNotEmpty(entityVetoables, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityVetoable> l = entityVetoables.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().delete(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }

        if (parent != null) {
            parent.fireOnPreDelete(context, entity, state);
        }
    }

    public void fireOnPostDelete(TopiaPersistenceContext context,
                                 TopiaEntity entity, Object[] state) {
        if (log.isDebugEnabled()) {
            log.debug("fireOnPostDelete");
        }
        notifyEntityDeleted(entity);
        if (isNotEmpty(entityListeners, entity.getClass())) {
            TopiaEntityEvent event = new TopiaEntityEvent(context, entity, state);
            for (Iterator<TopiaEntityListener> l = entityListeners.iterator(entity.getClass()); l.hasNext(); ) {
                try {
                    l.next().delete(event);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fireOnPostDelete for entity: " + entity, eee);
                    }
                }
            }
        }

        if (parent != null) {
            parent.fireOnPostDelete(context, entity, state);
        }
    }

    /* Fires sur les propriétés */

    public void fireOnPreRead(VetoableChangeSupport vetoables,
                              TopiaEntity entity,
                              String propertyName,
                              Object value) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPreRead");
        }
        try {
            vetoables.fireVetoableChange(propertyName, value, NO_CHANGE);
        } catch (Exception eee) {
            throw new TopiaVetoException(eee);
        }
    }

    public void fireOnPostRead(PropertyChangeSupport listeners,
                               TopiaEntity entity,
                               String propertyName,
                               Object value) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostRead");
        }
        notifyEntityRead(entity);
        try {
            listeners.firePropertyChange(propertyName, value, NO_CHANGE);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostRead", eee);
            }
        }

    }

    public void fireOnPostRead(PropertyChangeSupport listeners,
                               TopiaEntity entity,
                               String propertyName,
                               int index,
                               Object value) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostRead");
        }
        notifyEntityRead(entity);
        try {
            listeners.fireIndexedPropertyChange(propertyName, index, value, NO_CHANGE);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostRead", eee);
            }
        }

    }

    public void fireOnPreWrite(VetoableChangeSupport vetoables,
                               TopiaEntity entity,
                               String propertyName,
                               Object oldValue,
                               Object newValue) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPreWrite");
        }
        try {
            vetoables.fireVetoableChange(propertyName, oldValue, newValue);
        } catch (Exception eee) {
            throw new TopiaVetoException(eee);
        }
    }

    public void fireOnPostWrite(PropertyChangeSupport listeners,
                                TopiaEntity entity,
                                String propertyName,
                                Object oldValue,
                                Object newValue) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostWrite");
        }
        notifyEntityWritten(entity);
        if (isNotEmpty(propertyChangeListeners)) {
            PropertyChangeEvent e = new PropertyChangeEvent(entity, propertyName, oldValue, newValue);
            for (PropertyChangeListener l : propertyChangeListeners) {
                try {
                    l.propertyChange(e);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't fire property change for: " + propertyName, eee);
                    }
                }
            }
        }
        try {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostWrite: " + propertyName, eee);
            }
        }
    }

    public void fireOnPostWrite(PropertyChangeSupport listeners,
                                TopiaEntity entity,
                                String propertyName,
                                int index,
                                Object oldValue,
                                Object newValue) {

        if (log.isDebugEnabled()) {
            log.debug("fireOnPostWrite");
        }
        notifyEntityWritten(entity);
        try {
            listeners.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't fireOnPostWrite", eee);
            }
        }
    }

    /**
     * Notify topia context listeners for create schema pre operation
     *
     * @param context topia context
     */
    public void firePreCreateSchema(TopiaApplicationContext context) {
        Preconditions.checkState(parent == null, "This method is designed to be used only on parent TopiaFiresSupport");
        if (log.isDebugEnabled()) {
            log.debug("firePreCreateSchema");
        }
        if (isNotEmpty(topiaSchemaListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaSchemaListener topiaSchemaListener : topiaSchemaListeners) {
                try {
                    topiaSchemaListener.preCreateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for create schema post operation
     *
     * @param context topia context
     */
    public void firePostCreateSchema(TopiaApplicationContext context) {
        Preconditions.checkState(parent == null, "This method is designed to be used only on parent TopiaFiresSupport");
        if (log.isDebugEnabled()) {
            log.debug("firePostCreateSchema");
        }
        if (isNotEmpty(topiaSchemaListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaSchemaListener topiaSchemaListener : topiaSchemaListeners) {
                try {
                    topiaSchemaListener.postCreateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for create schema pre operation
     *
     * @param context topia context
     */
    public void firePreUpdateSchema(TopiaApplicationContext context) {
        Preconditions.checkState(parent == null, "This method is designed to be used only on parent TopiaFiresSupport");
        if (log.isDebugEnabled()) {
            log.debug("firePostCreateSchema");
        }
        if (isNotEmpty(topiaSchemaListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaSchemaListener topiaSchemaListener : topiaSchemaListeners) {
                try {
                    topiaSchemaListener.preUpdateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for create schema post operation
     *
     * @param context topia context
     */
    public void firePostUpdateSchema(TopiaApplicationContext context) {
        Preconditions.checkState(parent == null, "This method is designed to be used only on parent TopiaFiresSupport");
        if (log.isDebugEnabled()) {
            log.debug("firePostCreateSchema");
        }
        if (isNotEmpty(topiaSchemaListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaSchemaListener topiaSchemaListener : topiaSchemaListeners) {
                try {
                    topiaSchemaListener.postUpdateSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for drop schema pre operation
     *
     * @param context topia context
     * @since 3.0
     */
    public void firePreDropSchema(TopiaApplicationContext context) {
        Preconditions.checkState(parent == null, "This method is designed to be used only on parent TopiaFiresSupport");
        if (log.isDebugEnabled()) {
            log.debug("firePreDropSchema");
        }
        if (isNotEmpty(topiaSchemaListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaSchemaListener topiaSchemaListener : topiaSchemaListeners) {
                try {
                    topiaSchemaListener.preDropSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }

    /**
     * Notify topia context listeners for drop schema post operation
     *
     * @param context topia context
     * @since 3.0
     */
    public void firePostDropSchema(TopiaApplicationContext context) {
        Preconditions.checkState(parent == null, "This method is designed to be used only on parent TopiaFiresSupport");
        if (log.isDebugEnabled()) {
            log.debug("firePostDropSchema");
        }
        if (isNotEmpty(topiaSchemaListeners)) {
            TopiaContextEvent event = new TopiaContextEvent(context);
            for (TopiaSchemaListener topiaSchemaListener : topiaSchemaListeners) {
                try {
                    topiaSchemaListener.postDropSchema(event);
                } catch (Exception eee) {
                    throw new TopiaVetoException(eee);
                }
            }
        }
    }


    /**
     * Notify entities listeners for load operation
     *
     * @param <E>     type of entities
     * @param context  context used
     * @param entities entities loaded
     * @return the list of entities loaded
     */
    public <E extends TopiaEntity> List<E> fireEntitiesLoad(
            TopiaJpaSupport context, List<E> entities) {
        if (log.isDebugEnabled()) {
            log.debug("fireEntitiesLoad");
        }

        List<E> result = entities;
        for (TopiaEntitiesVetoable entitiesVetoable : entitiesVetoables) {
            try {
                //FIXME tchemit 20100513 Why instanciate n events? if necessary MUST add a comment
                TopiaEntitiesEvent<E> event = new TopiaEntitiesEvent<E>(context, result);
                result = entitiesVetoable.load(event);
            } catch (Exception eee) {
                throw new TopiaVetoException(eee);
            }
        }
        return result;
    }

}
