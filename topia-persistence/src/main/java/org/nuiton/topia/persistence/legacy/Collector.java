package org.nuiton.topia.persistence.legacy;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.DepthEntityVisitor;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;

/**
 * Un objet qui permet de parcourir des entites (via un
 * {@link CollectorVisitor}) et de collecter des donnees pendant le parcours.
 *
 * La classe a un type qui est le type de retour de la methode
 * {@link #detect(TopiaEntity...)}.
 *
 * On peut donc très facilement en faire un collecteur de donnees.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @param <R> les donnes a retourner
 * @since 2.2.0
 */
public abstract class Collector<R> {

    private static final Log log = LogFactory.getLog(Collector.class);

    /** le visiteur utiliser pour trouver les types */
    protected CollectorVisitor visitor;

    /** la liste des contracts des entites connues. */
    protected TopiaEntityEnum[] contracts;

    public Collector(CollectorVisitor visitor, TopiaEntityEnum[] contracts) {
        this.visitor = visitor == null ? new CollectorVisitor() : visitor;
        this.visitor.setCollector(this);
        this.contracts = ArrayUtils.clone(contracts);
    }

    public Collector(TopiaEntityEnum[] contracts) {
        this(null, contracts);
    }

    protected void clear() {
        visitor.clear();
    }

    protected boolean onStarting(TopiaEntity e) {
        return true;
    }

    protected void onStarted(TopiaEntity e, boolean enter) {
    }

    protected boolean onVisiting(TopiaEntity e, String name, Class<?> type,
                                 Object value) {
        return true;
    }

    protected void onVisited(TopiaEntity e, String name, Class<?> type,
                             Object value, boolean enter) {
    }

    protected boolean onVisiting(TopiaEntity e, String name,
                                 Class<?> collectionType,
                                 Class<?> type, Object value) {
        return true;
    }

    protected void onVisited(TopiaEntity e, String name,
                             Class<?> collectionType,
                             Class<?> type, Object value, boolean enter) {
    }

    protected boolean onVisiting(TopiaEntity e, String name,
                                 Class<?> collectionType,
                                 Class<?> type, int index, Object value) {
        return true;
    }

    protected void onVisited(TopiaEntity e, String name,
                             Class<?> collectionType,
                             Class<?> type,
                             int index, Object value, boolean enter) {
    }

    protected boolean onEnding(TopiaEntity e) {
        return true;
    }

    protected void onEnded(TopiaEntity e, boolean enter) {
    }

    protected void beforeAll(CollectorVisitor visitor,
                             TopiaEntity... entities) {
    }

    protected void before(CollectorVisitor visitor, TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug("Will detect "+entity.getTopiaId());
        }
    }

    protected void after(CollectorVisitor visitor, TopiaEntity entity) {
    }

    protected abstract R afterAll(CollectorVisitor visitor,
                                  TopiaEntity... entities);

    public R detect(TopiaEntity... entities) throws TopiaException {

        try {
            beforeAll(visitor, entities);

            for (TopiaEntity e : entities) {
                before(visitor, e);
                e.accept(visitor);
                after(visitor, e);
            }

            R result = afterAll(visitor, entities);

            return result;
        } finally {
            clear();
        }
    }

    public static class CollectorVisitor extends DepthEntityVisitor {

        /** la pile des entites en cours de parcours */
        protected Deque<TopiaEntity> stack = new ArrayDeque<TopiaEntity>();

        Collector<?> collector;

        protected void setCollector(Collector<?> collector) {
            this.collector = collector;
        }

        public Deque<TopiaEntity> getStack() {
            return stack;
        }

        public Collection<TopiaEntity> getAlreadyExplored() {
            return alreadyExplored;
        }

        @Override
        public void start(TopiaEntity e) {
            // on ajoute l'objet courant dans la pile
            stack.offerLast(e);
            boolean canContinue = collector.onStarting(e);
            if (canContinue) {
                super.start(e);
            }
            collector.onStarted(e, canContinue);
        }

        @Override
        public void end(TopiaEntity e) {
            boolean canContinue = collector.onEnding(e);
            if (canContinue) {
                super.end(e);
            }
            stack.removeLast();
            // on retire l'objet courant de la pile
            collector.onEnded(e, canContinue);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> type,
                          Object value) {
            boolean canContinue = collector.onVisiting(e, name, type, value);
            if (canContinue) {
                super.visit(e, name, type, value);
            }
            collector.onVisited(e, name, type, value, canContinue);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> collectionType,
                          Class<?> type, Object value) {
            boolean canContinue =
                    collector.onVisiting(e, name, collectionType, type, value);
            if (canContinue) {
                super.visit(e, name, collectionType, type, value);
            }
            collector.onVisited(e, name, collectionType, type, value,
                                canContinue);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> collectionType,
                          Class<?> type, int index, Object value) {
            boolean canContinue =
                    collector.onVisiting(e, name, collectionType, type, index,
                                         value);
            if (canContinue) {
                super.visit(e, name, collectionType, type, index, value);
            }
            collector.onVisited(e, name, collectionType, type, index, value,
                                canContinue);
        }

        @Override
        public void clear() {
            super.clear();
            stack.clear();
        }
    }

    protected int stackSize() {
        return visitor.getStack().size();
    }

    protected Deque<TopiaEntity> getStack() {
        return visitor.getStack();
    }

    protected Collection<TopiaEntity> getAlreadyExplored() {
        return visitor.getAlreadyExplored();
    }

    @Override
    protected void finalize() throws Throwable {
        clear();
        super.finalize();
    }

    protected Class<? extends TopiaEntity> getContractClass(TopiaEntity e) {
        if (contracts.length == 0) {
            return null;
        }
        Class<? extends TopiaEntity> contractClass =
                TopiaEntityHelper.getContractClass(contracts, e.getClass());
        if (contractClass != null) {
            return contractClass;
        }
        return null;
    }

    protected TopiaEntity getTopiaValue(Object value) {
        return (TopiaEntity) (value != null &&
                              value instanceof TopiaEntity ? value : null);
    }
}
