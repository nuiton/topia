package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import jakarta.persistence.metamodel.EntityType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.ReplicationMode;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaReplicationDestination;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaReplicationSupport;

import java.util.List;
import java.util.Set;

public class HibernateTopiaReplicationSupport implements TopiaReplicationSupport, TopiaReplicationDestination {

    private static final Log log = LogFactory.getLog(HibernateTopiaReplicationSupport.class);

    protected TopiaHibernateSupport topiaHibernateSupport;

    public HibernateTopiaReplicationSupport(TopiaHibernateSupport topiaHibernateSupport) {
        this.topiaHibernateSupport = topiaHibernateSupport;
    }

    @Override
    public void replicate(
            TopiaReplicationDestination topiaReplicationDestination,
            Object... entityAndCondition) throws IllegalArgumentException {

        String[] queries = buildQueries(entityAndCondition);

        try {
            for (String query : queries) {
                if (log.isDebugEnabled()) {
                    log.debug("acquire entities " + query);
                }
                // acquire data to replicate
                List<?> entities = topiaHibernateSupport.getHibernateSession().createQuery(query).list();
                replicate0(topiaReplicationDestination, entities.toArray());
                if (log.isDebugEnabled()) {
                    log.debug("replication of entities " + query +
                            " was sucessfully done.");
                }
            }
        } catch (HibernateException eee) {
            throw new TopiaException(String.format("An error occurs while a replication operation: %s",
                    eee.getMessage()), eee);
        }
    }

    @Override
    public <T extends TopiaEntity> void replicateEntity(
            TopiaReplicationDestination topiaReplicationDestination,
            T entity) throws IllegalArgumentException {

        replicate0(topiaReplicationDestination, entity);

    }

    @Override
    public <T extends TopiaEntity> void replicateEntities(
            TopiaReplicationDestination topiaReplicationDestination,
            List<T> entities) throws IllegalArgumentException {

        replicate0(topiaReplicationDestination, entities.toArray());

    }

    protected void replicate0(TopiaReplicationDestination topiaReplicationDestination,
                              Object... entities) {
        try {
            for (Object entity : entities) {
                // dettach entity to source session, to make possible copy of
                // collection without a hibernate exception (list opened in
                // two session...)
                topiaHibernateSupport.getHibernateSession().evict(entity);
                topiaReplicationDestination.replicate((TopiaEntity) entity);
            }

        } catch (HibernateException eee) {
            throw new TopiaException(String.format("An error occurs while a replication operation : %s",
                    eee.getMessage()), eee);
        }
    }

    /**
     * Build the list of queries from the given parameter
     * {@code entityAndCondition}.
     *
     * If no parameter is given, then build the queries for all entities is db,
     * with no condition.
     *
     * @param entityAndCondition the list of tuples (Class,String)
     * @return the list of queries.
     * @throws TopiaException           if any pb of db while getting entities
     *                                  classes.
     * @throws IllegalArgumentException if any pb with the given parameter
     *                                  (mainly ClassCastException).
     */
    protected String[] buildQueries(Object... entityAndCondition)
            throws TopiaException, IllegalArgumentException {
        Class<?> entityClass;
        String condition;

        // si entityAndcondition est vide alors il faut le remplir
        // avec toutes les entités du mapping (class, null)
        if (entityAndCondition.length == 0) {
            Set<EntityType<?>> entitiesNames = topiaHibernateSupport.getHibernateFactory().getMetamodel().getEntities();
            entityAndCondition = new Object[entitiesNames.size() * 2];
            int i = 0;
            for (EntityType<?> entityType : entitiesNames) {
                try {
                    entityAndCondition[i++] = Class.forName(entityType.getName());
                } catch (ClassNotFoundException e) {
                    // should never happen!
                    throw new TopiaException(
                            "class cast exception for entity " + entityType);
                }
                entityAndCondition[i++] = null;

            }
        }

        // prepare queries to perform before opening any transaction
        if (entityAndCondition.length % 2 != 0) {
            throw new IllegalArgumentException(
                    "entityAndCondition must be a couple of (Class, String)");
        }
        String queries[] = new String[entityAndCondition.length / 2];
        for (int i = 0; i < entityAndCondition.length; ) {
            try {
                entityClass = (Class<?>) entityAndCondition[i++];
                condition = (String) entityAndCondition[i++];
                String query = "from " + entityClass.getName();
                if (condition != null && !condition.isEmpty()) {
                    query += " where " + condition;
                }
                queries[(i - 1) / 2] = query;
            } catch (ClassCastException e) {
                if (i % 2 == 0) {
                    throw new IllegalArgumentException(
                            "Others argument must be String not " +
                                    entityAndCondition[i - 1], e);
                } else {
                    throw new IllegalArgumentException(
                            "Others argument must be Class not " +
                                    entityAndCondition[i - 1], e);
                }
            }
        }
        return queries;
    }

    @Override
    public void replicate(TopiaEntity entity) {
        topiaHibernateSupport.getHibernateSession().replicate(entity, ReplicationMode.EXCEPTION);
    }

}
