package org.nuiton.topia.persistence.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.Configuration;

/**
 * This API provides methods to interact with Hibernate
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaHibernateSupport {

    /**
     * @return Returns the Hibernate's Session.
     */
    Session getHibernateSession();

    /**
     * @return Returns the HibernateFactory.
     */
    SessionFactory getHibernateFactory();

    /**
     * @return Returns the Hibernate's Metadata.
     */
    Metadata getHibernateMetadata();

    /**
     * @return Returns the Hibernate configuration
     */
    Configuration getHibernateConfiguration();

}
