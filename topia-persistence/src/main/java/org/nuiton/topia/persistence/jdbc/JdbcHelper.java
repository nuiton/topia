package org.nuiton.topia.persistence.jdbc;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @since 3.0
 */
public class JdbcHelper {

    private static final Log log = LogFactory.getLog(JdbcHelper.class);

    protected JdbcConfiguration jdbcConfiguration;

    public JdbcHelper(JdbcConfiguration jdbcConfiguration) {
        this.jdbcConfiguration = jdbcConfiguration;
    }

    public String runSelectOnString(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = openConnection();
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(preparedStatement);
            closeQuietly(connection);
        }
    }

    public void createSchema(String schemaName) {
        Connection connection = null;
        CallableStatement callableStatement = null;
        try {
            connection = openConnection();
            callableStatement = connection.prepareCall("create schema " + schemaName);
            callableStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(callableStatement);
            closeQuietly(connection);
        }
    }

    public int runUpdate(String sql) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = openConnection();
            preparedStatement = connection.prepareStatement(sql);
            int result = preparedStatement.executeUpdate();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(preparedStatement);
            closeQuietly(connection);
        }
    }

    protected Connection openConnection() throws SQLException {
        registerDriver();
        Connection connection = DriverManager.getConnection(
                jdbcConfiguration.getJdbcConnectionUrl(),
                jdbcConfiguration.getJdbcConnectionUser(),
                jdbcConfiguration.getJdbcConnectionPassword());
        return connection;
    }

    protected void closeQuietly(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Unable to close: " + eee.getMessage() , eee);
                }
            }
        }
    }

    protected void closeQuietly(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Unable to close: " + eee.getMessage() , eee);
                }
            }
        }
    }

    protected void closeQuietly(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Unable to close: " + eee.getMessage() , eee);
                }
            }
        }
    }

    protected void registerDriver() {
        Driver driver = getJdbcDriver();
        try {
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            throw new RuntimeException("unable to register driver " + driver, e);
        }
    }

    protected Driver getJdbcDriver() {
        Class<? extends Driver> jdbcDriverClass = jdbcConfiguration.getJdbcDriverClass();
        try {
            Driver driver = jdbcDriverClass.newInstance();
            return driver;
        } catch (InstantiationException e) {
            throw new IllegalArgumentException(jdbcDriverClass + " is not instantiable", e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(jdbcDriverClass + " is not instantiable", e);
        }
    }

}
