package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Used to know the state of an entity during transaction.
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 */
public class EntityState implements Comparable<EntityState> {

    final static int NULL = 0;

    final static int LOADED = 1;

    final static int READ = 2;

    final static int CREATED = 4;

    final static int WRITTEN = 8;

    final static int UPDATED = 16;

    final static int DELETED = 32;

    /**
     * Internal representation of all states of entity (use bitwise operation).
     */
    protected int state = NULL;

    /**
     * Add loaded state.
     *
     * After the invocation, method {@link #isLoaded()} will always return {@code true}.
     */
    public void addLoaded() {
        state |= LOADED;
    }

    /**
     * Add read state
     *
     * After the invocation, method {@link #isRead()} will always return {@code true}.
     */
    public void addRead() {
        state |= READ;
    }

    /**
     * Add created state.
     *
     * After the invocation, method {@link #isCreated()} will always return {@code true}.
     */
    public void addCreated() {
        state |= CREATED;
    }

    /**
     * Add updated state.
     *
     * After the invocation, method {@link #isUpdated()} will always return {@code true}.
     */
    public void addUpdated() {
        state |= UPDATED;
    }

    /**
     * Add written state.
     *
     * After the invocation, method {@link #isWritten()} will always return {@code true}.
     */
    public void addWritten() {
        state |= WRITTEN;
    }

    /**
     * Add deleted state.
     *
     * After the invocation, method {@link #isDeleted()} will always return {@code true}.
     */
    public void addDeleted() {
        state |= DELETED;
    }

    /**
     * Tells if the {@code #LOADED} state is on.
     *
     * @return {@code true} if {@code #LOADED} state is on, {@code false} otherwise.
     */
    public boolean isLoaded() {
        return (state & LOADED) == LOADED;
    }

    /**
     * Tells if the {@code #READ} state is on.
     *
     * @return {@code true} if {@code #READ} state is on, {@code false} otherwise.
     */
    public boolean isRead() {
        return (state & READ) == READ;
    }

    /**
     * Tells if the {@code #CREATED} state is on.
     *
     * @return {@code true} if {@code #CREATED} state is on, {@code false} otherwise.
     */
    public boolean isCreated() {
        return (state & CREATED) == CREATED;
    }

    /**
     * Tells if the {@code #WRITTEN} state is on.
     *
     * @return {@code true} if {@code #WRITTEN} state is on, {@code false} otherwise.
     */
    public boolean isWritten() {
        return (state & WRITTEN) == WRITTEN;
    }

    /**
     * Tells if the {@code #UPDATED} state is on.
     *
     * @return {@code true} if {@code #UPDATED} state is on, {@code false} otherwise.
     */
    public boolean isUpdated() {
        return (state & UPDATED) == UPDATED;
    }

    /**
     * Tells if the {@code #DELETED} state is on.
     *
     * @return {@code true} if {@code #DELETED} state is on, {@code false} otherwise.
     */
    public boolean isDeleted() {
        return (state & DELETED) == DELETED;
    }

    @Override
    public int compareTo(EntityState o) {
        return state - o.state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EntityState that = (EntityState) o;

        return state == that.state;
    }

    @Override
    public int hashCode() {
        return state;
    }

}
