package org.nuiton.topia.persistence.legacy;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.SQLGrammarException;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Une classe qui permet d'obtenir les mapping de noms entre les entités et les objets de la base.
 *
 * On retrouve aussi ici des méthodes utils pour executer du code sql sur la base (notamment la gestion des séquences).
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class DBMapping {

    /** log */
    protected static final Log log = LogFactory.getLog(DBMapping.class);

    protected static final String CLASS_PATTERN = "(.+)\\.class\\.tagvalue\\.dbName";

    protected static final String DBNAME_ATTRIBUTE_PATTERN = "(.+).attribute.(\\w+)\\.tagvalue\\.dbName";

    protected static final String SEQUENCE_ATTRIBUTE_PATTERN = "(.+).attribute.(\\w+)\\.tagvalue\\.sequence";

    protected static final String CREATE_SEQUENCE_FORMAT = "create sequence %1$s%2$s_%3$s_sequence start (select max(%3$s) from %1$s%2$s);";

    protected static final String UPDATE_SEQUENCE_FORMAT = "alter sequence %1$s%2$s_%3$s_sequence restart with (select max(%3$s) from %1$s%2$s);";

    protected static final String CURRENT_VALUE_SEQUENCE_FORMAT = "select %1$s%2$s_%3$s_sequence.currval";

    protected static final String NEXT_VALUE_SEQUENCE_FORMAT = "select %1$s%2$s_%3$s_sequence.nextval";

    protected static final String SCHEMA_FORMAT = "model.tagvalue.dbSchema";

    protected static final String DOT = ".";

    protected Map<String, String> mappingBeanToDb;

    protected Map<String, Class<? extends TopiaEntity>> sequences;

    protected String schema;

    protected abstract Class<? extends TopiaEntity> getContractClass(Class<? extends TopiaEntity> entityClass) throws TopiaException;

    public DBMapping(String propertyFile, String path) throws IOException {
        mappingBeanToDb = new TreeMap<String, String>();
        sequences = new TreeMap<String, Class<? extends TopiaEntity>>();

        if (propertyFile == null) {
            propertyFile = path;
        }

        InputStream stream = getClass().getResourceAsStream(propertyFile);
        if (stream == null) {
            throw new IllegalStateException("no tagsvalues defined (did not find the resource : " + propertyFile + ")");
        }

        Properties props = new Properties();

        try {

            props.load(stream);

            initMapping(props);

        } finally {
            props.clear();
            stream.close();
        }
    }

    public void init(TopiaApplicationContext ctxt, boolean doCreate, boolean doUpdate) throws TopiaException {
        if (sequences.isEmpty()) {
            // no sequence registed
            return;
        }
        String firstSequenceKey = sequences.keySet().iterator().next();
        TopiaPersistenceContext newContext = ctxt.newPersistenceContext();
        TopiaSqlSupport sqlSupport = ((AbstractTopiaPersistenceContext)newContext).getSqlSupport();
        boolean exists = existSequence(firstSequenceKey, sqlSupport);

        if (!exists) {
            if (!doCreate) {
                // not exists and do not create
                return;
            }
            createSequences(sqlSupport);
        } else {
            if (doUpdate) {
                updateSequences(sqlSupport);
            }
        }
        newContext.commit();
        newContext.close();
    }

    public void createSequences(TopiaSqlSupport ctxt) throws TopiaException {
        if (log.isInfoEnabled()) {
            log.info("start create db sequences...");
        }
        for (String sequenceKey : sequences.keySet()) {
            createSequence(sequenceKey, ctxt, false);
        }
    }

    public void updateSequences(TopiaSqlSupport ctxt) throws TopiaException {
        if (log.isInfoEnabled()) {
            log.info("start update db sequences...");
        }
        for (String sequenceKey : sequences.keySet()) {
            updateSequence(sequenceKey, ctxt, false);
        }
    }

    public boolean existSequence(String sequenceKey, TopiaSqlSupport ctxt) throws TopiaException {
        return existSequence(sequenceKey, ctxt, true);
    }

    public void createSequence(String sequenceKey, TopiaSqlSupport ctxt) throws TopiaException {
        createSequence(sequenceKey, ctxt, true);
    }

    public void updateSequence(String sequenceKey, TopiaSqlSupport ctxt) throws TopiaException {
        updateSequence(sequenceKey, ctxt, true);
    }

    public BigInteger getCurrentValueFromSequence(String sequenceKey, TopiaSqlSupport ctxt) throws TopiaException {
        return getCurrentValueFromSequence(sequenceKey, ctxt, true);
    }

    public BigInteger getNextValueFromSequence(String sequenceKey, TopiaSqlSupport ctxt) throws TopiaException {
        return getNextValueFromSequence(sequenceKey, ctxt, true);
    }

    public boolean existSequence(Class<? extends TopiaEntity> entityClass, String propertyName, TopiaSqlSupport ctxt) throws TopiaException {
        String sequenceKey = checkSequence(entityClass, propertyName);
        return existSequence(sequenceKey, ctxt, false);
    }

    public void createSequence(Class<? extends TopiaEntity> entityClass, String propertyName, TopiaSqlSupport ctxt) throws TopiaException {
        String sequenceKey = checkSequence(entityClass, propertyName);
        createSequence(sequenceKey, ctxt, false);
    }

    public void updateSequence(Class<? extends TopiaEntity> entityClass, String propertyName, TopiaSqlSupport ctxt) throws TopiaException {
        String sequenceKey = checkSequence(entityClass, propertyName);
        updateSequence(sequenceKey, ctxt, false);
    }

    public BigInteger getCurrentValueFromSequence(Class<? extends TopiaEntity> entityClass, String propertyName, TopiaSqlSupport ctxt) throws TopiaException {
        String sequenceKey = checkSequence(entityClass, propertyName);
        return getCurrentValueFromSequence(sequenceKey, ctxt, false);
    }

    public BigInteger getNextValueFromSequence(Class<? extends TopiaEntity> entityClass, String propertyName, TopiaSqlSupport ctxt) throws TopiaException {
        String sequenceKey = checkSequence(entityClass, propertyName);
        return getNextValueFromSequence(sequenceKey, ctxt, false);
    }

    public Iterator<String> getSequenceKeysIterator() {
        return sequences.keySet().iterator();
    }

    public boolean existSequence(String sequenceKey, TopiaSqlSupport ctxt, boolean check) throws TopiaException {
        if (check) {
            checkSequence(sequenceKey);
        }
        try {
            getCurrentValueFromSequence(sequenceKey, ctxt, false);

        } catch (TopiaException e) {
            // the sequence's name does not exist in database, so it is a grammer exception
            if (e.getCause() != null && e.getCause().getClass() == SQLGrammarException.class) {
                return false;
            }
            throw e;
        }
        return true;
    }

    public void createSequence(String sequenceKey, TopiaSqlSupport ctxt, boolean check) throws TopiaException {
        if (check) {
            checkSequence(sequenceKey);
        }
        String sql = getSequenceSQL(CREATE_SEQUENCE_FORMAT, sequenceKey);
        doSQLWork(ctxt, sql);
        BigInteger currentValue = getNextValueFromSequence(sequenceKey, ctxt, false);

        if (log.isDebugEnabled()) {
            log.debug(sequenceKey + " currentValue " + currentValue.intValue());
        }
    }

    public void updateSequence(String sequenceKey, TopiaSqlSupport sqlSupport, boolean check) throws TopiaException {
        if (check) {
            checkSequence(sequenceKey);
        }
        String sql = getSequenceSQL(UPDATE_SEQUENCE_FORMAT, sequenceKey);
        doSQLWork(sqlSupport, sql);
        BigInteger currentValue = getNextValueFromSequence(sequenceKey, sqlSupport, false);
        if (log.isDebugEnabled()) {
            log.debug(sequenceKey + " currentValue " + currentValue.intValue());
        }
    }

    public BigInteger getCurrentValueFromSequence(String sequenceKey, TopiaSqlSupport ctxt, boolean check) throws TopiaException {
        if (check) {
            checkSequence(sequenceKey);
        }
        String sql = getSequenceSQL(CURRENT_VALUE_SEQUENCE_FORMAT, sequenceKey);
//        TopiaContext newCtxt = ctxt.beginTransaction();
        BigInteger bigInteger = getBigInteger(ctxt, sql, BigInteger.ZERO);
//        newCtxt.close();
        return bigInteger;
    }

    public BigInteger getNextValueFromSequence(String sequenceKey, TopiaSqlSupport ctxt, boolean check) throws TopiaException {
        if (check) {
            checkSequence(sequenceKey);
        }
        String sql = getSequenceSQL(NEXT_VALUE_SEQUENCE_FORMAT, sequenceKey);
        return getBigInteger(ctxt, sql, BigInteger.ZERO);
    }

    /**
     * @param entityClass the seek entity class
     * @param property    the name of the property to translate
     * @return the DB name for the given property
     * @throws TopiaException
     *          if any db pb
     */
    protected String getDBProperty(Class<? extends TopiaEntity> entityClass, String property) throws TopiaException {
        Class<? extends TopiaEntity> contractClass = getContractClass(entityClass);
        String key = contractClass.getName() + DOT + property;

        String colName = mappingBeanToDb.get(key);
        if (colName == null) {
            colName = property;
        }
        return colName;
    }

    /**
     * @param entityClass the seek entity class
     * @return the DB name for the given property
     * @throws TopiaException
     *          if any db pb
     */
    protected String getDBTable(Class<? extends TopiaEntity> entityClass) throws TopiaException {
        Class<? extends TopiaEntity> contractClass = getContractClass(entityClass);
        String key = contractClass.getName();
        String colName = mappingBeanToDb.get(key);

        if (colName == null) {
            colName = contractClass.getSimpleName().toLowerCase();
        }
        return colName;
    }

    protected String getSequenceSQL(String pattern, Class<? extends TopiaEntity> entityClass, String propertyName) throws TopiaException {
        String dbTable = getDBTable(entityClass);
        String dbPropertyName = getDBProperty(entityClass, propertyName);
        String sql = String.format(pattern, schema, dbTable, dbPropertyName);
        if (log.isTraceEnabled()) {
            log.trace("sql : " + sql);
        }
        return sql;
    }

    protected String getSequenceSQL(String pattern, String sequenceKey) throws TopiaException {
        Class<? extends TopiaEntity> entityClass = sequences.get(sequenceKey);
        String dbTable = getDBTable(entityClass);
        String propertyName = getSequencePropertyName(sequenceKey);
        String dbPropertyName = getDBProperty(entityClass, propertyName);
        String sql = String.format(pattern, schema, dbTable, dbPropertyName);
        if (log.isTraceEnabled()) {
            log.trace("sql : " + sql);
        }
        return sql;
    }

    protected BigInteger getBigInteger(TopiaSqlSupport sqlSupport, final String sql, BigInteger defaultSize) throws TopiaException {
        BigInteger size = defaultSize;
        if (sqlSupport != null) {
//            try {
//                SQLQuery query = ((AbstractTopiaContext) ctxt).getHibernate().createSQLQuery(sql);
//                size = (BigInteger) query.list().get(0);
//            } catch (SQLGrammarException e) {
//            // could not obtain sequence
//                throw new TopiaException(e);
//            }
            size = sqlSupport.findSingleResult(
                    sql,
                    set -> set.getBigDecimal(1).unscaledValue() // TODO AThimel 23/11/13 Don't know if it may work or not
            );

        }
        return size;
    }

    protected void doSQLWork(TopiaSqlSupport sqlSupport, final String sql) throws TopiaException {
        sqlSupport.executeSql(sql);
    }

    protected String getSequencePropertyName(String sequenceKey) {
        int dotIndex = sequenceKey.lastIndexOf(DOT);
        return sequenceKey.substring(dotIndex + 1);
    }

    protected String checkSequence(Class<? extends TopiaEntity> entityClass, String propertyName) throws IllegalArgumentException, TopiaException {
        Class<? extends TopiaEntity> contractClass = getContractClass(entityClass);
        String sequenceKey = contractClass.getName() + DOT + propertyName;
        if (!sequences.containsKey(sequenceKey)) {
            throw new IllegalArgumentException("could not find the sequence " + sequenceKey);
        }
        return sequenceKey;
    }

    protected String checkSequence(String sequenceKey) throws IllegalArgumentException, TopiaException {
        if (!sequences.containsKey(sequenceKey)) {
            throw new IllegalArgumentException("could not find the sequence " + sequenceKey);
        }
        return sequenceKey;
    }

    @SuppressWarnings({"unchecked"})
    protected void initMapping(Properties props) throws IOException {

        if (props.containsKey(SCHEMA_FORMAT)) {
            schema = props.getProperty(SCHEMA_FORMAT) + DOT;
        } else {
            schema = "";
        }

        Pattern classPattern = Pattern.compile(CLASS_PATTERN);

        Pattern dbNameAttributePattern = Pattern.compile(DBNAME_ATTRIBUTE_PATTERN);

        Pattern sequenceAttributePattern = Pattern.compile(SEQUENCE_ATTRIBUTE_PATTERN);

        for (Entry<Object, Object> entry : props.entrySet()) {
            String key = String.valueOf(entry.getKey());
            String value = String.valueOf(entry.getValue());
            Matcher matcher;

            matcher = dbNameAttributePattern.matcher(key);
            if (matcher.matches()) {
                // find a attribute property
                String clazz = matcher.group(1);
                String attribute = matcher.group(2);
                mappingBeanToDb.put(clazz + "." + attribute, value);

                continue;
            }
            matcher = classPattern.matcher(key);
            if (matcher.matches()) {
                // find a class property
                String clazz = matcher.group(1);
                mappingBeanToDb.put(clazz, value);
                continue;
            }
            matcher = sequenceAttributePattern.matcher(key);
            if (matcher.matches()) {
                // find a attribute property
                String clazz = matcher.group(1);
                String attribute = matcher.group(2);
                try {
                    boolean useSequence = Boolean.valueOf(value);
                    if (useSequence) {
                        Class<?> value1 = Class.forName(clazz);
                        if (TopiaEntity.class.isAssignableFrom(value1)) {
                            sequences.put(clazz + "." + attribute, (Class<? extends TopiaEntity>) value1);
                        } else {
                            log.warn("can not create a sequence on a non TopiaEntity class " + clazz);
                        }

                    }
                } catch (Exception e) {
                    log.warn("could not convert sequence value for entry " + key+" for reason "+e.getMessage());
                }

            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    public void close() {
        if (mappingBeanToDb != null) {
            mappingBeanToDb.clear();
        }
        if (sequences != null) {
            sequences.clear();
        }
    }

}
