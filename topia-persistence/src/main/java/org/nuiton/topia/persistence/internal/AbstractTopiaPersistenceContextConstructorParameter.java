package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: Persistence
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;

import java.time.Duration;
import java.util.Optional;

/**
 * Parameter-object design pattern for the {@link org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext}
 * constructor.
 *
 * This parameter-object is useful because we can add/change/remove parameters without changing
 * the constructor signature which is necessary to ensure backward compatibility with XxxPersistenceContext
 * overridden by the user.
 */
public class AbstractTopiaPersistenceContextConstructorParameter {

    protected final HibernateProvider hibernateProvider;

    protected final TopiaFiresSupport applicationFiresSupport;

    protected final TopiaIdFactory topiaIdFactory;

    protected final TopiaHibernateSessionRegistry sessionRegistry;

    protected final Optional<Duration> slowQueriesThreshold;

    /**
     * @param hibernateProvider       holds the Hibernate configuration and session factory
     * @param applicationFiresSupport the TopiaFiresSupport from the application context
     * @param topiaIdFactory          the TopiaIdFactory instance created according to the application's configuration
     * @param sessionRegistry         hibernate session registry
     * @param slowQueriesThreshold    configures after which delay a query is considered as slow
     */
    public AbstractTopiaPersistenceContextConstructorParameter(HibernateProvider hibernateProvider,
                                                               TopiaFiresSupport applicationFiresSupport,
                                                               TopiaIdFactory topiaIdFactory,
                                                               TopiaHibernateSessionRegistry sessionRegistry,
                                                               Optional<Duration> slowQueriesThreshold) {
        this.hibernateProvider = hibernateProvider;
        this.applicationFiresSupport = applicationFiresSupport;
        this.topiaIdFactory = topiaIdFactory;
        this.sessionRegistry = sessionRegistry;
        this.slowQueriesThreshold = slowQueriesThreshold;
    }

    public HibernateProvider getHibernateProvider() {
        return hibernateProvider;
    }

    public TopiaFiresSupport getApplicationFiresSupport() {
        return applicationFiresSupport;
    }

    public TopiaIdFactory getTopiaIdFactory() {
        return topiaIdFactory;
    }

    public TopiaHibernateSessionRegistry getSessionRegistry() {
        return sessionRegistry;
    }

    public Optional<Duration> getSlowQueriesThreshold() {
        return slowQueriesThreshold;
    }

}
