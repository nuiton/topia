package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.event.spi.AbstractEvent;
import org.hibernate.event.spi.EventSource;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.hibernate.event.spi.PostLoadEvent;
import org.hibernate.event.spi.PostLoadEventListener;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.event.spi.PreDeleteEvent;
import org.hibernate.event.spi.PreDeleteEventListener;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreLoadEvent;
import org.hibernate.event.spi.PreLoadEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.hibernate.event.spi.SaveOrUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaEntity;
import org.nuiton.topia.persistence.internal.TopiaHibernateSessionRegistry;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaHibernateEventListener implements PreInsertEventListener, PostInsertEventListener,
        PreLoadEventListener, PostLoadEventListener, PreUpdateEventListener, PostUpdateEventListener,
        PreDeleteEventListener, PostDeleteEventListener, SaveOrUpdateEventListener {

    private static final Log log = LogFactory.getLog(TopiaHibernateEventListener.class);

    private static final long serialVersionUID = -9206039888626756924L;

    protected TopiaHibernateSessionRegistry registry;

    public TopiaHibernateEventListener(TopiaHibernateSessionRegistry registry) {
        Preconditions.checkArgument(registry != null);
        this.registry = registry;
    }

    /**
     * Look for the TopiaPersistenceContext based on the given Hibernate session
     *
     * @param event the Hibernate event that contains the Session to use
     * @return the TopiaPersistenceContext using this Session, or null if not found
     */
    protected TopiaPersistenceContext getContext(AbstractEvent event) {
        EventSource session = event.getSession();
        TopiaPersistenceContext result = registry.getPersistenceContext(session);
        return result;
    }

    /**
     * Method to enrich an entity if possible with the daoSupplier or firesSupport instances
     *
     * @param entity       the entity to enrich
     * @param daoSupplier  the {@link org.nuiton.topia.persistence.TopiaDaoSupplier} instance
     * @param firesSupport the {@link org.nuiton.topia.persistence.internal.support.TopiaFiresSupport} instance
     */
    public static void attachContext(Object entity,
                                     TopiaDaoSupplier daoSupplier,
                                     TopiaFiresSupport firesSupport) {

        // If the entity is "contextable", inject the TopiaDaoSupplier
        if (entity instanceof TopiaEntityContextable) {
            TopiaEntityContextable topiaEntityContextable = (TopiaEntityContextable) entity;
            if (topiaEntityContextable.getTopiaDaoSupplier() == null) {
                try {
                    topiaEntityContextable.setTopiaDaoSupplier(daoSupplier);
                } catch (TopiaException eee) {
                    if (log.isWarnEnabled()) {
                        log.warn("Impossible d'initialiser le TopiaContext"
                                        + " sur cette entité : " + topiaEntityContextable,
                                eee);
                    }
                }
            }
        }

        // If the entity is a TopiaEntity, inject the TopiaFiresSupport
        if (entity instanceof AbstractTopiaEntity) {
            AbstractTopiaEntity topiaEntity = (AbstractTopiaEntity) entity;
            topiaEntity.setFiresSupport(firesSupport);
        }
    }

    /* Création */

    @Override
    public boolean onPreInsert(PreInsertEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) event.getEntity();

            context.getTopiaFiresSupport().fireOnPreCreate(context, entity, event.getState());

            // when using composition, hibernate will persist entities by him self
            // entity must have an id in this case.
            if (StringUtils.isBlank(entity.getTopiaId())) {
                if (log.isDebugEnabled()) {
                    log.debug("Adding topia id on entity " + entity.getClass().getSimpleName());
                }
            }
        }

        return false;
    }

    @Override
    public void onPostInsert(PostInsertEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPostCreate(context, entity, event.getState());
        }
    }

    // Hibernate 4.3.x
    @Override
    public boolean requiresPostCommitHandling(EntityPersister persister) {
        // XXX AThimel 17/12/13 I don't know what to return
        return false;
    }

    /* Chargement */

    @Override
    public void onPreLoad(PreLoadEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            //                try {
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPreLoad(context, entity, event.getState());
        }
    }

    @Override
    public void onPostLoad(PostLoadEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            attachContext(event.getEntity(), context, context.getTopiaFiresSupport());
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPostLoad(context, entity, new Object[]{});
        }
    }

    /* Modification */

    @Override
    public boolean onPreUpdate(PreUpdateEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPreUpdate(context, entity, event.getOldState());
        }
        return false;
    }

    @Override
    public void onPostUpdate(PostUpdateEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPostUpdate(context, entity, event.getState());
        }
    }

    /* Suppression */

    @Override
    public boolean onPreDelete(PreDeleteEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPreDelete(context, entity, event.getDeletedState());
        }
        return false;
    }

    @Override
    public void onPostDelete(PostDeleteEvent event) {
        TopiaPersistenceContext context = getContext(event);
        if (context != null && event.getEntity() instanceof TopiaEntity) {
            TopiaEntity entity = (TopiaEntity) event.getEntity();
            context.getTopiaFiresSupport().fireOnPostDelete(context, entity, event.getDeletedState());
        }
    }

    @Override
    public void onSaveOrUpdate(SaveOrUpdateEvent event) throws HibernateException {
        try {
            // this event is called when hibernate try to persist entities using cascade (save)
            TopiaPersistenceContext context = getContext(event);
            // warning, event.getEntity() return null here :(

            if (context != null && event.getObject() instanceof TopiaEntity) {
                TopiaEntity entity = (TopiaEntity) event.getObject();

                if (StringUtils.isBlank(entity.getTopiaId())) {
                    if (log.isDebugEnabled()) {
                        log.debug("Adding topiaId into entity " + entity.getClass());
                    }

                    // TopiaIdFactory#newTopiaId only requires interface. First get the entity class name.
                    String entityClassName = event.getEntityName();
                    // event.getEntityName() may be null in case .update(...) method is called on a new (not yet persisted) entity (see http://nuiton.org/issues/3153)
                    if (Strings.isNullOrEmpty(entityClassName)) {
                        entityClassName = entity.getClass().getName();
                    }

                    // Then extract interface name
                    // FIXME echatellier 20130713 : hack to find interface for current entity class
                    Class entityInterface = Class.forName(entityClassName.replace("Impl", ""));

                    // Generate topiaId
                    String topiaId = context.getTopiaIdFactory().newTopiaId(entityInterface, entity);
                    entity.setTopiaId(topiaId);
                }
            }
        } catch (ClassNotFoundException ex) {
            throw new HibernateException("Can't set id", ex);
        }
    }

}
