package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * {@link TopiaEntity} with {@link TopiaDaoSupplier} support (injected by
 * {@link org.nuiton.topia.persistence.internal.AbstractTopiaDao} into entities).
 *
 * WARNING, it is not recommended to use this mechanism as it breaks the POJO's Hibernate approach.
 *
 * @author chatellier
 */
public interface TopiaEntityContextable extends TopiaEntity {

    /**
     * @since 3.0
     */
    String PROPERTY_COMPOSITE = "composite";

    /**
     * @since 3.0
     */
    String PROPERTY_AGGREGATE = "aggregate";

    /**
     * @since 3.0
     */
    String PROPERTY_TOPIA_DAO_SUPPLIER = "topiaDaoSupplier";

    /**
     * Update entity in persistence context.
     */
    void update();

    /**
     * Delete entity in persistence context.
     */
    void delete();

    /**
     * Injects into this entity the given {@link TopiaDaoSupplier}, which can be used to
     * get any Dao instance. This is used to make {@link #update()} and {@link #delete()} implementation possible.
     *
     * @param topiaDaoSupplier an initialized {@link TopiaDaoSupplier} instance
     */
    void setTopiaDaoSupplier(TopiaDaoSupplier topiaDaoSupplier);

    /**
     * @return the currently injected {@link TopiaDaoSupplier}
     */
    TopiaDaoSupplier getTopiaDaoSupplier();

    /**
     * @return the TopiaDao managing the current entity. It may be null if the current entity isn't managed by ToPIA.
     * @since 3.0
     */
    public TopiaDao<?> getGenericEntityDao();

    /**
     * @return all objects that must be deleted if this object is deleted
     * @deprecated from 3.0, method will be moved to entity's generated Dao(cf http://nuiton.org/issues/2776)
     */
    @Deprecated
    List<TopiaEntity> getComposite();

    /**
     * @return all objects that are aggregate with this instance, aggregate object are not removed automatically
     * @deprecated from 3.0, method will be moved to entity's generated Dao (cf http://nuiton.org/issues/2776)
     */
    @Deprecated
    List<TopiaEntity> getAggregate();

}
