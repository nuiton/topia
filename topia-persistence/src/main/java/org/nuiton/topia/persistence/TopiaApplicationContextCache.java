package org.nuiton.topia.persistence;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.map.AbstractReferenceMap;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * <p>This class will provide a TopiaApplicationContext cache. Its usage is not necessary, contexts can be directly
 * created outside of this cache, then the cache will not contain any reference to the created ApplicationContexts.</p>
 *
 * <p>This class is 'inspired' from the TopiaContextFactory of ToPIA 2.x</p>
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public class TopiaApplicationContextCache {

    private static final Log log = LogFactory.getLog(TopiaApplicationContextCache.class);

    /**
     * Cache that contains all some already registered caches
     */
    protected static Map<Properties, TopiaApplicationContext> contextCache =
            new ReferenceMap<Properties, TopiaApplicationContext>(AbstractReferenceMap.ReferenceStrength.HARD,
                    AbstractReferenceMap.ReferenceStrength.SOFT);

    /**
     * Static method that can provide an ApplicationContext. If the found context is closed, or if it does not exist, a
     * new one is created using the given {@code Function<Properties, C>}.
     *
     * @param <C>                   the type of TopiaApplicationContext you expect
     * @param config                the configuration of the context
     * @param createContextFunction the function that will be in charge of the context creation. It might not be used
     *                              inside the method if the context is found and still opened
     * @return an opened instance of XyzTopiaApplicationContext
     */
    public static <C extends TopiaApplicationContext> C getContext(
            Properties config, Function<Properties, C> createContextFunction) {
        // Put all properties from a hierarchy in the current properties object.
        // Resolve problem with hibernate which used iterator to get properties
        // and so only values from the current properties object and not all
        // hierarchy
        Properties cloned = new Properties();
        for (String key : config.stringPropertyNames()) {
            cloned.setProperty(key, config.getProperty(key));
        }
        C result = (C) contextCache.get(cloned);
        if (result == null || result.isClosed()) {
            if (createContextFunction == null) {
                throw new TopiaException(
                        "Function<Properties, AbstractTopiaApplicationContext> is required to create context");
            } else {
                result = createContextFunction.apply(cloned);
            }
            if (log.isDebugEnabled()) {
                log.debug("New context created : " + result);
            }
            contextCache.put(cloned, result);
        } else if (log.isDebugEnabled()) {
            log.debug("Context found : " + result);
        }
        return result;
    }

    /**
     * Method to get the list of contexts in memory (each context is identified by its URL).
     *
     * To be used for debugging purpose only.
     *
     * @return a list of contexts URLs
     */
    public static List<String> getRegisteredContextUrls() {
        List<String> result = Lists.newArrayList();
        for (Properties e : contextCache.keySet()) {
            result.add(e.getProperty(TopiaConfigurationBuilder.CONFIG_URL));
        }
        return result;
    }

    /**
     * Remove the given TopiaApplicationContext from the registered ones
     *
     * @param context the context instance to remove
     */
    public static void removeContext(TopiaApplicationContext context) {

        Iterator<TopiaApplicationContext> it = contextCache.values().iterator();

        while (it.hasNext()) {
            TopiaApplicationContext curr = it.next();
            if (curr == context) {
                it.remove();
                break;
            }
        }
    }

}
