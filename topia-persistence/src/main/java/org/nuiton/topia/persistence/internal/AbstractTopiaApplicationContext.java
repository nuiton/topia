package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaApplicationContextCache;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaMigrationService;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaService;
import org.nuiton.topia.persistence.event.TopiaEntitiesVetoable;
import org.nuiton.topia.persistence.event.TopiaEntityListener;
import org.nuiton.topia.persistence.event.TopiaEntityVetoable;
import org.nuiton.topia.persistence.event.TopiaSchemaListener;
import org.nuiton.topia.persistence.event.TopiaTransactionListener;
import org.nuiton.topia.persistence.event.TopiaTransactionVetoable;
import org.nuiton.topia.persistence.internal.support.TopiaFiresSupport;
import org.nuiton.topia.persistence.internal.support.TopiaServiceSupportImpl;
import org.nuiton.topia.persistence.support.TopiaServiceSupport;
import org.nuiton.topia.persistence.util.TopiaUtil;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * The application context is the main class in ToPIA usage. This class is a kind of equivalent of the RootTopiaContext.
 * It contains only high level methods and new contexts creation (transaction begin, ...). This class has to be extended
 * by the user, even if some default one could be automatically generated.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public abstract class AbstractTopiaApplicationContext<K extends TopiaPersistenceContext> implements TopiaApplicationContext<K> {

    private static final Log log = LogFactory.getLog(AbstractTopiaApplicationContext.class);

    protected TopiaIdFactory topiaIdFactory;

    protected TopiaConfiguration configuration;

    protected TopiaFiresSupport topiaFiresSupport;

    protected TopiaServiceSupport topiaServiceSupport;

    protected HibernateProvider hibernateProvider;

    protected TopiaHibernateSessionRegistry sessionRegistry;

    protected boolean closed = false;

    /**
     * A set of known initialized opened persistence contexts. This set is mainly used to trigger shutdown on these
     * persistence contexts when the application context is closed.
     */
    protected Set<TopiaPersistenceContext> persistenceContexts = Collections.newSetFromMap(
            new WeakHashMap<TopiaPersistenceContext, Boolean>());

    /**
     * @param properties FIXME
     * @deprecated use {@link #AbstractTopiaApplicationContext(org.nuiton.topia.persistence.TopiaConfiguration)}
     */
    @Deprecated
    public AbstractTopiaApplicationContext(Properties properties) {
        this(new TopiaConfigurationBuilder().readProperties(properties));
    }

    /**
     * @param map FIXME
     * @deprecated use {@link #AbstractTopiaApplicationContext(org.nuiton.topia.persistence.TopiaConfiguration)}
     */
    @Deprecated
    public AbstractTopiaApplicationContext(Map<String, String> map) {
        this(new TopiaConfigurationBuilder().readMap(map));
    }

    public AbstractTopiaApplicationContext(TopiaConfiguration configuration) {
        this.configuration = configuration;
        init();
    }

    protected void init() {

        new TopiaConfigurationBuilder().check(configuration);

        topiaFiresSupport = new TopiaFiresSupport();
        sessionRegistry = new TopiaHibernateSessionRegistry();

        // First initialize all the services
        initServices();

        // ToPIA's schema init
        if (isInitSchema()) {
            if (log.isInfoEnabled()) {
                log.info("Schema initialization enabled");
            }
            initSchema();
        } else {
            if (log.isInfoEnabled()) {
                log.info("Schema initialization disabled");
            }
        }

        // AThimel 14/06/14 Make sure this method is called AFTER ToPIA's schema init, otherwise Hibernate may have created the schema itself
        // The next line will trigger the Configuration#buildMappings() method which really initializes Hibernate
        getHibernateProvider().getHibernateConfiguration();

    }

    protected void initServices() {
        TopiaServiceSupportImpl topiaServiceSupportImpl = new TopiaServiceSupportImpl();
        this.topiaServiceSupport = topiaServiceSupportImpl;
        topiaServiceSupportImpl.initServices(this);
    }

    /**
     * If application context should init schema.
     *
     * This simple check is in its own method so it can be overridden.
     *
     * @return FIXME
     * @see org.nuiton.topia.persistence.TopiaConfiguration#isInitSchema()
     */
    protected boolean isInitSchema() {
        boolean initSchema = configuration.isInitSchema();
        return initSchema;
    }

    /**
     * Will make everything possible to ensure the schema is ready to use.
     *
     * It will create unless it already exists. If it already exists, we will try to find
     * a migration service and call it.
     */
    @Override
    public void initSchema() {

        Collection<TopiaMigrationService> migrationServices =
                getServices(TopiaMigrationService.class).values();

        Preconditions.checkState(migrationServices.size() <= 1,
                "your configuration include multiple migration services: " + migrationServices);

        boolean migrationServiceEnabled = ! migrationServices.isEmpty();

        if (isSchemaEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("schema is empty, will create");
            }
            createSchema();
            if (migrationServiceEnabled) {
                if (log.isInfoEnabled()) {
                    log.info("schema created, will call migration service");
                }
                TopiaMigrationService migrationService =
                        Iterables.getOnlyElement(migrationServices);
                migrationService.initOnCreateSchema();
            } else {
                if (log.isInfoEnabled()) {
                    log.info("schema created, no migration service provided");
                }
            }
        } else {
            if (migrationServiceEnabled) {
                if (log.isInfoEnabled()) {
                    log.info("schema exists, will try to migrate");
                }
                TopiaMigrationService migrationService =
                        Iterables.getOnlyElement(migrationServices);
                migrationService.runSchemaMigration();
            } else {
                if (log.isInfoEnabled()) {
                    log.info("schema exists, no migration service provided");
                }
            }
        }
    }

    protected abstract Set<Class<? extends TopiaEntity>> getImplementationClasses();

    protected void registerPersistenceContext(TopiaPersistenceContext persistenceContext) {
        persistenceContexts.add(persistenceContext);
    }

    // FIXME AThimel 25/11/13 I don't like it to be public, but necessary for services. Review it
    public HibernateProvider getHibernateProvider() {
        if (hibernateProvider == null) {
            // La liste des entités (ToPIA ou non) qu'Hibernate gère
            List<Class<?>> persistenceClasses = getPersistenceClasses();
            hibernateProvider = new HibernateProvider(getConfiguration(), topiaServiceSupport, sessionRegistry, persistenceClasses);
        }
        return hibernateProvider;
    }

    protected TopiaFiresSupport getTopiaFiresSupport() {
        return topiaFiresSupport;
    }

    @Override
    public TopiaConfiguration getConfiguration() {
        return configuration;
    }

    protected TopiaIdFactory getTopiaIdFactory() {
        return getConfiguration().getTopiaIdFactory();
    }

    public TopiaHibernateSessionRegistry getSessionRegistry() {
        return sessionRegistry;
    }

    @Override
    public void addTopiaEntityListener(TopiaEntityListener listener) {
        topiaFiresSupport.addTopiaEntityListener(listener);
    }

    @Override
    public void addTopiaEntityListener(Class<? extends TopiaEntity> entityClass, TopiaEntityListener listener) {
        topiaFiresSupport.addTopiaEntityListener(entityClass, listener);
    }

    @Override
    public void removeTopiaEntityListener(TopiaEntityListener listener) {
        topiaFiresSupport.removeTopiaEntityListener(listener);
    }

    @Override
    public void removeTopiaEntityListener(Class<? extends TopiaEntity> entityClass, TopiaEntityListener listener) {
        topiaFiresSupport.removeTopiaEntityListener(entityClass, listener);
    }

    @Override
    public void addTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        topiaFiresSupport.addTopiaEntityVetoable(vetoable);
    }

    @Override
    public void addTopiaEntityVetoable(Class<? extends TopiaEntity> entityClass, TopiaEntityVetoable vetoable) {
        topiaFiresSupport.addTopiaEntityVetoable(entityClass, vetoable);
    }

    @Override
    public void removeTopiaEntityVetoable(TopiaEntityVetoable vetoable) {
        topiaFiresSupport.removeTopiaEntityVetoable(vetoable);
    }

    @Override
    public void removeTopiaEntityVetoable(Class<? extends TopiaEntity> entityClass, TopiaEntityVetoable vetoable) {
        topiaFiresSupport.removeTopiaEntityVetoable(entityClass, vetoable);
    }

    @Override
    public void addTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        topiaFiresSupport.addTopiaEntitiesVetoable(vetoable);
    }

    @Override
    public void removeTopiaEntitiesVetoable(TopiaEntitiesVetoable vetoable) {
        topiaFiresSupport.removeTopiaEntitiesVetoable(vetoable);
    }

    @Override
    public void addTopiaTransactionListener(TopiaTransactionListener listener) {
        topiaFiresSupport.addTopiaTransactionListener(listener);
    }

    @Override
    public void removeTopiaTransactionListener(TopiaTransactionListener listener) {
        topiaFiresSupport.removeTopiaTransactionListener(listener);
    }

    @Override
    public void addTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        topiaFiresSupport.addTopiaTransactionVetoable(vetoable);
    }

    @Override
    public void removeTopiaTransactionVetoable(TopiaTransactionVetoable vetoable) {
        topiaFiresSupport.removeTopiaTransactionVetoable(vetoable);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        topiaFiresSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        topiaFiresSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addTopiaSchemaListener(TopiaSchemaListener listener) {
        topiaFiresSupport.addTopiaSchemaListener(listener);
    }

    @Override
    public void removeTopiaSchemaListener(TopiaSchemaListener listener) {
        topiaFiresSupport.removeTopiaSchemaListener(listener);
    }

    @Override
    public Map<String, TopiaService> getServices() {
        return topiaServiceSupport.getServices();
    }

    @Override
    public <T extends TopiaService> Map<String, T> getServices(Class<T> interfaceService) {
        return topiaServiceSupport.getServices(interfaceService);
    }

    @Override
    public List<Class<?>> getPersistenceClasses() {
        // Par défaut la liste des entités qu'Hibernate gère est seulement la liste des entités ToPIA mais en
        // surchargeant cette méthode on peut ajouter des entités non ToPIA
        return new ArrayList<>(getImplementationClasses());
    }

    @Override
    public boolean isSchemaEmpty() {
        // AThimel 14/06/14 getHibernateConfiguration() may create the schema, prefer using newHibernateConfiguration() which doesn't
        Configuration configuration = getHibernateProvider().newHibernateConfiguration();
        configuration.getProperties().put(AvailableSettings.HBM2DDL_AUTO, "none");
        SessionFactory sessionFactory = hibernateProvider.newSessionFactory(configuration);
        try {
            Metadata metaData = getHibernateProvider().newMetaData(configuration, sessionFactory);
            boolean result = TopiaUtil.isSchemaEmpty(configuration, metaData);
            return result;
        } finally {
            sessionFactory.close();
        }
    }

    @Override
    public boolean isTableExists(Class<?> clazz) {
        // AThimel 14/06/14 getHibernateConfiguration() may create the schema, prefer using newHibernateConfiguration() which doesn't
        Configuration configuration = getHibernateProvider().getHibernateConfiguration();
        Metadata metaData = getHibernateProvider().getMetaData();
        boolean result = TopiaUtil.isSchemaExist(configuration, metaData, clazz.getName());
        return result;
    }

    @Override
    public String getSchemaName() {
        // TODO AThimel 02/08/13 I absolutely don't know if it works
        return getConfiguration().getSchemaName();
    }

    @Override
    public void createSchema() {
        try {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
            if (log.isDebugEnabled()) {
                targetTypes.add(TargetType.STDOUT);
            }
            topiaFiresSupport.firePreCreateSchema(this);
            Configuration configuration = getHibernateProvider().newHibernateConfiguration();
            configuration.getProperties().remove(AvailableSettings.HBM2DDL_AUTO);
            SessionFactory sessionFactory = getHibernateProvider().newSessionFactory(configuration);
            try {
                Metadata metadata = getHibernateProvider().newMetaData(configuration, sessionFactory);
                new SchemaExport().execute(targetTypes, SchemaExport.Action.CREATE, metadata);
                topiaFiresSupport.firePostCreateSchema(this);
            } finally {
                sessionFactory.close();
            }
        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not create schema for reason: %s", eee.getMessage()), eee);
        }
    }

    @Override
    public void showCreateSchema() {
        try {
            new SchemaExport().execute(EnumSet.of(TargetType.DATABASE, TargetType.STDOUT), SchemaExport.Action.CREATE, getHibernateProvider().getMetaData());
        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not show create schema for reason: %s", eee.getMessage()), eee);
        }

    }

    @Override
    public void updateSchema() {
        try {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
            if (log.isDebugEnabled()) {
                targetTypes.add(TargetType.STDOUT);
            }
            topiaFiresSupport.firePreUpdateSchema(this);
            new SchemaUpdate().execute(targetTypes, getHibernateProvider().getMetaData());
            topiaFiresSupport.firePostUpdateSchema(this);
        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not update schema for reason: %s", eee.getMessage()), eee);
        }
    }

    @Override
    public void dropSchema() {
        try {
            EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE);
            if (log.isDebugEnabled()) {
                targetTypes.add(TargetType.STDOUT);
            }
            topiaFiresSupport.firePreDropSchema(this);
            new SchemaExport().execute(targetTypes, SchemaExport.Action.DROP, getHibernateProvider().getMetaData());
            topiaFiresSupport.firePostDropSchema(this);
        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not drop schema for reason: %s", eee.getMessage()), eee);
        }
    }

    @Override
    public void close() {

        // Throw exception if context is already closed
        Preconditions.checkState(!closed, "TopiaApplicationContext was already closed");

        if (log.isDebugEnabled()) {
            log.debug("will close " + this);
        }

        // Iterate over the children PersistenceContexts and close them
        for (TopiaPersistenceContext persistenceContext : persistenceContexts) {
            if (persistenceContext == null) {
                if (log.isWarnEnabled()) {
                    log.warn("null TopiaPersistenceContext found in #persistenceContexts");
                }
            } else {
                // Avoid to have exception from checkNotClosed method on child
                try {
                    if (!persistenceContext.isClosed()) {
                        persistenceContext.close();
                    }
                } catch (Exception eee) {
                    // Don't let any exception stop the application closing
                    if (log.isWarnEnabled()) {
                        log.warn("unable to close TopiaPersistenceContext", eee);
                    }
                }
            }
        }

        hibernateProvider.close();

        // call TopiaService#close on all services
        for (TopiaService topiaService : getServices().values()) {
            topiaService.close();
        }

        closed = true;

        // Context is closed, make sure it is not referenced anymore from the context cache
        TopiaApplicationContextCache.removeContext(this);

        if (log.isDebugEnabled()) {
            log.debug(this + " closed");
        }
    }

    @Override
    public ImmutableSet<String> getSchemaNames() {

        ImmutableSet.Builder<String> result = ImmutableSet.builder();
        Collection<PersistentClass> classMappings = hibernateProvider.getMetaData().getEntityBindings();
        for (PersistentClass persistentClass : classMappings) {
            String schema = persistentClass.getIdentityTable().getSchema();
            if (StringUtils.isNotEmpty(schema)) {
                result.add(schema);
            }
        }
        return result.build();

    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public boolean isOpened() {
        return ! isClosed();
    }
}
