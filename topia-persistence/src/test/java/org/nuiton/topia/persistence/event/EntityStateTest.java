package org.nuiton.topia.persistence.event;

/*
 * #%L
 * ToPIA :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class EntityStateTest {

    @Test
    public void testStates() {
        EntityState state = new EntityState();

        Assert.assertFalse(state.isLoaded());
        Assert.assertFalse(state.isRead());
        Assert.assertFalse(state.isCreated());
        Assert.assertFalse(state.isWritten());
        Assert.assertFalse(state.isUpdated());
        Assert.assertFalse(state.isDeleted());

        state.addLoaded();
        Assert.assertTrue(state.isLoaded());
        Assert.assertFalse(state.isRead());
        Assert.assertFalse(state.isCreated());
        Assert.assertFalse(state.isWritten());
        Assert.assertFalse(state.isUpdated());
        Assert.assertFalse(state.isDeleted());

        state.addRead();
        Assert.assertTrue(state.isLoaded());
        Assert.assertTrue(state.isRead());
        Assert.assertFalse(state.isCreated());
        Assert.assertFalse(state.isWritten());
        Assert.assertFalse(state.isUpdated());
        Assert.assertFalse(state.isDeleted());

        state.addCreated();
        Assert.assertTrue(state.isLoaded());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreated());
        Assert.assertFalse(state.isWritten());
        Assert.assertFalse(state.isUpdated());
        Assert.assertFalse(state.isDeleted());

        state.addWritten();
        Assert.assertTrue(state.isLoaded());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreated());
        Assert.assertTrue(state.isWritten());
        Assert.assertFalse(state.isUpdated());
        Assert.assertFalse(state.isDeleted());

        state.addUpdated();
        Assert.assertTrue(state.isLoaded());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreated());
        Assert.assertTrue(state.isWritten());
        Assert.assertTrue(state.isUpdated());
        Assert.assertFalse(state.isDeleted());

        state.addDeleted();
        Assert.assertTrue(state.isLoaded());
        Assert.assertTrue(state.isRead());
        Assert.assertTrue(state.isCreated());
        Assert.assertTrue(state.isWritten());
        Assert.assertTrue(state.isUpdated());
        Assert.assertTrue(state.isDeleted());

    }

    @Test
    public void testStateDeletedFromScratch() {

        EntityState state = new EntityState();
        state.addDeleted();
        Assert.assertFalse(state.isRead());
        Assert.assertFalse(state.isCreated());
        Assert.assertFalse(state.isWritten());
        Assert.assertFalse(state.isUpdated());
        Assert.assertTrue(state.isDeleted());
    }

}
