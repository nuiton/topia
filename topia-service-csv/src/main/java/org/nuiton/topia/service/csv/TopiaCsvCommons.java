package org.nuiton.topia.service.csv;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.StringUtil;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.decorator.Decorator;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * More useful method added to {@link Common}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class TopiaCsvCommons extends Common {

    protected TopiaCsvCommons() {
        // no instance of this helper
    }

    public static final ValueParserFormatter<Date> DAY_TIME_SECOND_WITH_TIMESTAMP =
            new DateValue("dd/MM/yyyy HH:mm:ss") {

                @Override
                public Date parse(String value) throws ParseException {

                    Date parse = super.parse(value);
                    if (parse != null) {
                        parse = new Timestamp(parse.getTime());
                    }
                    return parse;
                }
            };

    public static final ValueParserFormatter<Date> DAY_TIME_SECOND_MILI_WITH_TIMESTAMP =
            new DateValue("dd/MM/yyyy HH:mm:ss.SSSS") {

                @Override
                public Date parse(String value) throws ParseException {

                    Date parse = super.parse(value);
                    if (parse != null) {
                        parse = new Timestamp(parse.getTime());
                    }
                    return parse;
                }
            };
    public static final AssociationValueParser ASSOCIATION_VALUE_PARSER = new AssociationValueParser();

    public static <E extends TopiaEntity> ForeignKeyValue<E> newForeignKeyValue(Class<E> type, String propertyName, Map<String, E> universe) {
        return new ForeignKeyValue<E>(type, propertyName, universe);
    }

    public static <E extends TopiaEntity> ForeignKeyValueForAssociation<E> newForeignKeyValueAssociation(Class<E> type, String propertyName, Map<String, E> universe) {
        return new ForeignKeyValueForAssociation<E>(type, propertyName, universe);
    }

    public static <E extends TopiaEntity> ValueFormatter<Collection<E>> newAssociationValueFormatter() {
        return new AssociationValueParserFormatter<E>(null, null);
    }

    public static <E> ForeignKeyDecoratedValue<E> newForeignKeyDecoratedValue(Decorator<E> decorator) {
        return new ForeignKeyDecoratedValue<E>(decorator);
    }

    /**
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.12
     */
    public static class AssociationValueParser implements ValueParser<String[]> {

        @Override
        public String[] parse(String value) throws ParseException {
            String[] ids = value.split("\\|");
            return ids;
        }
    }

    /**
     * @param <E>
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.12
     */
    public static class AssociationValueParserFormatter<E extends TopiaEntity> implements ValueParserFormatter<Collection<E>> {

        protected final Class<E> entityType;

        protected final Map<String, E> universe;

        public AssociationValueParserFormatter(
                Class<E> entityType,
                Map<String, E> universe) {
            this.entityType = entityType;
            this.universe = universe;
        }

        @Override
        public Collection<E> parse(String value) throws ParseException {
            Collection<E> result = Lists.newArrayList();
            if (StringUtils.isNotBlank(value)) {

                String[] ids = value.split("\\|");
                for (String id : ids) {
                    E association = universe.get(id);
                    association.setTopiaId(id);
                    result.add(association);
                }
            }
            return result;
        }

        @Override
        public String format(Collection<E> e) {

            String value;
            if (CollectionUtils.isEmpty(e)) {
                value = "";
            } else {
                Collection<String> ids = Lists.newArrayList();
                for (E e1 : e) {
                    ids.add(e1.getTopiaId());
                }
                value = StringUtil.join(ids, "|", true);
            }
            return value;
        }
    }

    /**
     * TODO
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.12
     */
    public static class ForeignKeyDecoratedValue<E> implements ValueFormatter<E> {

        protected final Decorator<E> decorator;

        public ForeignKeyDecoratedValue(Decorator<E> decorator) {
            this.decorator = decorator;
        }

        @Override
        public String format(E e) {
            String value = "";
            if (e != null) {
                value = decorator.toString(e);
            }
            return value;
        }
    }

    /**
     * @param <E>
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.12
     */
    public static class ForeignKeyValue<E extends TopiaEntity> implements ValueParserFormatter<E> {

        protected final String propertyName;

        protected final Class<E> entityType;

        protected final Map<String, E> universe;

        public ForeignKeyValue(Class<E> entityType,
                               String propertyName,
                               Map<String, E> universe) {
            this.entityType = entityType;
            this.propertyName = propertyName;
            this.universe = universe;
        }


        @Override
        public E parse(String value) throws ParseException {
            E result = null;
            if (StringUtils.isNotBlank(value)) {

                // get entity from universe
                result = universe.get(value);

                if (result == null) {

                    // can not find entity this is a big problem for us...
                    throw new TopiaException(
                            "Could not find entity of type " +
                            entityType.getSimpleName() + " with '" +
                            propertyName + "' = " + value);
                }
            }
            return result;
        }

        @Override
        public String format(E e) {
            String value = "";
            if (e != null) {
                value = e.getTopiaId();
            }
            return value;
        }
    }

    public static class ForeignKeyValueForAssociation<E extends TopiaEntity> implements ValueParser<Collection<E>> {

        protected final String propertyName;

        protected final Class<E> entityType;

        protected final Map<String, E> universe;

        public ForeignKeyValueForAssociation(Class<E> entityType,
                                             String propertyName,
                                             Map<String, E> universe) {
            this.entityType = entityType;
            this.propertyName = propertyName;
            this.universe = universe;
        }

        @Override
        public Collection<E> parse(String value) throws ParseException {
            E result = null;
            if (StringUtils.isNotBlank(value)) {

                // get entity from universe
                result = universe.get(value);

                if (result == null) {

                    // can not find entity this is a big problem for us...
                    throw new TopiaException(
                            "Could not find entity with '" + propertyName + "' = " + value);
                }
            }
            return Arrays.asList(result);
        }
    }
}
