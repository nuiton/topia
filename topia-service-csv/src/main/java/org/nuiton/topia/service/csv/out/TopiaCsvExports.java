package org.nuiton.topia.service.csv.out;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.MetaFilenameAware;
import org.nuiton.topia.persistence.metadata.Metadatas;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.util.TimeLog;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ext.RepeatableExport;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Helper for csv exports.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class TopiaCsvExports {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TopiaCsvExports.class);


    public static final TimeLog TIME_LOG = new TimeLog(TopiaCsvExports.class);


    protected TopiaCsvExports() {
        // no instance of this helper
    }

    public static <T extends TopiaEntityEnum> String exportData(TableMeta<T> tableMeta,
                                                                ExportModelFactory<T> modelFactory,
                                                                PrepareDataForExport<T> prepareDataForExport) {


        long s1 = TimeLog.getTime();
        Export<TopiaEntity> export = prepareExport(tableMeta, modelFactory, prepareDataForExport);
        TIME_LOG.log(s1, "exportData::prepareExport");
        long s2 = TimeLog.getTime();
        String result;
        try {
            result = export.toString(Charsets.UTF_8);
        } catch (Exception eee) {
            throw new TopiaException("Can not export datas", eee);
        }
        TIME_LOG.log(s2, "exportData::exportToString");
        return result;
    }

    public static <T extends TopiaEntityEnum> void exportData(TableMeta<T> tableMeta,
                                                              ExportModelFactory<T> modelFactory,
                                                              PrepareDataForExport<T> prepareDataForExport,
                                                              File file) {

        if (log.isInfoEnabled()) {
            log.info("Export table " + tableMeta + " to " + file);
        }
        long s1 = TimeLog.getTime();
        Export<TopiaEntity> export = prepareExport(tableMeta, modelFactory, prepareDataForExport);
        TIME_LOG.log(s1, "exportDatas::prepareExport");
        long s2 = TimeLog.getTime();
        try {
            export.write(file, Charsets.UTF_8);
        } catch (Exception eee) {
            throw new TopiaException("Can not export datas", eee);
        }
        TIME_LOG.log(s2, "exportData::exportToFile");
    }

    public static <T extends TopiaEntityEnum> void exportData(AssociationMeta<T> associationMeta,
                                                              ExportModelFactory<T> modelFactory,
                                                              PrepareDataForExport<T> prepareDataForExport,
                                                              File file) {

        if (log.isInfoEnabled()) {
            log.info("Export association " + associationMeta + " to " + file);
        }
        long s1 = TimeLog.getTime();

        Export<TopiaEntity> export = prepareExport(associationMeta, modelFactory, prepareDataForExport);
        TIME_LOG.log(s1, "exportData::prepareExport");

        long s2 = TimeLog.getTime();
        try {
            export.write(file, Charsets.UTF_8);
        } catch (Exception eee) {
            throw new TopiaException("Can not export datas", eee);
        }
        TIME_LOG.log(s2, "exportData::exportToFile");
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> Export<E> prepareExport(TableMeta<T> tableMeta,
                                                                                             ExportModelFactory<T> modelFactory,
                                                                                             PrepareDataForExport<T> prepareDataForExport) {

        Iterable<E> datas = prepareDataForExport.prepareData(tableMeta);
        ExportModel<E> model = modelFactory.buildForExport(tableMeta);
        return Export.newExport(model, datas);
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> Export<E> prepareExport(AssociationMeta<T> associationMeta,
                                                                                             ExportModelFactory<T> modelFactory,
                                                                                             PrepareDataForExport<T> prepareDataForExport) {

        Iterable<E> datas = prepareDataForExport.prepareData(associationMeta);
        ExportModel<E> model = modelFactory.buildForExport(associationMeta);
        return Export.newExport(model, datas);
    }

    public static <T extends TopiaEntityEnum> Map<T, EntityExportContext<T>> createReplicateEntityVisitorContexts(ExportModelFactory<T> modelFactory,
                                                                                                                  MetaFilenameAware<T>[] entityMetas,
                                                                                                                  Multimap<T, MetaFilenameAware<T>> associations,
                                                                                                                  File container) {

        Preconditions.checkNotNull(modelFactory);
        Preconditions.checkNotNull(entityMetas);
        Preconditions.checkNotNull(associations);
        Preconditions.checkNotNull(container);

        Map<T, EntityExportContext<T>> contexts = Maps.newHashMap();

        for (MetaFilenameAware<T> entityMeta : entityMetas) {
            TableMeta<T> meta = (TableMeta<T>) entityMeta;

            ExportModel<TopiaEntity> model = modelFactory.buildForExport(meta);

            EntityExportContext<T> exportContext = EntityExportContext.newExportContext(
                    model,
                    meta,
                    container);

            T source = meta.getSource();

            contexts.put(source, exportContext);

            for (MetaFilenameAware<T> metaFilenameAware : associations.get(source)) {
                AssociationMeta<T> associationMeta =
                        (AssociationMeta<T>) metaFilenameAware;

                ExportModel<TopiaEntity> associationModel =
                        modelFactory.buildForExport(associationMeta);
                exportContext.addAssociationExportContext(associationMeta,
                                                          associationModel,
                                                          container);
            }
        }
        return contexts;
    }


    public static <T extends TopiaEntityEnum> Map<T, EntityExportContext<T>> createReplicateEntityVisitorContexts(ExportModelFactory<T> modelFactory,
                                                                                                                  Iterable<TableMeta<T>> entityMetas,
                                                                                                                  Iterable<AssociationMeta<T>> associationMetas,
                                                                                                                  File container) {

        Preconditions.checkNotNull(modelFactory);
        Preconditions.checkNotNull(entityMetas);
        Preconditions.checkNotNull(associationMetas);
        Preconditions.checkNotNull(container);

        Multimap<T, AssociationMeta<T>> associations = Metadatas.split(associationMetas);
        Map<T, EntityExportContext<T>> contexts = Maps.newHashMap();

        for (TableMeta<T> meta : entityMetas) {

            ExportModel<TopiaEntity> model = modelFactory.buildForExport(meta);

            EntityExportContext<T> exportContext = EntityExportContext.newExportContext(
                    model,
                    meta,
                    container);

            T source = meta.getSource();

            contexts.put(source, exportContext);

            for (AssociationMeta<T> associationMeta : associations.get(source)) {

                ExportModel<TopiaEntity> associationModel =
                        modelFactory.buildForExport(associationMeta);
                exportContext.addAssociationExportContext(associationMeta,
                                                          associationModel,
                                                          container);
            }
        }
        return contexts;
    }

    /**
     * to export entity as csv files.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.12
     */
    public static class EntityExportContext<T extends TopiaEntityEnum> implements Closeable {

        /** meta to export. */
        protected final TableMeta<T> meta;

        /** Exporter object. */
        protected final RepeatableExport export;

        /** Where to export datas. */
        protected final Writer writer;

        /**
         * Unique list to store data to export. (will be shared with
         * association export contexts.)
         */
        private final List<TopiaEntity> data;

        /** Association export context for this type of entity. */
        protected final Collection<AssociationExportContext<T>> associationExportContexts;

        protected final File entryFile;

        public static <T extends TopiaEntityEnum> EntityExportContext<T> newExportContext(
                ExportModel<TopiaEntity> model,
                TableMeta<T> meta,
                File container) {
            return new EntityExportContext<T>(model, meta, container);
        }

        protected EntityExportContext(ExportModel<TopiaEntity> model,
                                      TableMeta<T> meta,
                                      File container) {
            Preconditions.checkNotNull(model);
            Preconditions.checkNotNull(meta);
            Preconditions.checkNotNull(container);

            this.meta = meta;
            data = Lists.newArrayList();
            export = RepeatableExport.newExport(model, data, true);
            entryFile = meta.newFile(container);
            if (log.isDebugEnabled()) {
                log.debug("Creates EntityExportContext::" + meta + " - " +
                          entryFile.getName());
            }
            writer = meta.newWriter(container);
            associationExportContexts = Lists.newArrayList();
        }

        public void addAssociationExportContext(AssociationMeta<T> meta,
                                                ExportModel<TopiaEntity> model,
                                                File container) {
            associationExportContexts.add(
                    new AssociationExportContext<T>(model,
                                                    meta,
                                                    container,
                                                    data)
            );
        }

        @Override
        public void close() throws IOException {

            try {
                if (export.isHeaderWritten()) {

                    if (log.isInfoEnabled()) {
                        log.info("Export table " + meta + " to " + entryFile);
                    }
                    writer.flush();
                } else {
                    // this file was not used, delete it
                    FileUtils.deleteQuietly(entryFile);
                }
            } finally {
                IOUtils.closeQuietly(writer);
                for (AssociationExportContext<T> c : associationExportContexts) {
                    c.close();
                }
            }
        }

        public void write(TopiaEntity data) throws Exception {
            this.data.add(data);
            try {
                export.write(writer);
            } finally {
                this.data.clear();
            }
        }

        public void writeAssociations(TopiaEntity data) throws Exception {
            this.data.add(data);
            try {
                for (AssociationExportContext<T> c : associationExportContexts) {
                    AssociationMeta<T> cMeta = c.meta;
                    boolean emptyChild = cMeta.isChildEmpty(data);
                    if (!emptyChild) {
                        c.write();
                    }
                }
            } finally {
                this.data.clear();
            }
        }
    }

    /**
     * To export associations as csv files.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.12
     */
    public static class AssociationExportContext<T extends TopiaEntityEnum> implements Closeable {

        /** association meta to export. */
        protected final AssociationMeta<T> meta;

        /** Exporter object. */
        protected final RepeatableExport export;

        /** Where to export datas. */
        protected final Writer writer;

        protected final File entryFile;

        protected AssociationExportContext(ExportModel<TopiaEntity> model,
                                           AssociationMeta<T> meta,
                                           File container,
                                           List<TopiaEntity> data) {
            Preconditions.checkNotNull(model);
            Preconditions.checkNotNull(meta);
            Preconditions.checkNotNull(container);
            Preconditions.checkNotNull(data);
            this.meta = meta;

            export = RepeatableExport.newExport(model, data, true);

            entryFile = meta.newFile(container);
            if (log.isDebugEnabled()) {
                log.debug("Creates AssociationExportContext::" + meta +
                          " - " + entryFile.getName());
            }
            writer = meta.newWriter(container);
        }

        @Override
        public void close() throws IOException {
            try {
                if (export.isHeaderWritten()) {

                    if (log.isInfoEnabled()) {
                        log.info("Export association " + meta + " to " + entryFile);
                    }
                    writer.flush();
                } else {

                    // this file was not used, delete it
                    FileUtils.deleteQuietly(entryFile);
                }
            } finally {
                IOUtils.closeQuietly(writer);
            }
        }

        public void write() throws Exception {
            export.write(writer);
        }
    }
}
