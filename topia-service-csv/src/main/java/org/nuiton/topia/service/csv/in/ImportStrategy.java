package org.nuiton.topia.service.csv.in;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportToMap;

import java.io.Reader;

/**
 * Strategy to import some stuff.
 *
 * Implements it and then you can use it with helper methods
 * {@link TopiaCsvImports#importTable(Reader, ImportStrategy, TableMeta, CsvImportResult)},
 * {@link TopiaCsvImports#importAssociation(Reader, ImportStrategy, AssociationMeta, CsvImportResult)}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public interface ImportStrategy<T extends TopiaEntityEnum> {

    /** @return the model factory (will be used to produce the model to import). */
    ImportModelFactory<T> getModelFactory();

    /**
     * Import a table given a {@code importer} with an optional csv result.
     *
     * @param meta      type of table to import
     * @param importer  the csv importer
     * @param csvResult optional object where to put csv import result
     * @throws TopiaException if any db problem
     */
    <E extends TopiaEntity> void importTable(TableMeta<T> meta, Import<E> importer,
                                             CsvImportResult<T> csvResult) throws TopiaException;

    /**
     * Import a table given a {@code importer} with an optional csv result,
     * and return them.
     *
     * @param meta      type of table to import
     * @param importer  the csv importer
     * @param csvResult optional object where to put csv import result
     * @return imported entities
     * @throws TopiaException if any db problem
     * @since 2.6.14
     */
    <E extends TopiaEntity> Iterable<E> importTableAndReturnThem(TableMeta<T> meta, Import<E> importer,
                                                                 CsvImportResult<T> csvResult) throws TopiaException;

    /**
     * Import a association given a {@code importer} with an optional csv result.
     *
     * @param meta      type of association to import
     * @param importer  the csv importer
     * @param csvResult optional object where to put csv import result
     * @throws TopiaException if any db problem
     */
    void importAssociation(AssociationMeta<T> meta,
                           ImportToMap importer,
                           CsvImportResult<T> csvResult) throws TopiaException;


}
