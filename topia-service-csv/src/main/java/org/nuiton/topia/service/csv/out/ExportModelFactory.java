package org.nuiton.topia.service.csv.out;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.csv.ExportModel;

/**
* To produce export model.
*
* @author Tony Chemit - chemit@codelutin.com
* @since 2.6.12
*/
public interface ExportModelFactory<T extends TopiaEntityEnum> {

    <E extends TopiaEntity> ExportModel<E> buildForExport(TableMeta<T> meta);

    <E extends TopiaEntity> ExportModel<E> buildForExport(AssociationMeta<T> associationMeta);
}
