package org.nuiton.topia.service.csv.in;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.service.csv.TopiaCsvCommons;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.csv.ImportModel;

import java.util.Map;

/**
 * A model to import associations of entities from csv files.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class EntityAssociationImportModel<T extends TopiaEntityEnum> extends AbstractImportModel<Map<String, Object>> {

    protected final AssociationMeta meta;

    public static <T extends TopiaEntityEnum> ImportModel<Map<String, Object>> newImportModel(char separator,
                                                                                              AssociationMeta<T> meta) {
        EntityAssociationImportModel<T> model = new EntityAssociationImportModel<T>(
                separator, meta);

        // topiaId <-> topiaId
        model.newMandatoryColumn(
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaCsvCommons.<Map<String, Object>, String>newMapProperty(TopiaEntity.PROPERTY_TOPIA_ID)
        );

        // add association -> target
        model.newMandatoryColumn(
                meta.getName(),
                TopiaCsvCommons.ASSOCIATION_VALUE_PARSER,
                TopiaCsvCommons.<Map<String, Object>, String[]>newMapProperty("target")
        );

        return model;
    }

    @Override
    public Map<String, Object> newEmptyInstance() {
        return null;
    }

    public EntityAssociationImportModel(char separator, AssociationMeta<T> meta) {
        super(separator);
        this.meta = meta;
    }

}
