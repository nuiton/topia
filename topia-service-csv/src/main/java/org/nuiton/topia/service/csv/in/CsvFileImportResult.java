package org.nuiton.topia.service.csv.in;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * To keep result of the import of a file.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class CsvFileImportResult<T extends TopiaEntityEnum> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Name of the csv file to import. */
    protected final String importFileName;

    /** type of entity to import csv datas. */
    protected final Set<T> entityTypes;

    /** Count of created entities. */
    protected final Map<T, Integer> numberCreated;

    /** Count of updated entities. */
    protected final Map<T, Integer> numberUpdated;

    public static <T extends TopiaEntityEnum> CsvFileImportResult<T> newResult(String importFileName, T... universe) {
        CsvFileImportResult<T> result = new CsvFileImportResult<T>(
                importFileName, universe
        );
        return result;
    }

    public CsvFileImportResult(String importFileName, T... universe) {
        this.importFileName = importFileName;
        entityTypes = Sets.newHashSet();
        numberCreated = Maps.newHashMap();
        numberUpdated = Maps.newHashMap();
        for (T t : universe) {
            numberCreated.put(t, 0);
            numberUpdated.put(t, 0);
        }
    }

    public Set<T> getEntityTypes() {
        return ImmutableSet.copyOf(entityTypes);
    }

    public int getNumberCreated(T entityType) {
        return getInteger(numberCreated, entityType);
    }

    public int getNumberUpdated(T entityType) {
        return getInteger(numberUpdated, entityType);
    }

    public String getImportFileName() {
        return importFileName;
    }

    public void incrementsNumberCreated(T entityType) {
        increments(numberCreated, entityType);
    }

    public void incrementsNumberUpdated(T entityType) {
        increments(numberUpdated, entityType);
    }

    protected int getInteger(Map<T, Integer> map, T entityType) {
        Integer result = map.get(entityType);
        return result;
    }

    protected void increments(Map<T, Integer> map, T entityType) {
        Integer result = map.get(entityType);
        if (result == 0) {
            entityTypes.add(entityType);
        }
        map.put(entityType, ++result);
    }
}
