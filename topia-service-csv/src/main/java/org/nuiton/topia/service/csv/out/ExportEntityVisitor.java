package org.nuiton.topia.service.csv.out;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntityEnumProvider;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.util.TimeLog;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Entity visitor to export data to csv files.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class ExportEntityVisitor<T extends TopiaEntityEnum> implements TopiaEntityVisitor, Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportEntityVisitor.class);

    public static final TimeLog TIME_LOG =
            new TimeLog(ExportEntityVisitor.class);

    /** Export for simple entity. */
    protected final Map<T, TopiaCsvExports.EntityExportContext<T>> entityExporters;

    protected final TopiaEntityEnumProvider<T> persistenceHelper;

    public static <T extends TopiaEntityEnum> ExportEntityVisitor<T> newVisitor(
            TopiaEntityEnumProvider<T> persistenceHelper,
            Map<T, TopiaCsvExports.EntityExportContext<T>> entityExporters) {
        return new ExportEntityVisitor<T>(
                persistenceHelper,
                entityExporters
        );
    }

    public ExportEntityVisitor(TopiaEntityEnumProvider<T> persistenceHelper,
                               Map<T, TopiaCsvExports.EntityExportContext<T>> entityExporters) {
        this.persistenceHelper = persistenceHelper;
        this.entityExporters = entityExporters;
    }

    public <E extends TopiaEntity> void export(Iterable<E> entities) {
        for (E entity : entities) {
            export(entity);
        }
    }

    public void export(TopiaEntity entity) {
        Preconditions.checkNotNull(entity);
        long s1 = TimeLog.getTime();
        try {
            entity.accept(this);
        } catch (TopiaException e) {
            throw new TopiaException(
                    "Could not export entity " + entity.getTopiaId(), e);
        } finally {
            TIME_LOG.log(s1, "export::" + entity.getTopiaId());
        }
    }

    @Override
    public void start(TopiaEntity entity) {
        String topiaId = entity.getTopiaId();
        try {
            if (log.isDebugEnabled()) {
                log.debug("Starts export of entity " + topiaId);
            }
            TopiaCsvExports.EntityExportContext entityExporter =
                    getEntityContext(entity.getClass());
            Preconditions.checkNotNull(entityExporter);
            entityExporter.write(entity);
        } catch (Exception e) {
            throw new TopiaException(
                    "Could not export entity " + entity, e);
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Ends export of entity " + topiaId);
            }
        }
    }

    @Override
    public void end(TopiaEntity entity) {
        try {
            if (log.isDebugEnabled()) {
                log.debug("Starts export of association of entity " +
                          entity.getTopiaId());
            }
            TopiaCsvExports.EntityExportContext entityExporter =
                    getEntityContext(entity.getClass());
            Preconditions.checkNotNull(entityExporter);
            entityExporter.writeAssociations(entity);
        } catch (Exception e) {
            throw new TopiaException(
                    "Could not export associations of entity " + entity, e);
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Ends export of association of entity " +
                          entity.getTopiaId());
            }
        }
    }


    @Override
    public void visit(TopiaEntity entity, String propertyName,
                      Class<?> type, Object value) {
    }

    @Override
    public void visit(TopiaEntity entity,
                      String propertyName,
                      Class<?> collectionType,
                      Class<?> type,
                      Object value) {

        if (TopiaEntity.class.isAssignableFrom(type) &&
            getEntityContext((Class<? extends TopiaEntity>) type) != null) {
            Collection<?> cValue = (Collection<?>) value;

            if (CollectionUtils.isNotEmpty(cValue)) {

                visitEntityCollection(entity,
                                      propertyName,
                                      collectionType,
                                      type,
                                      cValue);
            }
        }
    }

    protected void visitEntityCollection(TopiaEntity entity,
                                         String propertyName,
                                         Class<?> collectionType,
                                         Class<?> type,
                                         Collection<?> cValue) {
        for (Object currentValue : cValue) {
            try {
                ((TopiaEntity) currentValue).accept(this);
            } catch (TopiaException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not visit entity " + currentValue, e);
                }
            }
        }
    }

    @Override
    public void visit(TopiaEntity entity,
                      String propertyName,
                      Class<?> collectionType, Class<?> type,
                      int index,
                      Object value) {

        // nothing to do
    }

    @Override
    public void close() throws IOException {

        // use at the end of visit (or later)

        for (TopiaCsvExports.EntityExportContext<T> exportContext : entityExporters.values()) {
            exportContext.close();
        }
    }

    @Override
    public void clear() {
        // prefer use the close api
    }

    protected TopiaCsvExports.EntityExportContext getEntityContext(Class<? extends TopiaEntity> entityType) {
        T entityEnum = persistenceHelper.getEntityEnum(entityType);
        return entityEnum == null ? null : entityExporters.get(entityEnum);
    }
}
