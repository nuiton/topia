package org.nuiton.topia.service.csv.out;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ext.AbstractExportModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.service.csv.TopiaCsvCommons;

/**
 * A model to export associations of entities into csv files.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class EntityAssociationExportModel<T extends TopiaEntityEnum, E extends TopiaEntity> extends AbstractExportModel<E> {

    protected final AssociationMeta meta;

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> ExportModel<E> newExportModel(char separator,
                                                                                                   AssociationMeta<T> meta
    ) {
        EntityAssociationExportModel<T, E> model = new EntityAssociationExportModel<T, E>(
                separator,
                meta);

        // topiaId <-> topiaId
        model.newColumnForExport(TopiaEntity.PROPERTY_TOPIA_ID);

        model.newColumnForExport(
                meta.getName(),
                TopiaCsvCommons.newAssociationValueFormatter()
        );
        return model;
    }

    EntityAssociationExportModel(char separator, AssociationMeta<T> meta) {
        super(separator);
        this.meta = meta;
    }
}
