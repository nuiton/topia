package org.nuiton.topia.service.csv.in;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.service.csv.TopiaCsvCommons;

import java.util.List;
import java.util.Map;

/**
 * Abstract import model which add the useful methdo about importing foreign keys.
 *
 * @param <E> type of entity to import
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public abstract class AbstractImportModel<E> extends org.nuiton.csv.ext.AbstractImportModel<E> {

    public AbstractImportModel(char separator) {
        super(separator);
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
    }

    public <E extends TopiaEntity> void newForeignKeyColumn(String headerName, String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
        newMandatoryColumn(headerName, propertyName, TopiaCsvCommons.newForeignKeyValue(entityType, foreignKeyName, universe));
    }

    public <E extends TopiaEntity> void newForeignKeyColumn(String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
        newMandatoryColumn(propertyName, propertyName, TopiaCsvCommons.newForeignKeyValue(entityType, foreignKeyName, universe));
    }
}
