package org.nuiton.topia.service.csv.in;

/*
 * #%L
 * ToPIA :: Service Csv
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.csv.ImportToMap;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.MetaFilenameAware;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;
import org.nuiton.topia.service.csv.CsvProgressModel;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Helper for csv imports.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.12
 */
public class TopiaCsvImports {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TopiaCsvImports.class);

    protected static final String UPDATE_ASSOCIATION =
            "UPDATE %s SET %s = '%%s' WHERE topiaId ='%%s';";

    protected static final String INSERT_ASSOCIATION =
            "INSERT INTO %s (%s,%s) VALUES('%%s','%%s');";

    protected TopiaCsvImports() {
        // no instance of this helper
    }

    /**
     * Discover all files that can be imported (as a table or an association) from a zipfile.
     *
     * @param entryPrefix    prefix where to find files in the zip
     * @param possibleMetas  list of possible meta to be imported
     * @param zipFile        zip file where to seek for csv files to import
     * @param missingEntries to fill missing files
     * @param <T>            type of topia entity enum
     * @param <M>            type of data to import (table or association)
     * @return the map of found files indexed by his meta
     */
    public static <T extends TopiaEntityEnum, M extends MetaFilenameAware<T>> Map<M, ZipEntry> discoverEntries(
            String entryPrefix,
            Iterable<M> possibleMetas,
            ZipFile zipFile,
            List<String> missingEntries) {

        Map<M, ZipEntry> result = Maps.newLinkedHashMap();

        // check that all mandatories
        for (M entry : possibleMetas) {
            String filename = entry.getFilename();
            ZipEntry zipEntry = zipFile.getEntry(entryPrefix + filename);

            if (zipEntry == null) {
                missingEntries.add(filename);
            } else {
                result.put(entry, zipEntry);
            }
        }
        return result;
    }

    /**
     * Discover all files that can be imported (as a table or an association) from a directory.
     *
     * @param possibleMetas  list of possible meta to be imported
     * @param directory      directory where to seek for csv files to import
     * @param missingEntries to fill missing files
     * @param <T>            type of topia entity enum
     * @param <M>            type of data to import (table or association)
     * @return the map of found files indexed by his meta
     */
    public static <T extends TopiaEntityEnum, M extends MetaFilenameAware<T>> Map<M, File> discoverEntries(
            Iterable<M> possibleMetas,
            File directory,
            List<String> missingEntries) {

        Map<M, File> result = Maps.newLinkedHashMap();

        // check that all mandatories
        for (M entry : possibleMetas) {
            String filename = entry.getFilename();
            File zipEntry = new File(directory, filename);

            if (zipEntry.exists()) {
                result.put(entry, zipEntry);
            } else {
                missingEntries.add(filename);
            }
        }
        return result;
    }

    /**
     * To import a table (given by his {@code meta}) from a reader and a strategy.
     *
     * Result of import can be stored in an optional csv result.
     *
     * @param reader         where to read csv data
     * @param importStrategy import strategy used to store csv data
     * @param meta           meta of the data
     * @param csvResult      optional csv result
     * @param <T>            type of entity enum
     * @param <E>           type of data
     * @throws TopiaException if any db problem while storing datas
     * @throws IOException    if any pb while reading csv data
     * @see ImportStrategy#importTable(TableMeta, Import, CsvImportResult)
     */
    public static <T extends TopiaEntityEnum, E extends TopiaEntity> void importTable(Reader reader,
                                                                                      ImportStrategy<T> importStrategy,
                                                                                      TableMeta<T> meta,
                                                                                      CsvImportResult<T> csvResult) throws TopiaException, IOException {


        if (log.isInfoEnabled()) {
            log.info("Will import " + meta);
        }

        ImportModel<E> model = importStrategy.getModelFactory().buildForImport(meta);
        Import<E> importer = Import.newImport(model, reader);
        try {
            importStrategy.importTable(meta, importer, csvResult);
        } finally {
            importer.close();
        }
    }

    /**
     * To import a table (given by his {@code meta}) from a reader and a strategy.
     *
     * Result of import can be stored in an optional csv result.
     *
     * @param reader         where to read csv data
     * @param importStrategy import strategy used to store csv data
     * @param meta           meta of the data
     * @param csvResult      optional csv result
     * @param <T>            type of entity enum
     * @param <E>           type of data
     * @throws TopiaException if any db problem while storing datas
     * @throws IOException    if any pb while reading csv data
     * @see ImportStrategy#importTable(TableMeta, Import, CsvImportResult)
     */
    public static <T extends TopiaEntityEnum, E extends TopiaEntity> Iterable<E> importTableAndReturn(Reader reader,
                                                                                                      ImportStrategy<T> importStrategy,
                                                                                                      TableMeta<T> meta,
                                                                                                      CsvImportResult<T> csvResult) throws TopiaException, IOException {


        if (log.isInfoEnabled()) {
            log.info("Will import " + meta);
        }

        ImportModel<E> model = importStrategy.getModelFactory().buildForImport(meta);
        Import<E> importer = Import.newImport(model, reader);
        try {
            return importStrategy.importTableAndReturnThem(meta, importer, csvResult);
        } finally {
            importer.close();
        }
    }

    /**
     * To import a association (given by his {@code meta}) from a reader and a strategy.
     *
     * Result of import can be stored in an optional csv result.
     *
     * @param reader         where to read csv data
     * @param importStrategy import strategy used to store csv data
     * @param meta           meta of the data
     * @param csvResult      optional csv result
     * @param <T>            type of entity enum
     * @throws TopiaException if any db problem while storing datas
     * @throws IOException    if any pb while reading csv data
     * @see ImportStrategy#importAssociation(AssociationMeta, ImportToMap, CsvImportResult)
     */
    public static <T extends TopiaEntityEnum> void importAssociation(Reader reader,
                                                                     ImportStrategy<T> importStrategy,
                                                                     AssociationMeta<T> meta,
                                                                     CsvImportResult<T> csvResult) throws IOException, TopiaException {

        if (log.isInfoEnabled()) {
            log.info("Will import " + meta);
        }

        // load a association input
        ImportModelFactory<T> modelFactory = importStrategy.getModelFactory();
        ImportModel<Map<String, Object>> model =
                modelFactory.buildForImport(meta);
        ImportToMap importer = ImportToMap.newImportToMap(model, reader, false);

        try {
            importStrategy.importAssociation(meta, importer, csvResult);

        } finally {
            importer.close();
        }
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> void importAllEntities(TopiaDao<E> dao,
                                                                                            TableMeta<T> meta,
                                                                                            Import<E> importer,
                                                                                            CsvImportResult<T> csvResult) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();
        for (E entity : importer) {

            Map<String, Object> properties = meta.prepareCreate(
                    entity, entity.getTopiaId());
            E entityToSave = dao.create(properties);

            meta.copy(entity, entityToSave);

            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }
        }
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> void importAllEntities(TopiaHibernateSupport hibernateSupport,
                                                                                            TopiaDao<E> dao,
                                                                                            TableMeta<T> meta,
                                                                                            Import<E> importer,
                                                                                            CsvImportResult<T> csvResult,
                                                                                            int nbRowBuffer) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();

        int compt = 0;
        for (E entity : importer) {

            Map<String, Object> properties = meta.prepareCreate(
                    entity, entity.getTopiaId());
            E entityToSave = dao.create(properties);

            meta.copy(entity, entityToSave);

            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }
            compt++;
            if (compt % nbRowBuffer == 0) {
                // flush it
                hibernateSupport.getHibernateSession().flush();
            }
        }
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> Iterable<E> importAllEntitiesAndReturnThem(TopiaDao<E> dao,
                                                                                                                TableMeta<T> meta,
                                                                                                                Import<E> importer,
                                                                                                                CsvImportResult<T> csvResult) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();
        List<E> result = Lists.newArrayList();
        for (E entity : importer) {

            Map<String, Object> properties = meta.prepareCreate(
                    entity, entity.getTopiaId());
            E entityToSave = dao.create(properties);

            meta.copy(entity, entityToSave);

            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }

            result.add(entityToSave);
        }
        return result;
    }

    public static <T extends TopiaEntityEnum, E extends TopiaEntity> void importNotExistingEntities(TopiaDao<E> dao,
                                                                                                    TableMeta<T> meta,
                                                                                                    Map<String, TopiaEntity> universe,
                                                                                                    Import<E> importer,
                                                                                                    CsvImportResult<T> csvResult) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();
        for (E entity : importer) {

            String topiaId = entity.getTopiaId();

            Map<String, Object> properties = meta.prepareCreate(entity, null);
            E existingEntity = dao.forProperties(properties).findAnyOrNull();
            if (existingEntity == null) {

                // new entity to create
                E entityToSave = dao.create(properties);
                String newTopiaId = entityToSave.getTopiaId();
                Date topiaCreateDate = entityToSave.getTopiaCreateDate();
                meta.copy(entity, entityToSave);
                entityToSave.setTopiaId(newTopiaId);
                entityToSave.setTopiaCreateDate(topiaCreateDate);

                if (log.isInfoEnabled()) {
                    log.info(String.format("Create entity [%s becomes %s] with naturalId %s", topiaId, newTopiaId, properties));
                }
                universe.put(topiaId, entityToSave);

                if (csvResult != null) {
                    csvResult.incrementsNumberCreated();
                    if (progressModel != null) {
                        progressModel.incrementsProgress();
                    }
                }
            } else {
                // existing entity, nothing to create
                // just add a ref into universe to make translation possible by foreign keys

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Existing entity [%s] with naturalId %s, do not create anything", topiaId, properties));
                }

                universe.put(topiaId, existingEntity);
            }
        }
    }

    public static <T extends TopiaEntityEnum> void importAssociation(TopiaSqlSupport sqlSupport, AssociationMeta<T> meta,
                                                                     ImportToMap importer,
                                                                     CsvImportResult<T> csvResult,
                                                                     int nbRowBuffer) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();
        T source = meta.getSource();
        T target = meta.getTarget();

        StringBuilder builder = new StringBuilder();

        String targetTableName = target.getContract().getSimpleName();
        String sourceTableName = source.getContract().getSimpleName();
        String table = targetTableName;

        String updateString = String.format(UPDATE_ASSOCIATION, table, sourceTableName);

        if (log.isDebugEnabled()) {
            log.debug("Will apply " + updateString);
        }
        int compt = 0;
        for (Map<String, Object> row : importer) {
            String topiaId = (String) row.get(TopiaEntity.PROPERTY_TOPIA_ID);
            String[] associations = (String[]) row.get("target");
            for (String association : associations) {
                if (StringUtils.isNotEmpty(association)) {
                    builder.append(String.format(updateString, topiaId, association)).append('\n');
                    compt++;
                    if (compt % nbRowBuffer == 0) {
                        // flush it
                        sqlSupport.executeSql(builder.toString());
                        builder = new StringBuilder();
                    }
                }
            }
            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }
        }
        if (builder.length() > 0) {
            sqlSupport.executeSql(builder.toString());
        }
    }

    public static <T extends TopiaEntityEnum> void importNMAssociation(TopiaSqlSupport sqlSupport,
                                                                       AssociationMeta<T> meta,
                                                                       ImportToMap importer,
                                                                       CsvImportResult<T> csvResult,
                                                                       int nbRowBuffer) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();
        T source = meta.getSource();
        T target = meta.getTarget();

        StringBuilder builder = new StringBuilder();


        String targetTableName = target.getContract().getSimpleName();
        String sourceTableName = source.getContract().getSimpleName();

        // relation *-*
        String table = TopiaEntityHelper.getNormalizedAssociationTableName(
                sourceTableName, targetTableName);

        String updateString = String.format(INSERT_ASSOCIATION, table, sourceTableName, targetTableName);

        if (log.isDebugEnabled()) {
            log.debug("Will apply " + updateString);
        }
        int compt = 0;
        for (Map<String, Object> row : importer) {
            String topiaId = (String) row.get(TopiaEntity.PROPERTY_TOPIA_ID);
            String[] associations = (String[]) row.get("target");
            for (String association : associations) {
                if (StringUtils.isNotEmpty(association)) {
                    builder.append(String.format(updateString, topiaId, association)).append('\n');
                    compt++;
                    if (compt % nbRowBuffer == 0) {
                        // flush it
                        sqlSupport.executeSql(builder.toString());
                        builder = new StringBuilder();
                    }
                }
            }
            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }
        }
        if (builder.length() > 0) {
            sqlSupport.executeSql(builder.toString());
        }
    }

    public static <T extends TopiaEntityEnum> void importNMAssociation(TopiaSqlSupport sqlSupport,
                                                                       AssociationMeta<T> meta,
                                                                       Map<String, TopiaEntity> universe,
                                                                       ImportToMap importer,
                                                                       CsvImportResult<T> csvResult,
                                                                       int nbRowBuffer) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();
        T source = meta.getSource();
        T target = meta.getTarget();

        StringBuilder builder = new StringBuilder();


        String targetTableName = target.getContract().getSimpleName();
        String sourceTableName = source.getContract().getSimpleName();

        // relation *-*
        String table = TopiaEntityHelper.getNormalizedAssociationTableName(
                sourceTableName, targetTableName);

        String updateString = String.format(INSERT_ASSOCIATION, table, sourceTableName, targetTableName);

        if (log.isDebugEnabled()) {
            log.debug("Will apply " + updateString);
        }
        int compt = 0;
        for (Map<String, Object> row : importer) {
            String topiaId = (String) row.get(TopiaEntity.PROPERTY_TOPIA_ID);
            String[] associations = (String[]) row.get("target");
            for (String association : associations) {
                if (StringUtils.isNotEmpty(association)) {
                    TopiaEntity targetEntity = universe.get(association);
                    Preconditions.checkNotNull(targetEntity, "Could not find target entity from id: " + association);
                    builder.append(String.format(updateString, topiaId, targetEntity.getTopiaId())).append('\n');
                    compt++;
                    if (compt % nbRowBuffer == 0) {
                        // flush it
                        sqlSupport.executeSql(builder.toString());
                        builder = new StringBuilder();
                    }
                }
            }
            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }
        }
        if (builder.length() > 0) {
            sqlSupport.executeSql(builder.toString());
        }
    }

    public static <T extends TopiaEntityEnum> void importAssociation(TopiaSqlSupport sqlSupport,
                                                                     AssociationMeta<T> meta,
                                                                     Map<String, TopiaEntity> universe,
                                                                     ImportToMap importer,
                                                                     CsvImportResult<T> csvResult,
                                                                     int nbRowBuffer) throws TopiaException {

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();

        T source = meta.getSource();
        T target = meta.getTarget();

        StringBuilder builder = new StringBuilder();

        String targetTableName = target.getContract().getSimpleName();
        String sourceTableName = source.getContract().getSimpleName();
        String table = targetTableName;

        String updateString = String.format(UPDATE_ASSOCIATION, table, sourceTableName);

        if (log.isDebugEnabled()) {
            log.debug("Will apply " + updateString);
        }
        int compt = 0;
        for (Map<String, Object> row : importer) {
            String topiaId = (String) row.get(TopiaEntity.PROPERTY_TOPIA_ID);
            String[] associations = (String[]) row.get("target");
            for (String association : associations) {
                if (StringUtils.isNotEmpty(association)) {

                    TopiaEntity targetEntity = universe.get(association);
                    Preconditions.checkNotNull(targetEntity, "Could not find target entity from id: " + association);
                    builder.append(String.format(updateString, topiaId, targetEntity.getTopiaId())).append('\n');
                    compt++;
                    if (compt % nbRowBuffer == 0) {
                        // flush it
                        sqlSupport.executeSql(builder.toString());
                        builder = new StringBuilder();
                    }
                }
            }
            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }
        }
        if (builder.length() > 0) {
            sqlSupport.executeSql(builder.toString());
        }
    }
}
