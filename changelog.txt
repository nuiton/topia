
Since 2.2.2 changelog is no more maintained

see nuiton.org forge for changes : http://nuiton.org/projects/show/topia

--------------------------------------------------------------------------------

2.2.1 chemit 20090903

  * [FIX #38] manual migration service does not work on windows OS
  * [FEATURE #40] clean manual migration service
  * [FEATURE #42] add i18n in migration service

-- chemit -- Thu, 03 Sep 2009 22:50:14 +0200

2.2.0
  * migrate to nuiton

2.1.6 chemit 20090511
  * bump versions (lutinproject, lutinutil, lutinprocessor, maven-license-switcher-plugin)
  * use doxia-modules-jrst instead of maven-jrst-plugin
  * improve download section on site

2.1.5 chemit 20090420
 * use lutinproject 3.5.3, doxia-module-jrst 1.0.0, maven-license-switcher 0.7, xom 1.1
 * fix site (reports has disappeared)

2.1.3 chemit 20090220
 * 20090126 [chemit] - refactor poms (all dependencies in parent-pom dependencyManagment)
 * 20090129 [chemit] - use lutinproject 3.4

2.1.2 chemit 20090115
 * 20090114 [chemit] - using lutinproject 3.3

2.1.1 chemit 20081215
 * 20081215 [chemit] - new release for isis-fish :)

2.1.0 chemit 20081210
 * 20081205 [chemit] - improve poms, use lutinproject 3.2
                     - use JUnit4 for test, fix some tests (but not all).
                     - add some usefull method to build regex on topiaIds
 * 20081118 Switch to multi module project

ver-2-0-29 chatellier 20081117
 * 20081117 [chemit] improve EntityEnum
 * 20081117 [chemit] add method findAllWithOrder in DAO to make have sorted lists
 
ver-2-0-28 chatellier 20081114
 * 20081113 [chorlet] add support for lazy loading on attribute with 0..* multiplicity
 * 20081113 [chorlet] fix bug on bidirectional association by adding inverse attribute in the one side of two hibernate mapping files
 * 20081107 [chatellier] fix "result" named attribute generation in model
 * 20081107 [chatellier] fix ant:ant, org.apache.ant:ant conflict
 * 20081107 [chatellier] set lutingenerator provided scope
 * 20081101 [chemit] bump lutingenerator to 0.61
 * 20081101 [chemit] add a EntityEnum to have a generic way to access Entities. 
 * 20081101 [chemit] improve generators :
    - can exclude some generators on TopiaMetaGenerator
    - refactor TopiaMetaGenerator to use a simple List of Generator to launch
    - refactor Generators to have a default public constructor
     
 * 20081029 [chemit] fix infinitive recursion on method findByPrimaryKey and findByProperty in TopiaDAODeleagtor class
 * 20081026 [chemit] add dbName of table if explicit in javadoc
 * 20081024 [chemit] fix bug when no entity defined in model

ver-2-0-27 chemit 20081021
 * 20081021 [chemit] clean pom, use lutingenerator 0.60
 * 20080922 [thimel] Switched to lutinproject 3.0
 * 20080922 [thimel] Sources are correctly maven2 structuted (src/main/xxx)
 * 20080922 [thimel] Fixed maven-processor-plugin usage
 * 20080922 [thimel] License switched to LGPL

ver-2-0-26 thimel 20080922
 * 20080922 [thimel] Last version with lutinproject 2.2
 * 20082608 [chemit] permettre de recuperer uniquement une fenetre de resultat en hql (TopiaContext#find(String hql,int startIndex,int endIndex, Object ... args)
 * 20082907 [chemit] Suppression des dependances en dur sur les implantations d'entites
 * 20072012 [thimel] Support des index sur les attibuts
 * 20072012 [ruchaud] Récupération des classes persistées
 * 20072012 [ruchaud] Création d'un vetoable sur les finds
 
ver-2-0-25 poussin 20071214

 * 20071120 [chatellier] modify service init methods, return boolean to stay
   activated
 * 20071116 [chatellier] add support for topia context listeners
 * 20071114 [chatellier] add support for service without persitent classes
 * 20071109 [chatellier] add type="string" on topiaId in templates
    - update hibernate version to 3.2.5.ga (event patch)
    - jetty version 5.1.10 (better pom.xml)
 * 20071001 [chatellier] move tapestry version to 5.0.5
 * 20070528 [chatellier] add stateModel generation support
    - tapestry generation templates
    - tapestry 5.0.4 dependency

 * 20071106 [poussin] add removeContext on TopiaContextFactory and call it
   in TopiaContextImpl.closeContext().

 * 20071107 [thimel] add db schema support
 * 20071107 [thimel] several .hbm.xml refactoring
 * 20071108 [thimel] add DTO generator
 * 20071108 [thimel] add copyright support in generators
 * 20071108 [thimel] add serialVersionUID support in EntityAbstractGenerator

ver-2-0-24 poussin 20070425

 * 20070420 [chatellier] add services interfaces generation (stereotype=service)
 * 20070420 [chatellier] add topia services support
 * 20070330 [poussin] TopiaContextFactory return new TopiaContext if context is closed
 * 20070331 [poussin] change many string argument to class argument
 * 20070331 [poussin] add getService(Class) method on TopiaContext
 * 20070331 [poussin] remove helper in service that only return service instant
 * 20070331 [poussin] add Devel.rst documentation
 * 20070402 [poussin] replace init to preInit and postInit in TopiaService

ver-2-0-22 ruchaud 20061023

 * 20061023 [ruchaud][improve] manage events
 * 20061023 [ruchaud][bug fix] in generator of DAO Abstract on delete 

ver-2-0-21 poussin 20061019

 * add support for auto-import entities for hql
 * add getComposite and getAggregate on TopiaEntity
 * bug fix for association hibernate mapping wit have attributeh
   cascade="delete" for not navigable link to prevent constraint exception
 * bug fix in delete, remove link before do delete

ver-2-0-20 poussin 20061017

 * add support for TopiaService mechanisme

ver-2-0-16 poussin 20060907

 * Implantation des mÃ©thodes update et delete sur les TopiaUserImpl et TopiaEntityPermissionImpl
 * add backup/restore method on TopiaContext (backup only works for h2)
 * TopiaContext is set in Entity during preload
 * add updateSchema in TopiaContext interface
 * add methods update et delete on TopiaUserImpl and TopiaEntityPermissionImpl

ver-2-0-13 thimel 20060822

 * [Secu] ajout d'un champ notes sur les TopiaUser
 * [Secu] ajout d'un champ linkApplication sur les TopiaUser (permettant de faire le lien avec une application externe)
 * [Secu] ajout de commits et rollbacks sur les Manager
 * [Secu] ajout d'un setPassword (sans verification ancien mdp) sur les TopiaUser

ver-2-0-12 thimel 20060721

 * ajout des projections sur les DAO
 * positionnement unique des userManager et permissionManager au niveau du contexte racine
 * ajout de la possibilite de tester des permissions sur le permissionManager

ver-2-0-11 thimel 20060703

 * support de la fermeture d'un contexte
 * support de la recherche sur tous les attributs d'une entite / annotation sur les interfaces

ver-2-0-8 thimel 20060606

 * amelioration detection des classes abstraites
 * correction du polymorphisme avec les proxies d'Hibernate
 * Hibernate 3.2

ver-2-0-7 thimel 20060523

 * import/export XML "experimental"
 * one-to-one transformes en many-to-one + unique="true"
 * bug au niveau de la detection des parents abstract 

ver-2-0-6 thimel 20060504

 * support des annotations sur les attributs des XXXAbstract
 * re-correction du support des relations unidirectionnelles 1-n avec classes d'heritage du cote n
 * ajout de la validation pour les attributs sans inverses (pour les objectmodel faits a la main)

ver-2-0-5 thimel 20060427

 * isIndexed remplace isOrdered dans le mapping hibernate
 * add postCreate and postLoad on AbstractTopiaEntity
 * findContains sur les DAO (implante hors Hibernate pour l'instant)
 * support plus coherent des classes abstraites
 * projet compatible maven2
 * correction du support des relations unidirectionnelles 1-n avec classes d'heritage du cote n

ver-2-0-4 thimel 20060303

 * Correction du support des interfaces et classes abstraites (conjointement a LutinGenerator 0.30)
 * Ajout des exceptions sur le operations (conjointement a LutinGenerator 0.30)

ver-2-0-3 thimel 20060228

 * Correction du support des classes d'associations (mauvais mapping Hibernate)
 * Support de super classes pour les classes d'association (LutinGenerator 0.29)
 
