package org.nuiton.topia.it.legacy.topiatest.deletetest;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.Employe;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author desbois
 */
public class Contact2TopiaDao extends GeneratedContact2TopiaDao<Contact2> {

    public Set<Contact2> findAllByCompany(Company company) {
        Set<Contact2> contacts = new TreeSet<Contact2>();
        for (Employe e : company.getEmploye()) {
            contacts.addAll(e.getContacts());
        }
        return contacts;
    }
  
}
