package org.nuiton.topia.it.legacy.topiatest;

/*
 * #%L
 * ToPIA :: IT
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;

public class CompanyTopiaDao extends AbstractCompanyTopiaDao<Company> {

    public List<Company> findWithFetchDepartments(String... names) {
        InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep<Company> builder = newQueryBuilder();
        builder.addIn(Company.PROPERTY_NAME, Arrays.asList(names));
        builder.addFetch(Company.PROPERTY_DEPARTMENT);
        return builder.findAll();
    }

    public List<Company> findWithFetchHql(String ... names) {
        String hql = "SELECT DISTINCT c FROM " + Company.class.getName() + " as c " +
                "LEFT JOIN FETCH c." + Company.PROPERTY_DEPARTMENT  + " dept " +
                "WHERE c." + Company.PROPERTY_NAME + " IN ( :n ) ";
        return forHql(hql, "n", Arrays.asList(names)).findAll();
    }

} //CompanyTopiaDao
