package org.nuiton.topia.it.legacy;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;

import java.util.Map;
import java.util.Properties;

/**
 * Created on 12/19/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public abstract class AbstractTopiaItLegacyApplicationContext<E extends AbstractTopiaItLegacyPersistenceContext>
        extends AbstractTopiaApplicationContext<E> {

    protected AbstractTopiaItLegacyApplicationContext(Properties properties) {
        super(properties);
    }

    protected AbstractTopiaItLegacyApplicationContext(TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
    }

    protected AbstractTopiaItLegacyApplicationContext(Map<String, String> configuration) {
        super(configuration);
    }
}
