package org.nuiton.topia.it.legacy.topiatest.persistence;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Created: 11 mai 2010
 *
 * @author Florian Desbois- fdebois@codelutin.com
 */
public interface Entity1 extends TopiaEntity {

    String ATTR_1 = "attr1";

    String ATTR_2 = "attr2";

    String getAttr1();

    void setAttr1(String attr1);

    String getAttr2();

    void setAttr2(String attr2);

}
