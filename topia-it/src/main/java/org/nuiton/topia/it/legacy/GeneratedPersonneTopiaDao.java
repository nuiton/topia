package org.nuiton.topia.it.legacy;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.it.legacy.topiatest.Personne;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class GeneratedPersonneTopiaDao<E extends Personne> extends AbstractTopiaDao<E> implements PersonneDao {

    @Override
    public TopiaEntityEnum getTopiaEntityEnum() {
        return null;
    }

    @Override
    public <R extends TopiaEntity> List<R> findUsages(Class<R> type, E entity) throws TopiaException {
        return null;
    }

    @Override
    public Map<Class<? extends TopiaEntity>, List<? extends TopiaEntity>> findAllUsages(E entity) throws TopiaException {
        return null;
    }

    @Override
    public Class<E> getEntityClass() {
        return null;
    }

    @Override
    public List<TopiaEntity> getComposite(E entity) {
        return null;
    }

    @Override
    public List<TopiaEntity> getAggregate(E entity) {
        return null;
    }
}
