package org.nuiton.topia.it.mapping;

/*
 * #%L
 * ToPIA :: IT
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Properties;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;

public class TopiaItMappingTopiaApplicationContext extends AbstractTopiaItMappingTopiaApplicationContext {

    @Deprecated
    public TopiaItMappingTopiaApplicationContext(Properties properties) {
        super(properties);
    }

    @Deprecated
    public TopiaItMappingTopiaApplicationContext(Map<String, String> configuration) {
        super(configuration);
    }

    public TopiaItMappingTopiaApplicationContext(TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
    }

    @Override
    public void createSchema() {

        // Create DB schemas A and B for some of the entities
        JdbcHelper jdbcHelper = new JdbcHelper(configuration);
        jdbcHelper.createSchema("A");
        jdbcHelper.createSchema("B");

        super.createSchema();
    }
} //TopiaItMappingTopiaApplicationContext
