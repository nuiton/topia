package org.nuiton.topia.it.mapping.test16;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 6/30/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class FixAno3271Test {

    @Test
    public void testContracts() {

        // B16 extends A16
        Assert.assertTrue(A16.class.isAssignableFrom(B16.class));
        // C16 extends A16
        Assert.assertTrue(A16.class.isAssignableFrom(C16.class));
        // C16 extends B16
        Assert.assertTrue(B16.class.isAssignableFrom(C16.class));
    }
}
