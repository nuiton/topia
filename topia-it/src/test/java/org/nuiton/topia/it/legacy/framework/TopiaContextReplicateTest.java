package org.nuiton.topia.it.legacy.framework;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonTopiaDao;
import org.nuiton.topia.it.legacy.test.entities.Pet;
import org.nuiton.topia.it.legacy.test.entities.PetTopiaDao;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

/**
 * To test replication sugin TopiaContext.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.8
 */
public class TopiaContextReplicateTest {

    @Rule
    public final TopiaItLegacyDatabase dbSource =
            new TopiaItLegacyDatabase() {

                @Override
                protected TopiaItLegacyTopiaApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration) {
                    BeanTopiaConfiguration configuration =
                            new TopiaConfigurationBuilder().copyOf(topiaConfiguration);
                    configuration.setJdbcConnectionUrl(topiaConfiguration.getJdbcConnectionUrl() + "-source");
                    return super.createApplicationContext(configuration);
                }
            };

    @Rule
    public final TopiaItLegacyDatabase dbTarget =
            new TopiaItLegacyDatabase() {

                @Override
                protected TopiaItLegacyTopiaApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration) {
                    BeanTopiaConfiguration configuration =
                            new TopiaConfigurationBuilder().copyOf(topiaConfiguration);
                    configuration.setJdbcConnectionUrl(topiaConfiguration.getJdbcConnectionUrl() + "-target");
                    return super.createApplicationContext(configuration);
                }
            };

    @Test
    public void replicateEntity() throws Exception {
//
//        Properties configSource = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "replicateSource");
//
//        Properties configTarget = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "replicateTarget");
//


//        try {
//            contextSource = TopiaContextFactory.getContext(configSource);
//            contextTarget = TopiaContextFactory.getContext(configTarget);

        TopiaItLegacyTopiaPersistenceContext txSource;
        TopiaItLegacyTopiaPersistenceContext txTarget;
        PersonTopiaDao daoSource, daoTarget;
        PetTopiaDao petDAOSource, petDAOTarget;
        Person personSource, personTarget;
        Pet petSource, petTarget;

        txSource = dbSource.newPersistenceContext();
        daoSource = txSource.getPersonDao();
        petDAOSource = txSource.getPetDao();

        personSource = daoSource.create(Person.PROPERTY_FIRSTNAME, " firstName",
                                        Person.PROPERTY_NAME, " name"
        );

        petSource = petDAOSource.create(Pet.PROPERTY_NAME, "name",
                                        Pet.PROPERTY_TYPE, "type",
                                        Pet.PROPERTY_PERSON, personSource
        );

        personSource.addPet(petSource);

        txSource.commit();

        daoSource = txSource.getPersonDao();

        personSource = daoSource.forTopiaIdEquals(personSource.getTopiaId()).findUnique();
        Assert.assertNotNull(personSource);

        petSource = petDAOSource.forTopiaIdEquals(petSource.getTopiaId()).findUnique();
        Assert.assertNotNull(petSource);
        Assert.assertEquals(1, personSource.sizePet());
        Assert.assertEquals(petSource, personSource.getPet().iterator().next());

        txTarget = dbTarget.newPersistenceContext();

        txSource.replicateEntity(txTarget, petSource);
        txSource.replicateEntity(txTarget, personSource);

        txTarget.commit();

        daoTarget = txTarget.getPersonDao();
        petDAOTarget = txTarget.getPetDao();

        personTarget = daoTarget.forTopiaIdEquals(personSource.getTopiaId()).findUnique();
        Assert.assertNotNull(personTarget);
        Assert.assertEquals(personSource, personTarget);
        Assert.assertEquals(1, personTarget.sizePet());

        petTarget = petDAOTarget.forTopiaIdEquals(petSource.getTopiaId()).findUnique();
        Assert.assertNotNull(petTarget);
        Assert.assertEquals(petSource, petTarget);

        Assert.assertEquals(petTarget, personTarget.getPet().iterator().next());


//        } finally {
//            closeDb(contextSource);
//            closeDb(contextTarget);
//        }

    }
}
