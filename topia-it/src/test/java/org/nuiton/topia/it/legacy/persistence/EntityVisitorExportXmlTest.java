package org.nuiton.topia.it.legacy.persistence;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.topiatest.Address;
import org.nuiton.topia.it.legacy.topiatest.AddressTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.CompanyTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Department;
import org.nuiton.topia.it.legacy.topiatest.DepartmentTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Employe;
import org.nuiton.topia.it.legacy.topiatest.EmployeTopiaDao;
import org.nuiton.topia.persistence.DepthEntityVisitor;

/**
 * Test de visitor.
 *
 * @author chatellier
 */
public class EntityVisitorExportXmlTest {

    private static final Log log =
            LogFactory.getLog(EntityVisitorExportXmlTest.class);

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    /**
     * Prepare test.
     *
     * Add all tests commons data
     *
     * @throws TopiaException if could not create datas
     */
    @Before
    public void setUp() throws TopiaException {

        TopiaItLegacyTopiaPersistenceContext newContext = db.newPersistenceContext();
        try {
            // company
            CompanyTopiaDao companyDAO = newContext.getCompanyDao();
            Company clCompany = companyDAO.create(Company.PROPERTY_NAME, "CodeLutin");

            // employe
            EmployeTopiaDao employeDAO = newContext.getEmployeDao();
            Employe empl1 = employeDAO.create(Employe.PROPERTY_NAME, "boss", Employe.PROPERTY_SALARY, 30000);

            AddressTopiaDao adressDAO = newContext.getAddressDao();
            Address addr1 = adressDAO.create(Address.PROPERTY_CITY, "Nantes", Address.PROPERTY_ADRESS, "12 Avenue Jules Vernes");
            empl1.setAddress(addr1);

            Employe empl2 = employeDAO.create(Employe.PROPERTY_NAME, "boss2", Employe.PROPERTY_SALARY, 29000);
            Address addr2 = adressDAO.create(Address.PROPERTY_CITY, "Nantes", Address.PROPERTY_ADRESS, "12 Avenue Jules Vernes");
            empl2.setAddress(addr2);

            // departement
            DepartmentTopiaDao departmentDAO = newContext.getDepartmentDao();
            Department depComm = departmentDAO.create(Department.PROPERTY_NAME, "Commercial");
            depComm.setLeader(empl1);

            Department depDev = departmentDAO.create(Department.PROPERTY_NAME, "Dev");
            depDev.setLeader(empl2);
            clCompany.addDepartment(depComm);
            clCompany.addDepartment(depDev);

            newContext.commit();
        } finally {

            newContext.close();
        }
    }


    /**
     * Test l'export XML via un visiteur.
     *
     * Parcourt en profondeur.
     *
     * @throws TopiaException FIXME
     */
    @Test
    public void testExportXMLDepth() throws TopiaException {

        TopiaItLegacyTopiaPersistenceContext context = db.newPersistenceContext();

        CompanyTopiaDao companyDAO = context.getCompanyDao();
        Company clCompany = companyDAO.forNameEquals("CodeLutin").findAnyOrNull();

        TopiaEntityVisitor delegateVisitor = new ExportXMLVisitor();
        TopiaEntityVisitor visitor = new DepthEntityVisitor(delegateVisitor);
        clCompany.accept(visitor);

        if (log.isInfoEnabled()) {
            log.info("Export XML = \n" + delegateVisitor.toString());
        }
    }
}
