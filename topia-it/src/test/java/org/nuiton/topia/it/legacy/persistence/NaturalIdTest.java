package org.nuiton.topia.it.legacy.persistence;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.PropertyValueException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.topiatest.NaturalizedEntity;
import org.nuiton.topia.it.legacy.topiatest.NaturalizedEntityTopiaDao;
import org.nuiton.topia.persistence.TopiaException;

/**
 * NaturalIdTest
 *
 * Created: 18 févr. 2010
 *
 * @author fdesbois
 */
public class NaturalIdTest {

    private static final Log log = LogFactory.getLog(NaturalIdTest.class);

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    @Test
    public void testCreateSucessfull() {
        log.debug("Test naturalId : create succesfull");
        TopiaItLegacyTopiaPersistenceContext persistenceContext = db.newPersistenceContext();

        NaturalizedEntityTopiaDao dao =
                persistenceContext.getNaturalizedEntityDao();

        // No exception will be thrown with the two properties
        dao.createByNaturalId(5, "str");
        persistenceContext.commit();

        // No exception will only the need property
        dao.createByNotNull(3);
        persistenceContext.commit();

        // No exception will only the need property
        dao.create(NaturalizedEntity.PROPERTY_NATURAL_ID_NOT_NULL, 3);
        persistenceContext.commit();
    }

    @Test
    public void testCreateFailed() {
        log.debug("Test naturalId : create failed");
        TopiaItLegacyTopiaPersistenceContext persistenceContext = db.newPersistenceContext();

        NaturalizedEntityTopiaDao dao =
                persistenceContext.getNaturalizedEntityDao();

        // Exception will be throw
        try {
            dao.create();
            persistenceContext.commit();

            // Note : this is possible to create an empty entity if the type
            // is primitive like 'int' which have a default value of '0'
        } catch (TopiaException eee) {
            Assert.assertNotNull(eee.getCause());
            Assert.assertTrue(eee.getCause() instanceof PropertyValueException);
            Assert.assertEquals("naturalIdNotNull", ((PropertyValueException)eee.getCause()).getPropertyName());
        }
//        catch (PropertyValueException eee) {
//            Assert.assertEquals("naturalIdNotNull", eee.getPropertyName());
//        }
    }

    @Test
    public void testUpdateFailed() {
        log.debug("Test naturalId : update failed");

        TopiaItLegacyTopiaPersistenceContext persistenceContext = db.newPersistenceContext();

        NaturalizedEntityTopiaDao dao =
                persistenceContext.getNaturalizedEntityDao();

        NaturalizedEntity entity =
                dao.createByNaturalId(5, "str");
        persistenceContext.commit();

        // Exception will be throw : not allowed to modify a naturalId property
        try {
            entity.setNaturalIdNotNull(8);
            persistenceContext.commit();
        } catch (TopiaException eee) {
            Assert.assertEquals(HibernateException.class, eee.getCause().getClass());
        }
    }

    @Test
    public void testFindByNaturalId() {
        log.debug("Test naturalId : findByNaturalId");
        TopiaItLegacyTopiaPersistenceContext persistenceContext = db.newPersistenceContext();


        NaturalizedEntityTopiaDao dao =
                persistenceContext.getNaturalizedEntityDao();

        NaturalizedEntity entity =
                dao.createByNaturalId(5, "str");
        persistenceContext.commit();

        NaturalizedEntity result = dao.forNaturalId(5, "str").findUnique();

        Assert.assertEquals(entity, result);
    }

    @Test
    public void testExistNaturalId() {
        log.debug("Test naturalId : existNaturalId");
        TopiaItLegacyTopiaPersistenceContext persistenceContext = db.newPersistenceContext();


        NaturalizedEntityTopiaDao dao =
                persistenceContext.getNaturalizedEntityDao();

        dao.createByNaturalId(5, "str");
        persistenceContext.commit();

        boolean result = dao.forNaturalId(5, "str").exists();

        Assert.assertTrue(result);

        // not find with only one correct property
        result = dao.forNaturalId(8, "str").exists();

        Assert.assertFalse(result);
    }

    @Test
    public void testNaturalIdAreGeneralized() {

        // test that natural ids are generalized
        String[] generalizedNaturalizedNaturalIds = TopiaItLegacyEntityEnum.GeneralizedNaturalizedEntity.getNaturalIds();
        String[] naturalizedNaturalIds = TopiaItLegacyEntityEnum.NaturalizedEntity.getNaturalIds();
        Assert.assertArrayEquals(generalizedNaturalizedNaturalIds, naturalizedNaturalIds);
    }

    @Test
    public void testNotNullsAreGeneralized() {

        // test that not nulls are generalized
        String[] generalizedNaturalizedNotNulls = TopiaItLegacyEntityEnum.GeneralizedNaturalizedEntity.getNotNulls();
        String[] naturalizedNotNulls = TopiaItLegacyEntityEnum.NaturalizedEntity.getNotNulls();
        Assert.assertArrayEquals(generalizedNaturalizedNotNulls, naturalizedNotNulls);
    }
}
