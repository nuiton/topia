package org.nuiton.topia.it.legacy;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.topiatest.Address;
import org.nuiton.topia.it.legacy.topiatest.AddressTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Gender;
import org.nuiton.topia.it.legacy.topiatest.Personne;
import org.nuiton.topia.it.legacy.topiatest.PersonneTopiaDao;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Tests the TopiaContext#find|findAll|findUnique methods
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaJpaSupportTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    protected TopiaItLegacyTopiaPersistenceContext persistenceContext;

    protected TopiaJpaSupport jpaSupport;

    protected AddressTopiaDao addressDao;

    protected PersonneTopiaDao personneDao;

    protected Address address;

    @Before
    public void createCompanies() throws TopiaException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        persistenceContext = db.newPersistenceContext();

        Field field = FieldUtils.getField(AbstractTopiaPersistenceContext.class, "jpaSupport", true);
        jpaSupport = (TopiaJpaSupport) field.get(persistenceContext);

        personneDao = persistenceContext.getPersonneDao();
        addressDao = persistenceContext.getAddressDao();

        address = addressDao.create(
                Address.PROPERTY_ADRESS, "17 rue de la Pote Gellée, 44200 NANTES");

        personneDao.create(
                Personne.PROPERTY_NAME, "Arnaud",
                Personne.PROPERTY_GENDER, Gender.MALE);
        personneDao.create(
                Personne.PROPERTY_NAME, "Charlotte",
                Personne.PROPERTY_GENDER, Gender.FEMALE);
        personneDao.create(
                Personne.PROPERTY_NAME, "Hortense",
                Personne.PROPERTY_GENDER, Gender.FEMALE);
        persistenceContext.commit();
    }

    @Test
    public void testFindDAO() throws TopiaException {
        Assert.assertEquals(3, personneDao.count());

        Assert.assertEquals(2, personneDao.forGenderEquals(Gender.FEMALE).count());
        Assert.assertNotNull(personneDao.forGenderEquals(Gender.FEMALE).findAnyOrNull());
        Assert.assertNotNull(personneDao.forGenderEquals(Gender.MALE).findAnyOrNull());
        Assert.assertNull(personneDao.forGenderEquals(null).findAnyOrNull());

        Assert.assertEquals(0, personneDao.forNameEquals("nobody").count());
    }

    @Test
    public void testFindAll() throws TopiaException {
        Assert.assertEquals(3, personneDao.count());

        String query = "from " + Personne.class.getName() +
                       " where " + Personne.PROPERTY_GENDER + "=:g";

        Map<String, Object> args = Maps.newHashMap();
        args.put("g", Gender.FEMALE);
        List females = jpaSupport.findAll(query, args);
        Assert.assertEquals(2, females.size());

        args = Maps.newHashMap();
        args.put("g", Gender.MALE);
        List males = jpaSupport.findAll(query, args);
        Assert.assertEquals(1, males.size());

        args = Maps.newHashMap();
        List all = jpaSupport.findAll("from " + Personne.class.getName(), args);
        Assert.assertEquals(3, all.size());

        args = Maps.newHashMap();
        args.put("pax", "nobody");
        List none = jpaSupport.findAll("from " + Personne.class.getName() +
                                       " where " + Personne.PROPERTY_NAME + "=:pax", args);
        Assert.assertEquals(0, none.size());
    }

    @Test
    public void testStream() {
        Preconditions.checkState(personneDao.count() == 3);

        String query = "from " + Personne.class.getName() +
                       " where " + Personne.PROPERTY_GENDER + "=:g";
        try (Stream<Personne> stream = jpaSupport.stream(query, ImmutableMap.of("g", Gender.FEMALE))) {
            List females = stream.collect(Collectors.toList());
            Assert.assertEquals(2, females.size());
        }

        try (Stream<Personne> stream = jpaSupport.stream(query, ImmutableMap.of("g", Gender.MALE))) {
            List males = stream.collect(Collectors.toList());
            Assert.assertEquals(1, males.size());
        }

        try (Stream<Personne> stream = jpaSupport.stream("from " + Personne.class.getName(), ImmutableMap.of())) {
            List all = stream.collect(Collectors.toList());
            Assert.assertEquals(3, all.size());
        }

        try (Stream<Personne> stream = jpaSupport.stream("from " + Personne.class.getName() +
                " where " + Personne.PROPERTY_NAME + "=:pax", ImmutableMap.of("pax", "nobody"))) {
            List none = stream.collect(Collectors.toList());
            Assert.assertEquals(0, none.size());
        }
    }

    @Test
    public void testFind() throws TopiaException {
        Assert.assertEquals(3, personneDao.count());

        String query = "from " + Personne.class.getName() +
                       " where " + Personne.PROPERTY_GENDER + "=:g" +
                       " order by id ";


        Map<String, Object> args = Maps.newHashMap();
        args.put("g", Gender.FEMALE);
        List females = jpaSupport.find(query, 0, 100, args);
        Assert.assertEquals(2, females.size());

        females = jpaSupport.find(query, 0, 0, args);
        Assert.assertEquals(1, females.size());
        Personne charlotte = (Personne) females.get(0);

        females = jpaSupport.find(query, 1, 1, args);
        Assert.assertEquals(1, females.size());
        Personne hortense = (Personne) females.get(0);

        Assert.assertFalse(hortense.equals(charlotte));

        females = jpaSupport.find(query, 0, -1, args);
        Assert.assertEquals(2, females.size());
    }

    @Test
    public void testFindUnique() throws TopiaException {
        Assert.assertEquals(3, personneDao.count());

        String query = "from " + Personne.class.getName() +
                       " where " + Personne.PROPERTY_GENDER + "=:g";

        Map<String, Object> args = Maps.newHashMap();
        args.put("g", Gender.MALE);
        Object male = jpaSupport.findUnique(query, args);
        Assert.assertNotNull(male);

        args = Maps.newHashMap();
        args.put("pax", "nobody");
        Object none = jpaSupport.findUnique("from " + Personne.class.getName() +
                                            " where " + Personne.PROPERTY_NAME + "=:pax", args);
        Assert.assertNull(none);
    }

    @Test(expected = TopiaException.class)
    public void testFindUniqueOutOfBounds() throws TopiaException {
        Assert.assertEquals(3, personneDao.count());

        String query = "from " + Personne.class.getName() +
                       " where " + Personne.PROPERTY_GENDER + "=:g";

        Map<String, Object> args = Maps.newHashMap();
        args.put("g", Gender.FEMALE);
        Object female = jpaSupport.findUnique(query, args);
        Assert.assertNotNull(female);
    }

}
