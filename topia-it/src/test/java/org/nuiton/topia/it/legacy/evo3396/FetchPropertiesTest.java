package org.nuiton.topia.it.legacy.evo3396;

/*
 * #%L
 * ToPIA :: IT
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.LazyInitializationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.AbstractLegacyTest;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.CompanyTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Department;
import org.nuiton.topia.it.legacy.topiatest.DepartmentTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Employe;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * This test is about "addFetch" evolution http://nuiton.org/issues/3396
 * @author Arnaud Thimel (Code Lutin)
 */
public class FetchPropertiesTest extends AbstractLegacyTest {

    protected static final Function<Department, String> GET_DEPARTMENT_NAME = new Function<Department, String>() {
        @Override
        public String apply(Department input) {
            return input.getName();
        }
    };

    protected static final String COMPANY_CODE_LUTIN = "Code Lutin";
    protected static final String COMPANY_COGITEC = "Cogitec";

    protected static final Set<String> DEPARTMENT_NAMES = Sets.newHashSet("ABC", "DEF", "GHI");

    protected TopiaItLegacyTopiaPersistenceContext tx;

    protected CompanyTopiaDao companyDao;
    protected DepartmentTopiaDao departmentDao;

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        companyDao = getDao(tx, Company.class);
        departmentDao = getDao(tx, Department.class);

        Company company = companyDao.create(Company.PROPERTY_NAME, COMPANY_CODE_LUTIN);
        for (String departmentName : DEPARTMENT_NAMES) {
            departmentDao.create(Department.PROPERTY_NAME, departmentName);
        }
        companyDao.create(Company.PROPERTY_NAME, COMPANY_COGITEC);
        company.addAllDepartment(departmentDao.findAll());

        tx.commit();
        tx.getHibernateSupport().getHibernateSession().clear();
    }

    @Test(expected = LazyInitializationException.class)
    public void testForNameEquals() throws Exception {

        List<Company> companies = companyDao.forNameIn(Arrays.asList("Code Lutin", "Cogitec")).findAll();

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithFetchHql() throws Exception {

        List<Company> companies = companyDao.findWithFetchHql("Code Lutin", "Cogitec");

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartments() throws Exception {

        List<Company> companies = companyDao.findWithFetchDepartments("Code Lutin", "Cogitec");

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartmentsInline() throws Exception {

        List<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .findAll();

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartmentAndLeader() throws Exception {

        List<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT + "." + Department.PROPERTY_LEADER)
                .findAll();

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartmentAndLeaderAndAddress() throws Exception {

        List<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT + "." + Department.PROPERTY_LEADER + "." + Employe.PROPERTY_ADDRESS)
                .addFetch(Company.PROPERTY_EMPLOYE + "." + Employe.PROPERTY_ADDRESS)
                .findAll();

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartmentAndLeaderAndAddressAndTitle() throws Exception {

        List<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT + "." + Department.PROPERTY_LEADER + "." + Employe.PROPERTY_ADDRESS)
                .addFetch(Company.PROPERTY_DEPARTMENT + "." + Department.PROPERTY_LEADER + "." + Employe.PROPERTY_TITLE)
                .findAll();

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartmentAndLeaderAndTitle() throws Exception {

        List<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .addFetch(Company.PROPERTY_DEPARTMENT + "." + Department.PROPERTY_LEADER + "." + Employe.PROPERTY_TITLE)
                .findAll();

        Assert.assertEquals(2, companies.size());

        tx.close();

        for (Company company : companies) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }
    }

    @Test
    public void testFindWithDepartmentPage() throws Exception {

        PaginationResult<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .findPage(PaginationParameter.of(0, 1, Company.PROPERTY_NAME, false));

        Assert.assertEquals(2, companies.getCount());
        Assert.assertEquals(2, companies.getPageCount());
        Assert.assertEquals(1, companies.getElements().size());

        tx.close();

        for (Company company : companies.getElements()) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }

        tx = db.newPersistenceContext();
        companyDao = getDao(tx, Company.class);

        PaginationResult<Company> companies2 = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .findPage(companies.getNextPage());

        Assert.assertEquals(2, companies2.getCount());
        Assert.assertEquals(2, companies2.getPageCount());
        Assert.assertEquals(1, companies2.getElements().size());

        tx.close();

        for (Company company : companies2.getElements()) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }


    }

    @Test
    public void testFindWithDepartmentPageAndSortAsc() throws Exception {

        PaginationResult<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .findPage(PaginationParameter.of(0, 2, Company.PROPERTY_NAME, false));

        Assert.assertEquals(2, companies.getCount());
        Assert.assertEquals(1, companies.getPageCount());
        Assert.assertEquals(2, companies.getElements().size());

        Assert.assertEquals("Code Lutin", companies.getElements().iterator().next().getName());
        tx.close();

        for (Company company : companies.getElements()) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }


    }

    @Test
    public void testFindWithDepartmentPageAndSortDesc() throws Exception {

        PaginationResult<Company> companies = companyDao
                .forNameIn(Arrays.asList("Code Lutin", "Cogitec"))
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .findPage(PaginationParameter.of(0, 2, Company.PROPERTY_NAME, true));

        Assert.assertEquals(2, companies.getCount());
        Assert.assertEquals(1, companies.getPageCount());
        Assert.assertEquals(2, companies.getElements().size());

        Assert.assertEquals("Cogitec", companies.getElements().iterator().next().getName());
        tx.close();

        for (Company company : companies.getElements()) {
            if ("Code Lutin".equals(company.getName())) {
                Assert.assertEquals(3, company.getDepartment().size());
                Iterable<String> names = Iterables.transform(company.getDepartment(), GET_DEPARTMENT_NAME);
                Assert.assertTrue(Sets.difference(DEPARTMENT_NAMES, Sets.newHashSet(names)).isEmpty());
            } else {
                Assert.assertTrue(CollectionUtils.isEmpty(company.getDepartment()));
            }
        }


    }

    @Test
    public void testFindPageWithEmptyResult() throws Exception {

        PaginationResult<Company> companies = companyDao
                .forNameEquals("azetryu")
                .addFetch(Company.PROPERTY_DEPARTMENT)
                .setOrderByArguments(Company.PROPERTY_NAME)
                .findPage(PaginationParameter.of(0, 5));

        Assert.assertEquals(0, companies.getCount());
        Assert.assertEquals(0, companies.getPageCount());
        Assert.assertEquals(0, companies.getElements().size());

    }

}
