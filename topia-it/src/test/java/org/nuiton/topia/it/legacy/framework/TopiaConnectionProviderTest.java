package org.nuiton.topia.it.legacy.framework;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.hibernate.cfg.AvailableSettings;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Personne;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.TopiaConnectionProvider;

import java.io.File;
import java.util.Locale;
import java.util.Map;

/**
 * To test the {@link TopiaConnectionProvider} and make sure all connections
 * are done from here...
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.3
 */
public class TopiaConnectionProviderTest {

    public static final String TEST_URL = "hibernate.taiste.testURL";

    @Rule
    public final TopiaItLegacyDatabase db =
            new TopiaItLegacyDatabase() {
                @Override
                protected TopiaItLegacyTopiaApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration) {

                    BeanTopiaConfiguration configuration = new TopiaConfigurationBuilder().copyOf(topiaConfiguration);

                    Map<String, String> extraConf = configuration.getHibernateExtraConfiguration();

                    // Declare an alternative connection provider. This connection provider will use a different DB path
                    extraConf.put(AvailableSettings.CONNECTION_PROVIDER, TopiaConnectionProviderHardCoded.class.getName());

                    String tmpdir = System.getProperty("java.io.tmpdir");
                    File testDir = new File(tmpdir, String.valueOf(Math.random()));

                    // Real DB Path is where the DB will be created
                    File realFileDir = new File(testDir, "real");
                    String realDbPath = new File(realFileDir,"db").getAbsolutePath();
                    extraConf.put("hibernate.taiste.realDbPath", realDbPath);

                    // Fake DB Path is where the DB would have been created by the classic connection provider
                    File fakeFileDir = new File(testDir, "fake");
                    String fakeDbPath = new File(fakeFileDir, "db").getAbsolutePath();
                    extraConf.put("hibernate.taiste.fakeDbPath", fakeDbPath);

                    // The custom connection provider will use another JDBC URL than the expected one for Hibernate
                    extraConf.put(TEST_URL, "jdbc:h2:file:" + realDbPath);
                    // Let Hibernate believe that this is the good db path
                    configuration.setJdbcConnectionUrl("jdbc:h2:file:" + fakeDbPath);

                    return super.createApplicationContext(configuration);
                }
            };

    @Test
    public void testWithHardcoded() throws Exception {

        Map<String, String> hibernateExtraConfiguration = db.getApplicationContext().getConfiguration().getHibernateExtraConfiguration();
        String realDbPath = hibernateExtraConfiguration.get("hibernate.taiste.realDbPath");
        String fakeDbPath = hibernateExtraConfiguration.get("hibernate.taiste.fakeDbPath");

        Locale.setDefault(Locale.FRANCE);

        doStuffOnDb();

        // the db file must have been created (the one created by our hardcoded connection provider)
        Assert.assertTrue(new File(realDbPath).getParentFile().exists());

        // make sure the fake db path was never used (the one Hibernate may have created)
        Assert.assertFalse(new File(fakeDbPath).getParentFile().exists());
    }

    private void doStuffOnDb() throws TopiaException {
        TopiaItLegacyTopiaPersistenceContext transaction = db.newPersistenceContext();

        try {
            PersonTopiaDao dao = transaction.getPersonDao();

            Person personne = dao.create(Personne.PROPERTY_NAME, "Jack Bauer");
            transaction.commit();
            String idPersonne = personne.getTopiaId();
            Assert.assertNotNull(idPersonne);

            transaction.commit();
        } finally {
            transaction.close();
        }
    }
}
