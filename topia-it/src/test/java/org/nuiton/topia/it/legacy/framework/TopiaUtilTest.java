package org.nuiton.topia.it.legacy.framework;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.hibernate.boot.Metadata;
import org.hibernate.cfg.Configuration;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.test.entities.PersonImpl;
import org.nuiton.topia.it.legacy.topiatest.Personne;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.util.TopiaUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test pour les methodes de {@link TopiaUtil}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TopiaUtilTest {

    protected static final String PERSON_ID = "org.nuiton.topia.it.legacy.topiatest.Personne#1226701039001#0.6502325993664224";

    protected static final String PERSON_ID2 = "org.nuiton.topia.it.legacy.topiatest.Personne#1226701039001#0.6502325993664999";

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase() {

        @Override
        protected TopiaItLegacyTopiaApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration) {
            BeanTopiaConfiguration configuration = new TopiaConfigurationBuilder().copyOf(topiaConfiguration);
            configuration.setInitSchema(false);
            configuration.setValidateSchema(false);
            return super.createApplicationContext(configuration);
        }
    };

    @Test
    public void testGetTopiaIdPattern() throws Exception {
        String expected;
        String actual;

        expected = "org\\.nuiton\\.topia\\.it\\.legacy\\.topiatest\\.Personne#(?:\\d+?)#(?:\\d+)\\.(?:\\d+)";
        actual = TopiaUtil.getTopiaIdPattern(Personne.class);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTopiaPattern() throws Exception {
        String expected;
        Pattern pattern;

        expected = "(\\d+)-(org\\.nuiton\\.topia\\.it\\.legacy\\.topiatest\\.Personne#(?:\\d+?)#(?:\\d+)\\.(?:\\d+))-(org\\.nuiton\\.topia\\.it\\.legacy\\.topiatest\\.Personne#(?:\\d+?)#(?:\\d+)\\.(?:\\d+))(.*)";
        pattern = TopiaUtil.getTopiaPattern("(\\d+)-%1$s-%1$s(.*)", Personne.class);
        assertEquals(expected, pattern.toString());

        String expression = 123 + "-" + PERSON_ID + "-" + PERSON_ID2 + "-afterall";

        Matcher matcher = pattern.matcher(expression);

        assertTrue(matcher.matches());

        assertTrue(matcher.matches());
        assertEquals(4, matcher.groupCount());
        assertEquals("123", matcher.group(1));
        assertEquals(PERSON_ID, matcher.group(2));
        assertEquals(PERSON_ID2, matcher.group(3));
        assertEquals("-afterall", matcher.group(4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsSchemaExistFailed() throws Exception {
        Configuration hibernateCfg = db.getApplicationContext().getHibernateProvider().getHibernateConfiguration();
        Metadata metaData = db.getApplicationContext().getHibernateProvider().getMetaData();
        TopiaUtil.isSchemaExist(hibernateCfg,metaData,  "fake");
    }

    @Test
    public void testIsSchemaExist() throws Exception {

        String personClassName = PersonImpl.class.getName();
        Configuration hibernateCfg = db.getApplicationContext().getHibernateProvider().getHibernateConfiguration();
        Metadata metaData = db.getApplicationContext().getHibernateProvider().getMetaData();

        // First, test before DB is created, the table should not exist
        assertFalse(TopiaUtil.isSchemaExist(hibernateCfg,metaData, personClassName));

        // Create schema
        db.getApplicationContext().createSchema();

        // Now table should exist
        assertTrue(TopiaUtil.isSchemaExist(hibernateCfg, metaData, personClassName));
    }
}
