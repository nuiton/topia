
package org.nuiton.topia.it.mapping.test6;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;

/**
 * To test a simple OneToMany indexed relation (usage of JPA order-colum)
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class SimpleOneToManyWithRoleNamedIndexedTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A61TopiaDao aDAO;

    protected B61TopiaDao bDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A61.class);
        bDAO = getDao(tx, B61.class);
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();
        long nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(0, nbB);

        A61 a = aDAO.create();
        B61 b = bDAO.create();
        B61 b2 = bDAO.create();
        B61 b3 = bDAO.create();

        a.addRoleB61(b);
        a.addRoleB61(b2);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(1, nbA);
        Assert.assertEquals(3, nbB);

        A61 aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);
        Assert.assertEquals(2, aBis.sizeRoleB61());
        List<B61> bs = aBis.getRoleB61();
        Assert.assertEquals(b, bs.get(0));
        Assert.assertEquals(b2, bs.get(1));

        // change order
        List<B61> bsBis;

        bsBis = Lists.newArrayList();
        bsBis.add(b2);
        bsBis.add(b);
        a.setRoleB61(bsBis);
        tx.commit();

        aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);
        Assert.assertEquals(2, aBis.sizeRoleB61());
        bs = aBis.getRoleB61();
        Assert.assertEquals(b2, bs.get(0));
        Assert.assertEquals(b, bs.get(1));

        // rechange order

        bsBis = Lists.newArrayList();
        bsBis.add(b3);
        bsBis.add(b2);
        bsBis.add(b);
        a.setRoleB61(bsBis);
        tx.commit();

        aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);
        Assert.assertEquals(3, aBis.sizeRoleB61());
        bs = aBis.getRoleB61();
        Assert.assertEquals(b3, bs.get(0));
        Assert.assertEquals(b2, bs.get(1));
        Assert.assertEquals(b, bs.get(2));
    }

}