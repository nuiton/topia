package org.nuiton.topia.it.legacy.topiatest.deletetest;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.topiatest.Gender;
import org.nuiton.topia.it.legacy.topiatest.Personne;
import org.nuiton.topia.it.legacy.topiatest.PersonneTopiaDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Deleting tests with DAO and Entities generated with ToPIA (diagram
 * delete-test in topia-it-legacy.zargo). Different case of deleting, with inheritance
 * or NMultiplicity relationship between two entities, or both. Initiate from an
 * issue with DAOAbstractGenerator delete method generation. Tests with H2
 * Database. Configuration in src/test/resources/TopiaContextImpl.properties
 */
public class DeleteEntityTest {

    private static final Log log = LogFactory.getLog(DeleteEntityTest.class);

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    /**
     * Test for deleting entities with inheritance. Delete from the DAO linked
     * with the subclass entity and from the DAO linked with the superclass
     * entity. In the test model, the two entities have NMultiplicity
     * relationship without association class entity.
     *
     * @throws TopiaException if any exception while manipulating db
     */
    @Test
    public void testDeleteEntityWithInheritance() throws TopiaException {
        log.debug("START TEST : testDeleteEntityWithInheritance");

        TopiaItLegacyTopiaPersistenceContext transaction = db.newPersistenceContext();

        log.debug("DAO : PersonneDAO");
        PersonneTopiaDao dao = transaction.getPersonneDao();

        log.debug("CREATE PERSONNE : Bob Marley");
        Personne personne = dao.create(Personne.PROPERTY_NAME, "Bob Marley");
        transaction.commit();
        String idPersonne = personne.getTopiaId();
        assertNotNull(idPersonne);
        log.debug("ENTITY PERSONNE SAVED !");

        log.debug("DELETE PERSONNE");
        dao.delete(personne);
        transaction.commit();
        Personne res = dao.forTopiaIdEquals(idPersonne).findAnyOrNull();
        assertNull(res);
        log.debug("ENTITY PERSONNE DELETED !");

        log.debug("CREATE PERSONNE : Ziggy Marley");
        Personne personne2 = dao.create(Personne.PROPERTY_NAME, "Ziggy Marley");
        transaction.commit();
        String idPersonne2 = personne2.getTopiaId();
        assertNotNull(idPersonne2);
        log.debug("ENTITY PERSONNE SAVED !");

        log.debug("DAO parent (abstract) : PartyDAO");
        Party2TopiaDao dao2 = transaction.getParty2Dao();

        log.debug("DELETE PERSONNE with PartyDAO");
        dao2.delete(personne2);
        transaction.commit();
        Party2 res2 = dao2.forTopiaIdEquals(idPersonne2).findAnyOrNull();
        assertNull(res2);
        log.debug("ENTITY PERSONNE DELETED !");


    }

    /**
     * Test for deleting entities with NMultiplicity relation without
     * association class entity. Test DAO generation for deleting references
     * between two entities with NMultiplicity relation. In the test model, the
     * two entities have both inheritance.
     *
     * @throws TopiaException if any exception while manipulating db
     */
    @Test
    public void testDeleteEntityWithManyToManyRelation() throws TopiaException {
        log.debug("START TEST : testDeleteEntityWithManyToManyRelation");

        TopiaItLegacyTopiaPersistenceContext transaction = db.newPersistenceContext();

        PersonneTopiaDao dao = transaction.getPersonneDao();

        log.debug("CREATE PERSONNE : Bob Marley");
        Personne personne = dao.create(Personne.PROPERTY_NAME, "Bob Marley");
        transaction.commit();
        String idPersonne = personne.getTopiaId();
        assertNotNull(idPersonne);
        log.debug("ENTITY PERSONNE SAVED !");

        Contact2TopiaDao contactDAO = transaction.getContact2Dao();

        log.debug("CREATE CONTACT : jaja@codelutin.com");
        Contact2 contact = contactDAO.create(Contact2.PROPERTY_CONTACT_VALUE, "jaja@codelutin.com");
        transaction.commit();
        String idContact = contact.getTopiaId();
        assertNotNull(idContact);
        log.debug("ENTITY CONTACT SAVED !");

        log.debug("ADD CONTACT TO PERSONNE");
        personne.addContacts(contact);
        transaction.commit();
        assertEquals(1, personne.getContacts().size());
        log.debug("CONTACT ADDED !");

        log.debug("DELETE PERSONNE");
        dao.delete(personne);
        transaction.commit();
        Personne res = dao.forTopiaIdEquals(idPersonne).findAnyOrNull();
        assertNull(res);
        log.debug("ENTITY PERSONNE DELETED !");

        assertEquals(0, contact.getParty2().size());

        log.debug("DELETE CONTACT");
        contactDAO.delete(contact);
        transaction.commit();
        Contact2 res2 = contactDAO.forTopiaIdEquals(idContact).findAnyOrNull();
        assertNull(res2);
        log.debug("ENTITY PERSONNE DELETED !");

    }

    /**
     * Test than deleting entities will modify isPersisted() result
     */
    @Test
    public void testIsPersisted() {
        log.debug("START TEST : testIsPersisted");

        TopiaItLegacyTopiaPersistenceContext transaction = db.newPersistenceContext();

        PersonneTopiaDao dao = transaction.getPersonneDao();

        Personne person = dao.newInstance();
        Assert.assertNull(person.getTopiaId());

        person.setName("Arno");
        person.setGender(Gender.MALE);
        Assert.assertFalse(person.isPersisted());

        Personne person2 = dao.create(person);
        Assert.assertTrue(person.isPersisted());
        Assert.assertTrue(person2.isPersisted());

        dao.delete(person2);
        Assert.assertFalse(person.isPersisted());
        Assert.assertFalse(person2.isPersisted());
    }

}
