package org.nuiton.topia.it.legacy.framework;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.engine.jdbc.connections.internal.ConnectionProviderInitiator;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.UnknownUnwrapTypeException;
import org.hibernate.service.spi.Configurable;
import org.hibernate.service.spi.Stoppable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Customized connection provider. It will not use the JDBC URL provided by Hibernate, but the one from
 * {@link TopiaConnectionProviderTest#TEST_URL}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.3
 */
public class TopiaConnectionProviderHardCoded implements ConnectionProvider, Configurable, Stoppable {

    private static final long serialVersionUID = 7911628440635459964L;

    private String url;

    private Properties connectionProps;

    private Integer isolation;

    /**
     * Our pool of connections which are not closed and availables.
     */
    protected final List<Connection> pool;

    private int poolSize;

    private boolean stopped;

    private int checkedOut = 0;

    private boolean autocommit;

    /**
     * Logger.
     */
    private static final Log log =
            LogFactory.getLog(TopiaConnectionProviderHardCoded.class);

    public TopiaConnectionProviderHardCoded() {
        pool = new ArrayList<Connection>();
    }

    @Override
    public void configure(Map configurationValues) throws HibernateException {

        poolSize = ConfigurationHelper.getInt(AvailableSettings.POOL_SIZE, configurationValues, 20); //default pool size 20
        if (log.isDebugEnabled()) {
            log.debug("Connection pool size: " + poolSize);
        }

        autocommit = ConfigurationHelper.getBoolean(AvailableSettings.AUTOCOMMIT, configurationValues);
        if (log.isDebugEnabled()) {
            log.debug("autocommit mode: " + autocommit);
        }

        isolation = ConfigurationHelper.getInteger(AvailableSettings.ISOLATION, configurationValues);
        if (isolation != null) {
            if (log.isDebugEnabled()) {
                log.debug("JDBC isolation level: " + isolation);
            }
        }

        String driverClass = ConfigurationHelper.getString(AvailableSettings.JAKARTA_JDBC_DRIVER, configurationValues);
        if (driverClass == null) {

            if (log.isWarnEnabled()) {
                log.warn("no JDBC Driver class was specified by property " +
                        AvailableSettings.JAKARTA_JDBC_DRIVER);
            }
        } else {
            try {
                // trying via forName() first to be as close to DriverManager's semantics
                Class.forName(driverClass);
            } catch (ClassNotFoundException cnfe) {
                try {
                    ReflectHelper.classForName(driverClass);
                } catch (ClassNotFoundException e) {
                    String msg = "JDBC Driver class not found: " + driverClass;
                    log.error(msg, e);
                    throw new HibernateException(msg, e);
                }
            }
        }

        // use a dummy directory to make sure only the connection provider knows
        // the real directory where db is and then make sure hibernate always
        // use the connection provider...
        String directory =
                (String) configurationValues.get(TopiaConnectionProviderTest.TEST_URL);

        url = directory;
//        url = props.getProperty(Environment.URL);

//        if (url == null) {
//            String msg = "JDBC URL was not specified by property " + Environment.URL;
//            log.error(msg);
//            throw new HibernateException(msg);
//        }

        connectionProps = ConnectionProviderInitiator.getConnectionProperties(configurationValues);

        if (log.isDebugEnabled()) {
            log.debug("using driver: " + driverClass + " at URL: " + url);
        }

        // if debug level is enabled, then log the password, otherwise mask it
        if (log.isTraceEnabled()) {
            log.debug("connection properties: " + connectionProps);
        } else if (log.isDebugEnabled()) {
            log.debug("connection properties: " +
                    ConfigurationHelper.maskOut(connectionProps, "password"));
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (log.isTraceEnabled()) {
            log.trace("total checked-out connections: " + checkedOut);
        }

        synchronized (pool) {
            if (!pool.isEmpty()) {
                int last = pool.size() - 1;
                if (log.isTraceEnabled()) {
                    log.trace("using pooled JDBC connection, pool size: " + last);
                }
                checkedOut++;
                Connection pooled = pool.remove(last);
                if (isolation != null) {
                    pooled.setTransactionIsolation(isolation.intValue());
                }
                if (pooled.getAutoCommit() != autocommit) {
                    pooled.setAutoCommit(autocommit);
                }
                return pooled;
            }
        }

        log.debug("opening new JDBC connection");
        Connection conn = DriverManager.getConnection(url, connectionProps);
        if (isolation != null) conn.setTransactionIsolation(isolation);
        if (conn.getAutoCommit() != autocommit) conn.setAutoCommit(autocommit);

        if (log.isDebugEnabled()) {
            log.debug("created connection to: " + url + ", Isolation Level: " + conn.getTransactionIsolation());
        }
//		if ( log.isTraceEnabled() )
        checkedOut++;

        return conn;
    }

    @Override
    public void closeConnection(Connection conn) throws SQLException {
//        if ( log.isDebugEnabled() )
        checkedOut--;

        synchronized (pool) {
            int currentSize = pool.size();
            if (currentSize < poolSize) {
                if (log.isTraceEnabled()) {
                    log.trace("returning connection to pool, pool size: " + (currentSize + 1));
                }
                pool.add(conn);
                return;
            }
        }

        log.debug("closing JDBC connection");

        conn.close();
    }

    @Override
    protected void finalize() throws Throwable {
        if (!stopped) {
            stop();
        }
        super.finalize();
    }

    @Override
    public void stop() {

        if (log.isDebugEnabled()) {
            log.debug("cleaning up connection pool: " + url);
        }

        for (Connection connection : pool) {
            try {
                connection.close();
            } catch (SQLException sqle) {
                if (log.isWarnEnabled()) {
                    log.warn("problem closing pooled connection", sqle);
                }
            }
        }
        pool.clear();
        stopped = true;
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    @Override
    public boolean isUnwrappableAs(Class unwrapType) {
        return ConnectionProvider.class.equals(unwrapType) ||
                getClass().isAssignableFrom(unwrapType);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public <T> T unwrap(Class<T> unwrapType) {
        if (ConnectionProvider.class.equals(unwrapType) ||
                getClass().isAssignableFrom(unwrapType)) {
            return (T) this;
        } else {
            throw new UnknownUnwrapTypeException(unwrapType);
        }
    }
}
