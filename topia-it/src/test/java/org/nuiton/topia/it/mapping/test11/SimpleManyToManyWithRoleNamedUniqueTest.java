
package org.nuiton.topia.it.mapping.test11;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.Iterator;

/**
 * Tests the tag-value {@code unique} on a {@code ManyToMany} relation with a
 * renamed role.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class SimpleManyToManyWithRoleNamedUniqueTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A11DTopiaDao aDAO;

    protected B11DTopiaDao bDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A11D.class);
        bDAO = getDao(tx, B11D.class);
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();
        long nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(0, nbB);

        A11D a = aDAO.create();
        B11D b = bDAO.create();

        a.addRoleB113(b);
        a.addRoleB113(b);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(1, nbA);
        Assert.assertEquals(1, nbB);

        A11D aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);
        Assert.assertEquals(1, aBis.sizeRoleB113());
        Collection<B11D> bs = aBis.getRoleB113();
        Iterator<B11D> iterator = bs.iterator();
        Assert.assertEquals(b, iterator.next());

    }
}