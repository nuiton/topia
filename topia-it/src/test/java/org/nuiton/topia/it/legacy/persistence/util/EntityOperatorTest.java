package org.nuiton.topia.it.legacy.persistence.util;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.CompanyImpl;
import org.nuiton.topia.it.legacy.topiatest.Department;
import org.nuiton.topia.it.legacy.topiatest.DepartmentImpl;
import org.nuiton.topia.it.legacy.topiatest.Employe;
import org.nuiton.topia.it.legacy.topiatest.EmployeImpl;
import org.nuiton.topia.it.legacy.topiatest.Personne;
import org.nuiton.topia.it.legacy.topiatest.PersonneImpl;
import org.nuiton.topia.persistence.util.EntityOperator;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/** @author Tony Chemit - chemit@codelutin.com */
public class EntityOperatorTest {

    public EntityOperatorTest() {
    }

    static EntityOperator<Company> operationC;

    static EntityOperator<Employe> operationE;

    static EntityOperator<Personne> operationP;

    static EntityOperator<Department> operationD;

    Company c;

    Department d;

    Employe e;

    Personne p;

    @BeforeClass
    public static void setUpClass() throws Exception {
        operationC = TopiaItLegacyEntityEnum.getOperator(Company.class);
        operationE = TopiaItLegacyEntityEnum.getOperator(Employe.class);
        operationP = TopiaItLegacyEntityEnum.getOperator(Personne.class);
        operationD = TopiaItLegacyEntityEnum.getOperator(Department.class);
    }

    @AfterClass
    public static void tearDownClass() throws Throwable {
//        operationC.finalize();
//        operationE.finalize();
//        operationP.finalize();
//        operationD.finalize();
    }

    @Before
    public void setUp() {
        c = new CompanyImpl();
        d = new DepartmentImpl();
        e = new EmployeImpl();
        p = new PersonneImpl();
    }

    @After
    public void tearDown() {
        c = null;
        d = null;
        e = null;
        p = null;
    }

    /** Test of newOperator method, of class EntityOperator. */
    @Test
    public void testGet() {

        Object actual;
        String name;

        e.setName(name = "name");
        actual = operationE.get(Employe.PROPERTY_NAME, e);
        assertEquals(name, actual);

        actual = operationP.get(Employe.PROPERTY_NAME, e);
        assertEquals(name, actual);

        actual = operationD.get(Department.PROPERTY_COMPANY, d);
        assertNull(actual);

        d.setCompany(c);
        actual = operationD.get(Department.PROPERTY_COMPANY, d);
        assertNotNull(actual);
        assertEquals(c, actual);

        actual = operationC.get(Company.PROPERTY_DEPARTMENT, c);
        assertNull(actual);

        c.addDepartment(d);
        actual = operationC.get(Company.PROPERTY_DEPARTMENT, c);
        assertNotNull(actual);
        assertFalse(((Collection<?>) actual).isEmpty());
    }

    /** Test of set method, of class EntityOperator. */
    @Test
    public void testSet() {

        String name;

        operationE.set(Employe.PROPERTY_NAME, e, name = "name");
        assertEquals(name, e.getName());

        operationP.set(Employe.PROPERTY_NAME, e, name = "name2");
        assertEquals(name, e.getName());
    }

    /** Test of getChild method, of class EntityOperator. */
    @Test
    public void testGetChild() {
        Object actual;
        String topiaId;

        topiaId = "0";

        actual = operationC.get(Company.PROPERTY_DEPARTMENT, c);
        assertNull(actual);

        actual = operationC.getChild(Company.PROPERTY_DEPARTMENT, c, topiaId);
        assertNull(actual);

        c.addDepartment(d);

        actual = operationC.get(Company.PROPERTY_DEPARTMENT, c);
        assertNotNull(actual);
        assertFalse(((Collection<?>) actual).isEmpty());

        actual = operationC.getChild(Company.PROPERTY_DEPARTMENT, c, topiaId);
        assertNull(actual);

        d.setTopiaId(topiaId);
        actual = operationC.getChild(Company.PROPERTY_DEPARTMENT, c, topiaId);
        assertNotNull(actual);
        assertEquals(d, actual);

    }

    /** Test of addChild method, of class EntityOperator. */
    @Test
    public void testAddChild() {

        assertTrue(c.isDepartmentEmpty());

        operationC.addChild(Company.PROPERTY_DEPARTMENT, c, d);
        assertFalse(c.isDepartmentEmpty());
        assertEquals(d, c.getDepartment().iterator().next());
    }

    /** Test of isChildEmpty method, of class EntityOperator. */
    @Test
    public void testIsChildEmpty() {

        assertTrue(c.isDepartmentEmpty());

        boolean actual = operationC.isChildEmpty(Company.PROPERTY_DEPARTMENT, c);
        assertTrue(actual);

        c.addDepartment(d);

        actual = operationC.isChildEmpty(Company.PROPERTY_DEPARTMENT, c);
        assertFalse(actual);
    }

    /** Test of sizeChild method, of class EntityOperator. */
    @Test
    public void testChildSize() {

        assertTrue(c.isDepartmentEmpty());

        int actual = operationC.sizeChild(Company.PROPERTY_DEPARTMENT, c);
        assertEquals(0, actual);

        c.addDepartment(d);

        actual = operationC.sizeChild(Company.PROPERTY_DEPARTMENT, c);
        assertEquals(1, actual);
        c.clearDepartment();

        actual = operationC.sizeChild(Company.PROPERTY_DEPARTMENT, c);
        assertEquals(0, actual);
    }

    /** Test of removeChild method, of class EntityOperator. */
    @Test
    public void testRemoveChild() {

        assertTrue(c.isDepartmentEmpty());

        c.addDepartment(d);

        assertFalse(c.isDepartmentEmpty());

        operationC.removeChild(Company.PROPERTY_DEPARTMENT, c, d);

        assertTrue(c.isDepartmentEmpty());
    }
}
