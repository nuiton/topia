package org.nuiton.topia.it.legacy;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.junit.AbstractDatabaseResource;
import org.nuiton.topia.persistence.TopiaConfiguration;

/**
 * Put this class as a Rule in test to obtain a new isolated db for each test.
 *
 * Here is a simple example of usage :
 * <pre>
 * public class MyTest {
 *
 *   \@Rule
 *   public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();
 *
 *   \@Test
 *   public void testMethod() throws TopiaException {
 *
 *       TopiaContext tx = db.beginTransaction();
 *       ...
 * }
 * </pre>
 * The db created will be unique for each test method (and for each build also).
 *
 * You don't need to close any transaction, it will be done for you and the end
 * of each method test.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.8
 */
public class TopiaItLegacyDatabase extends AbstractDatabaseResource<TopiaItLegacyTopiaPersistenceContext, TopiaItLegacyTopiaApplicationContext> {

    @Override
    protected TopiaItLegacyTopiaApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration) {
        return new TopiaItLegacyTopiaApplicationContext(topiaConfiguration);
    }

}
