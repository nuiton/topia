package org.nuiton.topia.it.mapping.test17;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Test;
import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class FixAno2342Test extends AbstractMappingTest {

    @Test
    public void testColumnsExists() {

        TopiaItMappingTopiaPersistenceContext tx = db.newPersistenceContext();

        TopiaSqlSupport sqlSupport = tx.getSqlSupport();

        // check there is a column A.A.aa
        sqlSupport.executeSql("select aa from a.a;");

        // check there is a column B.B.b
        sqlSupport.executeSql("select b from b.b;");

        // check there is a column B.BB17.cc
        sqlSupport.executeSql("select cc from b.BB17;");

        // check there is a column B.BB17.bb1
        sqlSupport.executeSql("select bb1 from b.BB17;");

    }
}