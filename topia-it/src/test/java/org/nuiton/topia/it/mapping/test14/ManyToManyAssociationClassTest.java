package org.nuiton.topia.it.mapping.test14;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class ManyToManyAssociationClassTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A14DTopiaDao aDAO;

    protected B14DTopiaDao bDAO;

    protected Assoc14DTopiaDao assocDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A14D.class);
        bDAO = getDao(tx, B14D.class);
        assocDAO = getDao(tx, Assoc14D.class);
    }

    @Test
    public void testCreate() throws TopiaException {

        A14D a = aDAO.create();
        B14D b = bDAO.create();

        Assoc14D assoc = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a, Assoc14D.PROPERTY_B14_D, b);
        a.addB14DAssoc14D(assoc);
        b.addRoleA(assoc);
        aDAO.update(a);
        bDAO.update(b);
        tx.commit();


        Assoc14D assocFound = assocDAO.forTopiaIdEquals(assoc.getTopiaId()).findUnique();
        A14D aFound = assocFound.getRoleA();
        Assert.assertEquals(a, aFound);
        B14D bFound = assocFound.getB14D();
        Assert.assertEquals(b, bFound);

        Assert.assertEquals(1, aFound.sizeB14DAssoc14D());
        Assert.assertEquals(1, bFound.sizeRoleA());

    }


    @Test
    public void testFindAll() throws TopiaException {

        A14D a1 = aDAO.create();
        A14D a2 = aDAO.create();
        B14D b1 = bDAO.create();
        B14D b2 = bDAO.create();

        Assoc14D assoc11 = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a1, Assoc14D.PROPERTY_B14_D, b1);
        Assoc14D assoc12 = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a1, Assoc14D.PROPERTY_B14_D, b2);
        Assoc14D assoc21 = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a2, Assoc14D.PROPERTY_B14_D, b1);

        a1.addB14DAssoc14D(assoc11);
        a1.addB14DAssoc14D(assoc12);
        a2.addB14DAssoc14D(assoc21);
        b1.addRoleA(assoc11);
        b1.addRoleA(assoc21);
        b2.addRoleA(assoc12);

        aDAO.update(a1);
        aDAO.update(a2);
        bDAO.update(b1);
        bDAO.update(b2);

        tx.commit();

        Assert.assertEquals(3, assocDAO.count());

        List<Assoc14D> allByB14D = assocDAO.forProperties(Assoc14D.PROPERTY_B14_D, b1).findAll();
        Assert.assertEquals(2, allByB14D.size());

        List<Assoc14D> allByRoleA = assocDAO.forProperties(Assoc14D.PROPERTY_ROLE_A, a2).findAll();
        Assert.assertEquals(1, allByRoleA.size());

    }

    @Test
    @Ignore
    //FIXME AThimel 09/07/2012 #2177: Test is ignored as cascade-delete does not work as expected
    public void testDeleteCascade() throws TopiaException {

        A14D a1 = aDAO.create();
        A14D a2 = aDAO.create();
        B14D b1 = bDAO.create();
        B14D b2 = bDAO.create();

        Assoc14D assoc11 = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a1, Assoc14D.PROPERTY_B14_D, b1);
        Assoc14D assoc12 = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a1, Assoc14D.PROPERTY_B14_D, b2);
        Assoc14D assoc21 = assocDAO.create(Assoc14D.PROPERTY_ROLE_A, a2, Assoc14D.PROPERTY_B14_D, b1);

        a1.addB14DAssoc14D(assoc11);
        a1.addB14DAssoc14D(assoc12);
        a2.addB14DAssoc14D(assoc21);
        b1.addRoleA(assoc11);
        b1.addRoleA(assoc21);
        b2.addRoleA(assoc12);

        aDAO.update(a1);
        aDAO.update(a2);
        bDAO.update(b1);
        bDAO.update(b2);

        tx.commit();

        Assert.assertEquals(3, assocDAO.count());

        aDAO.delete(a1);

        tx.commit();

        Assert.assertEquals(1, assocDAO.count());

        List<Assoc14D> all = assocDAO.findAll();
        Assert.assertEquals(1, all.size());
        Assoc14D first = all.iterator().next();
        Assert.assertEquals(a2, first.getRoleA());
        Assert.assertEquals(b1, first.getB14D());
    }

}
