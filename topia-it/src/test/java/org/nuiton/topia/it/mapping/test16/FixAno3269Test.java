package org.nuiton.topia.it.mapping.test16;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

/**
 * Created on 6/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class FixAno3269Test {

    @Test
    public void testAccept() {

        {
            // A16 has only one property code
            MyTopiaEntityVisitor v = new MyTopiaEntityVisitor();

            new A16Impl().accept(v);

            Assert.assertEquals("code", v.visit);
        }
        {   // B16 has two properties code (from A16) + code2
            MyTopiaEntityVisitor v = new MyTopiaEntityVisitor();

            new B16Impl().accept(v);

            Assert.assertEquals("codecode2", v.visit);
        }
        {   // C16 has three properties code (from A16) + code2 (from B16) + code3
            MyTopiaEntityVisitor v = new MyTopiaEntityVisitor();

            new C16Impl().accept(v);

            Assert.assertEquals("codecode2code3", v.visit);
        }
    }

    private static class MyTopiaEntityVisitor implements TopiaEntityVisitor {

        String visit ="";


        @Override
        public void start(TopiaEntity entity) {
        }

        @Override
        public void end(TopiaEntity entity) {
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
            visit+=propertyName;
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
            visit+=propertyName;
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
            visit+=propertyName;
        }

        @Override
        public void clear() {
        }
    }
}
