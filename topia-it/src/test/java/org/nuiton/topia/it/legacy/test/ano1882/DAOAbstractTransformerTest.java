package org.nuiton.topia.it.legacy.test.ano1882;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;

public class DAOAbstractTransformerTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    @Test
    public void testAno1882() throws Exception {
        TopiaItLegacyTopiaPersistenceContext transaction = db.newPersistenceContext();

        FrenchCompanyTopiaDao dao = transaction.getFrenchCompanyDao();
        SIRETTopiaDao siretDAO = transaction.getSIRETDao();
        SIRET siret = siretDAO.create();
        FrenchCompany entity =
                dao.create(
                        FrenchCompany.PROPERTY_S_IREN, null,
                        FrenchCompany.PROPERTY_SIREN2, null,
                        FrenchCompany.PROPERTY_S_IRET, Arrays.asList(siret),
                        FrenchCompany.PROPERTY_SIRET2, null);
        transaction.commit();
        dao.delete(entity);
        transaction.commit();
    }
}
