package org.nuiton.topia.it.legacy.topiatest;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class CascadeSaveTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    /**
     * The following test will fail if no topiaId is injected on cascade save. This will test code within this method :
     * org.nuiton.topia.persistence.internal.support.TopiaHibernateEventListener#onSaveOrUpdate(org.hibernate.event.spi.SaveOrUpdateEvent)
     */
    @Test
    public void testSaveCascade() {

        TopiaItLegacyTopiaPersistenceContext persistenceContext = db.newPersistenceContext();

        CompanyTopiaDao companyDao = persistenceContext.getCompanyDao();
        DepartmentTopiaDao departmentDao = persistenceContext.getDepartmentDao();
        Company company = companyDao.newInstance();
        company.setName("Toto corp.");
        Department department = departmentDao.newInstance();
        department.setName("Jokes");
        company.addDepartment(department);

        companyDao.create(company);

    }

}
