package org.nuiton.topia.it.legacy.persistence.util;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.persistence.internal.FullyQualifiedNamePlusUuidTopiaIdFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.CompanyImpl;
import org.nuiton.topia.it.legacy.topiatest.Department;
import org.nuiton.topia.it.legacy.topiatest.DepartmentImpl;
import org.nuiton.topia.it.legacy.topiatest.Employe;
import org.nuiton.topia.it.legacy.topiatest.EmployeAbstract;
import org.nuiton.topia.it.legacy.topiatest.EmployeImpl;
import org.nuiton.topia.it.legacy.topiatest.Personne;
import org.nuiton.topia.it.legacy.topiatest.PersonneAbstract;
import org.nuiton.topia.it.legacy.topiatest.PersonneImpl;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;
import org.nuiton.topia.persistence.util.TopiaEntityIdsMap;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/** @author Tony Chemit - chemit@codelutin.com */
public class TopiaEntityHelperTest {

    protected static TopiaIdFactory topiaIdFactory = new FullyQualifiedNamePlusUuidTopiaIdFactory();

    protected static TopiaEntityEnum[] contracts;

    protected final Set<Class<? extends TopiaEntity>> contractsClass;

    public TopiaEntityHelperTest() {
        contractsClass = new HashSet<Class<? extends TopiaEntity>>();
        contractsClass.add(Company.class);
        contractsClass.add(Employe.class);
        contractsClass.add(Department.class);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        contracts = TopiaItLegacyEntityEnum.getContracts();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        contracts = null;
    }

    /** Test of getContractClass method, of class TopiaEntityHelper. */
    @Test
    public void testgetContract() {

        Class<?> result = TopiaEntityHelper.getContractClass(null, Company.class);
        assertNull(result);

        Class<? extends TopiaEntity> expResult;

        expResult = Employe.class;

        getContractClass(expResult, EmployeImpl.class);
        getContractClass(expResult, EmployeAbstract.class);
        getContractClass(expResult, Employe.class);

        expResult = Personne.class;

        getContractClass(expResult, PersonneImpl.class);
        getContractClass(expResult, PersonneAbstract.class);
        getContractClass(expResult, Personne.class);
    }

    /** Test of retainContracts method, of class TopiaEntityHelper. */
    @Test
    public void testRetainContracts() {
        Set<Class<? extends TopiaEntity>> classes = new HashSet<Class<? extends TopiaEntity>>();
        Set<Class<? extends TopiaEntity>> result = TopiaEntityHelper.retainContracts(contracts, classes);

        assertTrue(result.isEmpty());

        classes.add(PersonneImpl.class);
        result = TopiaEntityHelper.retainContracts(contracts, classes);
        assertEquals(1, result.size());
        assertTrue(result.contains(Personne.class));

        classes.add(PersonneAbstract.class);
        result = TopiaEntityHelper.retainContracts(contracts, classes);
        assertEquals(1, result.size());
        assertTrue(result.contains(Personne.class));

        classes.add(Personne.class);
        result = TopiaEntityHelper.retainContracts(contracts, classes);
        assertEquals(1, result.size());
        assertTrue(result.contains(Personne.class));

        classes.clear();
        classes.add(EmployeImpl.class);
        result = TopiaEntityHelper.retainContracts(contracts, classes);
        assertEquals(1, result.size());
        assertTrue(result.contains(Employe.class));

        classes.add(PersonneImpl.class);
        result = TopiaEntityHelper.retainContracts(contracts, classes);
        assertEquals(2, result.size());
        assertTrue(result.contains(Personne.class));
        assertTrue(result.contains(Employe.class));

    }

    /**
     * Test of detectTypes method, of class TopiaEntityHelper.
     *
     * @throws TopiaException FIXME
     */
    @Test
    public void testDetectTypes() throws TopiaException {
        Company company = new CompanyImpl();
        EmployeImpl employe = new EmployeImpl();
        Department departmnet = new DepartmentImpl();

        detectTypes(new Class<?>[]{Company.class, Employe.class, Department.class}, company, employe, departmnet);

        company.addEmploye(employe);
        detectTypes(new Class<?>[]{Company.class, Employe.class}, company);

        company.addDepartment(departmnet);
        departmnet.setCompany(company);
        detectTypes(new Class<?>[]{Company.class, Employe.class, Department.class}, company);

        company.removeEmploye(employe);
        company.removeDepartment(departmnet);
        detectTypes(new Class<?>[]{Company.class}, company);
    }

    @Test
    public void testDetector() throws Exception {

        Company company = new CompanyImpl();
        company.setTopiaId(topiaIdFactory.newTopiaId(Company.class, company));
        Employe employe = new EmployeImpl();
        employe.setTopiaId(topiaIdFactory.newTopiaId(Employe.class, employe));
        Department department = new DepartmentImpl();
        department.setTopiaId(topiaIdFactory.newTopiaId(Department.class, department));

        detectEntities(new Class<?>[]{Company.class, Employe.class, Department.class}, new int[]{1, 1, 1}, company, employe, department);
        detectEntityIds(new Class<?>[]{Company.class, Employe.class, Department.class}, new int[]{1, 1, 1}, company, employe, department);

        company.addEmploye(employe);
        detectEntities(new Class<?>[]{Company.class, Employe.class}, new int[]{1, 1, 1}, company);
        detectEntityIds(new Class<?>[]{Company.class, Employe.class}, new int[]{1, 1, 1}, company);

        company.addDepartment(department);
        department.setCompany(company);

        detectEntities(new Class<?>[]{Company.class, Employe.class, Department.class}, new int[]{1, 1, 1}, company);
        detectEntityIds(new Class<?>[]{Company.class, Employe.class, Department.class}, new int[]{1, 1, 1}, company);

        company.removeEmploye(employe);
        company.removeDepartment(department);


        detectEntities(new Class<?>[]{Company.class}, new int[]{1}, company);
        detectEntityIds(new Class<?>[]{Company.class}, new int[]{1}, company);

        detectEntities(new Class<?>[]{Company.class}, new int[]{2}, company, new CompanyImpl());
        detectEntityIds(new Class<?>[]{Company.class}, new int[]{2}, company, new CompanyImpl());

        department.setCompany(company);

        detectEntities(new Class<?>[]{Company.class, Department.class}, new int[]{1, 1}, department);
        detectEntityIds(new Class<?>[]{Company.class, Department.class}, new int[]{1, 1}, department);

        //TODO faire des tests avec des entites avec cycles
    }

    protected void detectEntities(Class<?>[] expected,
                                  int[] sizes,
                                  TopiaEntity... data) throws TopiaException {

        Map<Class<? extends TopiaEntity>, List<TopiaEntity>> actual = null;

        try {
            actual = TopiaEntityHelper.detectEntities(contracts, contractsClass, data);

            Assert.assertEquals(expected.length, actual.size());
            int index = 0;
            for (Class<?> c : expected) {
                List<TopiaEntity> value = actual.get(c);
                int expectedSize = sizes[index++];
                Assert.assertEquals(expectedSize, value.size());
            }
        } finally {
            if (actual != null) {
                actual.clear();
            }
        }
    }

    protected void detectEntityIds(Class<?>[] expected,
                                   int[] sizes,
                                   TopiaEntity... data) throws TopiaException {

        TopiaEntityIdsMap actual = null;

        try {
            actual = TopiaEntityHelper.detectEntityIds(contracts,
                                                       contractsClass,
                                                       data
            );

            Assert.assertEquals(expected.length, actual.size());
            int index = 0;
            for (Class<?> c : expected) {
                List<String> value = actual.get(c);
                int expectedSize = sizes[index++];
                Assert.assertEquals(expectedSize, value.size());
            }
        } finally {
            if (actual != null) {
                actual.clear();
            }
        }
    }

    protected void detectTypes(Class<?>[] expected, TopiaEntity... data) throws TopiaException {
        Set<String> fqns = new HashSet<String>(expected.length);
        Set<Class<? extends TopiaEntity>> actual = null;
        for (Class<?> c : expected) {
            fqns.add(c.getName());
        }
        try {
            actual = TopiaEntityHelper.detectTypes(contracts, data);
            Assert.assertEquals(expected.length, actual.size());
            for (Class<? extends TopiaEntity> c : actual) {
                Assert.assertTrue(fqns.contains(c.getName()));
            }
        } finally {
            fqns.clear();
            if (actual != null) {
                actual.clear();
            }
        }
    }

    protected void getContractClass(Class<?> expected, Class<? extends TopiaEntity> klass) {
        Class<?> result = TopiaEntityHelper.getContractClass(contracts, klass);
        assertEquals(expected, result);
    }
}
