package org.nuiton.topia.it.mapping.test14;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class OneToManyAssociationClassTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A14CTopiaDao aDAO;

    protected B14CTopiaDao bDAO;

    protected Assoc14CTopiaDao assocDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A14C.class);
        bDAO = getDao(tx, B14C.class);
        assocDAO = getDao(tx, Assoc14C.class);
    }

    @Test
    public void testCreate() throws TopiaException {

        A14C a = aDAO.create();
        B14C b = bDAO.create();

        Assoc14C assoc = assocDAO.create(Assoc14C.PROPERTY_A14_C, a, Assoc14C.PROPERTY_B14_C, b);
        a.addB14CAssoc14C(assoc);
        b.setA14CAssoc14C(assoc);
        aDAO.update(a);
        bDAO.update(b);
        tx.commit();


        Assoc14C assocFound = assocDAO.forTopiaIdEquals(assoc.getTopiaId()).findUnique();
        A14C aFound = assocFound.getA14C();
        Assert.assertEquals(a, aFound);
        Assert.assertEquals(b, assocFound.getB14C());

        Assert.assertEquals(1, aFound.sizeB14CAssoc14C());
        Assert.assertEquals(b, aFound.getB14CAssoc14C().iterator().next().getB14C());

    }


    @Test
    public void testFindAll() throws TopiaException {

        A14C a = aDAO.create();
        B14C b1 = bDAO.create();
        B14C b2 = bDAO.create();

        Assoc14C assoc1 = assocDAO.create(Assoc14C.PROPERTY_A14_C, a, Assoc14C.PROPERTY_B14_C, b1);
        a.addB14CAssoc14C(assoc1);
        b1.setA14CAssoc14C(assoc1);
        aDAO.update(a);
        bDAO.update(b1);

        tx.commit();

        List<B14C> allByA14C = bDAO.forA14CEquals(a).findAll();
        Assert.assertEquals(1, allByA14C.size());
        Assert.assertEquals(b1, allByA14C.iterator().next());

        B14C bFound = bDAO.findByAssoc14C(assoc1);
        Assert.assertEquals(b1, bFound);

        Assoc14C assoc2 = assocDAO.create(Assoc14C.PROPERTY_A14_C, a, Assoc14C.PROPERTY_B14_C, b2);
        a.addB14CAssoc14C(assoc2);
        b2.setA14CAssoc14C(assoc2);

        aDAO.update(a);
        bDAO.update(b2);

        tx.commit();

        bFound = bDAO.findByAssoc14C(assoc1);
        Assert.assertEquals(b1, bFound);
        bFound = bDAO.findByAssoc14C(assoc2);
        Assert.assertEquals(b2, bFound);

        allByA14C = bDAO.forA14CEquals(a).findAll();
        Assert.assertEquals(2, allByA14C.size());
        A14C aFound = allByA14C.iterator().next().getA14CAssoc14C().getA14C();
        Assert.assertEquals(2, aFound.sizeB14CAssoc14C());

    }

}
