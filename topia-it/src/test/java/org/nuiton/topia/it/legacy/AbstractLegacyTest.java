package org.nuiton.topia.it.legacy;

/*
 * #%L
 * ToPIA :: IT
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Rule;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class AbstractLegacyTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    protected <E extends TopiaEntity, D extends TopiaDao<E>> D getDao(
            TopiaItLegacyTopiaPersistenceContext tx, Class<E> entityType) throws TopiaException {
        D dao = (D)tx.getDao(entityType);
        return dao;
    }
}
