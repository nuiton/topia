package org.nuiton.topia.it.legacy.topiatest;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;

/**
 * Test the support of possibility to have an attribute of type enumeration
 * in a entity
 */
public class EnumTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    /**
     * Create an entity having two field of type enumeration. One is stored
     * using ordinal, the other using the name.
     *
     * The test check that values are stored, and that find methods works
     *
     * @throws TopiaException if any exception with db
     */
    @Test
    public void storeEntityWithEnumValue() throws TopiaException {
        TopiaItLegacyTopiaPersistenceContext transaction = db.newPersistenceContext();

        PersonneTopiaDao dao = transaction.getPersonneDao();
        Personne personne = new PersonneImpl();
        personne.setGender(Gender.FEMALE);
        personne.setOtherGender(Gender.MALE);
        dao.create(personne);
        String topiaId = personne.getTopiaId();
        transaction.commit();
        transaction.close();

        transaction = db.newPersistenceContext();
        dao = transaction.getPersonneDao();
        dao.forTopiaIdEquals(topiaId).findUnique();
        Assert.assertEquals(Gender.FEMALE, personne.getGender());
        Assert.assertEquals(Gender.MALE, personne.getOtherGender());

        Assert.assertNotNull(dao.forGenderEquals(Gender.FEMALE).findAnyOrNull());
        Assert.assertNotNull(dao.forOtherGenderEquals(Gender.MALE).findAnyOrNull());
        transaction.close();
    }
}
