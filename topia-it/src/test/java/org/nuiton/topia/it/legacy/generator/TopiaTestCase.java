package org.nuiton.topia.it.legacy.generator;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.CompanyTopiaDao;
import org.nuiton.topia.it.legacy.topiatest.Department;
import org.nuiton.topia.it.legacy.topiatest.DepartmentTopiaDao;

/**
 * TopiaTestCase.
 *
 * @author chatellier
 */
public class TopiaTestCase {

    /** Logger */
    private final static Log log = LogFactory.getLog(TopiaTestCase.class);

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

//    /** Proprietes */
//    protected static Properties config;
//
//    /** TopiaContext */
//    protected static TopiaContext context;

//    /**
//     * Init les proprietes de connection a la base
//     *
//     * @throws IOException for any IO error while getting configuration.
//     */
//    @BeforeClass
//    public static void init() throws IOException {
//
//        File testBasedir = TestHelper.getTestBasedir(TopiaTestCase.class);
//
//        config = TestHelper.initTopiaContextConfiguration(
//                testBasedir,
//                "/TopiaContextImpl.properties",
//                "TopiaTestCaseDb");
////        config = new Properties();
////        config.setProperty("topia.persistence.classes", TopiaTestDAOHelper.getImplementationClassesAsString());
////
////        config.setProperty(Environment.USER, "sa");
////        config.setProperty(Environment.PASS, "");
////        config.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
////        config.setProperty(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
////        config.setProperty(Environment.DRIVER, "org.h2.Driver");
////        config.setProperty(Environment.URL, "jdbc:h2:file:" + testBasedir + "/db/data_" + System.currentTimeMillis());
//    }
//
//    @AfterClass
//    public static void after() throws TopiaException {
//        // destroy database
//        context.clear(false);
//    }

//    /** Create base with schema created. */
//    @Before
//    public void setUp() {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Junit beforeTest");
//        }
//
//        try {
//            context = TopiaContextFactory.getContext(config);
//
//            try {
//                context.createSchema();
//            } catch (TopiaException e) {
//                log.error("Erreur à la creation du schema", e);
//            }
//        } catch (TopiaNotFoundException e) {
//            log.error("Erreur à la creation du topia context", e);
//        }
//    }

    @Test
    public void testCompositeAssociations() throws TopiaException {
        if (log.isDebugEnabled()) {
            log.debug("Junit Test testCompositeAssociations");
        }

//        try {
        TopiaItLegacyTopiaPersistenceContext newContext = db.newPersistenceContext();

        CompanyTopiaDao companyDAO = newContext.getCompanyDao();
        DepartmentTopiaDao departmentDAO = newContext.getDepartmentDao();

        Company company = companyDAO.create();
        company.setName("Ma société");


        Department dep1 = departmentDAO.create();
        dep1.setName("Departement 1");
        Department dep2 = departmentDAO.create();
        dep2.setName("Departement 2");
        Department dep3 = departmentDAO.create();
        dep3.setName("Departement 3");
        Department dep4 = departmentDAO.create();
        dep4.setName("Departement 7");

        departmentDAO.update(dep1);
        departmentDAO.update(dep2);
        departmentDAO.update(dep3);
        departmentDAO.update(dep4);

        company.addDepartment(dep1);
        company.addDepartment(dep2);
        company.addDepartment(dep3);
        company.addDepartment(dep4);

        companyDAO.update(company);
        newContext.commit();

        newContext = db.newPersistenceContext();

        companyDAO = newContext.getCompanyDao();

        company = companyDAO.forTopiaIdEquals(company.getTopiaId()).findUnique();

        Assert.assertEquals(company.getName(), "Ma société");
        Assert.assertEquals(company.getDepartment().size(), 4);

        newContext.commit();
//            newContext.close();
//        } catch (TopiaException e) {
//            log.error("Erreur pendant le test testCompositeAssociations", e);
//        }
    }
}
