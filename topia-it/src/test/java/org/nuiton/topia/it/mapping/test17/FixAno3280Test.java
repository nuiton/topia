package org.nuiton.topia.it.mapping.test17;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcH2Helper;

import java.sql.SQLException;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class FixAno3280Test extends AbstractMappingTest {

    @Test
    public void testTablesExists() throws SQLException {

        JdbcConfiguration configuration = db.getApplicationContext().getConfiguration();

        JdbcH2Helper jdbcH2Helper = new JdbcH2Helper(configuration);
        Assert.assertTrue("Could not find table A.A", jdbcH2Helper.isTableExist("A", "A"));
        Assert.assertTrue("Could not find table B.B", jdbcH2Helper.isTableExist("B", "B"));
        Assert.assertTrue("Could not find table B.BB17", jdbcH2Helper.isTableExist("B", "BB17"));

    }

}
