
package org.nuiton.topia.it.mapping.test11;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Tests basic unique fields on a entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class EntityWithBasicUniqueFieldsTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A11ETopiaDao aDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A11E.class);
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();

        Assert.assertEquals(0, nbA);

        // a with all fields to null
        A11E a = aDAO.create();

        tx.commit();

        nbA = aDAO.count();
        Assert.assertEquals(1, nbA);

        A11E aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);

        // try another one with null value, can do it since null is not unique ?
        A11E a2 = aDAO.create();
        tx.commit();

        nbA = aDAO.count();
        Assert.assertEquals(2, nbA);

        aBis = aDAO.forTopiaIdEquals(a2.getTopiaId()).findUnique();
        Assert.assertEquals(a2, aBis);

        A11E a3 = aDAO.create(A11E.PROPERTY_INTEGER_FIELD, 1);
        tx.commit();

        nbA = aDAO.count();
        Assert.assertEquals(3, nbA);

        aBis = aDAO.forTopiaIdEquals(a3.getTopiaId()).findUnique();
        Assert.assertEquals(a3, aBis);

        A11E a4 = aDAO.create(A11E.PROPERTY_INTEGER_FIELD, 1);
        try {
            tx.commit();
            Assert.fail();

        } catch (TopiaException e) {
            // ok duplicate key
            tx.rollback();
        }

    }
}