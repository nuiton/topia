package org.nuiton.topia.it.mapping.test16;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.it.mapping.TopiaItMappingEntityEnum;

/**
 * Created on 6/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class FixAno3267Test {

    class D extends C16Impl {
    }

    @Test
    public void testValueOf() {
        Assert.assertEquals(TopiaItMappingEntityEnum.A16, TopiaItMappingEntityEnum.valueOf(A16.class));
        Assert.assertEquals(TopiaItMappingEntityEnum.A16, TopiaItMappingEntityEnum.valueOf(A16Impl.class));

        Assert.assertEquals(TopiaItMappingEntityEnum.B16, TopiaItMappingEntityEnum.valueOf(B16.class));
        Assert.assertEquals(TopiaItMappingEntityEnum.B16, TopiaItMappingEntityEnum.valueOf(B16Impl.class));

        Assert.assertEquals(TopiaItMappingEntityEnum.C16, TopiaItMappingEntityEnum.valueOf(C16.class));
        Assert.assertEquals(TopiaItMappingEntityEnum.C16, TopiaItMappingEntityEnum.valueOf(C16Impl.class));
        Assert.assertEquals(TopiaItMappingEntityEnum.C16, TopiaItMappingEntityEnum.valueOf(D.class));
    }
}
