
package org.nuiton.topia.it.mapping.test4;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.it.mapping.test3.B3TopiaDao;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Arrays;

/**
 * Tests that everything is ok with fields with multiplicity → 1 on primitive data.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ElementCollectionTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A4TopiaDao aDAO;

    protected B3TopiaDao bDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A4.class);
    }

    @Test
    public void create() throws TopiaException {

        A4 a = aDAO.create();
        a.setStrings(Arrays.asList("a", "b", "c"));
        a.setEnumerations(Arrays.asList(E4.LITERAL1, E4.LITERAL2));

        tx.commit();

        A4 aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();

        Assert.assertNotNull(aBis);
        Assert.assertEquals(a, aBis);
        Assert.assertNotNull(aBis.getStrings());
        Assert.assertEquals(3, aBis.sizeStrings());
        Assert.assertEquals(a.getStrings(), aBis.getStrings());

        Assert.assertNotNull(aBis.getEnumerations());
        Assert.assertEquals(2, aBis.sizeEnumerations());
        Assert.assertEquals(a.getEnumerations(), aBis.getEnumerations());
    }

    @Test
    public void delete() throws TopiaException {

        create();

        long nbA = aDAO.count();

        Assert.assertEquals(1, nbA);

        A4 a = aDAO.findAll().get(0);
        aDAO.delete(a);

        tx.commit();

        nbA = aDAO.count();

        Assert.assertEquals(0, nbA);
    }
}