
package org.nuiton.topia.it.mapping.test5;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Test behaviour of a bi-directional OnetoMany (composition) relation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class BiDirectionalOneToManyCompositionTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A51TopiaDao aDAO;

    protected B51TopiaDao bDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A51.class);
        bDAO = getDao(tx, B51.class);
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();
        long nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(0, nbB);

        A51 a = aDAO.create();
        B51 b = bDAO.create();

        a.addB51(b);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(1, nbA);
        Assert.assertEquals(1, nbB);

        A51 aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);
        Assert.assertEquals(1, aBis.sizeB51());
        Assert.assertEquals(b, aBis.getB51().iterator().next());

        B51 bBis = bDAO.forTopiaIdEquals(b.getTopiaId()).findUnique();
        Assert.assertEquals(b, bBis);
        Assert.assertEquals(aBis.getB51().iterator().next(), bBis);
    }

    @Test
    public void delete() throws TopiaException {

        create();

        long nbA = aDAO.count();
        long nbB = bDAO.count();

        Assert.assertEquals(1, nbA);
        Assert.assertEquals(1, nbB);

        A51 a = aDAO.findAll().get(0);
        aDAO.delete(a);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(0, nbB);
    }
}