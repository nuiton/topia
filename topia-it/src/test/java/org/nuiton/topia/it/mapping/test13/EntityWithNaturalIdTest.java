
package org.nuiton.topia.it.mapping.test13;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.mapping.AbstractMappingTest;

import java.util.Date;

/**
 * Tests a entity with a {@code natural id}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class EntityWithNaturalIdTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A13ATopiaDao aDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A13A.class);
    }

    @Test(expected = TopiaException.class)
    public void createWithNull() throws TopiaException {

        long nbA = aDAO.count();

        Assert.assertEquals(0, nbA);

        // a with all fields to null
        aDAO.create();
        tx.commit();
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();

        Assert.assertEquals(0, nbA);

        // a with all fields to null
        Date date = new Date();
        A13A a = aDAO.create(
                A13A.PROPERTY_NATURAL_ID_STRING, "string",
                A13A.PROPERTY_NATURAL_ID_INTEGER, 1,
                A13A.PROPERTY_NATURAL_ID_DATE, date
        );
        a.setIntegerField(10);

        tx.commit();

        nbA = aDAO.count();
        Assert.assertEquals(1, nbA);

        A13A aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);

        aBis = aDAO.forNaturalId(1, "string", date).findUnique();
        Assert.assertEquals(a, aBis);

        aDAO.create(
                A13A.PROPERTY_NATURAL_ID_STRING, "string",
                A13A.PROPERTY_NATURAL_ID_INTEGER, 1,
                A13A.PROPERTY_NATURAL_ID_DATE, date
        );

        try {
            tx.commit();
            // duplicated natural id
            Assert.fail();
        } catch (TopiaException e) {
            Assert.assertTrue(true);
        }

    }
}