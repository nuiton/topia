package org.nuiton.topia.it.mapping.test14;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.mapping.AbstractMappingTest;

import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class BidiAssociationClassTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A14ATopiaDao aDAO;

    protected B14ATopiaDao bDAO;

    protected Assoc14ATopiaDao assocDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A14A.class);
        bDAO = getDao(tx, B14A.class);
        assocDAO = getDao(tx, Assoc14A.class);
    }

    @Test
    public void testCreate() throws TopiaException {

        A14A a = aDAO.create();
        B14A b = bDAO.create();

        Assoc14A assoc = assocDAO.create(Assoc14A.PROPERTY_A14_A, a, Assoc14A.PROPERTY_ROLE_B, b);
        tx.commit();
        Assert.assertNotNull(assoc.getTopiaId());

        Assoc14A assocFound = assocDAO.forTopiaIdEquals(assoc.getTopiaId()).findUnique();
        A14A aFound = assocFound.getA14A();
        B14A bFound = assocFound.getRoleB();

        Assert.assertNotNull(aFound);
        Assert.assertEquals(a, aFound);
        Assert.assertNotNull(bFound);
        Assert.assertEquals(b, bFound);
    }

    @Test(expected = TopiaException.class)
    public void testCreateWithNull() throws TopiaException {

        A14A a = aDAO.create();

        Assert.assertNotNull(a.getTopiaId());

        Assoc14A assoc = assocDAO.create(Assoc14A.PROPERTY_A14_A, a);
        Assert.assertNotNull(assoc.getTopiaId());

        // Will throw an exception because 'b' is mandatory
        tx.commit();
    }

    @Test
    public void testFindAll() throws TopiaException {

        A14A a1 = aDAO.create();
        A14A a2 = aDAO.create();
        B14A b = bDAO.create();

        Assoc14A assoc = assocDAO.create(Assoc14A.PROPERTY_A14_A, a1, Assoc14A.PROPERTY_ROLE_B, b);
        a1.setRoleB(assoc);
        aDAO.update(a1);
        b.setA14AAssoc14A(assoc);
        bDAO.update(b);

        tx.commit();

        A14A aFound = aDAO.findByAssoc14A(assoc);
        Assert.assertEquals(a1, aFound);

        aFound = aDAO.forRoleBEquals(b).findAnyOrNull();
        Assert.assertEquals(a1, aFound);

        B14A bFound = bDAO.forA14AEquals(a1).findAnyOrNull();
        Assert.assertEquals(b, bFound);

        bFound = bDAO.forA14AEquals(a2).findAnyOrNull();
        Assert.assertNull(bFound);

        bFound = bDAO.findByAssoc14A(assoc);
        Assert.assertEquals(b, bFound);

    }

    @Test
    @Ignore //FIXME AThimel 09/07/2012 #2177: Test is ignored as cascade-delete does not work as expected
    public void testDeleteCascade() throws TopiaException {

        A14A a1 = aDAO.create();
        A14A a2 = aDAO.create();
        B14A b1 = bDAO.create();
        B14A b2 = bDAO.create();

        Assoc14A assoc11 = assocDAO.create(Assoc14A.PROPERTY_A14_A, a1, Assoc14A.PROPERTY_ROLE_B, b1);
        Assoc14A assoc22 = assocDAO.create(Assoc14A.PROPERTY_A14_A, a2, Assoc14A.PROPERTY_ROLE_B, b2);

        a1.setRoleB(assoc11);
        a2.setRoleB(assoc22);
        b1.setA14AAssoc14A(assoc11);
        b2.setA14AAssoc14A(assoc22);

        aDAO.update(a1);
        aDAO.update(a2);
        bDAO.update(b1);
        bDAO.update(b2);

        tx.commit();

        Assert.assertEquals(2, assocDAO.count());

        aDAO.delete(a1);

        tx.commit();

        Assert.assertEquals(1, assocDAO.count());

        List<Assoc14A> all = assocDAO.findAll();
        Assert.assertEquals(1, all.size());
        Assoc14A first = all.iterator().next();
        Assert.assertEquals(a2, first.getA14A());
        Assert.assertEquals(b2, first.getRoleB());
    }

}
