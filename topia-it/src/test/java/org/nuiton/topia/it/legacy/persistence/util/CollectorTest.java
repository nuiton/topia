package org.nuiton.topia.it.legacy.persistence.util;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.it.legacy.topiatest.Company;
import org.nuiton.topia.it.legacy.topiatest.CompanyImpl;
import org.nuiton.topia.it.legacy.topiatest.Department;
import org.nuiton.topia.it.legacy.topiatest.DepartmentImpl;
import org.nuiton.topia.it.legacy.topiatest.EmployeImpl;
import org.nuiton.topia.persistence.legacy.Collector;

/** @author Tony Chemit - chemit@codelutin.com */
public class CollectorTest {

    private static Log log = LogFactory.getLog(CollectorTest.class);

    static TopiaEntityEnum[] contracts;

    @BeforeClass
    public static void setUpClass() throws Exception {
        contracts = TopiaItLegacyEntityEnum.getContracts();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        contracts = null;
    }

    @Test
    public void testCollector() throws Exception {

        Collector<Integer> detector = new Collector<Integer>(contracts) {

            int hits;

            @Override
            protected void beforeAll(CollectorVisitor visitor, TopiaEntity... entities) {
                super.beforeAll(visitor, entities);
                hits = 0;
            }

            @Override
            protected Integer afterAll(CollectorVisitor visitor, TopiaEntity... entities) {
                return hits;
            }

            @Override
            protected void onStarted(TopiaEntity e, boolean enter) {
                super.onStarted(e, enter);
                int level = stackSize();
                log.info(String.format("(%1$2d) %2$" + level * 2 + "s %3$s", level, ">>", getStack()));

                hits++;
            }

            @Override
            protected void onEnded(TopiaEntity e, boolean enter) {
                super.onEnded(e, enter);
                int level = stackSize() + 1;
                log.info(String.format("(%1$2d) %2$" + level * 2 + "s %3$s", level, "<<", getStack()));
            }
        };

        Company company = new CompanyImpl();
        EmployeImpl employe = new EmployeImpl();
        Department department = new DepartmentImpl();

        detect(detector, 1, company);

        company.addEmploye(employe);
        detect(detector, 2, company);

        company.addDepartment(department);
        detect(detector, 3, company);

        company.removeEmploye(employe);
        detect(detector, 2, company);

        company.removeDepartment(department);
        detect(detector, 1, company);
    }

    protected void detect(Collector<Integer> detector,
                          int expectedResult,
                          TopiaEntity... entities) throws TopiaException {
        Integer result = detector.detect(entities);
        Assert.assertNotNull(result);
        Assert.assertEquals(expectedResult, result.intValue());
    }
}
