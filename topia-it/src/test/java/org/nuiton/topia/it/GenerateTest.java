package org.nuiton.topia.it;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.it.legacy.AbstractTopiaItLegacyEntity;
import org.nuiton.topia.it.legacy.AbstractTopiaItLegacyApplicationContext;
import org.nuiton.topia.it.legacy.AbstractTopiaItLegacyDao;
import org.nuiton.topia.it.legacy.AbstractTopiaItLegacyPersistenceContext;
import org.nuiton.topia.it.legacy.AbstractTopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.AbstractTopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.TopiaItLegacyApplicationContext;
import org.nuiton.topia.it.legacy.TopiaItLegacyDaoSupplier;
import org.nuiton.topia.it.legacy.TopiaItLegacyPersistenceContext;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaDaoSupplier;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.test.entities.AbstractPetTopiaDao;
import org.nuiton.topia.it.legacy.test.entities.GeneratedPetTopiaDao;
import org.nuiton.topia.it.legacy.test.entities.PetAbstract;
import org.nuiton.topia.it.legacy.test.entities.PetImpl;
import org.nuiton.topia.it.legacy.test.entities.PetTopiaDao;

/**
 * To test generated classes.
 *
 * Created on 12/19/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class GenerateTest {

    @Test
    public void testApplicationContext() {

        // add contract from class-path
        Assert.assertTrue(TopiaItLegacyApplicationContext.class.isAssignableFrom(AbstractTopiaItLegacyTopiaApplicationContext.class));
        Assert.assertTrue(TopiaItLegacyApplicationContext.class.isAssignableFrom(TopiaItLegacyTopiaApplicationContext.class));

        // add super-class from tag-value applicationContextSuperClass
        Assert.assertTrue(AbstractTopiaItLegacyApplicationContext.class.isAssignableFrom(AbstractTopiaItLegacyTopiaApplicationContext.class));
        Assert.assertTrue(AbstractTopiaItLegacyApplicationContext.class.isAssignableFrom(TopiaItLegacyTopiaApplicationContext.class));
    }

    @Test
    public void testDaoSupplier() {

        // add contract from class-path
        Assert.assertTrue(TopiaItLegacyDaoSupplier.class.isAssignableFrom(TopiaItLegacyTopiaDaoSupplier.class));
    }

    @Test
    public void testPersistenceContext() {

        // use dao supplier as contract of topia persistence context
        Assert.assertTrue(TopiaItLegacyDaoSupplier.class.isAssignableFrom(AbstractTopiaItLegacyTopiaPersistenceContext.class));

        // add contract from class-path
        Assert.assertTrue(TopiaItLegacyPersistenceContext.class.isAssignableFrom(AbstractTopiaItLegacyTopiaPersistenceContext.class));
        Assert.assertTrue(TopiaItLegacyPersistenceContext.class.isAssignableFrom(TopiaItLegacyTopiaPersistenceContext.class));

        // add super-class from tag-value persistenceContextSuperClass
        Assert.assertTrue(AbstractTopiaItLegacyPersistenceContext.class.isAssignableFrom(AbstractTopiaItLegacyTopiaPersistenceContext.class));
        Assert.assertTrue(AbstractTopiaItLegacyPersistenceContext.class.isAssignableFrom(TopiaItLegacyTopiaPersistenceContext.class));
    }

    @Test
    public void testDao() {

        // add super-class from tag-value daoSuperClass
        Assert.assertTrue(AbstractTopiaItLegacyDao.class.isAssignableFrom(GeneratedPetTopiaDao.class));
        Assert.assertTrue(AbstractTopiaItLegacyDao.class.isAssignableFrom(AbstractPetTopiaDao.class));
        Assert.assertTrue(AbstractTopiaItLegacyDao.class.isAssignableFrom(PetTopiaDao.class));
    }

    @Test
    public void testEntity() {

        // add super-class from tag-value entitySuperClass
        Assert.assertTrue(AbstractTopiaItLegacyEntity.class.isAssignableFrom(PetAbstract.class));
        Assert.assertTrue(AbstractTopiaItLegacyEntity.class.isAssignableFrom(PetImpl.class));
    }

}
