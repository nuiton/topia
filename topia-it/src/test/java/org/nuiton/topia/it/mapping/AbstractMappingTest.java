
package org.nuiton.topia.it.mapping;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Rule;
import org.nuiton.topia.junit.AbstractDatabaseResource;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;


/**
 * Base test for all tests using the {@code mapping} model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public abstract class AbstractMappingTest {

    @Rule
    public final AbstractDatabaseResource<TopiaItMappingTopiaPersistenceContext, TopiaItMappingTopiaApplicationContext> db =
            new AbstractDatabaseResource<TopiaItMappingTopiaPersistenceContext, TopiaItMappingTopiaApplicationContext>() {

                @Override
                protected TopiaItMappingTopiaApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration) {
                    return new TopiaItMappingTopiaApplicationContext(topiaConfiguration);
                }
            };

    protected <E extends TopiaEntity, D extends TopiaDao<E>> D getDao(
            TopiaItMappingTopiaPersistenceContext tx, Class<E> entityType) throws TopiaException {
        D dao = (D)tx.getDao(entityType);
        return dao;
    }
}
