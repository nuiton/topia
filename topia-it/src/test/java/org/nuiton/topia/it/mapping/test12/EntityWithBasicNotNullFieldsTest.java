
package org.nuiton.topia.it.mapping.test12;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.it.mapping.AbstractMappingTest;

import java.util.Date;

/**
 * Tests entity with basic {@code not null} fields.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class EntityWithBasicNotNullFieldsTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A12ATopiaDao aDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A12A.class);
    }

    @Test(expected = TopiaException.class)
    public void createWithNull() throws TopiaException {

        long nbA = aDAO.count();

        Assert.assertEquals(0, nbA);

        // a with all fields to null
        aDAO.create();
        tx.commit();
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();

        Assert.assertEquals(0, nbA);

        // a with all fields to null
        A12A a = aDAO.create(
                A12A.PROPERTY_STRING_FIELD, "string",
                A12A.PROPERTY_INTEGER_FIELD, 1,
                A12A.PROPERTY_DATE_FIELD, new Date()
        );

        tx.commit();

        nbA = aDAO.count();
        Assert.assertEquals(1, nbA);

        A12A aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);

    }
}