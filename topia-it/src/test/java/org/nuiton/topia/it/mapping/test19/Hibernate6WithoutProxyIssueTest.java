package org.nuiton.topia.it.mapping.test19;

/*-
 * #%L
 * ToPIA :: IT
 * %%
 * Copyright (C) 2004 - 2024 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.hibernate.query.QueryArgumentException;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaQueryException;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Migrating ToPIA from Hibernate 5 to 6 raised an issue when passing an entity as a query parameter
 * if given entity is a proxyfied. An internal validation check in Hibernate check that
 * the objet passed is of type "MyEntityImpl" but the proxy is on "MyEntity" the contract so
 * the validation fail. We had to circumvent that issue by adding an {@link org.hibernate.Hibernate#unproxy(Object)}
 * on every parameters.
 */
public class Hibernate6WithoutProxyIssueTest extends AbstractMappingTest {

    @Test
    public void testUsingProxyfiedEntityAsQueryParameterWorks() {

        try (TopiaItMappingTopiaPersistenceContext persistenceContext = db.newPersistenceContext()) {
            A19TopiaDao a19Dao = persistenceContext.getA19Dao();
            B19TopiaDao b19Dao = persistenceContext.getB19Dao();
            C19TopiaDao c19Dao = persistenceContext.getC19Dao();

            C19 c19 = c19Dao.create();
            a19Dao.create(A19.PROPERTY_C19, c19);
            b19Dao.create(B19.PROPERTY_C19, c19);

            persistenceContext.commit();
        }

        try (TopiaItMappingTopiaPersistenceContext persistenceContext = db.newPersistenceContext()) {
            A19TopiaDao a19Dao = persistenceContext.getA19Dao();
            B19TopiaDao b19Dao = persistenceContext.getB19Dao();
            A19 a19 = a19Dao.newQueryBuilder().findUnique();
            try {
                b19Dao.forC19Equals(a19.getC19()).findUnique();
                b19Dao.forC19In(Set.of(a19.getC19())).findUnique();
            } catch (TopiaQueryException e) {
                // we expect a stack trace like

                // org.nuiton.topia.persistence.TopiaQueryException: unable to find page startIndex=0, endIndex=1
                //	at org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport.find0(HibernateTopiaJpaSupport.java:299)
                //	at org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport.findUnique(HibernateTopiaJpaSupport.java:237)
                //	at org.nuiton.topia.persistence.internal.AbstractTopiaDao.findUniqueOrNull(AbstractTopiaDao.java:523)
                //	at org.nuiton.topia.persistence.internal.AbstractTopiaDao.findUnique(AbstractTopiaDao.java:507)
                //	at org.nuiton.topia.persistence.internal.AbstractTopiaDao$InnerTopiaQueryBuilderRunQueryStep.findUnique(AbstractTopiaDao.java:1199)
                //	at org.nuiton.topia.persistence.internal.AbstractTopiaDao$InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep.findUnique(AbstractTopiaDao.java:1040)
                //	at org.nuiton.topia.it.mapping.test19.Hibernate6WithoutProxyIssueTest.testEntityGenerated(Hibernate6WithoutProxyIssueTest.java:31)
                //	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
                //	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:77)
                //	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
                //	at java.base/java.lang.reflect.Method.invoke(Method.java:568)
                //	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:59)
                //	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
                //	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:56)
                //	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
                //	at org.junit.rules.TestWatcher$1.evaluate(TestWatcher.java:61)
                //	at org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)
                //	at org.junit.runners.BlockJUnit4ClassRunner$1.evaluate(BlockJUnit4ClassRunner.java:100)
                //	at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:366)
                //	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:103)
                //	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:63)
                //	at org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)
                //	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)
                //	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)
                //	at org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)
                //	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)
                //	at org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)
                //	at org.junit.runners.ParentRunner.run(ParentRunner.java:413)
                //	at org.junit.runner.JUnitCore.run(JUnitCore.java:137)
                //	at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:69)
                //	at com.intellij.rt.junit.IdeaTestRunner$Repeater$1.execute(IdeaTestRunner.java:38)
                //	at com.intellij.rt.execution.junit.TestsRepeater.repeat(TestsRepeater.java:11)
                //	at com.intellij.rt.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:35)
                //	at com.intellij.rt.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:232)
                //	at com.intellij.rt.junit.JUnitStarter.main(JUnitStarter.java:55)
                //Caused by: org.hibernate.query.QueryArgumentException: Argument [org.nuiton.topia.it.mapping.test19.C19Impl@c0fe91fd] of type [org.nuiton.topia.it.mapping.test19.C19Impl$HibernateProxy$xOjVqHgc] did not match parameter type [org.nuiton.topia.it.mapping.test19.C19Impl (n/a)]
                //	at org.hibernate.query.spi.QueryParameterBindingValidator.validate(QueryParameterBindingValidator.java:85)
                //	at org.hibernate.query.spi.QueryParameterBindingValidator.validate(QueryParameterBindingValidator.java:32)
                //	at org.hibernate.query.internal.QueryParameterBindingImpl.validate(QueryParameterBindingImpl.java:362)
                //	at org.hibernate.query.internal.QueryParameterBindingImpl.setBindValue(QueryParameterBindingImpl.java:137)
                //	at org.hibernate.query.spi.AbstractCommonQueryContract.setParameter(AbstractCommonQueryContract.java:835)
                //	at org.hibernate.query.spi.AbstractSelectionQuery.setParameter(AbstractSelectionQuery.java:895)
                //	at org.hibernate.query.sqm.internal.QuerySqmImpl.setParameter(QuerySqmImpl.java:1255)
                //	at org.hibernate.query.sqm.internal.QuerySqmImpl.setParameter(QuerySqmImpl.java:140)
                //	at org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport.prepareQuery(HibernateTopiaJpaSupport.java:136)
                //	at org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport.find0(HibernateTopiaJpaSupport.java:270)
                //	... 34 more

                Throwable cause = e.getCause();
                if (cause instanceof QueryArgumentException) {
                    QueryArgumentException queryArgumentException = (QueryArgumentException) cause;
                    boolean parameterTypeMatch = queryArgumentException.getParameterType().equals(C19Impl.class);
                    boolean argumentClassMatch = queryArgumentException.getArgument().getClass().getName().startsWith("org.nuiton.topia.it.mapping.test19.C19Impl$HibernateProxy$");
                    String message = queryArgumentException.getMessage();
                    String noise1 = StringUtils.substringBetween(message, "[org.nuiton.topia.it.mapping.test19.C19Impl@", "]");
                    String noise2 = StringUtils.substringBetween(message, "[org.nuiton.topia.it.mapping.test19.C19Impl$HibernateProxy$", "]");
                    String messageWithoutNoise = StringUtils.remove(StringUtils.remove(message, noise1), noise2);
                    boolean messageMatch = messageWithoutNoise.equals("Argument [org.nuiton.topia.it.mapping.test19.C19Impl@] of type [org.nuiton.topia.it.mapping.test19.C19Impl$HibernateProxy$] did not match parameter type [org.nuiton.topia.it.mapping.test19.C19Impl (n/a)]");
                    if (parameterTypeMatch && argumentClassMatch && messageMatch) {
                        Assert.fail("expected no issue but got " + e);
                    } else {
                        throw e;
                    }
                }
            }
        }
    }
}
