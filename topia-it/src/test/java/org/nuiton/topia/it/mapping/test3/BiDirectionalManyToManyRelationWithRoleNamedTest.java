
package org.nuiton.topia.it.mapping.test3;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import org.nuiton.topia.it.mapping.AbstractMappingTest;
import org.nuiton.topia.it.mapping.TopiaItMappingTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Tests a bi-directional ManyToMany relation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class BiDirectionalManyToManyRelationWithRoleNamedTest extends AbstractMappingTest {

    protected TopiaItMappingTopiaPersistenceContext tx;

    protected A33TopiaDao aDAO;

    protected B33TopiaDao bDAO;

    @Before
    public void before() throws TopiaException {
        tx = db.newPersistenceContext();
        aDAO = getDao(tx, A33.class);
        bDAO = getDao(tx, B33.class);
    }

    @Test
    public void create() throws TopiaException {

        long nbA = aDAO.count();
        long nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(0, nbB);

        A33 a = aDAO.create();
        B33 b = bDAO.create();

        b.addRoleA33(a);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(1, nbA);
        Assert.assertEquals(1, nbB);

        A33 aBis = aDAO.forTopiaIdEquals(a.getTopiaId()).findUnique();
        Assert.assertEquals(a, aBis);
        Assert.assertEquals(1, aBis.sizeRoleB33());
        Assert.assertEquals(b, aBis.getRoleB33().iterator().next());
        Assert.assertEquals(a.getRoleB33(), aBis.getRoleB33());

        B33 bBis = bDAO.forTopiaIdEquals(b.getTopiaId()).findUnique();
        Assert.assertEquals(b, bBis);
        Assert.assertEquals(1, bBis.sizeRoleA33());
        Assert.assertEquals(a, bBis.getRoleA33().iterator().next());
    }

    @Test
    public void delete() throws TopiaException {

        create();

        long nbA = aDAO.count();
        long nbB = bDAO.count();

        Assert.assertEquals(1, nbA);
        Assert.assertEquals(1, nbB);

        A33 a = aDAO.findAll().get(0);
        aDAO.delete(a);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(1, nbB);

        B33 b = bDAO.findAll().get(0);
        bDAO.delete(b);

        tx.commit();

        nbA = aDAO.count();
        nbB = bDAO.count();

        Assert.assertEquals(0, nbA);
        Assert.assertEquals(0, nbB);
    }
}
