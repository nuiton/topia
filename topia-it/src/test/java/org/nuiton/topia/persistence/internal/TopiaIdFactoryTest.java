package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: IT
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonImpl;
import org.nuiton.topia.persistence.TopiaIdFactory;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TopiaIdFactoryTest {

    protected void testTopiaId(TopiaIdFactory topiaIdFactory, String topiaId) {

        Assert.assertTrue("Invalid topiaId: " + topiaId, topiaIdFactory.isTopiaId(topiaId));
        Assert.assertTrue("Invalid topiaId: " + topiaId, topiaId.contains(topiaIdFactory.getClassName(topiaId).getSimpleName() + topiaIdFactory.getSeparator() + topiaIdFactory.getRandomPart(topiaId)));
        Assert.assertEquals("Invalid topiaId: " + topiaId, Person.class, topiaIdFactory.getClassName(topiaId));

    }
    protected void testTopiaIdFactory(TopiaIdFactory topiaIdFactory) {
        String topiaId = topiaIdFactory.newTopiaId(Person.class, new PersonImpl());
        testTopiaId(topiaIdFactory, topiaId);
    }

    @Test
    public void testDefaultTopiaIdFactory() {
        TopiaIdFactory topiaIdFactory = new FullyQualifiedNamePlusUuidTopiaIdFactory();
        testTopiaIdFactory(topiaIdFactory);
    }

    @Test
    public void testLegacyTopiaIdFactory() {
        TopiaIdFactory topiaIdFactory = new LegacyTopiaIdFactory();
        testTopiaIdFactory(topiaIdFactory);
    }

    @Test
    public void testShortTopiaIdFactory() {
        TopiaIdFactory topiaIdFactory = new ShortTopiaIdFactory();
        testTopiaIdFactory(topiaIdFactory);
    }

    @Test
    public void testShortTopiaIdFactoryForGivenTopiaId() {
        TopiaIdFactory topiaIdFactory = new ShortTopiaIdFactory();
        testTopiaId(topiaIdFactory, "Person_4SHxCuN4R_yFZA1lZXaw_g");
    }

    @Test
    public void testShortTopiaIdFactoryIsTopiaId() {
        TopiaIdFactory topiaIdFactory = new ShortTopiaIdFactory();
        Assert.assertFalse(topiaIdFactory.isTopiaId("Person"));
        Assert.assertFalse(topiaIdFactory.isTopiaId("Person_"));
        Assert.assertTrue(topiaIdFactory.isTopiaId("Person_d"));
    }

    @Test
    public void testLegacyTopiaIdFactoryForGivenTopiaId() {
        TopiaIdFactory topiaIdFactory = new LegacyTopiaIdFactory();
        testTopiaId(topiaIdFactory, "org.nuiton.topia.it.legacy.test.entities.Person#1409923331552#0.36362338680380535");
    }

}
