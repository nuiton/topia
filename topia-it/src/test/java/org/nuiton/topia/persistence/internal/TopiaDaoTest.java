package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonTopiaDao;
import org.nuiton.topia.persistence.QueryMissingOrderException;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Test on {@link TopiaDao}.
 *
 * @author chatellier
 */
public class TopiaDaoTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    protected TopiaItLegacyTopiaPersistenceContext context;

    protected PersonTopiaDao dao;

    @Before
    public void setup() throws TopiaException {

        context = db.newPersistenceContext();
        dao = context.getPersonDao();
    }

    /**
     * Test de creer une entité et de verifier qu'elle est
     * présente dans la persistence au sein de la transaction.
     *
     * @throws Exception if any exception while test
     */
    @Test
    public void testCreateAndFindInTransaction() throws Exception {

        // appel 1 find all
        createPerson("toto");
        List<Person> allPerson = dao.findAll();
        Assert.assertEquals(1, allPerson.size());
        context.commit();

        // recherce la personne créée dans la même transaction
        Person person2 = createPerson("titi");
        allPerson = dao.findAll();
        Assert.assertEquals(2, allPerson.size());
        Assert.assertThat(allPerson, CoreMatchers.hasItem(person2));

        context.rollback();

        // meme test après rollback
        Person person3 = createPerson("tata");
        allPerson = dao.findAll();
        Assert.assertEquals(2, allPerson.size());
        Assert.assertThat(allPerson, CoreMatchers.hasItem(person3));

        context.commit();
    }

    @Test // Test to prove issue http://nuiton.org/issues/3001
    public void findAllLazy() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        createPersons(1);

        Iterable<Person> persons = dao.forNameEquals("toto00").findAllLazy();
        Assert.assertTrue(persons.iterator().hasNext());
    }

    @Test // Test to prove issue http://nuiton.org/issues/3336
    public void findAllLazyOnPropertyWithDistinct() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        for (int i = 0; i < 10; i++) {
            String name = "toto" + Strings.padStart(String.valueOf(i % 5), 2, '0');
            createPerson(name);
        }

        context.commit();

        // Error only happens when batchSize is smaller than the number of results
        dao.setBatchSize(3);

        String query =
                "SELECT DISTINCT " + Person.PROPERTY_NAME + " " +
                        dao.newFromClause() + " ORDER BY " + Person.PROPERTY_NAME;
        Iterable<String> names = dao.findAllLazy(query, new HashMap<String, Object>());
        Assert.assertEquals(5, Iterables.size(names));
    }

    @Test // Test to prove issue http://nuiton.org/issues/3336
    public void findAllLazyOnPropertyWithoutDistinct() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        for (int i = 0; i < 10; i++) {
            int number = i % 5;
            String name = null;
            if (number > 1) {
                name = "toto" + Strings.padStart(String.valueOf(number), 2, '0');
            }
            // null, null, toto02, toto03, toto04, null, null, ...
            createPerson(name);
        }

        context.commit();

        Assert.assertEquals(dao.count(), 10);

        // Error only happens when batchSize is smaller than the number of results
        dao.setBatchSize(3);

        String query =
                "SELECT " + Person.PROPERTY_NAME + " " +
                        dao.newFromClause() + " ORDER BY " + Person.PROPERTY_NAME;

        PaginationResult<Person> pager = dao.initPagination(query, new HashMap<String, Object>(), 3);
        Assert.assertEquals(10, pager.getCount());

        Iterable<String> names = dao.findAllLazy(query, new HashMap<String, Object>());
        Assert.assertEquals(10, Iterables.size(names));
    }

    @Test
    public void findAllLazyWithSeveralSelectClauses() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        createPersons(10);

        // Error only happens when batchSize is smaller than the number of results
        dao.setBatchSize(3);

        String query =
                "SELECT " + Person.PROPERTY_NAME + ", " + Person.PROPERTY_FIRSTNAME + " " +
                        dao.newFromClause() + " ORDER BY " + Person.PROPERTY_NAME;
        Iterable<String[]> names = dao.findAllLazy(query, new HashMap<String, Object>());

        Assert.assertEquals(10, Iterables.size(names));
    }

    @Test(expected = IllegalStateException.class)
    public void newPagerWithDistinctWithBrackets() throws TopiaException {

        String query =
                "SELECT DISTINCT (" + Person.PROPERTY_NAME + ") " +
                        dao.newFromClause() + " ORDER BY " + Person.PROPERTY_NAME;
        dao.initPagination(query, new HashMap<String, Object>(), 3);

    }

    @Test
    public void findAllLazyByQuery() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        createPersons(101);

        Map<String, Object> emptyArgs = Maps.newHashMap();

        dao.setBatchSize(100);
        Iterable<Person> allByLazy = dao.findAllLazy(
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id", emptyArgs);

        List<Person> actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        dao.setBatchSize(54);
        allByLazy = dao.findAllLazy(
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id", emptyArgs);

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        dao.setBatchSize(49);
        allByLazy = dao.findAllLazy(
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id", emptyArgs);

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        dao.setBatchSize(101);
        allByLazy = dao.findAllLazy(
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id", emptyArgs);

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());

        dao.setBatchSize(102);
        allByLazy = dao.findAllLazy(
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " ORDER BY id", emptyArgs);

        actual = Lists.newArrayList();

        for (Person person : allByLazy) {
            actual.add(person);
        }
        Assert.assertEquals(dao.count(), actual.size());
    }

    @Test
    public void findAllLazyWithSelect() throws TopiaException {

        Assert.assertEquals(dao.count(), 0);

        createPersons(25);

        Map<String, Object> emptyArgs = Maps.newHashMap();

        dao.setBatchSize(10);

        String hql = "SELECT DISTINCT " + Person.PROPERTY_NAME + " " +
                "FROM " + dao.getTopiaEntityEnum().getImplementationFQN() + " " +
                "ORDER BY " + Person.PROPERTY_NAME;
        Iterable<String> allByLazy = dao.findAllLazy(hql, emptyArgs);

        Assert.assertEquals(25, Iterables.size(allByLazy));
    }

    @Test
    public void iterateOnTopiaDao() throws TopiaException {

        createPersons(1999);

        List<Person> excepted = dao.findAll();

        List<Person> actual = Lists.newArrayList();

        for (Person person : dao) {
            Assert.assertThat(excepted, CoreMatchers.hasItem(person));
            actual.add(person);
        }
        Assert.assertEquals(excepted.size(), actual.size());

        dao.setBatchSize(54);

        actual = Lists.newArrayList();

        for (Person person : dao) {
            Assert.assertThat(excepted, CoreMatchers.hasItem(person));
            actual.add(person);
        }
        Assert.assertEquals(excepted.size(), actual.size());

    }

    @Test
    public void iterateOnEmptyTopiaDAO() throws TopiaException {
        Assert.assertFalse("No person has been created, hasNext should return false", dao.iterator().hasNext());
    }

    protected void createPersons(int number) throws TopiaException {
        for (int i = 0; i < number; i++) {
            String name = "toto" + Strings.padStart(String.valueOf(i), 2, '0');
            createPerson(name);
        }

        context.commit();
    }

    protected Person createPerson(String name) throws TopiaException {
        return dao.create(Person.PROPERTY_NAME, name);
    }

    @Test // Test to avoid NPE during update(...) on non persisted entity (see http://nuiton.org/issues/3153)
    public void findUpdateNonPersistedEntity() throws TopiaException {

        Person person = dao.newInstance();
        person.setName("whatever");

        Assert.assertFalse(person.isPersisted());

        dao.update(person);

        Assert.assertTrue(person.isPersisted());
        Assert.assertTrue(!Strings.isNullOrEmpty(person.getTopiaId()));
    }

    @Test
    public void testFindUsingPagination() throws TopiaException {

        createPersons(17);

        List<Person> excepted = dao.findAll();
        Assert.assertEquals(17, excepted.size());

        {
            PaginationParameter page = PaginationParameter.ALL;
            List<Person> list = dao.find(dao.newFromClause() + " ORDER BY topiaId ", new HashMap<String, Object>(), page);
            Assert.assertEquals(17, list.size());
        }

        {
            PaginationParameter page = PaginationParameter.of(0, -1, Person.PROPERTY_TOPIA_ID, false);
            List<Person> list = dao.find(dao.newFromClause(), new HashMap<String, Object>(), page);
            Assert.assertEquals(17, list.size());
        }

        Set<String> alreadyLoaded = Sets.newHashSet();
        {
            PaginationParameter page = PaginationParameter.of(0, 6, Person.PROPERTY_TOPIA_ID, false);
            List<Person> list = dao.find(dao.newFromClause(), new HashMap<String, Object>(), page);
            Assert.assertEquals(6, list.size());

            // Now make sure that a same entity is not in several pages
            Set<String> newlyLoaded = Sets.newHashSet(Iterables.transform(list, TopiaEntities.getTopiaIdFunction()));
            Assert.assertTrue(Sets.intersection(alreadyLoaded, newlyLoaded).isEmpty());
            alreadyLoaded.addAll(newlyLoaded);
        }

        {
            PaginationParameter page = PaginationParameter.of(1, 6, Person.PROPERTY_TOPIA_ID, false);
            List<Person> list = dao.find(dao.newFromClause(), new HashMap<String, Object>(), page);
            Assert.assertEquals(6, list.size());

            // Now make sure that a same entity is not in several pages
            Set<String> newlyLoaded = Sets.newHashSet(Iterables.transform(list, TopiaEntities.getTopiaIdFunction()));
            Assert.assertTrue(Sets.intersection(alreadyLoaded, newlyLoaded).isEmpty());
            alreadyLoaded.addAll(newlyLoaded);
        }

        {
            PaginationParameter page = PaginationParameter.of(2, 6, Person.PROPERTY_TOPIA_ID, false);
            List<Person> list = dao.find(dao.newFromClause(), new HashMap<String, Object>(), page);
            Assert.assertEquals(5, list.size());

            // Now make sure that a same entity is not in several pages
            Set<String> newlyLoaded = Sets.newHashSet(Iterables.transform(list, TopiaEntities.getTopiaIdFunction()));
            Assert.assertTrue(Sets.intersection(alreadyLoaded, newlyLoaded).isEmpty());
            alreadyLoaded.addAll(newlyLoaded);
        }

        Assert.assertEquals(17, alreadyLoaded.size());

    }

    @Test
    public void testFindUsingPaginationOrderClauses() throws TopiaException {

        createPersons(15);

        List<Person> excepted = dao.findAll();
        Assert.assertEquals(15, excepted.size());

        Set<String> setAsc = Sets.newHashSet();
        Set<String> setDesc = Sets.newHashSet();

        Function<Person, String> getNameFunction = new Function<Person, String>() {
            @Override
            public String apply(Person input) {
                return input.getName();
            }
        };

        {
            PaginationParameter page = PaginationParameter.of(0, 8, Person.PROPERTY_NAME, false);
            List<Person> list = dao.find(dao.newFromClause(), new HashMap<String, Object>(), page);
            Assert.assertEquals(8, list.size());

            Set<String> newlyLoaded = Sets.newHashSet(Iterables.transform(list, getNameFunction));
            setAsc.addAll(newlyLoaded);
        }

        {
            PaginationParameter page = PaginationParameter.of(0, 8, Person.PROPERTY_NAME, true);
            List<Person> list = dao.find(dao.newFromClause(), new HashMap<String, Object>(), page);
            Assert.assertEquals(8, list.size());

            Set<String> newlyLoaded = Sets.newHashSet(Iterables.transform(list, getNameFunction));
            setDesc.addAll(newlyLoaded);
        }

        Sets.SetView<String> intersection = Sets.intersection(setAsc, setDesc);

        // Sorting a list of 15 elements asc and desc by 8-elements pages, intersection will be the middle one (toto7)
        //   <---------ASC-page-0----------><-------ASC-page-1---------->
        //    00  01  02  03  04  05  06 |7| 8  9  10  11  12  13  14
        // <---------DESC-page-1--------><--------DESC-page-0-------->

        Assert.assertEquals(1, intersection.size());
        Assert.assertTrue(intersection.contains("toto07"));
    }

    @Test
    public void testFindPage() throws TopiaException {

        createPersons(19);

        String hql = dao.newFromClause("p") + " WHERE p." + Person.PROPERTY_NAME + " LIKE 'toto%' ORDER BY id ";
        PaginationResult<Person> page = dao.forHql(hql).findPage(PaginationParameter.of(0, 5));

        Assert.assertEquals(5, page.getElements().size());
        Assert.assertEquals(0, page.getCurrentPage().getStartIndex());
        Assert.assertEquals(4, page.getCurrentPage().getEndIndex());
        Assert.assertEquals(19, page.getCount());
        Assert.assertEquals(4, page.getPageCount());

        while (page.hasNextPage()) {
            PaginationParameter nextPage = page.getNextPage();
            page = dao.forHql(hql).findPage(nextPage);

            if (page.hasNextPage()) {
                Assert.assertEquals(5, page.getElements().size());
            } else {
                // last page only has 4 elements
                Assert.assertEquals(4, page.getElements().size());
            }
            Assert.assertEquals(19, page.getCount());
            Assert.assertEquals(4, page.getPageCount());
        }
    }

    @Test
    public void testFindPageWithOrder() throws TopiaException {

        createPersons(19);

        String hql = dao.newFromClause("p") + " WHERE p." + Person.PROPERTY_NAME + " LIKE 'toto%' ";
        PaginationResult<Person> page = dao.forHql(hql).findPage(PaginationParameter.of(0, 7, Person.PROPERTY_NAME, true));

        Assert.assertEquals(7, page.getElements().size());
        Assert.assertEquals(0, page.getCurrentPage().getStartIndex());
        Assert.assertEquals(6, page.getCurrentPage().getEndIndex());
        Assert.assertEquals(19, page.getCount());
        Assert.assertEquals(3, page.getPageCount());

        Assert.assertEquals("toto18", page.getElements().iterator().next().getName());
        Assert.assertEquals("toto12", Iterables.getLast(page.getElements()).getName());

        PaginationParameter lastPage = page.getLastPage();
        page = dao.forHql(hql).findPage(lastPage);

        Assert.assertEquals(5, page.getElements().size());
        Assert.assertEquals(14, page.getCurrentPage().getStartIndex());
        Assert.assertEquals(20, page.getCurrentPage().getEndIndex());
        Assert.assertEquals(19, page.getCount());
        Assert.assertEquals(3, page.getPageCount());
        Assert.assertFalse(page.hasNextPage());

        Assert.assertEquals("toto04", page.getElements().iterator().next().getName());
        Assert.assertEquals("toto00", Iterables.getLast(page.getElements()).getName());

    }

    @Test
    // Test for http://nuiton.org/issues/3272
    public void testFindPageWithOrderOnQuery() throws TopiaException {

        createPersons(6);

        List<String> names = Lists.newArrayList("toto01", "toto02", "toto05");

        { // order by is on pagination parameter
            PaginationResult<Person> page = dao.forNameIn(names).findPage(PaginationParameter.of(0, 2, Person.PROPERTY_NAME, false));

            Assert.assertEquals(2, page.getElements().size());
            Assert.assertEquals(0, page.getCurrentPage().getStartIndex());
            Assert.assertEquals(1, page.getCurrentPage().getEndIndex());
            Assert.assertEquals(3, page.getCount());
            Assert.assertEquals(2, page.getPageCount());
            Assert.assertEquals("toto01", page.getElements().iterator().next().getName());
            Assert.assertEquals("toto02", Iterables.getLast(page.getElements()).getName());
        }


        { // order by is on pagination parameter
            PaginationResult<Person> page = dao.forNameIn(names).setOrderByArguments(Person.PROPERTY_NAME).findPage(PaginationParameter.of(0, 2));

            Assert.assertEquals(2, page.getElements().size());
            Assert.assertEquals(0, page.getCurrentPage().getStartIndex());
            Assert.assertEquals(1, page.getCurrentPage().getEndIndex());
            Assert.assertEquals(3, page.getCount());
            Assert.assertEquals(2, page.getPageCount());
            Assert.assertEquals("toto01", page.getElements().iterator().next().getName());
            Assert.assertEquals("toto02", Iterables.getLast(page.getElements()).getName());
        }


    }

    @Test
    public void testFindPageNoData() throws TopiaException {

        String hql = dao.newFromClause("p");
        PaginationResult<Person> page = dao.forHql(hql).findPage(PaginationParameter.of(0, 5, Person.PROPERTY_NAME, false));

        Assert.assertEquals(0, page.getElements().size());
        Assert.assertEquals(0, page.getCount());
        Assert.assertEquals(0, page.getPageCount());
        Assert.assertFalse(page.hasNextPage());

    }


    @Test (expected = QueryMissingOrderException.class)
    public void testFindPageNoOrder() throws TopiaException {

        String hql = dao.newFromClause("p");
        PaginationParameter page = PaginationParameter.of(0, 5);
        dao.forHql(hql).findPage(page);

    }

}