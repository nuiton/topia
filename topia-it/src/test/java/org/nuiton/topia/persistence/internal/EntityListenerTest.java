package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * ToPIA :: IT
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyDatabase;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaPersistenceContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonImpl;
import org.nuiton.topia.it.legacy.test.entities.PersonTopiaDao;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.event.TopiaEntityEvent;
import org.nuiton.topia.persistence.event.TopiaEntityListener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class EntityListenerTest {

    @Rule
    public final TopiaItLegacyDatabase db = new TopiaItLegacyDatabase();

    protected TopiaItLegacyTopiaPersistenceContext context;

    protected PersonTopiaDao dao;

    @Before
    public void setup() throws TopiaException {

        context = db.newPersistenceContext();
        dao = context.getPersonDao();
    }

    @Test
    public void testEntityUpdatedOnPersistenceContext() throws Exception {
        final Set<Boolean> handler = Sets.newHashSet(false);
        context.getTopiaFiresSupport().addTopiaEntityListener(new TopiaEntityListener() {
            @Override
            public void create(TopiaEntityEvent event) {

            }

            @Override
            public void load(TopiaEntityEvent event) {

            }

            @Override
            public void update(TopiaEntityEvent event) {
                handler.clear();
                handler.add(true);
            }

            @Override
            public void delete(TopiaEntityEvent event) {

            }
        });

        Person person = dao.create(Person.PROPERTY_NAME, "azerty");

        Assert.assertFalse(handler.iterator().next());

        person.setName("pouette");
        dao.update(person);
        context.commit();

        Assert.assertTrue(handler.iterator().next());
    }

    @Test
    public void testEntityUpdatedOnApplicationContext() throws Exception {
        final Set<Boolean> handler = Sets.newHashSet(false);
        db.getApplicationContext().addTopiaEntityListener(new TopiaEntityListener() {
            @Override
            public void create(TopiaEntityEvent event) { }

            @Override
            public void load(TopiaEntityEvent event) { }

            @Override
            public void update(TopiaEntityEvent event) {
                handler.clear();
                handler.add(true);
            }

            @Override
            public void delete(TopiaEntityEvent event) { }
        });

        Person person = dao.create(Person.PROPERTY_NAME, "azerty");

        Assert.assertFalse(handler.iterator().next());

        person.setName("pouette");
        dao.update(person);
        context.commit();

        Assert.assertTrue(handler.iterator().next());

    }

    @Test
    public void testEntityModifiedOnFieldFromDao() throws Exception {
        final Set<PropertyChangeEvent> handler = Sets.newHashSet();
        Person person = dao.create(Person.PROPERTY_NAME, "azerty");

        person.addPostWriteListener(Person.PROPERTY_FIRSTNAME, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                handler.add(evt);
            }
        });

        Assert.assertFalse(handler.iterator().hasNext());

        person.setFirstname("totoFromDao");

        Assert.assertTrue(handler.iterator().hasNext());
        PropertyChangeEvent event = handler.iterator().next();

        Assert.assertNull(event.getOldValue());
        Assert.assertEquals("totoFromDao", event.getNewValue());
        Assert.assertEquals(person, event.getSource());

    }

    @Test
    public void testEntityModifiedOnFieldOnManuallyCreatedEntity() throws Exception {
        final Set<PropertyChangeEvent> handler = Sets.newHashSet();
        Person person = new PersonImpl();

        person.addPostWriteListener(Person.PROPERTY_FIRSTNAME, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                handler.add(evt);
            }
        });

        Assert.assertFalse(handler.iterator().hasNext());

        person.setFirstname("totoCreatedManually");

        Assert.assertTrue(handler.iterator().hasNext());
        PropertyChangeEvent event = handler.iterator().next();

        Assert.assertNull(event.getOldValue());
        Assert.assertEquals("totoCreatedManually", event.getNewValue());
        Assert.assertEquals(person, event.getSource());

    }

}
