package org.nuiton.topia.junit;

/*
 * #%L
 * ToPIA :: JUnit
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;

/**
 * Put this class as a Rule in test to obtain a new isolated db for each test.
 *
 * Here is a simple example of usage :
 * <pre>
 * public class MyTest {
 *
 *   \@Rule
 *   public final TopiaDatabase db = new TopiaDatabase();
 *
 *   \@Test
 *   public void testMethod() throws TopiaException {
 *
 *       TopiaContext tx = db.beginTransaction();
 *       ...
 * }
 * </pre>
 * The db created will be unique for each test method (and for each build also).
 *
 * You don't need to close any transaction, it will be done for you and the end
 * of each method test.
 *
 * Created on 11/22/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public abstract class AbstractDatabaseResource<PersistenceContext extends TopiaPersistenceContext, ApplicationContext extends AbstractTopiaApplicationContext<PersistenceContext>> extends TestWatcher {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractDatabaseResource.class);

    protected ApplicationContext applicationContext;
    protected TopiaConfiguration topiaConfiguration;

    protected abstract ApplicationContext createApplicationContext(TopiaConfiguration topiaConfiguration);

    @Override
    protected void starting(Description description) {
        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        topiaConfiguration =
                topiaConfigurationBuilder.forTest(
                        description.getTestClass(),
                        description.getMethodName());
        applicationContext = createApplicationContext(topiaConfiguration);
    }

    @Override
    public void finished(Description description) {
        if (applicationContext != null && !applicationContext.isClosed()) {
            applicationContext.close();
        }
        applicationContext = null;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public PersistenceContext newPersistenceContext() {
        return applicationContext.newPersistenceContext();
    }

    public TopiaConfiguration getTopiaConfiguration() {
        return topiaConfiguration;
    }
}
