package org.nuiton.topia.replication;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonImpl;
import org.nuiton.topia.it.legacy.test.entities.Pet;
import org.nuiton.topia.it.legacy.test.entities.PetImpl;
import org.nuiton.topia.it.legacy.test.entities.Race;
import org.nuiton.topia.it.legacy.test.entities.RaceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.util.TopiaEntityIdsMap;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.operation.DettachAssociation;
import org.nuiton.topia.replication.operation.Duplicate;
import org.nuiton.topia.replication.operation.FakeOperation;
import org.nuiton.topia.replication.operation.UncreatableOperation;
import org.nuiton.topia.replication.operation.UnregistredOperation;

/**
 * TopiaReplicationServiceImplTest on model TopiaTest
 *
 * Created: 07 jun. 09 17:14:22
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class TopiaReplicationServiceImplTest extends AbstractTopiaReplicationServiceTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(TopiaReplicationServiceImplTest.class);

    protected static final TopiaEntityEnum[] contracts = {TopiaItLegacyEntityEnum.Person, TopiaItLegacyEntityEnum.Pet, TopiaItLegacyEntityEnum.Race};

    protected static final String entitiesList = PersonImpl.class.getName() + "," + PetImpl.class.getName() + "," + RaceImpl.class.getName();

    protected static Person person, person2;

    protected static Pet pet, pet2, pet3;

    protected static Race race, race2, race3;

    @AfterClass
    public static void after() throws Exception {
        AbstractTopiaReplicationServiceTest.after();
    }

    @Before
    @Override
    public void setUp() throws Exception {

        super.setUp();

        person = update(person);
        person2 = update(person2);
        pet = update(pet);
        pet2 = update(pet2);
        race = update(race);
        race2 = update(race2);
        race3 = update(race3);
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (destinationContext != null && !destinationContext.isClosed()) {
            destinationContext.close();
        }
    }

    @Test
    @Override
    public void testDetectTypes() throws Exception {

        detectTypes(race, Race.class);
        detectTypes(pet, Pet.class, Person.class, Race.class);
        detectTypes(person, Pet.class, Person.class, Race.class);

        detectTypes(pet2, Pet.class);
        detectTypes(person2, Person.class);
        detectTypes(race2, Race.class);

        detectTypes(race3, Race.class);
        detectTypes(pet3, Pet.class, Race.class);
    }

    @Test
    @Override
    public void testGetOperation() throws Exception {

        getOperation(UnregistredOperation.class, false);
        getOperation(UncreatableOperation.class, true);
        getOperation(FakeOperation.class, true);
        getOperation(Duplicate.class, true);
        getOperation(DettachAssociation.class, true);
    }

    @Test
    @Override
    public void testDetectAssociations() throws Exception {

        detectAssociations(person, TopiaItLegacyEntityEnum.Person, Person.PROPERTY_PET);
        detectAssociations(race);
        detectAssociations(pet);

        detectAssociations(person2);
        detectAssociations(race2);
        detectAssociations(pet2);
    }

    @Test
    @Override
    public void testDetectDirectDependencies() throws Exception {

        detectDirectDependencies(person);
        detectDirectDependencies(race);
        detectDirectDependencies(pet, TopiaItLegacyEntityEnum.Pet, Pet.PROPERTY_PERSON, TopiaItLegacyEntityEnum.Pet, Pet.PROPERTY_RACE);

        detectDirectDependencies(person2);
        detectDirectDependencies(race2);
        detectDirectDependencies(pet2);
    }

    @Test
    @Override
    public void testDetectShell() throws Exception {

        detectShell(person, TopiaItLegacyEntityEnum.Pet, TopiaItLegacyEntityEnum.Race);
        detectShell(race);
        detectShell(pet, TopiaItLegacyEntityEnum.Person, TopiaItLegacyEntityEnum.Race);
        detectShell(person2);
        detectShell(race2);
        detectShell(pet2);
    }

    @Test
    @Override
    public void testDetectDependencies() throws Exception {

        detectDependencies(person, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Race}, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Person}, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Pet});
        detectDependencies(race, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Race});
        detectDependencies(pet, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Race}, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Person}, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Pet});

        detectDependencies(person2, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Person});
        detectDependencies(race2, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Race});
        detectDependencies(pet2, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Pet});
    }

    @Test
    @Override
    public void testDetectObjectsToDettach() throws Exception {

        detectObjectsToDettach(person, TopiaItLegacyEntityEnum.Person, new String[]{Person.PROPERTY_PET});
        detectObjectsToDettach(race);
        detectObjectsToDettach(pet, TopiaItLegacyEntityEnum.Person, new String[]{Person.PROPERTY_PET});

        detectObjectsToDettach(person2);
        detectObjectsToDettach(race2);
        detectObjectsToDettach(pet2);

        detectObjectsToDettach(race3);
        detectObjectsToDettach(pet3);
    }

    @Test
    @Override
    public void testDetectOperations() throws Exception {

        //TODO Make some real test on detected operations...

        detectOperations(person);
        detectOperations(pet);
        detectOperations(race);

        detectOperations(person2);
        detectOperations(pet2);
        detectOperations(race2);

        detectOperations(race3);
        detectOperations(pet3);
    }

    @Test
    @Override
    public void testDoReplicate() throws Exception {

        doReplicate(TopiaItLegacyEntityEnum.Person, person);
        doReplicate(TopiaItLegacyEntityEnum.Person, person2);
        doReplicate(TopiaItLegacyEntityEnum.Person, person, person2);

        doReplicate(TopiaItLegacyEntityEnum.Pet, pet);
        doReplicate(TopiaItLegacyEntityEnum.Pet, pet2);
        doReplicate(TopiaItLegacyEntityEnum.Pet, pet, pet2, pet3);
        doReplicate(TopiaItLegacyEntityEnum.Pet, person2, pet3);

        doReplicate(TopiaItLegacyEntityEnum.Race, race);
        doReplicate(TopiaItLegacyEntityEnum.Race, race2);
        doReplicate(TopiaItLegacyEntityEnum.Race, race, race2);

    }

    /**
     * Cette methode montre pourquoi la simple replication ne peut pas
     * fonctionner :)
     *
     * Le replicateur ne deplique pas dans le bon ordre et on a donc des
     * violations de clef etrangeres...
     *
     * @throws Exception pour toute erreur
     */
    @Test(expected = TopiaException.class)
    public void testSimpleReplicateFailed() throws Exception {

        TopiaApplicationContext dstRootCtxt = createDb2("testSimpleReplicateFailed");

        //model = service.prepare(contracts, pet.getTopiaId());

        TopiaPersistenceContext srcCtxt = sourceContext.newPersistenceContext();
        TopiaPersistenceContext destinationPC = dstRootCtxt.newPersistenceContext();

        try {

            srcCtxt.replicateEntity(destinationPC, pet);

            destinationPC.commit();

        } finally {
            srcCtxt.rollback();
            srcCtxt.close();
            destinationPC.close();
        }
    }

    /**
     * Cette methode montre comment manuellement on peut effectuer la
     * replication (en detachant les dependances qui forment des cycles)
     *
     * La methode utilisee ici peut ne pas fonctionner : si une clef metier est
     * posee sur une dependance alors cela ne fonctionne pas.
     */
    @Test
    public void testSimpleReplicateNotSure() {

        TopiaApplicationContext dstRootCtxt = createDb2("testSimpleReplicateNotSure");

        TopiaPersistenceContext srcCtxt = sourcePC;

        try (TopiaPersistenceContext destinationPC = dstRootCtxt.newPersistenceContext()) {
            srcCtxt.replicateEntity(destinationPC, race);

            // on detache les entites qui posent probleme
            pet.setPerson(null);
            pet.setPerson(null);

            srcCtxt.replicateEntity(destinationPC, pet);
            srcCtxt.rollback();

            srcCtxt.replicateEntity(destinationPC, person);

            destinationPC.commit();
            Pet petCopy = destinationPC.findByTopiaId(pet.getTopiaId());
            Race raceCopy = destinationPC.findByTopiaId(race.getTopiaId());
            Person personCopy = destinationPC.findByTopiaId(person.getTopiaId());
            petCopy.setPerson(personCopy);
            petCopy.setRace(raceCopy);

            destinationPC.commit();

            srcCtxt.rollback();
            person = update(person);

            assertEntityEquals(person, personCopy, null);
        } finally {
            srcCtxt.rollback();
        }
    }

    /**
     * Cette methode montre comment manuellement on peut effectuer la
     * replication (en dettachant les associations qui forment des cycles)
     *
     * La methode utilisee ici fonctionne mieux que la precedante : il parrait
     * dificille de pose une une clef metier sur une association :).
     *
     * On remarque que l'on dettache l'assocation qui forme un cycle et que l'on
     * est pas obligee de la reattachee car elle est bi-directionnelle.
     *
     * On doit optimiser l'algorithme dans la methode {@link
     * ReplicationModel#adjustOperations(TopiaEntityIdsMap)}.
     *
     * @throws Exception pour toute erreur
     */
    @Test
    public void testSimpleReplicateSure() throws Exception {

        TopiaApplicationContext dstRootCtxt = createDb2("testSimpleReplicateSure");

        //model = service.prepare(contracts, pet.getTopiaId());

        TopiaPersistenceContext srcCtxt = sourcePC;
        TopiaPersistenceContext destinationPC = dstRootCtxt.newPersistenceContext();

        try {

            srcCtxt.replicateEntity(destinationPC, race);
            // on dettache l'association qui pose probleme
            person.setPet(null);
            srcCtxt.replicateEntity(destinationPC, person);

            srcCtxt.replicateEntity(destinationPC, pet);
            srcCtxt.rollback();
            destinationPC.commit();

            //((Person) destinationContext.findByTopiaId(person.getTopiaId())).addPet(((Pet) destinationContext.findByTopiaId(pet.getTopiaId())));
            //destinationContext.commit();

            srcCtxt.rollback();

            srcCtxt.close();
            destinationPC.close();

            sourcePC = sourceContext.newPersistenceContext();
            destinationPC = dstRootCtxt.newPersistenceContext();

            person = update(person);

            assertEntityEquals(person, destinationPC.findByTopiaId(person.getTopiaId()), null);

        } finally {
            destinationPC.close();
        }
    }

    @Override
    protected TopiaApplicationContext createDb(String name) throws Exception {

        TopiaItLegacyTopiaApplicationContext sourceContext = newTopiaItLegacyTopiaApplicationContext(name);

        TopiaPersistenceContext tx = sourceContext.newPersistenceContext();

        person = tx.getDao(Person.class).create(Person.PROPERTY_NAME, "pudding master");
        race = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race I");
        pet = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding", Pet.PROPERTY_PERSON, person, Pet.PROPERTY_RACE, race);

        person2 = tx.getDao(Person.class).create(Person.PROPERTY_NAME, "pudding II master");
        pet2 = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding II");
        race2 = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race II");

        race3 = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race III");
        pet3 = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding III", Pet.PROPERTY_RACE, race3);

        tx.commit();
        tx.close();
        return sourceContext;
    }

    @Override
    protected TopiaEntityEnum[] getContracts() {
        return contracts;
    }

    @Override
    protected Log getLog() {
        return log;
    }

}


