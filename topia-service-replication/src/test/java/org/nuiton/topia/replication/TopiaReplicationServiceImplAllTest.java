package org.nuiton.topia.replication;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonImpl;
import org.nuiton.topia.it.legacy.test.entities.Pet;
import org.nuiton.topia.it.legacy.test.entities.PetImpl;
import org.nuiton.topia.it.legacy.test.entities.Race;
import org.nuiton.topia.it.legacy.test.entities.RaceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

/**
 * TopiaReplicationServiceImplTest on model TopiaTest
 *
 * Created: 07 jun. 09 17:14:22
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class TopiaReplicationServiceImplAllTest extends AbstractTopiaReplicationServiceTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(TopiaReplicationServiceImplTest.class);

    protected static final TopiaEntityEnum[] contracts = {
            TopiaItLegacyEntityEnum.Person,
            TopiaItLegacyEntityEnum.Pet,
            TopiaItLegacyEntityEnum.Race
    };

    protected static final String entitiesList =
            PersonImpl.class.getName() + "," +
            PetImpl.class.getName() + "," +
            RaceImpl.class.getName();

    protected static Person person, person2;

    protected static Pet pet, pet2, pet3;

    protected static Race race, race2, race3;

    @AfterClass
    public static void after() throws Exception {
        AbstractTopiaReplicationServiceTest.after();
    }

    @Before
    @Override
    public void setUp() throws Exception {

        super.setUp();

        person = update(person);
        person2 = update(person2);
        pet = update(pet);
        pet2 = update(pet2);
        race = update(race);
        race2 = update(race2);
        race3 = update(race3);
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (destinationContext != null && !destinationContext.isClosed()) {
            destinationContext.close();
        }
    }

//    @Ignore

    @Test
    @Override
    public void testDetectTypes() throws Exception {

        detectTypes(race, Race.class);
        detectTypes(pet, Pet.class, Person.class, Race.class);
        detectTypes(person, Pet.class, Person.class, Race.class);

        detectTypes(pet2, Pet.class);
        detectTypes(person2, Person.class);
        detectTypes(race2, Race.class);

        detectTypes(race3, Race.class);
        detectTypes(pet3, Pet.class, Race.class);
    }

//    @Ignore

    @Test
    @Override
    public void testGetOperation() throws Exception {
    }

//    @Ignore

    @Test
    @Override
    public void testDetectAssociations() throws Exception {

        detectAssociations(person, TopiaItLegacyEntityEnum.Person, Person.PROPERTY_PET);
        detectAssociations(race);
        detectAssociations(pet);

        detectAssociations(person2);
        detectAssociations(race2);
        detectAssociations(pet2);

    }

//    @Ignore

    @Test
    @Override
    public void testDetectDirectDependencies() throws Exception {

        detectDirectDependencies(person);
        detectDirectDependencies(race);
        detectDirectDependencies(pet, TopiaItLegacyEntityEnum.Pet, Pet.PROPERTY_PERSON, TopiaItLegacyEntityEnum.Pet, Pet.PROPERTY_RACE);

        detectDirectDependencies(person2);
        detectDirectDependencies(race2);
        detectDirectDependencies(pet2);
    }

//    @Ignore

    @Test
    @Override
    public void testDetectShell() throws Exception {

        detectShell(person, TopiaItLegacyEntityEnum.Pet, TopiaItLegacyEntityEnum.Race);
        detectShell(race);
        detectShell(pet, TopiaItLegacyEntityEnum.Person, TopiaItLegacyEntityEnum.Race);
        detectShell(person2, TopiaItLegacyEntityEnum.Pet, TopiaItLegacyEntityEnum.Race);
        detectShell(race2);
        detectShell(pet2, TopiaItLegacyEntityEnum.Person, TopiaItLegacyEntityEnum.Race);
    }

//    @Ignore

    @Test
    @Override
    public void testDetectDependencies() throws Exception {

        detectDependencies(null,
                           new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Race}, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Person}, new TopiaItLegacyEntityEnum[]{TopiaItLegacyEntityEnum.Pet});
    }

//    @Ignore

    @Test
    @Override
    public void testDetectObjectsToDettach() throws Exception {

        detectObjectsToDettach(null, TopiaItLegacyEntityEnum.Person, new String[]{Person.PROPERTY_PET});
    }

//    @Ignore

    @Test
    @Override
    public void testDetectOperations() throws Exception {

        detectOperations(null);
    }

//    @Ignore

    @Test
    @Override
    public void testDoReplicate() throws Exception {

        doReplicateAll();

    }

    @Override
    protected TopiaApplicationContext createDb(String name) throws Exception {

        TopiaItLegacyTopiaApplicationContext sourceContext = newTopiaItLegacyTopiaApplicationContext(name);

        TopiaPersistenceContext tx = sourceContext.newPersistenceContext();

        person = tx.getDao(Person.class).create(Person.PROPERTY_NAME, "pudding master");
        race = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race I");
        pet = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding", Pet.PROPERTY_PERSON, person, Pet.PROPERTY_RACE, race);

        person2 = tx.getDao(Person.class).create(Person.PROPERTY_NAME, "pudding II master");
        pet2 = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding II");
        race2 = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race II");

        race3 = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race III");
        pet3 = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding III", Pet.PROPERTY_RACE, race3);

        tx.commit();
        tx.close();
        return sourceContext;
    }

    @Override
    protected TopiaEntityEnum[] getContracts() {
        return contracts;
    }

    @Override
    protected Log getLog() {
        return log;
    }

    @Override
    protected void createModel(TopiaEntity entity) throws TopiaException {
        model = getModelBuilder().createModelForAll(getContracts());
    }

    @Override
    protected void prepareModel(String... ids) throws TopiaException {
        model = service.prepareForAll(getContracts());
    }

}


