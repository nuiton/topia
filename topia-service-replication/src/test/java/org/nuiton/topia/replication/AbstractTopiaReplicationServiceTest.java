package org.nuiton.topia.replication;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import org.apache.commons.logging.Log;
import org.junit.Assert;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.persistence.util.EntityOperatorStore;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.model.ReplicationNode;
import org.nuiton.topia.replication.model.ReplicationOperationDef;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * TopiaReplicationServiceImplTest.
 *
 * Created: 07 jun. 09 17:14:22
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public abstract class AbstractTopiaReplicationServiceTest extends Assert {

    protected static TopiaApplicationContext sourceContext;

    protected static TopiaPersistenceContext sourcePC;

    protected TopiaApplicationContext destinationContext;

    protected TopiaReplicationService service;

    protected ReplicationModel model;

    protected static boolean init;

    private static  Long testsTimeStamp;

    private static  File testsBasedir;

    private static final String TEST_BASEDIR = "target%1$ssurefire-tests%1$s%2$td_%2$tm_%2$tY%1$s%2$tH_%2$tM_%2$tS";

    protected TopiaItLegacyTopiaApplicationContext newTopiaItLegacyTopiaApplicationContext(String name) {
        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        TopiaConfiguration topiaConfiguration = topiaConfigurationBuilder.forTest(getClass(), name);
        BeanTopiaConfiguration beanTopiaConfiguration = topiaConfigurationBuilder.copyOf(topiaConfiguration);
        beanTopiaConfiguration.addDeclaredService("replication", TopiaReplicationServiceImpl.class, Collections.<String, String>emptyMap());
        return new TopiaItLegacyTopiaApplicationContext(beanTopiaConfiguration);
    }

    protected static final Function<Properties,TopiaApplicationContext> CREATE_TOPIA_TEST_APPLICATION_CONTEXT = new Function<Properties, TopiaApplicationContext>() {
        @Override
        public TopiaApplicationContext apply(Properties input) {
            return new TopiaItLegacyTopiaApplicationContext(input);
        }
    };

    public static void after() throws Exception {
        if (sourceContext != null && !sourceContext.isClosed()) {
            sourceContext.close();
        }
        init = false;
    }

    public void setUp() throws Exception {

        if (!init) {

            try {
                sourceContext = createDb("source");
            } catch (Exception e) {
                getLog().error("could not create db source.", e);
                throw e;
            }
            init = true;
        }

        sourcePC = sourceContext.newPersistenceContext();

        service = sourceContext.getServices(TopiaReplicationService.class).get("replication");
    }

    public void tearDown() throws Exception {
        if (sourcePC != null) {
            sourcePC.rollback();
            sourcePC.close();
            sourcePC = null;
        }
        service = null;
    }


    protected TopiaReplicationModelBuilder getModelBuilder() {
        return service.getModelBuilder();
    }

    protected TopiaApplicationContext createDb2(String name) {
        TopiaItLegacyTopiaApplicationContext db2 = newTopiaItLegacyTopiaApplicationContext(name);
        return db2;
    }

    protected abstract TopiaApplicationContext createDb(String name) throws Exception;

    protected TopiaApplicationContext createReplicateDb(Object contract) throws Exception {
        TopiaApplicationContext rootCtxt = createDb2(contract.toString() + dbCounter++);
        return rootCtxt;
    }

    protected abstract TopiaEntityEnum[] getContracts();

    protected abstract Log getLog();

    protected <E extends TopiaEntity> E update(E e) throws TopiaException {
        return (E) sourcePC.findByTopiaId(e.getTopiaId());
    }

    /**
     * Test of detectTypes method, of class ReplicationServiceImplementor.
     *
     * @throws Exception if any error
     */
    public void testDetectTypes() throws Exception {
    }

    /**
     * Test of getOperation method, of class ReplicationServiceImplementor.
     *
     * @throws Exception if any error
     */
    public void testGetOperation() throws Exception {
    }

    /**
     * Test of detectAssociations method, of class ReplicationModel.
     *
     * @throws Exception if any error
     */
    public void testDetectAssociations() throws Exception {
    }

    /**
     * Test of detectDirectDependencies method, of class ReplicationModel.
     *
     * @throws Exception if any error
     */
    public void testDetectDirectDependencies() throws Exception {
    }

    /**
     * Test of detectShell method, of class ReplicationModel.
     *
     * @throws Exception if any error
     */
    public void testDetectShell() throws Exception {
    }

    /**
     * Test of detectDependencies method, of class ReplicationModel.
     *
     * @throws Exception if any error
     */
    public void testDetectDependencies() throws Exception {
    }

    /**
     * Test of detectObjectsToDettach method, of class ReplicationModel.
     *
     * @throws Exception if any error
     */
    public void testDetectObjectsToDettach() throws Exception {
    }

    /**
     * Test of detectOperations method, of class ReplicationModel.
     *
     * @throws Exception if any error
     */
    public void testDetectOperations() throws Exception {
    }

    /**
     * Test of doReplicate method, of class ReplicationService.
     *
     * @throws Exception if any error
     */
    public void testDoReplicate() throws Exception {
    }

    protected void detectTypes(TopiaEntity entity, Object... expectedCouple) throws TopiaException {

        Set<?> detectTypes;

        detectTypes = service.getModelBuilder().detectTypes(sourceContext, getContracts(), entity.getTopiaId());
        assertEquals("expected types : " +
                     Arrays.toString(expectedCouple) +
                     " but was " + detectTypes,
                     expectedCouple.length, detectTypes.size());
        for (Object o : expectedCouple) {
            assertTrue(detectTypes.contains(o));
        }
    }

    protected void getOperation(Class<? extends TopiaReplicationOperation> operationClass, boolean shouldExist) throws TopiaException {
        TopiaReplicationOperation operation = getModelBuilder().getOperationProvider().getOperation(operationClass);
        assertEquals(shouldExist, operation != null);
    }

    protected void detectAssociations(TopiaEntity entity,
                                      Object... expectedCouple)
            throws TopiaException {

        createModel(entity);
        model.detectAssociations();

        assertEquals(0, expectedCouple.length % 2);

        for (int i = 0, j = expectedCouple.length / 2; i < j; i++) {
            TopiaEntityEnum src = (TopiaEntityEnum) expectedCouple[2 * i];
            String name = (String) expectedCouple[2 * i + 1];
            ReplicationNode nodeSrc = model.getNode(src);
            assertNotNull("association " + name + " not found", nodeSrc);
            assertTrue(nodeSrc.hasAssociation());
            assertTrue(nodeSrc.getAssociations().containsKey(name));
        }
    }

    protected void detectDirectDependencies(TopiaEntity entity,
                                            Object... expectedCouple)
            throws TopiaException {

        createModel(entity);
        model.detectDirectDependencies();

        assertEquals(0, expectedCouple.length % 2);

        for (int i = 0, j = expectedCouple.length / 2; i < j; i++) {
            TopiaEntityEnum src = (TopiaEntityEnum) expectedCouple[2 * i];
            String name = (String) expectedCouple[2 * i + 1];
            ReplicationNode nodeSrc = model.getNode(src);
            assertTrue(nodeSrc + " should have dependency but was not!", nodeSrc.hasDependency());
            assertTrue(nodeSrc + " should contain dependency " + name + "but was not! (" + nodeSrc.getDependencies() + ")", nodeSrc.getDependencies().containsKey(name));
        }
    }

    protected void detectShell(TopiaEntity entity,
                               TopiaEntityEnum... expected) throws
            TopiaException {
        Set<ReplicationNode> shell;

        createModel(entity);
        model.detectAssociations();
        model.detectDirectDependencies();
        model.detectShell();

        TopiaEntityEnum c = TopiaEntityHelper.getEntityEnum(
                entity.getClass(), getContracts());
        assertNotNull(c);
        shell = model.getNode(c).getShell();
        assertEquals(
                "expected shell : " + Arrays.toString(expected) + ", but was " +
                shell, expected.length, shell.size());

        for (int i = 0, j = expected.length; i < j; i++) {
            TopiaEntityEnum type = expected[i];
            ReplicationNode node = model.getNode(type);
            assertTrue(shell.contains(node));
            assertEquals(type, node.getContract());
        }
    }

    protected void detectDependencies(
            TopiaEntity entity,
            TopiaEntityEnum[]... expected) throws TopiaException {

        createModel(entity);
        model.detectAssociations();
        model.detectDirectDependencies();
        model.detectShell();
        model.detectDependencies();
        List<ReplicationNode> dependencies = model.getOrder();

        int i = 0;
        for (ReplicationNode level : dependencies) {
            getLog().info("level " + level + " = " + level);
        }

//        assertEquals("expected  " + expected.length + " levels but had " + dependencies.size(), expected.length, dependencies.size());
//
//        Iterator<List<ReplicationNode>> order = dependencies.iterator();
//        if (entity != null) {
//            getLog().info("for " + entity.getTopiaId());
//        }
//        int index = 0;
//        for (TopiaEntityEnum[] expectedLevel : expected) {
//
//            List<ReplicationNode> next = order.next();
//            getLog().info("level " + (index++) + " : " + next);
//            for (TopiaEntityEnum ee : expectedLevel) {
//                ReplicationNode expectedNode = model.getNode(ee);
//
//                assertTrue("should have contains node " + expectedNode, next.contains(expectedNode));
//            }
//
//        }
    }

    protected void detectObjectsToDettach(TopiaEntity entity, Object... expected) throws TopiaException {

        assertEquals(0, expected.length % 2);

        createModel(entity);
        model.detectAssociations();
        model.detectDirectDependencies();
        model.detectShell();
        model.detectDependencies();
        model.detectObjectsToDettach();
        Set<ReplicationNode> nodes = new HashSet<ReplicationNode>();

        for (int i = 0, j = expected.length / 2; i < j; i++) {
            TopiaEntityEnum e = (TopiaEntityEnum) expected[2 * i];
            ReplicationNode node = model.getNode(e);
            String[] ids = (String[]) expected[2 * i + 1];
            assertEquals(ids.length > 0, node.hasAssociationsToDettach());
            for (String id : ids) {
                assertTrue(node.getAssociationsToDettach().contains(id));
            }
            nodes.add(node);
        }

        for (ReplicationNode node : model.getNodes()) {
            if (!nodes.contains(node)) {
                // on verifie bien qu'il n' y a pas d'associations dettachee
                assertFalse(node.hasAssociationsToDettach());
            }
        }

    }

    protected void detectOperations(TopiaEntity entity, Object... expected) throws TopiaException {

        assertEquals(0, expected.length % 2);

        if (entity == null) {
            prepareModel();
        } else {
            prepareModel(entity.getTopiaId());
        }
//        createModel(entity);
//        model.detectAssociations();
//        model.detectDirectDependencies();
//        model.detectShell();
//        model.detectDependencies();
//        model.detectObjectsToDettach();
//        model.detectOperations();

        if (getLog().isInfoEnabled()) {
            getLog().info("==========================================================================");
            if (entity == null) {

                getLog().info("resume of operations for all ");
            } else {
                getLog().info("resume of operations for entity " + entity.getTopiaId());
            }

            for (ReplicationNode node : model.getOrder()) {
                ReplicationOperationDef[] operations = node.getOperations();
                for (ReplicationOperationDef op : operations) {
                    getLog().info("[" + node + "] : operation " + op);
                }
            }
            getLog().info("==========================================================================");
        }
    }

    private static int dbCounter;

    protected void doReplicate(TopiaEntityEnum contract,
                               TopiaEntity... entity) throws Exception {

        TopiaApplicationContext rootCtxt = createReplicateDb("doReplicate_" + contract);

        List<String> ids = TopiaEntityHelper.getTopiaIdList(Arrays.asList(entity));
        getLog().info("entity " + ids);

        prepareModel(ids.toArray(new String[ids.size()]));

        destinationContext = rootCtxt;

        service.doReplicate(model, rootCtxt);

        //destinationContext.close();

        if (entity.length == 0) {

            return;
        }
        TopiaPersistenceContext destinationPC = rootCtxt.newPersistenceContext();

        for (TopiaEntity e : entity) {
            TopiaEntity actual = destinationPC.findByTopiaId(e.getTopiaId());
            assertNotNull(actual);
            assertEquals(e, actual);
        }

        destinationPC.close();

        destinationContext = rootCtxt;
    }

    protected void doReplicateAll() throws Exception {

        TopiaApplicationContext rootCtxt = createReplicateDb("doReplicateAll");

        prepareModelAll();

        destinationContext = rootCtxt;

        service.doReplicate(model, destinationContext);

        TopiaPersistenceContext destinationPC = rootCtxt.newPersistenceContext();

        assertDbEquals(model.getContracts(), sourcePC, destinationPC);

        destinationPC.close();

        destinationContext = rootCtxt;
    }

    protected void doReplicateWithComputedOrder(TopiaEntity... entity) throws Exception {

        TopiaApplicationContext rootCtxt = createReplicateDb("doReplicateWithComputedOrder");

        List<String> ids = TopiaEntityHelper.getTopiaIdList(Arrays.asList(entity));

        prepareModelWithComputedOrder(ids.toArray(new String[ids.size()]));

        destinationContext = rootCtxt;

        service.doReplicate(model, destinationContext);

        getLog().info("replication is done for " + Arrays.toString(entity) + ", will verify data...");

        TopiaPersistenceContext destinationPC = rootCtxt.newPersistenceContext();

        assertDbEquals(model.getContracts(), sourcePC, destinationPC);

        destinationPC.close();

        destinationContext = rootCtxt;
    }

    protected void assertDbEquals(TopiaEntityEnum[] contracts,
                                  TopiaDaoSupplier ctxt,
                                  TopiaDaoSupplier ctxt2) throws TopiaException {
        Set<String> ids = new HashSet<String>();

        if (getLog().isInfoEnabled()) {
            getLog().info("will verify db for contracts " + Arrays.toString(contracts));
        }
        for (TopiaEntityEnum c : contracts) {
            if (getLog().isDebugEnabled()) {
                getLog().debug("verify for contract " + c);
            }
            TopiaDao<? extends TopiaEntity> daoSrc = ctxt.getDao(c.getContract());
            TopiaDao<? extends TopiaEntity> daoDst = ctxt2.getDao(c.getContract());
            long nbSrc = daoSrc.count();
            long nbDst = daoDst.count();
            assertEquals("le nombres d'entites de type " + c + " devrait etre " + nbSrc + " mais est " + nbDst, nbSrc, nbDst);
            List<String> idsSrc = daoSrc.findAllIds();
            List<String> idsDst = daoDst.findAllIds();
            Collections.sort(idsSrc);
            Collections.sort(idsDst);
            assertEquals(idsSrc, idsDst);
            for (String id : idsSrc) {
                if (getLog().isDebugEnabled()) {
                    getLog().debug("verify for entity " + id);
                }
                TopiaEntity eSrc = daoSrc.forTopiaIdEquals(id).findUnique();
                TopiaEntity eDst = daoDst.forTopiaIdEquals(id).findUnique();
                assertEquals(eSrc, eDst);
                assertEntityEquals(eSrc, eDst, ids);
            }
        }
    }

    protected void assertEntityEquals(TopiaEntity expected,
                                      TopiaEntity actual,
                                      Set<String> treated) {
        if (treated == null) {
            treated = new HashSet<String>();
        }
        if (treated.contains(actual.getTopiaId())) {
            return;
        }
        if (getLog().isDebugEnabled()) {
            getLog().debug(expected);
        }
        assertEquals(actual.getTopiaId(), expected.getTopiaId());
        treated.add(actual.getTopiaId());
        if (getLog().isDebugEnabled()) {
            getLog().debug("expected : " + expected + " / actual " + actual);
        }
        TopiaEntityEnum contract = TopiaEntityHelper.getEntityEnum(expected.getClass(), getContracts());
        if (contract == null) {
            // this type of entity in not dealed here...
            getLog().debug("untested property type " + expected.getClass());
            return;
        }
        Assert.assertNotNull(
                "contract not found for " + expected.getClass() + " in " +
                Arrays.toString(getContracts()), contract);
        EntityOperator<TopiaEntity> operator = EntityOperatorStore.getOperator(contract);
        List<String> associationProperties = operator.getAssociationProperties();
        for (String name : associationProperties) {
            if (getLog().isDebugEnabled()) {
                getLog().debug("association " + name);
            }
            if (operator.isChildEmpty(name, expected)) {
                assertTrue("l'association " + name + " devrait etre vide mais possede " + operator.sizeChild(name, actual) + " entrees", operator.isChildEmpty(name, actual));
            } else {
                assertFalse("l'association " + name + " devrait posseder " + operator.isChildEmpty(name, expected) + " mais est vide", operator.isChildEmpty(name, actual));

            }
            assertEquals(operator.isChildEmpty(name, actual), operator.isChildEmpty(name, expected));

            Class<?> type = operator.getAssociationPropertyType(name);
            Collection<?> src = (Collection<?>) operator.get(name, expected);
            Collection<?> dst = (Collection<?>) operator.get(name, actual);
//            assertEquals(src, dst);
            Iterator<?> itrSrc = src.iterator();
            Iterator<?> itrDst = dst.iterator();
            while (itrSrc.hasNext()) {
                if (TopiaEntity.class.isAssignableFrom(type)) {
                    assertEntityEquals((TopiaEntity) itrSrc.next(), (TopiaEntity) itrDst.next(), treated);
                } else {
                    assertEquals(itrSrc.next(), itrDst.next());
                }
            }
        }

        for (String name : operator.getProperties()) {
            if (getLog().isDebugEnabled()) {
                getLog().debug("dependency " + name);
            }
            if (associationProperties.contains(name)) {
                // deja traite au dessus
                continue;
            }
            Class<?> type = operator.getPropertyType(name);
            Object src = operator.get(name, expected);
            Object dst = operator.get(name, actual);
            assertFalse(src == null && dst != null);
            assertFalse(src != null && dst == null);
            if (src == null) {
                continue;
            }
            if (TopiaEntity.class.isAssignableFrom(type)) {
                assertEntityEquals((TopiaEntity) src, (TopiaEntity) dst, treated);
            } else {
                assertEquals(src, dst);
            }
        }
    }

    protected void createSupportedBeforeOperation(TopiaEntityEnum contract,
                                                  TopiaEntity entity,
                                                  Class<? extends TopiaReplicationOperation> operationClass,
                                                  Object... parameters) throws Exception {

        getLog().info("entity " + entity.getTopiaId());
        prepareModel(entity.getTopiaId());

        getModelBuilder().addBeforeOperation(model, contract, operationClass, parameters);
        // on doit avoir le droit de creer cette operation
        Assert.assertTrue(true);
    }

    protected void createSupportedAfterOperation(
            TopiaEntityEnum contract,
            TopiaEntity entity,
            Class<? extends TopiaReplicationOperation> operationClass,
            Object... parameters) throws Exception {

        getLog().info("entity " + entity.getTopiaId());
        prepareModel(entity.getTopiaId());
//        model = service.createModel(getContracts());
//        model.detectDirectDependencies();
        getModelBuilder().addAfterOperation(model, contract, operationClass, parameters);
        // on doit avoir le droit de creer cette operation
        Assert.assertTrue(true);
    }

    protected Long getTestsTimeStamp() {
        if (testsTimeStamp == null) {
            testsTimeStamp = System.currentTimeMillis();
            getLog().info("tests timestamp : " + testsTimeStamp);
        }
        return testsTimeStamp;
    }

    protected File getTestDir(Class<?> testClass) {
        if (testsBasedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            String name = String.format(TEST_BASEDIR, File.separator, new Date(getTestsTimeStamp()));
            testsBasedir = new File(new File(tmp), name);
            getLog().info("tests basedir   : " + testsBasedir);
        }
        return new File(testsBasedir, testClass.getSimpleName());
    }

    protected void createModel(TopiaEntity entity) throws TopiaException {
        model = getModelBuilder().createModel(sourceContext,
                                              getContracts(),
                                              true,
                                              entity.getTopiaId()
        );
    }

    protected void prepareModel(String... ids) throws TopiaException {
        model = service.prepare(getContracts(), true, ids);
    }

    protected void prepareModelAll() throws TopiaException {
        model = service.prepareForAll(getContracts());
    }

    protected void prepareModelWithComputedOrder(String... ids) throws TopiaException {
        model = service.prepare(getContracts(), false, ids);
    }
}
