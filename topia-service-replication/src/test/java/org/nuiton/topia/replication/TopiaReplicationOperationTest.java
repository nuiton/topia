package org.nuiton.topia.replication;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.it.legacy.TopiaItLegacyEntityEnum;
import org.nuiton.topia.it.legacy.TopiaItLegacyTopiaApplicationContext;
import org.nuiton.topia.it.legacy.test.entities.Person;
import org.nuiton.topia.it.legacy.test.entities.PersonImpl;
import org.nuiton.topia.it.legacy.test.entities.Pet;
import org.nuiton.topia.it.legacy.test.entities.PetImpl;
import org.nuiton.topia.it.legacy.test.entities.Race;
import org.nuiton.topia.it.legacy.test.entities.RaceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;
import org.nuiton.topia.replication.operation.DettachAssociation;
import org.nuiton.topia.replication.operation.Duplicate;
import org.nuiton.topia.replication.operation.FakeOperation;
import org.nuiton.topia.replication.operation.UncreatableOperation;
import org.nuiton.topia.replication.operation.UnregistredOperation;

/**
 * TopiaReplicationServiceImplTest on model TopiaTest
 *
 * Created: 07 jun. 09 17:14:22
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class TopiaReplicationOperationTest extends AbstractTopiaReplicationServiceTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(TopiaReplicationOperationTest.class);

    protected static final TopiaEntityEnum[] contracts = {
            TopiaItLegacyEntityEnum.Person,
            TopiaItLegacyEntityEnum.Pet,
            TopiaItLegacyEntityEnum.Race
    };

    protected static final String entitiesList =
            PersonImpl.class.getName() + "," +
            PetImpl.class.getName() + "," +
            RaceImpl.class.getName();

    protected static Person person, person2;

    protected static Pet pet, pet2;

    protected static Race race, race2;

    @AfterClass
    public static void after() throws Exception {
        AbstractTopiaReplicationServiceTest.after();
    }

    @Before
    @Override
    public void setUp() throws Exception {

        super.setUp();

        person = update(person);
        person2 = update(person2);
        pet = update(pet);
        pet2 = update(pet2);
        race = update(race);
        race2 = update(race2);
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (destinationContext != null && !destinationContext.isClosed()) {
            destinationContext.close();
        }
    }

    @Test(expected = NullPointerException.class)
    public void testGetOperation_nullOperationClass() throws Exception {
        getModelBuilder().getOperationProvider().getOperation((Class<? extends TopiaReplicationOperation>) null);
    }

    protected TopiaReplicationModelBuilder getModelBuilder() {
        return service.getModelBuilder();
    }

    @Test
    @Override
    public void testGetOperation() throws Exception {

        getOperation(UnregistredOperation.class, false);
        getOperation(UncreatableOperation.class, true);
        getOperation(FakeOperation.class, true);
        getOperation(Duplicate.class, true);
//        getOperation(AttachAssociation.class, true);
        getOperation(DettachAssociation.class, true);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullModel() throws Exception {
        getModelBuilder().createOperation(null, null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullType() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true);
        getModelBuilder().createOperation(model, null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullPhase() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true);
        getModelBuilder().createOperation(model, TopiaItLegacyEntityEnum.Pet, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateOperation_nullOperationClass() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true);
        getModelBuilder().createOperation(model, TopiaItLegacyEntityEnum.Pet, ReplicationOperationPhase.before, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_noNode() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true);
        // le noeud Pet n'existe pas
        getModelBuilder().addAfterOperation(model, TopiaItLegacyEntityEnum.Pet, Duplicate.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_noOperation() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true);
        // le noeud Pet n'existe pas
        getModelBuilder().addAfterOperation(model, TopiaItLegacyEntityEnum.Pet, UnregistredOperation.class);
    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedBeforeOperation_Duplicate() throws Exception {
        createSupportedBeforeOperation(TopiaItLegacyEntityEnum.Person, person, Duplicate.class);
    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateUnsupportedBeforeOperation_AttachAssociation() throws Exception {
//        createUnsupportedBeforeOperation(TopiaItLegacyEntityEnum.Person, person, AttachAssociation.class);
//    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedBeforeOperation_DetachAssociation() throws Exception {
        createSupportedBeforeOperation(TopiaItLegacyEntityEnum.Person, person, DettachAssociation.class);
    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedAfterOperation_Duplicate() throws Exception {
        createSupportedAfterOperation(TopiaItLegacyEntityEnum.Person, person, Duplicate.class);
    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateSupportedAfterOperation_AttachAssociation() throws Exception {
//        createUnsupportedAfterOperation(TopiaItLegacyEntityEnum.Person, person, AttachAssociation.class);
//    }

    @Test
//    @Test(expected = UnsupportedOperationException.class)
    public void testCreateSupportedAfterOperation_DetachAssociation() throws Exception {
        createSupportedAfterOperation(TopiaItLegacyEntityEnum.Person, person, DettachAssociation.class);
    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateUnsupportedBeforeOperation_UncreatableOperation() throws Exception {
//        createUnsupportedBeforeOperation(TopiaItLegacyEntityEnum.Person, person, UncreatableOperation.class);
//    }

//    @Test(expected = UnsupportedOperationException.class)
//    public void testCreateUnsupportedAfterOperation_UncreatableOperation() throws Exception {
//        createUnsupportedAfterOperation(TopiaItLegacyEntityEnum.Person, person, UncreatableOperation.class);
//    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_wrongParameterNumber() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaItLegacyEntityEnum.Pet, FakeOperation.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_wrongParameterNumber2() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaItLegacyEntityEnum.Pet, FakeOperation.class, String.class, String.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_wrongParameterType() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaItLegacyEntityEnum.Pet, FakeOperation.class, Integer.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateOperation_nullParameter() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaItLegacyEntityEnum.Pet, FakeOperation.class, (Object) null);
    }

    @Test
    public void testCreateOperation() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true, pet.getTopiaId());
        getModelBuilder().addBeforeOperation(model, TopiaItLegacyEntityEnum.Pet, FakeOperation.class, "before");
        getModelBuilder().addAfterOperation(model, TopiaItLegacyEntityEnum.Race, FakeOperation.class, "after");
    }

    @Test(expected = NullPointerException.class)
    public void testDoReplicate_nullModel() throws Exception {

        service.doReplicate(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testDoReplicate_nullDstCtxt() throws Exception {

        model = getModelBuilder().createModel(sourceContext, contracts, true);
        service.doReplicate(model, null);
    }

    @Override
    protected TopiaApplicationContext createDb(String name) throws Exception {

        TopiaItLegacyTopiaApplicationContext sourceContext = newTopiaItLegacyTopiaApplicationContext(name);

        TopiaPersistenceContext tx = sourceContext.newPersistenceContext();

        person = tx.getDao(Person.class).create(Person.PROPERTY_NAME, "pudding master");
        race = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race I");
        pet = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding", Pet.PROPERTY_PERSON, person, Pet.PROPERTY_RACE, race);

        person2 = tx.getDao(Person.class).create(Person.PROPERTY_NAME, "pudding II master");
        pet2 = tx.getDao(Pet.class).create(Pet.PROPERTY_NAME, "pudding II");
        race2 = tx.getDao(Race.class).create(Race.PROPERTY_NAME, "race II");

        tx.commit();
        tx.close();

        return sourceContext;

    }

    @Override
    protected TopiaEntityEnum[] getContracts() {
        return contracts;
    }

    @Override
    protected Log getLog() {
        return log;
    }

    @Override
    public void testDetectTypes() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectAssociations() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectDirectDependencies() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectShell() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectDependencies() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectObjectsToDettach() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDetectOperations() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void testDoReplicate() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}


