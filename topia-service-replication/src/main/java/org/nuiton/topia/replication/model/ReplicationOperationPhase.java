package org.nuiton.topia.replication.model;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.replication.operation.Duplicate;

/**
 * Une enumeration pour definir quand appliquer une operation.
 *
 * L'ordre induit par cette enumeration sera utilisé pour trier les operations
 * a realiser sur chaque noeud de replication.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public enum ReplicationOperationPhase {

    /** a appliquer avant la duplicate d'un noeud */
    before,
    /**
     * pour dupliquer un noeud (cette phase ne doit etre utilise que sur l'operation
     * de type {@link Duplicate}
     */
    duplicate,
    /** a appliquer apres avoir duplique le noeud */
    after
}
