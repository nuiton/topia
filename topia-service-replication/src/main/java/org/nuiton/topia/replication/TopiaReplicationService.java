package org.nuiton.topia.replication;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaService;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.replication.model.ReplicationModel;

/**
 * User visible interface for replication engin
 *
 * To use replicator you must have properties defined in config file:
 * <ul>
 * <li>topia.replication.engin=[class used to indexation]</li>
 * <li>possible specific property for index engin used</li>
 * </ul>
 * The replication is always done in two phases :
 * <ul>
 * <li> - prepare the replication model - ({@link #prepare(TopiaEntityEnum[], boolean, String...)} - ({@link #prepareForAll(TopiaEntityEnum[])}</li>
 * <li> - lanch replication {@link #doReplicate(ReplicationModel,TopiaApplicationContext)}</li>
 * </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public interface TopiaReplicationService extends TopiaService {

    /**
     * Obtains the {@code model builder} use to creat the replication's model.
     *
     * @return the model builder
     * @since 2.4.3
     */
    TopiaReplicationModelBuilder getModelBuilder();

    /**
     * Prepare le modele de replication pour les entites dans les topiaIds sont
     * donnes.
     *
     * @param contracts    les contrats a repliquer
     * @param computeOrder drapeau positionné à {@code true} si on doit calculer
     *                     l'ordre des entités à repliquer, sinon on utilise
     *                     l'ordre induit par les {@code contracts}.
     * @param topiaIds     la liste des ids d'entites a repliquer
     * @return le model de replication initialise
     * @throws TopiaException pour toute erreur recontree
     */
    ReplicationModel prepare(TopiaEntityEnum[] contracts,
                             boolean computeOrder,
                             String... topiaIds) throws TopiaException;

    /**
     * Prepare le modele de replication pour toutes les entites des types
     * donnes.
     *
     * La méthode calcule l'ordre de replication des données.
     *
     * Actuellement, on n'est pas capable de calculer l'ordre si le graphe des
     * entités contient des cycles.
     *
     * TODO : faire en sorte de pouvoir gérer les cycles.
     *
     * @param contracts les contrats des types a repliquer
     * @return le modele pour la replication
     * @throws TopiaException pour toute erreur rencontree
     */
    ReplicationModel prepareForAll(TopiaEntityEnum[] contracts) throws TopiaException;

    /**
     * Lance l'operation de replication a partir du context source vers le
     * context de destination sur les entites donnees et en utilisant le modele
     * de replication precedemment construit via la methode {@link
     * #prepare(TopiaEntityEnum[], boolean, String...)} ou {@link
     * #prepareForAll(TopiaEntityEnum[])}.
     *
     * @param model   le modele de replication
     * @param dstCtxt le context sur la source de donnees ou repliquer
     * @throws Exception pour toute erreur pendant la replication
     */
    void doReplicate(ReplicationModel model,
                     TopiaApplicationContext dstCtxt) throws Exception;

    /**
     * Pour revenir en arrière lorsque la réplication a échouée .
     *
     * @param replicationContext the replication's context used to start replication.
     * @throws Exception pour toute erreur pendant la replication
     */
    void doRollback(TopiaReplicationContext replicationContext) throws Exception;
}


