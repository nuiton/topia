package org.nuiton.topia.replication.operation;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.replication.TopiaReplicationContext;
import org.nuiton.topia.replication.TopiaReplicationOperation;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.model.ReplicationNode;
import org.nuiton.topia.replication.model.ReplicationOperationDef;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;

/**
 * Pour dettacher une association.
 *
 * Note : cette operation est interne, et n'est pas creable par
 * l'utilisateur via la methode
 * {@link #register(ReplicationModel, ReplicationNode, ReplicationOperationPhase, Object...)}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class DettachAssociation implements TopiaReplicationOperation {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(DettachAssociation.class);

    @Override
    public void register(ReplicationModel model,
                         ReplicationNode ownerNode,
                         ReplicationOperationPhase phase,
                         Object... parameters) {

//        throw new UnsupportedOperationException(
//                _("topia.replication.error.operation.uncreatable", getClass()));
    }

    @Override
    public void run(TopiaReplicationContext replicationContext,
                    ReplicationOperationDef operationDef,
                    TopiaPersistenceContext srcCtxt,
                    TopiaPersistenceContext dstCtxt,
                    List<? extends TopiaEntity> entities
    ) throws TopiaException {

        String name = (String) operationDef.getParameters()[0];

        EntityOperator<? super TopiaEntity> operator;
        operator = operationDef.getNode().getOperator();

        // dettach les associations
        for (TopiaEntity e : entities) {
            int size = operator.sizeChild(name, e);

            if (size > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("will dettach " + size + " association(s) '" +
                            name + "' from " + e);
                }
                operator.setNull(name, e);
            }
        }
    }
}
