package org.nuiton.topia.replication.operation;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.replication.TopiaReplicationContext;
import org.nuiton.topia.replication.TopiaReplicationOperationUndoable;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.model.ReplicationNode;
import org.nuiton.topia.replication.model.ReplicationOperationDef;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;

import java.util.List;

/**
 * L'operation de duplication d'un noeud de replication.
 *
 * Note : cette operation est interne, et n'est pas creable par
 * l'utilisateur via la methode
 * {@link #register(ReplicationModel, ReplicationNode, ReplicationOperationPhase, Object...)}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class Duplicate implements TopiaReplicationOperationUndoable {


    /**
     * Logger
     */
    private static final Log log =
            LogFactory.getLog(Duplicate.class);

    @Override
    public void register(ReplicationModel model,
                         ReplicationNode ownerNode,
                         ReplicationOperationPhase phase,
                         Object... parameters) throws UnsupportedOperationException {
//        throw new UnsupportedOperationException(
//                _("topia.replication.error.operation.uncreatable", getClass()));
    }

    @Override
    public void run(TopiaReplicationContext replicationContext,
                    ReplicationOperationDef operationDef,
                    TopiaPersistenceContext srcCtxt,
                    TopiaPersistenceContext dstCtxt,
                    List<? extends TopiaEntity> entities) throws TopiaException {

        // fix http://nuiton.org/issues/1547
        //FIXME tchemit-2011-06-03 : while using hibernate 3.5.6, while duplicating entities we can have some associations in double sessions
        //FIXME This hack works but it should be better to resolve the bug :( perharps this is not possible
        //FIXME since we can not have a fresh empty hibernate session...
        if (srcCtxt instanceof AbstractTopiaPersistenceContext) {
            ((AbstractTopiaPersistenceContext)srcCtxt).getHibernateSupport().getHibernateSession().clear();
        }
        
        // replication des donnees vers la destination
        srcCtxt.replicateEntities(dstCtxt, entities);

        // sauvegarde de la destination
        dstCtxt.commit();
    }


    @Override
    public void rollback(ReplicationOperationDef operationDef,
                         TopiaReplicationContext replicationContext,
                         TopiaPersistenceContext dstCtxt) throws Exception {

        List<String> ids =
                replicationContext.getEntityIds(operationDef.getNode());

        if (CollectionUtils.isEmpty(ids)) {

            // rien a supprimer
            return;
        }

        Class<? extends TopiaEntity> entityClass = operationDef.getEntityType();
        TopiaDao<TopiaEntity> dao =
                (TopiaDao<TopiaEntity>) dstCtxt.getDao(entityClass);

        List<String> allIds = dao.findAllIds();
        try {
            for (String id : ids) {
                if (allIds.contains(id)) {

                    // on peut supprimer cette entité
                    log.info("Will delete " + id);
                    TopiaEntity entity = dao.forTopiaIdEquals(id).findUnique();
                    dao.delete(entity);
                }
            }
        } finally {
            allIds.clear();

            // commit des suppressions
            dstCtxt.commit();
        }
    }
}
