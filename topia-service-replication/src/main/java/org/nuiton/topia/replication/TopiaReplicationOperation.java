package org.nuiton.topia.replication;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.replication.model.ReplicationModel;
import org.nuiton.topia.replication.model.ReplicationNode;
import org.nuiton.topia.replication.model.ReplicationOperationDef;
import org.nuiton.topia.replication.model.ReplicationOperationPhase;

import java.util.List;
import java.util.ServiceLoader;

/**
 * Le contrat d'une operation a effectuer lors de la replication.
 *
 * Le module propose des operations de base dans le paquetage
 * {@code org.nuiton.topia.replication.operation}.
 *
 *
 * Pour definir une nouvelle implantation d'operation, il faut l'enregister
 * en tant que service (au sens de la classe {@link ServiceLoader},
 * c'est à dire ajouter dans un fichier (du class-path)
 * {@code META-INF/services/org.nuiton.topia.replication.TopiaReplicationOperation}
 *
 * une ligne avec le nom qualifie de votre implantation.
 *
 * Lors du chargement du service, on detecte toutes les operations disponibles.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public interface TopiaReplicationOperation {

    /**
     * Creer et enregister une operation utilisateur apres la creation du
     * modele via la methode
     *
     * {@link TopiaReplicationService#prepare(TopiaEntityEnum[], boolean, String...)}.
     *
     * @param model      le modele de replication
     * @param ownerNode  le noeud proprietaire de l'operation
     * @param phase      la phase ou positionner l'operation
     * @param parameters les parametres de l'operation
     * @throws UnsupportedOperationException if can not register this operation
     *                                       (says when operation is only internal)
     * @see ReplicationModel
     * @see ReplicationOperationPhase
     * @deprecated since 2.5.2, there is no difference between a user operation and an internal one. This method will be
     *             removed in version 2.6 and never replaced
     */
    @Deprecated
    void register(ReplicationModel model,
                  ReplicationNode ownerNode,
                  ReplicationOperationPhase phase,
                  Object... parameters)
            throws UnsupportedOperationException;

    /**
     * Execute l'operation avec le parametrage donnee.
     *
     * Note : le commit sur le context cible doit etre geree dans la methode.
     *
     * @param replicationContext le contexte de replication
     * @param operationDef       la definition de l'operation a realiser
     * @param source             le context source
     * @param destination        le context destination
     * @param entities           la liste des entités à traiter
     * @throws Exception pour toute erreur
     */
    void run(TopiaReplicationContext replicationContext,
             ReplicationOperationDef operationDef,
             TopiaPersistenceContext source,
             TopiaPersistenceContext destination,
             List<? extends TopiaEntity> entities) throws Exception;

}
