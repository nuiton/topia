package org.nuiton.topia.replication.model;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Pour definir un lien entre deux entites (deux noeuds de replication).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class ReplicationLink {

    /** le noeud source du lien */
    protected final ReplicationNode source;

    /** le noeud destination du lien */
    protected final ReplicationNode target;

    /**
     * liste des noeuds requis.
     *
     * TODO tchemit 2010-08-14 Expliquer à quoi ça sert vraiment...
     */
    protected final Set<ReplicationNode> requires;

    /** nom du lien */
    protected final String name;

    /** drapeau positionné à {@code true} lorsque le lien est une association. */
    protected final boolean association;

    public ReplicationLink(ReplicationNode source,
                           ReplicationNode target,
                           String name,
                           boolean association) {
        this.source = source;
        this.target = target;
        this.name = name;
        this.association = association;
        Set<ReplicationNode> tmpSet = new HashSet<ReplicationNode>();
        tmpSet.add(source);
        tmpSet.addAll(source.getAssociations().values());
        tmpSet.addAll(source.getDependencies().values());
        tmpSet.remove(target);
        requires = Collections.unmodifiableSet(tmpSet);
    }

    public String getName() {
        return name;
    }

    public ReplicationNode getSource() {
        return source;
    }

    public ReplicationNode getTarget() {
        return target;
    }

    public boolean isAssociation() {
        return association;
    }

    /**
     * Teste si on peut reattacher le lien en connaissant l'univers des
     * noeuds disponibles.
     *
     * On teste si toutes les pre-requis sont disponibles.
     *
     * Si oui, on peut reattacher.
     *
     * @param universe    l'univers des noeuds disponibles
     * @param currentNode le noeud qui vient d'etre replique
     * @return {@code true} si on peut reattacher ce lien
     */
    public boolean canReattach(Set<ReplicationNode> universe,
                               ReplicationNode currentNode) {
        boolean result = universe.containsAll(requires);
        if (result) {
            result = currentNode.equals(target) || universe.contains(target);
        }
        return result;
    }

    @Override
    public String toString() {
        return "<source:" + source + ", target:" + target + ", name:" + name +
               ", association:" + association + ">";
    }
}
