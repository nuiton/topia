package org.nuiton.topia.replication.model;

/*
 * #%L
 * ToPIA :: Service Replication
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.EntityOperator;
import org.nuiton.topia.persistence.util.EntityOperatorStore;
import org.nuiton.topia.replication.TopiaReplicationOperationUndoable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Model of a replication's node.
 *
 * The invariant of a replication's node is his {@link #contract}, means the
 * type of entity to replicate.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2.0
 */
public class ReplicationNode {

    /** Logger */
    private static final Log log = LogFactory.getLog(ReplicationNode.class);

    /** contract of entity of the node. */
    protected final TopiaEntityEnum contract;

    /** entity operator. */
    protected final EntityOperator<? super TopiaEntity> operator;

    /** shell of the node. */
    protected Set<ReplicationNode> shell;

    /**
     * dictionnary of associations defined on the node (keys are association
     * name, and values are target node).
     */
    protected final Map<String, ReplicationNode> associations;

    /** names of association to dettach while replication. */
    protected final Set<String> associationsToDettach;

    /**
     * compositions defined on the node (keys are association name,
     * and values are target node).
     */
    protected final Map<String, ReplicationNode> dependencies;

    /** names of dependency to dettach while replication. */
    protected final Set<String> dependenciesToDettach;

    /** operations to fire when replication pass on this node. */
    protected final List<ReplicationOperationDef> operations;

    public ReplicationNode(TopiaEntityEnum contract) {
        this.contract = contract;
        operator = EntityOperatorStore.<TopiaEntity>getOperator(contract);
        associations = new HashMap<String, ReplicationNode>();
        dependencies = new HashMap<String, ReplicationNode>();
        shell = new HashSet<ReplicationNode>();
        associationsToDettach = new HashSet<String>();
        dependenciesToDettach = new HashSet<String>();
        operations = new ArrayList<ReplicationOperationDef>();
        if (log.isTraceEnabled()) {
            log.trace("new node : " + this);
        }
    }

    public void addAssociation(String name, ReplicationNode node) {
        associations.put(name, node);
    }

    public void addOperation(int index, ReplicationOperationDef op) {
        operations.add(index, op);
        if (log.isDebugEnabled()) {
            log.debug(op + " to node " + this);
        }
    }

    public void addOperation(ReplicationOperationDef op) {
        operations.add(op);
        if (log.isDebugEnabled()) {
            log.debug(op + " to node " + this);
        }
    }

    public void setOperations(List<ReplicationOperationDef> operations) {
        clearOperations();
        this.operations.addAll(operations);
    }

    public ReplicationOperationDef[] getOperations() {
        return operations.toArray(
                new ReplicationOperationDef[operations.size()]);
    }

    public ReplicationOperationDef[] getUndoableOperations() {
        List<ReplicationOperationDef> result =
                new ArrayList<ReplicationOperationDef>();
        for (ReplicationOperationDef operation : operations) {
            if (TopiaReplicationOperationUndoable.class.isAssignableFrom(
                    operation.getOperationClass())) {
                result.add(operation);
            }
        }
        return result.toArray(new ReplicationOperationDef[result.size()]);
    }

    public boolean hasAssociation() {
        return !associations.isEmpty();
    }

    public boolean hasAssociationsToDettach() {
        return !associationsToDettach.isEmpty();
    }

    public String[] getAssociationsDettached(ReplicationNode node) {
        Set<String> result = new HashSet<String>();
        for (String name : associationsToDettach) {
            ReplicationNode get = associations.get(name);
            if (node.equals(get)) {
                result.add(name);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    public String[] getDependenciesDettached(ReplicationNode node) {
        Set<String> result = new HashSet<String>();
        for (String name : dependenciesToDettach) {
            ReplicationNode get = dependencies.get(name);
            if (node.equals(get)) {
                result.add(name);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    public boolean hasDependenciesToDettach() {
        return !dependenciesToDettach.isEmpty();
    }

    public boolean hasDependency() {
        return !dependencies.isEmpty();
    }

    public void addDependency(String name, ReplicationNode node) {
        dependencies.put(name, node);
    }

    public void addAssociationToDettach(String key) {
        associationsToDettach.add(key);
    }

    public void addDependencyToDettach(String key) {
        dependenciesToDettach.add(key);
    }

    public Map<String, ReplicationNode> getAssociations() {
        return associations;
    }

    public Set<String> getAssociationsToDettach() {
        return associationsToDettach;
    }

    public Set<String> getDependenciesToDettach() {
        return dependenciesToDettach;
    }

    public TopiaEntityEnum getContract() {
        return contract;
    }

    public Class<? extends TopiaEntity> getEntityType() {
        return contract.getContract();
    }

    public EntityOperator<? super TopiaEntity> getOperator() {
        return operator;
    }

    public Map<String, ReplicationNode> getDependencies() {
        return dependencies;
    }

    public Set<ReplicationNode> getShell() {
        return shell;
    }

    public void setShell(Set<ReplicationNode> shell) {
        this.shell = shell;
    }

    /**
     * sort operation by their phase.
     *
     * @see ReplicationOperationPhase
     */
    public void sortOperations() {
        Collections.sort(operations);
    }

    /**
     * Remove all operation of the node (for example when no data is associated
     * with the type of the node, then no needed operations).
     */
    public void clearOperations() {
        operations.clear();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ReplicationNode other = (ReplicationNode) obj;
        return contract == other.contract;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + contract.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return contract.toString();
    }
}
