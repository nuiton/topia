package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA :: Templates
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelPackageImpl;

public class TopiaHibernateTagValuesTest {

    protected TopiaHibernateTagValues provider;

    @Before
    public void setUp() throws Exception {
        provider = new TopiaHibernateTagValues();
    }

    @Test
    public void testDefaultValues() {
       Assert.assertEquals("union-subclass", provider.getInheritanceStrategyTagValue(new ObjectModelClassImpl(), new ObjectModelPackageImpl()));
       Assert.assertEquals(true, provider.getIndexForeignKeysTagValue(new ObjectModelAttributeImpl(), new ObjectModelPackageImpl(), new ObjectModelImpl()));
    }

    protected void validate(String name, boolean expected, Class<?>... types) {
        for (Class<?> type : types) {
            try {
                provider.validate(name, type);
                Assert.assertTrue(expected);
            } catch (TagValueNotFoundException e) {
                Assert.assertFalse(expected);
            } catch (MismatchTagValueTargetException e) {
                Assert.assertFalse(expected);
            }
        }
    }

}