package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA :: Templates
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.matcher.TagValueDefinitionMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.sql.Blob;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * All extra tag values usable in topia generators.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
@Component( role = TagValueMetadatasProvider.class, hint="topia-hibernate")
public class TopiaHibernateTagValues extends DefaultTagValueMetadatasProvider {

    private static final String TAG_ATTRIBUTE_TYPE = "attributeType";
    private static final String TAG_HIBERNATE_ATTRIBUTE_TYPE = "hibernateAttributeType";

    @Override
    public String getDescription() {
        return t("topia.hibernate.tagvalues");
    }

    public enum Store implements TagValueMetadata {

        /**
         * Tag pour le type de persistence.
         *
         * @see TopiaTemplateHelper#getPersistenceType(ObjectModelClassifier)
         * @see #getPersistenceTypeTagValue(ObjectModelClassifier)
         * @since 2.5
         */
        persistenceType(n("topia.hibernate.tagValue.persistenceType"), String.class, null, ObjectModelClassifier.class),

        /**
         * Tag pour le nom du champ / entité en BD.
         *
         * @see #getDbNameTagValue(ObjectModelElement)
         * @see TopiaTemplateHelper#getDbName(ObjectModelElement)
         * @see TopiaTemplateHelper#getReverseDbName(ObjectModelAttribute)
         */
        dbName(n("topia.hibernate.tagValue.dbName"), String.class, null, ObjectModelElement.class),

        /**
         * Tag to specify the reverse db name of an attribute in database.
         *
         * @see TopiaTemplateHelper#getReverseDbName(ObjectModelAttribute)
         */
        reverseDbName(n("topia.hibernate.tagValue.reverseDbName"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag to specify the reverse db name of an attribute in database.
         *
         * @see #getManytoManyTableNameTagValue(ObjectModelAttribute)
         * @see TopiaTemplateHelper#getManyToManyTableName(ObjectModelAttribute)
         * @since 2.9.2
         */
        manyToManyTableName(n("topia.hibernate.tagValue.manyToManyTableName"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour le nom du schema en BD.
         *
         * @see #getDbSchemaNameTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 2.5
         */
        dbSchema(n("topia.hibernate.tagValue.dbSchema"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelAttribute.class),

        /**
         * Tag pour la taille du champ en BD.
         *
         * @see #getLengthTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        length(n("topia.hibernate.tagValue.length"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour specfier le type d'acces a un champ.
         *
         * @see #getAccessTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        access(n("topia.hibernate.tagValue.access"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour ajouter un attribut dans une clef métier.
         *
         * @see #getNaturalIdTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        naturalId(n("topia.hibernate.tagValue.naturalId"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour specifier si une clef metier est mutable.
         *
         * @see #getNaturalIdMutableTagValue(ObjectModelClassifier)
         * @since 2.5
         */
        naturalIdMutable(n("topia.hibernate.tagValue.naturalIdMutable"), boolean.class, null, ObjectModelClassifier.class),

        /**
         * Tag pour permettre de choisir qui contrôle la relation N-N
         * bidirectionnelle. A utiliser sur les deux extremités de l'association.
         * Mettre inverse=false sur le rôle fils et inverse=true sur le rôle père.
         * Par défaut le inverse=true est placé sur le premier rôle trouvé dans
         * l'ordre alphabétique.
         *
         * @see #getInverseTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        inverse(n("topia.hibernate.tagValue.inverse"), boolean.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour spécifier la caractère lazy d'une association multiple.
         *
         * @see #getLazyTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        lazy(n("topia.hibernate.tagValue.lazy"), boolean.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour spécifier la caractère fetch d'une association multiple.
         *
         * @see #getFetchTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        fetch(n("topia.hibernate.tagValue.fetch"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour spécifier la caractère order-by d'une association multiple.
         *
         * @see #getOrderByTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        orderBy(n("topia.hibernate.tagValue.orderBy"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag pour spécifier la caractère not-null d'un attribut.
         *
         * @see #getNotNullTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        notNull(n("topia.hibernate.tagValue.notNull"), String.class, null, ObjectModelAttribute.class),

        /**
         * Tag à placer sur un l'attribut d'une entité. Cet attribut est de type
         * énumération : l'ajout de la tagValue indique qu'il faut utiliser le
         * {@code name} de l'énumération et non l'ordinal pour stocker la valeur en
         * base
         *
         * @see #hasUseEnumerationNameTagValue(ObjectModelAttribute, ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        useEnumerationName(n("topia.hibernate.tagValue.useEnumerationName"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelAttribute.class),

        /**
         * Tag pour configurer l'interface du proxy sur autre chose que l'implementation par defaut.
         *
         * Par defaut :
         * null → generere le proxy sur l'interface de l'implementation
         * Autre valeur :
         * "none" → laisse la configuration par defaut d'hibernate
         *
         * @see #getPersistenceTypeTagValue(ObjectModelClassifier)
         * @since 2.5
         */
        hibernateProxyInterface(n("topia.hibernate.tagValue.hibernateProxyInterface"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag pour configurer la stategie d'heritage choisie.
         *
         * Par defaut : union-subclass : Table per class strategy
         * Autre valeur :
         * "subclass" → Single table per class hierarchy strategy
         * "joined-subclass" → Joined subclass strategy
         *
         * @see #getPersistenceTypeTagValue(ObjectModelClassifier)
         * @since 3.0
         */
        inheritanceStrategy(n("topia.hibernate.tagValue.inheritanceStrategy"), String.class, "union-subclass", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag pour specifier de trier les attributs par nom lors de la generation.
         *
         * @see #getSortAttributeTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 2.5
         */
        sortAttribute(n("topia.hibernate.tagValue.sortAttribute"), boolean.class, "false", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag pour spécifier le type d'une propriété dans le mapping hibernate.
         *
         * @see #getTypeTagValue(ObjectModelAttribute)
         * @since 2.5
         * @deprecated since 3.0, use now {@link #hibernateAttributeType} which
         * permits to deal the same thing but at also model and classifier scope.
         */
        type(n("topia.hibernate.tagValue.type"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag pour spécifier le type sql d'une propriété dans le mapping hibernate.
         *
         * @see #getSqlTypeTagValue(ObjectModelAttribute)
         * @since 2.5
         */
        sqlType(n("topia.hibernate.tagValue.sqlType"), String.class, null, ObjectModelAttribute.class),

        /**
         * Stéréotype pour les attributs avec multiplicité nécessitant la création d'un index.
         *
         * @see #getIndexForeignKeysTagValue(ObjectModelAttribute, ObjectModelPackage, ObjectModel)
         * @since 2.6.5
         */
        indexForeignKeys(n("topia.hibernate.tagValue.indexForeignKeys"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelAttribute.class),

        /**
         * Tag to generate deterministic foreign key names in hibernate mapping files.
         *
         * @see #isGenerateForeignKeyNames(ObjectModelClassifier, ObjectModel)
         * @since 3.0.1
         */
        generateForeignKeyNames(n("topia.hibernate.tagValue.generateForeignKeyNames"), boolean.class, "false", ObjectModel.class, ObjectModelClassifier.class),

        /**
         * Tag to change the type of an attribute in a hibernate mapping.
         *
         * This is a special tagValue that is dynamic.
         *
         * For example to change the type {@code String} into hibernate mapping type {@code text}, add this:
         * <pre>
         * model.tagValue.hibernateAttributeType.String=text
         * </pre>
         *
         * Before 3.0, you could do the same thing using:
         * <pre>
         * model.tagValue.String=text
         * </pre>
         *
         * The new way permits us to validate the usage of the tagValue, old way can't.
         *
         * @see #getHibernateAttributeType(ObjectModelAttribute, ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        hibernateAttributeType(n("topia.hibernate.tagValue.hibernateAttributeType"), String.class, null, org.nuiton.eugene.models.extension.tagvalue.matcher.StartsWithTagNameMatcher.class, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelAttribute.class),

        /**
         * Tag to change the type of an attribute.
         *
         * This is a special tagValue that is dynamic.
         *
         * For example to change the type {@code String} into type {@code java.lang.String}, add this:
         * <pre>
         * model.tagValue.attributeType.String=java.lang.String
         * </pre>
         *
         * Before 3.0, you could do the same thing using:
         * <pre>
         * model.tagValue.String=java.lang.String
         * </pre>
         *
         * The new way permits us to validate the usage of the tagValue, old way can't.
         *
         * @see #getHibernateAttributeType(ObjectModelAttribute, ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        attributeType(n("topia.hibernate.tagValue.attributeType"), String.class, null, org.nuiton.eugene.models.extension.tagvalue.matcher.StartsWithTagNameMatcher.class, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelAttribute.class);

        private final Set<Class<?>> targets;
        private final Class<?> tagValueType;
        private final String i18nDescriptionKey;
        private final String defaultValue;
        private final Class<? extends TagValueDefinitionMatcher> matcher;

        Store(String i18nDescriptionKey, Class<?> tagValueType, String defaultValue, Class<?>... targets) {
            this(i18nDescriptionKey, tagValueType, defaultValue, EqualsTagValueNameMatcher.class, targets);
        }

        Store(String i18nDescriptionKey, Class<?> tagValueType, String defaultValue, Class<? extends TagValueDefinitionMatcher> matcher, Class<?>... targets) {
            this.targets = ImmutableSet.copyOf(targets);
            this.tagValueType = tagValueType;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
            this.matcher = matcher;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return tagValueType;
        }

        @Override
        public Class<? extends TagValueDefinitionMatcher> getMatcherClass() {
            return matcher;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }

    public TopiaHibernateTagValues() {
        super((TagValueMetadata[]) Store.values());
    }

    /**
     * Obtain the value of the {@link Store#persistenceType} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#persistenceType
     * @since 2.5
     */
    public String getPersistenceTypeTagValue(ObjectModelClassifier classifier) {
        return TagValueUtil.findTagValue(Store.persistenceType, classifier);
    }

    /**
     * Obtain the value of the {@link Store#inheritanceStrategy} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#inheritanceStrategy
     * @since 3.0
     */
    public String getInheritanceStrategyTagValue(ObjectModelClassifier classifier,
                                                 ObjectModelPackage aPackage) {
        return TagValueUtil.findTagValue(Store.inheritanceStrategy, classifier, aPackage);
    }

    /**
     * Obtain the value of the {@link Store#dbName} tag value on the given element.
     *
     * <strong>Note:</strong> We just try a direct search on the element and do not
     * walk through his declaring elements if not found (see https://forge.nuiton.org/issues/2342).
     *
     * @param element element to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#dbName
     * @since 2.5
     */
    public String getDbNameTagValue(ObjectModelElement element) {
        return TagValueUtil.findDirectTagValue(Store.dbName, null, element);
    }

    /**
     * Obtain the value of the {@link Store#dbSchema} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#dbSchema
     * @since 2.5
     */
    public String getDbSchemaNameTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.dbSchema, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#manyToManyTableName}
     * tag value on the given attribute.
     *
     *
     * Note that it won't and search on declaring element or anywhere else than on the given element.
     * See https://forge.nuiton.org/issues/2342
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#manyToManyTableName
     * @since 2.9.2
     */
    public String getManytoManyTableNameTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findDirectTagValue(Store.manyToManyTableName, attribute);
    }

    /**
     * Obtain the value of the {@link Store#length} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#length
     * @since 2.5
     */
    public String getLengthTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.length, attribute);
    }

    /**
     * Obtain the value of the {@link Store#access} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#access
     * @since 2.5
     */
    public String getAccessTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.access, attribute);
    }

    /**
     * Obtain the value of the {@link Store#naturalId} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#naturalId
     * @since 2.5
     */
    public boolean getNaturalIdTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findBooleanTagValue(Store.naturalId, attribute);
    }

    /**
     * Obtain the value of the {@link Store#naturalIdMutable} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#naturalIdMutable
     * @since 2.5
     */
    public boolean getNaturalIdMutableTagValue(ObjectModelClassifier classifier) {
        return TagValueUtil.findBooleanTagValue(Store.naturalIdMutable, classifier);
    }

    /**
     * Obtain the value of the {@link Store#inverse} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#inverse
     * @since 2.5
     */
    public String getInverseTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.inverse, attribute);
    }

    /**
     * Obtain the value of the {@link Store#lazy} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#lazy
     * @since 2.5
     */
    public String getLazyTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.lazy, attribute);
    }

    /**
     * Obtain the value of the {@link Store#fetch} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#fetch
     * @since 2.5
     */
    public String getFetchTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.fetch, attribute);
    }

    /**
     * Obtain the value of the {@link Store#orderBy} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#orderBy
     * @since 2.5
     */
    public String getOrderByTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.orderBy, attribute);
    }

    /**
     * Obtain the value of the {@link Store#notNull} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#notNull
     * @since 2.5
     */
    public Boolean getNotNullTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findNullableBooleanTagValue(Store.notNull, attribute);
    }

    /**
     * Obtain the value of the {@link Store#hibernateProxyInterface} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @param aPackage   FIXME
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#hibernateProxyInterface
     * @since 2.5
     */
    public String getProxyInterfaceTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.hibernateProxyInterface, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#sortAttribute} tag value on the given classifier.
     *
     * @param classifier classifier to seek
     * @param aPackage   FIXME
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#sortAttribute
     * @since 2.5
     */
    public boolean getSortAttributeTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.sortAttribute, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateForeignKeyNames}
     * tag value on the given model or classifier and returns  {@code true} if the tag value was found and value {@code true}.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param model      model to seek
     * @param classifier classifier to seek
     * @return {@code true} if tag value was found on classifier or model and his value is {@code true}, otherwise {@code false}.
     * @see Store#generateForeignKeyNames
     * @since 2.10
     */
    public boolean isGenerateForeignKeyNames(ObjectModelClassifier classifier, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateForeignKeyNames, classifier, model);
    }

    protected static final Map<String, String> HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES = Maps.newHashMap();

    static {
        // String
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(String.class.getSimpleName(), String.class.getName());

        // primitive types wrappers
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Boolean.class.getSimpleName(), Boolean.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Byte.class.getSimpleName(), Byte.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Character.class.getSimpleName(), Character.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Short.class.getSimpleName(), Short.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Integer.class.getSimpleName(), Integer.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Long.class.getSimpleName(), Long.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Float.class.getSimpleName(), Float.class.getName());
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Double.class.getSimpleName(), Double.class.getName());

        // some particular types
        HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.put(Blob.class.getSimpleName(), Blob.class.getName().toLowerCase());
    }

    /**
     * Obtain the value of the {@link Store#hibernateAttributeType} tag value on the given attribute, classifier or model.
     *
     * @param attribute  attribute to seek
     * @param aPackage   FIXME
     * @param model      FIXME
     * @param classifier FIXME
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#hibernateAttributeType
     * @since 3.0
     */
    public String getHibernateAttributeType(ObjectModelAttribute attribute, ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {

        String value = TagValueUtil.findDirectTagValue(Store.hibernateAttributeType, null, attribute);
        if (value == null) {
            String tagValueName = TAG_HIBERNATE_ATTRIBUTE_TYPE + "." + attribute.getType();
            String defaultValue = HIBERNATE_ATTRIBUTE_TYPE_DEFAULT_VALUES.get(attribute.getType());
            value = TagValueUtil.findTagValue(tagValueName, defaultValue, attribute, classifier, aPackage, model);
        }
        return value;

    }

    /**
     * Obtain the value of the {@link Store#attributeType} tag value on the given attribute, classifier or model.
     *
     * @param attribute  attribute to seek
     * @param aPackage   FIXME
     * @param model      FIXME
     * @param classifier FIXME
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#attributeType
     * @since 3.0.1
     */
    public String getAttributeType(ObjectModelAttribute attribute, ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {

        String value = TagValueUtil.findDirectTagValue(Store.attributeType, null, attribute);
        if (value == null) {
            String tagValueName = TAG_ATTRIBUTE_TYPE + "." + attribute.getType();
            //TODO do not use this deprecated api
            value = TagValueUtil.findTagValue(tagValueName, null, attribute, classifier, aPackage, model);
        }
        return value;

    }

    /**
     * Obtain the value of the {@link Store#sqlType} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#sqlType
     * @since 2.5
     */
    public String getSqlTypeTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.sqlType, attribute);
    }

    /**
     * Obtains the value of the  tag value {@link Store#indexForeignKeys} on the model or on the
     * given attribute.
     *
     * @param attribute attribute to test
     * @param model     model to test
     * @param aPackage  FIXME
     * @return none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#indexForeignKeys
     * @since 2.6.5
     */
    public boolean getIndexForeignKeysTagValue(ObjectModelAttribute attribute, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.indexForeignKeys, attribute, aPackage, model);
    }

    //TODO Javadoc
    public boolean hasUseEnumerationNameTagValue(ObjectModelAttribute attr, ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.useEnumerationName, attr, classifier, aPackage, model);
    }

    //---------------------------------------------------------------------------------------------------------------
    //--  DEPRECATED API TO REMOVE  ---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------

    /**
     * Obtain the value of the {@link Store#type} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#type
     * @since 2.5
     * @deprecated since 3.0, use now {@link #getHibernateAttributeType(ObjectModelAttribute, ObjectModelClassifier, ObjectModelPackage, ObjectModel)} or {@link #getAttributeType(ObjectModelAttribute, ObjectModelClassifier, ObjectModelPackage, ObjectModel)}
     */
    @Deprecated
    public String getTypeTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.type, attribute);
    }

    public String getDeprecatedAttributeTagValueMessage(ObjectModelClass clazz,
                                                        ObjectModelAttribute attr,
                                                        String deprecatedTagValue,
                                                        String newTagValue,
                                                        String value) {
        String tagValuePrefix = clazz.getQualifiedName() + ".attribute." + attr.getName() + ".tagValue.";

        String deprecatedTagName = tagValuePrefix + deprecatedTagValue + "=" + value;
        String tagName = tagValuePrefix + newTagValue + "=" + value;
        return "\n---------\nYou are using a deprecated tagValue (" + deprecatedTagName + "), replace it by\n" + tagName + "\n---------\n";

    }

    public String getDeprecatedModelTagValueMessage(ObjectModel model,
                                                    String deprecatedTagValue,
                                                    String newTagValue,
                                                    String value) {
        String tagValuePrefix = "model.tagValue.";

        String deprecatedTagName = tagValuePrefix + deprecatedTagValue + "=" + value;
        String tagName = tagValuePrefix + newTagValue + "=" + value;
        return "\n---------\nYou are using a deprecated tagValue (" + deprecatedTagName + "), replace it by\n" + tagName + "\n---------\n";

    }

}
