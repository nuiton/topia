package org.nuiton.topia.templates;

/*
 * #%L
 * ToPIA :: Templates
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.*;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.util.TopiaEntityBinder;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;
import org.nuiton.util.beans.BinderModelBuilder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * A template to generate a helper for {@link TopiaEntityBinder}.
 * 
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.1
 */
@Component( role = Template.class, hint="org.nuiton.topia.templates.BinderHelperTransformer")
public class BinderHelperTransformer extends ObjectModelTransformerToJava {

    private static final Log log =
            LogFactory.getLog(BinderHelperTransformer.class);

    protected TopiaTemplateHelper templateHelper;

    @Override
    public void transformFromModel(ObjectModel model) {

        if (templateHelper == null) {
            templateHelper = new TopiaTemplateHelper(model);
        }

        ObjectModelClass resultClass;

        List<ObjectModelClass> classes = templateHelper.getEntityClasses(model, true);

        if (CollectionUtils.isEmpty(classes)) {

            // no entity classes, so no generation
            log.warn("No entity to generate, " + getClass().getName() + " is skipped");
            return;
        }
        
        String packageName = getDefaultPackageName();
        String modelName = model.getName();
        String binderHelperClazzName = modelName + "BinderHelper";

        String entityEnumName = templateHelper.getEntityEnumName(model);
        String entityEnumPackage = packageName + "." + entityEnumName;

        resultClass = createClass(binderHelperClazzName, packageName);

        setSuperClass(resultClass, BinderFactory.class);

        addImport(resultClass, TopiaEntityBinder.class);
        addImport(resultClass, TopiaEntityHelper.class);
        addImport(resultClass, TopiaEntity.class);
        addImport(resultClass, BinderModelBuilder.class);
        addImport(resultClass, entityEnumPackage);

        ObjectModelOperation op;

        op = addOperation(resultClass,
                          "getTopiaBinder",
                          "<E extends TopiaEntity> TopiaEntityBinder<E>",
                          ObjectModelJavaModifier.PUBLIC,
                          ObjectModelJavaModifier.STATIC);
        addParameter(op, "Class<E>", "entityClass");
        addParameter(op, "String", "contextName");
        setOperationBody(op, ""
/*{
        return (TopiaEntityBinder<E>) newBinder(entityClass, entityClass, contextName, TopiaEntityBinder.class);
    }*/
        );

        op = addOperation(resultClass,
                          "getSimpleTopiaBinder",
                          "<E extends TopiaEntity> TopiaEntityBinder<E>",
                          ObjectModelJavaModifier.PUBLIC,
                          ObjectModelJavaModifier.STATIC);
        addParameter(op, "Class<E>", "entityClass");
        setOperationBody(op, ""
/*{
        return getTopiaBinder(entityClass, "<%=modelName%>");
    }*/
        );

        op = addOperation(resultClass,
                          "registerTopiaBinder",
                          "void",
                          ObjectModelJavaModifier.PUBLIC,
                          ObjectModelJavaModifier.STATIC);
        addParameter(op, "BinderModelBuilder", "builder");
        addParameter(op, "String", "contextName");
        setOperationBody(op, ""
/*{
       registerBinderModel(builder, contextName);
    }*/
        );

        op = addOperation(resultClass,
                          "registerTopiaBinder",
                          "<E extends TopiaEntity> TopiaEntityBinder<E>",
                          ObjectModelJavaModifier.PUBLIC,
                          ObjectModelJavaModifier.STATIC);
        addParameter(op, "Class<E>", "entityClass");
        addParameter(op, "BinderModelBuilder", "builder");
        addParameter(op, "String", "contextName");
        setOperationBody(op, ""
/*{
       registerBinderModel(builder, contextName);
       return getTopiaBinder(entityClass, contextName);
    }*/
        );

        op = addOperation(resultClass,
                          "copy",
                          "<E extends TopiaEntity> void",
                          ObjectModelJavaModifier.PUBLIC,
                          ObjectModelJavaModifier.STATIC);
        addParameter(op, "String", "contextName");
        addParameter(op, "E", "source");
        addParameter(op, "E", "target");
        addParameter(op, "boolean", "tech");
        setOperationBody(op, ""
/*{
        Class<E> entityClass = (Class<E>) TopiaEntityHelper.getContractClass(<%=entityEnumName%>.values(), target.getClass());
        TopiaEntityBinder<E> binder = getTopiaBinder(entityClass, contextName);
        if (binder == null) {
            throw new NullPointerException("Could not find a simple topia binder of type : " + target.getClass());
        }
        binder.load(source, target, tech);
    }*/
        );

        op = addOperation(resultClass,
                          "simpleCopy",
                          "<E extends TopiaEntity> void",
                          ObjectModelJavaModifier.PUBLIC,
                          ObjectModelJavaModifier.STATIC);
        addParameter(op, "E", "source");
        addParameter(op, "E", "target");
        addParameter(op, "boolean", "tech");
        setOperationBody(op, ""
/*{
        Class<E> entityClass = (Class<E>) TopiaEntityHelper.getContractClass(<%=entityEnumName%>.values(), target.getClass());
        TopiaEntityBinder<E> binder = getSimpleTopiaBinder(entityClass);
        if (binder == null) {
            throw new NullPointerException("Could not find a simple topia binder of type : " + target.getClass());
        }
        binder.load(source, target, tech);
    }*/
        );

        StringBuilder initCode = new StringBuilder();

        for (ObjectModelClass clazz : classes) {

            String prefix = getConstantPrefix(clazz);

            if (StringUtils.isEmpty(prefix)) {

                // no specific prefix, so no prefix
                if (log.isWarnEnabled()) {
                    log.warn("[" + clazz.getName() + "] Will generate constants with NO prefix, not a good idea...");
                }
            }

            setConstantPrefix(prefix);

            generateBinder(modelName, clazz, resultClass, initCode);

        }

        op = addOperation(resultClass, "initBinders", "void", ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, initCode.toString());

        op = addOperation(resultClass, null, (String) null, ObjectModelJavaModifier.STATIC);
        setOperationBody(op, ""
 /*{
        initBinders();
}*/
        );
    }

    protected void generateBinder(String modelName,
                                  ObjectModelClass clazz,
                                  ObjectModelClass resultClass,
                                  StringBuilder initCode) {

        List<ObjectModelAttribute> list = new ArrayList<>();

        Collection<ObjectModelAttribute> attributes = new HashSet<>();
        attributes.addAll(clazz.getAllOtherAttributes());
        attributes.addAll(clazz.getAttributes());

        for (ObjectModelAttribute attr : attributes) {
            if (!attr.isNavigable()) {
                continue;
            }

            if (GeneratorUtil.isNMultiplicity(attr)) {
                // not dealing with association
                continue;
            }

            list.add(attr);
        }

        String clazzName = clazz.getName();

        if (list.isEmpty()) {
            // no attribute, do nothing
            if (log.isDebugEnabled()) {
                log.debug("no attribute to add in a binder for " + clazzName +
                          ", will not generate it.");
            }
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("generate simple binder for " + clazzName);
        }
        addImport(resultClass, clazz);
        initCode.append(""
/*{
        BinderModelBuilder<<%=clazzName%>, <%=clazzName%>> builder<%=clazzName%> =
            BinderModelBuilder.newEmptyBuilder(<%=clazzName%>.class);
        builder<%=clazzName%>.addSimpleProperties(
}*/
        );
        Iterator<ObjectModelAttribute> itr = list.iterator();
        while (itr.hasNext()) {
            ObjectModelAttribute attr = itr.next();
            String attrName;
            if (!attr.hasAssociationClass()) {
                attrName = attr.getName();
            } else {
                attrName = GeneratorUtil.getAssocAttrName(attr);
            }
            boolean hasNext = itr.hasNext();
            initCode.append(""
/*{            <%=clazzName%>.<%=getConstantName(attrName)%><%=(hasNext?",\n":"")%>}*/
            );
        }
        initCode.append(""
/*{
        );
        registerTopiaBinder(builder<%=clazzName%>, "<%=modelName%>");
    }*/
        );
    }
}

