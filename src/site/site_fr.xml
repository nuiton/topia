<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ToPIA
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2004 - 2014 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<project name="${project.name}" xmlns="http://maven.apache.org/DECORATION/1.3.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/DECORATION/1.3.0 http://maven.apache.org/xsd/decoration-1.3.0.xsd">

  <bannerLeft>
    <alt>ToPIA</alt>
    <src>${siteCommonResourcesUrl}/images/logos/topia_224_75.png</src>
    <name>${project.name}</name>
    <href>index.html</href>
  </bannerLeft>

  <bannerRight>
    <alt>Code Lutin</alt>
    <src>https://www.codelutin.com/images/lutinorange-codelutin.png</src>
    <href>http://www.codelutin.com</href>
  </bannerRight>


  <custom>
    <fluidoSkin>
      <topBarEnabled>true</topBarEnabled>
      <topBarIcon>
        <name>ToPIA : Tools for Portable and Independent Architecture</name>
        <alt>ToPIA : Tools for Portable and Independent Architecture</alt>
        <src>${siteCommonResourcesUrl}/images/logos/topia_60_20.png</src>
        <href>/index.html</href>
      </topBarIcon>
      <sideBarEnabled>false</sideBarEnabled>
      <googleSearch>false</googleSearch>
      <searchEnabled>true</searchEnabled>
      <sourceLineNumbersEnabled>true</sourceLineNumbersEnabled>
    </fluidoSkin>
  </custom>

  <publishDate position="right" format="dd/MM/yyyy"/>
  <version position="right"/>

  <poweredBy>

    <logo href="http://maven.apache.org" name="Maven"
          img="${siteCommonResourcesUrl}/images/logos/maven-feather.png"/>

    <logo href="http://docutils.sourceforge.net/rst.html"
          name="ReStructuredText"
          img="${siteCommonResourcesUrl}/images/logos/restructuredtext-logo.png"/>

    <logo href="http://doc.nuiton.org/jrst" name="JRst"
          img="${siteCommonResourcesUrl}/images/logos/jrst-logo.png"/>

    <logo href="http://argouml.tigris.org/" name="ArgoUML"
          img="${siteCommonResourcesUrl}/images/logos/argouml-logo.png"/>

  </poweredBy>

  <body>

    <links>
      <item name="Nuiton.org" href="https://forge.nuiton.org"/>
      <item name="Nuiton.org (ToPIA)" href="https://forge.nuiton.org/projects/topia"/>
      <item name="Code Lutin" href="http://www.codelutin.com"/>
      <item name="Libre-Entreprise" href="http://www.libre-entreprise.org/"/>
    </links>

    <breadcrumbs>
      <item name="${project.name}" href="${project.url}/index.html"/>
      <item name="${project.version}" href="${project.url}/v/${siteDeployClassifier}/index.html"/>
    </breadcrumbs>

    <menu name="Tutoriels">
      <item name="Créer un projet ToPIA en partant de rien" href="tutos/from_scratch.html"/>
      <item name="Migration vers ToPIA 3.0" href="migrate_to_3.0.html"/>
      <item name="Comment personnaliser la génération de code ?" href="tutos/howto_customize_generation.html"/>
      <item name="Comment intégrer ToPIA dans votre application ?" href="tutos/howto_architecture_with_topia.html"/>
      <item name="Comment gérer les évolutions de schéma avec ToPIA ?" href="tutos/howto_migration_service.html"/>
      <item name="Examples de migration pour le refonte des indexed/ordered"
            href="user/ordered_vs_indexed_migration.html"/>
      <item name="[deprecated]Commencer à développer avec l'API ToPIA" href="user/start_using_api.html"/>
      <item name="[deprecated]Étendre le modèle" href="user/extend_model.html"/>
      <item name="[deprecated]Continuer le développement" href="user/continue_devel.html"/>
    </menu>

    <menu name="Documentation">
      <item name="Présentation" href="index.html"/>
      <item name="Utilisation" href="user/howto.html"/>
      <item name="Génération des modèles" href="user/model_generation.html"/>
      <item name="Tag values" href="user/tagvalues.html"/>
      <item name="FAQ" href="user/faq.html"/>
      <item name="Todo" href="docs/todo.html"/>
      <item name="Mapping Hibernate" href="docs/hibernate_mapping.html"/>
      <item name="Isolation" href="docs/isolation.html"/>
      <item name="Migration de schéma" href="docs/schema_migration.html"/>
      <item name="Sécurité" href="docs/security.html"/>
      <item name="TopiaContextFactory" href="docs/devel.html"/>
      <item name="Projets similaires" href="docs/project.html"/>
      <item name="Gestion des évènements" href="user/event.html"/>
      <item name="Qu'est-ce qu'un cycle de développement en Y ?" href="docs/y_cycle.html"/>
      <item name="Détails de l'intégration avec Maven" href="docs/maven_integration.html"/>
    </menu>

    <menu ref="modules"/>

    <menu ref="reports" name="Infos"/>

    <footer>
      <div id='mavenProjectProperties' locale='fr'
           projectId='${project.projectId}'
           version='${project.siteDeployClassifier}'
           sourcesType='${project.siteSourcesType}'
           scmwebeditor_vmFiles=',library,'/>
    </footer>

  </body>
</project>
